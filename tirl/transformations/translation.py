#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np
from numbers import Real


# TIRL IMPORTS

import tirl.utils as tu
from tirl import settings as ts
from tirl.transformations.linear import TxLinear


# IMPLEMENTATION

class TxTranslation(TxLinear):
    """
    TxTranslation (subclass of TxLinear) - applies a shift along each of the
    specified coordinate axes. Skipping an axis is possible by setting the
    respective translation component to zero and locking the parameter.

    """

    RESERVED_KWARGS = ("bounds", "lock", "name", "invertible")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~ INITIALISATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __init__(self, *parameters, bounds=None, lock=None, name=None,
                 invertible=True, **metaparameters):
        """
        Initialisation of TxTranslation.

        :param parameters:
            Amount of shift along each coordinate dimension. Values are in
            arbitrary units.
        :type: Union[int, float]
        :param bounds:
            Parameter bounds can be defined in one of the following formats:
            1. ((lb0, ub0),) - sequence of lower and upper bounds for each
               parameter. The order must correspond to the order of the
               transformation parameters.
            2. {param_no: (lb, ub)} - dictionary of lower and upper bound
               pairs, where the keys represent parameter indices (starting
               from 0).
            3. (lb_arr, ub_arr) - flat iterable with lower and upper bounds for
               all parameters.
            4. "auto" - the get_default_bounds() method is called to create
               default lower and upper bounds.
            5. None - parameter bounds are not set.
            6. (lb, ub) - global scalar values for lower and upper bounds
        :type bounds: Union[tuple, list, dict, str]
        :param lock:
            Boolean mask or index sequence that defines which parameters
            are locked, i.e. should not be updated by default. If None, all
            parameters are updated by default when calling the update() method.
        :type lock: Union[None, tuple, int, np.ndarray]
        :param name:
            Name of the Transformation object. If None (default), a default
            name of the form "tx_objectID" will be generated.
        :type name: str
        :param invertible:
            Indicates whether the current transformation is invertible (True) or
            not (False, default). Transformations marked 'invertible' must
            implement the inverse() method.
        :type invertible: bool
        :param metaparameters:
            Metaparameters for Transformation
        :type metaparameters: Any

        """
        if parameters == ():
            raise ValueError("No parameters were specified.")

        # Interpret the parameter and shape input. Shape is None for this class.
        parameters, shape = \
            self._read_parameters_and_shape(parameters, shape=None)

        # Verify translation parameters
        if all(isinstance(v, Real) for v in parameters):
            parameters = np.asarray(parameters)
        else:
            raise ValueError("All translation components must be real.")

        # Calculate the shape of the corresponding affine matrix
        shape = (parameters.size, parameters.size + 1)

        # Call superclass initialisation
        super(TxTranslation, self).__init__(
            *parameters, shape=shape, bounds=bounds, lock=lock,
            name=name, invertible=invertible, **metaparameters)

        # Set class-specific metaparameters
        # Nothing to do.

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATTRIBUTES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @property
    def dim(self):
        """
        Returns the number of coordinates expected by the transformation.

        """
        return self.parameters.size

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PRIVATE METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @classmethod
    def _load(cls, dump):
        parameters = dump.get("parameters")
        params = parameters.parameters
        lb = parameters.lower_bounds
        ub = parameters.upper_bounds
        lock = parameters.locked
        meta = dump.get("metaparameters")
        name = meta.pop("name")
        invertible = meta.pop("invertible")
        shape = meta.pop("shape")
        obj = cls(*params, bounds=(lb, ub), lock=lock, name=name,
                  invertible=invertible, **meta)
        return obj

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ARITHMETICS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __add__(self, other):
        if isinstance(other, TxTranslation):
            n = max(self.parameters.size, other.parameters.size)
            vector = np.zeros(n, dtype=self.parameters.dtype)
            vector[:self.parameters.size] = self.parameters[:]
            vector[:other.parameters.size] += other.parameters[:]
            return type(self)(*vector)
        else:
            return super(TxTranslation, self).__add__(other)

    def __mul__(self, other):
        if isinstance(other, Real):
            parameters = self.parameters.copy() * other
            return type(self)(*parameters[:])
        else:
            raise NotImplementedError(
                f"Multiplication of {self.__class__.__name__} by other than a "
                f"real number is undefined.")

    def __truediv__(self, other):
        if isinstance(other, Real):
            return self.__mul__(1. / other)
        else:
            raise NotImplementedError(
                f"Division of {self.__class__.__name__} by other than a "
                f"non-zero real number is undefined.")

    def __floordiv__(self, other):
        if isinstance(other, Real):
            parameters = self.parameters.copy() // other
            return type(self)(*parameters[:])
        else:
            raise NotImplementedError(
                f"Floor division of {self.__class__.__name__} by other than a "
                f"non-zero real number is undefined.")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PUBLIC METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @staticmethod
    def matrix2params(mat, **kwargs):
        """
        Creates parameter vector from transformation matrix. This method
        provides flexibility for subclassing TxLinear by allowing to create
        different parametrisations of the linear transformation matrix. The
        base class implements C-order flattening of the transformation matrix
        to obtain the parameter vector.

        Note that the resultant parameter vector are not linked to the elements
        of the matrix, hence any changes in the resultant parameter vector will
        not be represented in the original matrix.

        The 'matrix' property of the the class can be regarded as the inverse
        operation of this method.

        :param mat: transformation matrix
        :type mat: np.ndarray
        :param kwargs:
            Keyword arguments required to retrieve parameters from
            affine matrix.
        :type kwargs: Any

        :returns: linear parameter array
        :rtype: np.ndarray

        """
        mat = np.asarray(mat, dtype=ts.DEFAULT_PARAMETER_DTYPE)
        parameters = mat[:, -1].ravel()
        return parameters

    @staticmethod
    def params2matrix(parameters, **kwargs):
        """
        Creates affine transformation matrix from translation parameters.

        :param parameters: translation paramters
        :type parameters: np.ndarray
        :param kwargs:
            Keyword arguments required to retrieve matrix from parameters.
        :type kwargs: Any

        :returns: affine transformation matrix
        :rtype: np.ndarray

        """
        shape = kwargs.get("shape")
        parameters = np.asarray(parameters, dtype=ts.DEFAULT_PARAMETER_DTYPE)
        mat = np.eye(*shape, dtype=parameters.dtype)
        mat[:, -1] = parameters
        return mat

    def inverse(self):
        """
        Creates inverse transformation by negating the shifts.

        :returns: inverse transformation object
        :rtype: TxTranslation

        """
        parameters = -self.parameters.copy()[:]
        meta = tu.rcopy(self.metaparameters)
        name = meta.pop("name")
        name = f"{name}_inv"
        invertible = meta.pop("invertible")
        shape = meta.pop("shape")
        obj = type(self)(*parameters, bounds=None, lock=None, name=name,
                         invertible=invertible, **meta)
        return obj

    def map(self, coords, signature=None):
        """
        Shifts the input coordinates.

        :param coords: (n_points, n_dimensions) table of input coordinates
        :type coords: np.ndarray
        :param signature: signature of the input coordinates
        :type signature: list

        :returns: (n_points, m_dimensions) table of transformed coordinates
        :rtype: np.ndarray

        """
        # Call the Transformation base class method
        coords = super(TxLinear, self).map(coords, signature)
        n = coords.shape[-1]
        vector = np.zeros(n, dtype=coords.dtype)
        vector[:self.parameters.size] = self.parameters[:].astype(coords.dtype)
        return coords + vector

    def map_vector(self, vects, coords=None, rule=None, signature=None):
        # Do not waste time on the matrix calculation implemented in TxLinear,
        # because vectors are invariant under translation.
        return super(TxTranslation, self).map_vector(
            vects, coords=coords, rule=rule, signature=signature)

    def map_tensor(self, tensors, coords=None, rule=None, signature=None):
        # Do not waste time on the matrix calculation implemented in TxLinear,
        # because tensors are invariant under translation.
        return super(TxTranslation, self).map_tensor(
            tensors, coords=coords, rule=rule, signature=signature)

    def map_vector_and_coordinates(
            self, vects, coords=None, rule=None, signature=None):
        # Do not waste time on the matrix calculation implemented in TxLinear,
        # because vectors are invariant under translation.
        coords = self.map(coords, signature)
        vects = self.map_vector(
            vects, coords=coords, rule=rule, signature=signature)
        return vects, coords

    def map_tensor_and_coordinates(
            self, tensors, coords=None, rule=None, signature=None):
        # Do not waste time on the matrix calculation implemented in TxLinear,
        # because tensors are invariant under translation.
        coords = self.map(coords, signature)
        tensors = self.map_tensor(
            tensors, coords=coords, rule=rule, signature=signature)
        return tensors, coords

    # TODO: Deprecate Transformation.jacobian()
    # def jacobian(self, coords):
    #     coords = super(TxLinear, self).map(coords)
    #     n_params = self.parameters.size
    #     n_points, n_coords = coords.shape
    #     return np.tile(np.eye(n_params, n_coords), (n_points, 1, 1))
    #
    # # TODO: Deprecate Transformation.propagator()
    # def propagator(self, coords=None):
    #     return super(TxTranslation, self).propagator(coords)


if __name__ == "__main__":  # pragma: no cover
    print("""This module is not intended for execution.""")

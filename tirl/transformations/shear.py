#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np


# TIRL IMPORTS

import tirl.utils as tu
from tirl import settings as ts
from tirl.transformations.linear import TxLinear


# DEFINITIONS

EPSILON = 1e-4


# IMPLEMENTATION

class TxShear(TxLinear):
    """
    TxShear - Shear (or skew) transformation object (subclass of TxLinear)

    """

    RESERVED_KWARGS = ("parameters", "dim_or_mat", "upper", "lower", "bounds",
                       "lock", "name", "invertible", "metaparameters")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~ INITIALISATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __init__(self, dim_or_mat, *upper, lower=(), bounds=None, lock=None,
                 name=None, invertible=True, **metaparameters):
        """
        Initialisation of TxShear.

        Shear parameters correspond to the off-diagonal elements of the shear
        matrix, but only half of them is unique: the other half can be derived
        from the former by orthogonal rotations. By default we consider the off-
        diagonal elements of the upper triangular matrix as shear parameters,
        but non-corresponding pairs may be defined from the lower triangle as
        well. Hence the parameters of the TxShear object can be specified by
        one of the following methods:

        1. TxShear(3, xy, xz, yz) defines a 3D shear matrix with the
           upper-triangular shear parameters, from left to right, top to bottom.
        2. TxShear(3, upper=(xy, None, yz), lower=(None, zx, None)) defines
           non-corresponding shear parameters in both the lower and the upper
           triangle. Correspondence between the two lists/tuples must be
           avoided by using the None keyword. To maintain linear correspondence
           between the parameters in the two triangles, the upper triangle are
           indexed from left to right, top to bottom (C order), whereas the
           parameters of the lower triangular matrix are indexed from top to
           bottom, left to right (F order).
        3. TxShear(smat) defines shear parameters by a suitably constructed
           shear matrix, which is free from parameter correspondences. An
           example 3D shear matrix can be seen below:

                                    [1, xy, 0]
                                    [0, 1, yz]
                                    [zx, 0, 1]

           When the matrix is specified, the dimension is automatically set to
           match the matrix size (n).

        :param dim_or_mat:
            Number of spatial dimensions (n) or (n, n) shear matrix. When a
            matrix is given, no further parameters should be specified, only
            metaparameters.
        :type dim_or_mat: Union[int, np.ndarray]
                :type bounds: Union[tuple, list, dict, str]
        :param lock:
            Boolean mask or index sequence that defines which parameters
            are locked, i.e. should not be updated by default. If None, all
            parameters are updated by default when calling the update() method.
        :type lock: Union[None, tuple, int, np.ndarray]
        :param name:
            Name of the Transformation object. If None (default), a default
            name of the form "tx_objectID" will be generated.
        :type name: str
        :param invertible:
            Indicates whether the current transformation is invertible (True) or
            not (False, default). Transformations marked 'invertible' must
            implement the inverse() method.
        :type invertible: bool
        :param metaparameters:
            Metaparameters for TxShear
        :type metaparameters: Any

        """
        # By the end of the initialisation the goal is to store the following:
        #   - parameter vector
        #   - shear matrix shape
        #   - linear indices to the parameter in the shear matrix (C order)

        # If the input is a square matrix
        if hasattr(dim_or_mat, "__array__"):
            mat = np.asarray(dim_or_mat)
            if (dim_or_mat.ndim == 2) and (len(set(mat.shape)) == 1):
                params, indices = self._mat2params(mat)
            else:
                raise ValueError(f"Invalid shear matrix: {mat.shape}")

        # Direct parameter specification
        elif isinstance(dim_or_mat, (int, np.integer)):
            n = int(dim_or_mat)
            if n <= 1:
                raise ValueError(
                    f"Expected at at least 2 spatial dimensions, got {n}.")

            # Upper-triangular parameters
            n_upper = len(upper)
            n_upper_expected = int(round((n ** 2 - n) / 2))
            if n_upper < n_upper_expected:
                upper = list(upper) + [0] * (n_upper_expected - n_upper)
            elif n_upper > n_upper_expected:
                raise ValueError(f"The expected number of shear parameters for "
                                 f"dimension {n} is {n_upper_expected}, got "
                                 f"{n_upper}.")

            # Lower-triangular parameters
            n_lower = len(lower)
            n_lower_expected = int(round((n ** 2 - n) / 2))
            if n_lower < n_lower_expected:
                lower = list(lower) + [0] * (n_lower_expected - n_lower)
            elif n_lower > n_lower_expected:
                raise ValueError(f"The expected number of shear parameters for "
                                 f"dimension {n} is {n_lower_expected}, got "
                                 f"{n_lower}.")

            # Convert to shear matrix and obtain reduced parameters with indices
            mat = self._parampair2mat(upper, lower)
            params, indices = self._mat2params(mat)

        # If the number of spatial dimensions is incorrect
        # and no matrix was given
        else:
            raise ValueError("Error in the input arguments.")

        # Call superclass initialisation
        super(TxShear, self).__init__(
            *params, shape=mat.shape, bounds=bounds, lock=lock, name=name,
            invertible=invertible, indices=indices, **metaparameters)

        # No need to add this as the superclass initialiser will add it with the
        # metaparameters
        # Set class-specific metaparameters : indices (read-only property)
        # self.metaparameters.update({"indices": indices})

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATTRIBUTES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @property
    def indices(self):
        """
        Returns linear parameter indices within the shear matrix (C axis
        indexing order). This is a read-only property.

        :returns: flat indices of shear parameters within the shear matrix
        :rtype: np.ndarray

        """
        return self.metaparameters.get("indices")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PRIVATE METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @staticmethod
    def _mat2params(mat):
        """
        Decomposes shear matrix to corresponding upper and lower triangular
        shear parameter pairs. Upper triangular elements are sorted according
        to C-style array indexing (row major), while lower triangular
        elements are sorted according to F-style array indexing (column major).
        The reason for the opposing sorting order is to maintain index
        correspondence between symmetrical elements.

        Example (values are random integers):

                               [0, 6, 6, 9, 9, 4],
                               [9, 1, 4, 0, 4, 5],
                               [7, 1, 3, 1, 8, 6],
                               [1, 6, 6, 1, 0, 9],
                               [9, 1, 2, 1, 7, 7],
                               [8, 5, 2, 8, 5, 0]

        Decomposes into:

            upper: [6, 6, 9, 9, 4, 4, 0, 4, 5, 1, 8, 6, 0, 9, 7]
            lower: [9, 7, 1, 9, 8, 1, 6, 1, 5, 6, 2, 2, 1, 8, 5]

        :param mat: shear matrix
        :type: np.ndarray

        :returns: independent non-zero shear parameters and their linear
                  indices in the shear matrix
        :rtype: tuple[np.ndarray, np.ndarray]

        """
        # Get the coordinates of the upper and lower triangular elements of the
        # shear matrix
        n = mat.shape[0]
        if not np.allclose(np.diag(mat), 1):
            raise ValueError(
                "The diagonal of the shear matrix must be all ones.")
        uix = set(tuple(ij) for ij in np.stack(np.triu_indices(n), axis=0).T)
        lix = set(tuple(ij) for ij in np.stack(np.tril_indices(n), axis=0).T)
        # Get the coordinates of the diagonal elements
        dix = set(tuple(ij) for ij in np.stack(np.diag_indices(n, 2), axis=0).T)
        # Dismiss the coordinates of the diagonal elements
        uix = list(uix.difference(dix))
        lix = list(lix.difference(dix))
        # Sort the upper and lower elements to establish correspondence and
        # dereference the coordinates; sorting is hierarchical
        uixc = sorted(sorted(uix, key=lambda c: c[1]), key=lambda c: c[0])
        lixc = sorted(sorted(lix, key=lambda c: c[0]), key=lambda c: c[1])
        uix = np.asarray([mat[t] for t in uixc])
        lix = np.asarray([mat[t] for t in lixc])
        # Confirm independence: one of the symmetric elements must be zero
        invalid = np.logical_and(np.abs(uix) > EPSILON, np.abs(lix) > EPSILON)
        if np.any(invalid):
            ix = np.nonzero(invalid)
            uinv = uix[ix]
            linv = lix[ix]
            raise ValueError(f"The following shear parameters are mutually "
                             f"dependent: {tuple(zip(uinv, linv))}")
        else:
            # Pairwise reduce the parameters.
            # Add epsilon to the upper triangular elements as these are
            # preferred when both parameters are zero.
            params = np.where(np.abs(uix) + EPSILON > np.abs(lix), uix, lix)
            params = np.asarray(params, dtype=ts.DEFAULT_PARAMETER_DTYPE)
            # Determine the linear indices of the chosen parameters
            uixc = [np.ravel_multi_index(ix, (n, n)) for ix in uixc]
            lixc = [np.ravel_multi_index(ix, (n, n)) for ix in lixc]
            indices = np.where(np.abs(uix) + EPSILON > np.abs(lix), uixc, lixc)

            return params, indices

    @staticmethod
    def _indexedparams2mat(dim, params, indices):
        mat = np.eye(dim)
        mat.flat[np.asarray(indices)] = params
        return mat

    @staticmethod
    def _parampair2mat(upper, lower):

        # Zero-fill non-numeric values of the input
        upper = [float(val) if val is not None else 0 for val in upper]
        lower = [float(val) if val is not None else 0 for val in lower]

        # Determine the size of the shear matrix and initialise to identity
        if len(upper) != len(lower):
            raise ValueError("There must be an equal number of lower and upper "
                             "triangular parameters.")
        n = 0.5 * (1 + np.sqrt(1 + 8 * len(upper)))
        if not n.is_integer():
            raise ValueError(f"Invalid number of parameters: {len(upper)} The "
                             f"corresponding number of spatial dimensions "
                             f"is {n}")
        mat = np.eye(int(round(n)))

        # Upper triangular mask: where the vertical coordinate is smaller
        utmask = np.argmax(np.stack(np.mgrid[0:n, 0:n], axis=-1), axis=-1) == 1
        indices = np.flatnonzero(utmask)
        assert indices.size == len(upper)
        mat.flat[indices] = np.asarray(upper)

        # Lower triangular mask: where the horizontal coordinate is smaller
        mat.T.flat[indices] = np.asarray(lower)

        return mat

    @classmethod
    def _load(cls, dump):
        parameters = dump.get("parameters")
        params = parameters.parameters
        lb = parameters.lower_bounds
        ub = parameters.upper_bounds
        lock = parameters.locked
        meta = dump.get("metaparameters")
        shape = meta.pop("shape")
        indices = meta.pop("indices")
        name = meta.pop("name")
        invertible = meta.pop("invertible")
        dim = shape[0]
        mat = cls._indexedparams2mat(dim, params, indices)
        obj = cls(mat, bounds=(lb, ub), lock=lock, name=name,
                  invertible=invertible, **meta)
        return obj

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PUBLIC METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @staticmethod
    def matrix2params(mat, **kwargs):
        """
        Creates shear parameter vector from shear matrix.

        :param mat: shear matrix
        :type mat: np.ndarray
        :param kwargs:
            Keyword arguments required to retrieve parameters from
            transformation matrix.
        :type kwargs: Any

        :returns: shear parameter vector
        :rtype: np.ndarray

        """
        # This class maps parameters to matrix by storing the parameters'
        # linear indices. To maintain compatibility with the TxLinear class, an
        # indirection is used here to return only the parameters from the
        # matrix but not the indices.
        params, indices = TxShear._mat2params(mat)
        return params

    @staticmethod
    def params2matrix(parameters, **kwargs):
        """
        Creates shear matrix from shear parameters.

        :param parameters: shear parameters
        :type parameters: np.ndarray
        :param kwargs:
            Keyword arguments required to retrieve matrix from parameters.
        :type kwargs: Any

        :returns: shear matrix
        :rtype: np.ndarray

        """
        shape = kwargs.get("shape")
        indices = kwargs.get("indices")
        parameters = np.asarray(parameters, dtype=ts.DEFAULT_PARAMETER_DTYPE)
        mat = TxShear._indexedparams2mat(dim=shape[0], params=parameters,
                                         indices=indices)
        return mat

    def inverse(self):
        """
        Creates an inverse transformation object that undoes the shears. The
        inverse transformation is also a shear matrix with negated shear
        parameters.

        :returns: inverse transformation object
        :rtype: TxShear

        """
        invmat = np.linalg.inv(self.matrix)
        meta = tu.rcopy(self.metaparameters)
        name = meta.pop("name")
        name = f"{name}_inv"
        invertible = meta.pop("invertible")
        shape = meta.pop("shape")
        indices = meta.pop("indices")

        obj = type(self)(invmat, bounds=None, lock=None, name=name,
                         invertible=invertible, **meta)
        return obj

    # TODO: Deprecate Transformation.jacobian()
    # def jacobian(self, coords):
    #     # TODO: Update this!
    #     coords = super(TxLinear, self).map(coords)
    #     n_points, n_coords = coords.shape
    #     n_dim = self.ndim
    #     jac = np.zeros(shape=(n_points, n_dim, n_coords))
    #     y = coords[:, 1]
    #     z = coords[:, 2]
    #     jac[:, 0, 0] = y
    #     jac[:, 1, 0] = z
    #     jac[:, 2, 1] = z
    #     return jac


if __name__ == "__main__":  # pragma: no cover
    print("""This module is not intended for execution.""")

#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np


# TIRL IMPORTS

from tirl.cache import Cache
from tirl.transformations import Transformation


# DEFINITIONS

from tirl.constants import *


# IMPLEMENTATION

class TxNonLinear(Transformation):
    """
    TxNonLinear (child of Transformation) - generic non-linear transformation
    base class.

    Non-linear transformations inherit from the Transformation base class.
    In addition to Transformation, TxNonLinear has an associated domain as a
    metaparameter that defines the extent of the transformation. TxNonLinear
    has two static methods (params2field and field2params) that map
    transformation parameters to the equivalent deformation field and vice
    versa. The deformation field is always an instance of TField
    (or a subclass).

    TxNonLinear should be subclassed to perform non-linear transformations with
    custom parametrisations by overloading the params2field and field2params
    methods.

    Non-linear transformations are flagged as non-invertable by default, but
    this should be changed for subclasses that implement the inverse() method.

    """
    __tx_priority__ = 2000

    RESERVED_KWARGS = ("parameters", "domain", "interpolator", "bounds",
                       "lock", "name", "invertible", "metaparameters")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ INITIALISATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __init__(self, *parameters, domain=None, interpolator=None,
                 mode=NL_ABS, bounds=None, lock=None, name=None,
                 invertible=False, **metaparameters):
        """
        Intialisation of TxNonLinear.

        :param parameters:
            Non-linear transformation parameters may be specified by one of the
            following:
                1. sequence of numbers
                2. NDArray
                3. TField
        :type parameters: Union[int, float, np.ndarray, TField]
        :param domain:
            Spatial extent of the non-linear transformation.

            When parameters are set by a TField, the domain of the TField
            instance is used automatically, which will override any input for
            this argument.

            If None (default), it means that the non-linear transformation is
            not bound to a finite spatial extent, but it is valid globally.
            (This is most likely the case if the deformation field can be
            defined analytically.)

            If not None, coordinates that fall outside the specified domain
            will not be affected by the transformation. If the non-linear
            transformation parameters are defined for a subset or all points of
            the domain, the deformations at the input coordinates must first be
            calculated by interpolation. If the domain is identical to the
            domain from which the input coordinates are generated, the
            transformation is said to be "dynamically linked" to the source.
            In this case the source and the transformation move together, and
            the coordinate mapping can be performed without interpolation by
            sourcing the deformation values directly from the field, as these
            correspond to the same points on the source domain that are being
            mapped.
        :type domain: Domain
        :param interpolator:
            Interpolator class or instance that used to create the deformation
            field. If the parameters were set by a TField instance, the
            TField interpolator is used, and any input for this argument will
            be overridden. If None (default), the interpolator will be chosen
            for the domain based on what is specified in tirlconfig:
            DEFAULT_COMPACT_INTERPOLATOR for compact domains, and
            DEFAULT_NONCOMPACT_INTERPOLATOR for sparse domains.
        :type interpolator: Union[Interpolator, TIRLObject]
        :param voxelunits:
            If True (default), the transformation will assume that its
            parameters are in voxel units. If False, the parameters are assumed
            to be in the units of the physical space. If the transformation is
            dynamically linked to an image that has an affine transformation,
            the effect of the non-linear transformation will be the same w.r.t.
            the image even when the parameters of the affine are changed.
            However this comes at the cost of an additional computation step in
            the mapping algorithm, which is required to compute the
            displacement vectors in physical space before coordinates can be
            mapped. In short, set voxelunits to True if the non-linear
            transformation needs to "follow" an image irrespective of the
            antecedent affine transformation.
        :type voxelunits: bool
        :param bounds:
            Parameter bounds can be defined in one of the following formats:
            1. ((lb0, ub0),) - sequence of lower and upper bounds for each
               parameter. The order must correspond to the order of the
               transformation parameters.
            2. {param_no: (lb, ub)} - dictionary of lower and upper bound
               pairs, where the keys represent parameter indices (starting
               from 0).
            3. (lb_arr, ub_arr) - flat iterable with lower and upper bounds for
               all parameters.
            4. "auto" - the get_default_bounds() method is called to create
               default lower and upper bounds.
            5. None - parameter bounds are not set.
            6. (lb, ub) - global scalar values for lower and upper bounds
        :type bounds: Union[tuple, list, dict, str]
        :param lock:
            Boolean mask or index sequence that defines which parameters
            are locked, i.e. should not be updated by default. If None, all
            parameters are updated by default when calling the update() method.
        :type lock: Union[None, tuple, int, np.ndarray]
        :param name:
            Name of the Transformation object. If None (default), a default
            name of the form "tx_objectID" will be generated.
        :type name: str
        :param invertible:
            Indicates whether the current transformation is invertible (True) or
            not (False, default). Transformations marked 'invertible' must
            implement the inverse() method.
        :type invertible: bool
        :param metaparameters:
            Metaparameters for TxNonLinear.
        :type metaparameters: Any

        """
        if parameters == ():
            raise ValueError("No parameters were specified.")

        # Read parameter, domain and interpolator specification from TField
        if hasattr(parameters[0], "__tfield__"):
            f = parameters[0]
            # Note: the values of the input field are taken a
            parameters = (self.field2params(f, mode=mode),)
            domain = f.domain
            interpolator = interpolator or f.interpolator.copy()

        # Call the initialisation of the Transformation base class to store the
        # parameters and metaparameters of TxNonLinear
        parameters = np.array(parameters)
        super(TxNonLinear, self).__init__(
            *parameters, bounds=bounds, lock=lock, name=name,
            invertible=invertible, domain=domain, interpolator=interpolator,
            **metaparameters)

        # Re-set class-specific attributes
        # Domain
        if hasattr(domain, "__domain__") or domain is None:
            self.metaparameters.update({"domain": domain})
        else:
            raise TypeError("Invalid domain specification.")
        # Interpolator
        if hasattr(interpolator, "__interpolator__"):
            self.metaparameters.update({"interpolator": interpolator.copy()})

        # Set mode
        self.mode = mode

        # Cache:
        self._cache = Cache(maxsize=1)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATTRIBUTES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @property
    def cache(self):
        """
        Read-only property that protects the delegated Cache instance from
        accidental overwriting, dereferencing, and garbage collection.

        """
        return self._cache

    @property
    def domain(self):
        """
        Read-only property that returns the spatial extent of the non-linear
        transformation.

        Changing the domain of a non-linear transformation may also change the
        number of parameters, hence it is disabled. That functionality is
        instead provided by the 'regrid' method, which returns a new
        transformation object.

        """
        return self.metaparameters.get("domain")

    @property
    def dim(self):
        """
        Returns the number of input coordinates expected by the transformation.
        Subclasses SHOULD overload this property if necessary.

        """
        return self.field().tshape[-1]

    @property
    def mode(self):
        return self.metaparameters.get("mode")

    @mode.setter
    def mode(self, m):
        if isinstance(m, str):
            self.metaparameters.update(mode=m)
        else:
            raise TypeError(f"Expected str input for mode, "
                            f"got {m.__class__.__name__} instead.")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~ PRIVATE METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PUBLIC METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def params2field(self, parameters, **kwargs):
        """
        Calculates deformation field from the transformation parameters.
        Subclasses MUST overload this method to implement the forward model.

        :param parameters:
            Transformation parameters.
        :type parameters: Union[np.memmap, np.ndarray]
        :param kwargs:
            Keyword arguments required to calculate the deformation field.
        :type kwargs: Any

        :returns: transformation-equivalent deformation field
        :rtype: TField

        """
        raise NotImplementedError(
            "The params2field() method must be implemented by the "
            "subclasses of TxNonLinear.")

    def field2params(self, field, **kwargs):
        """
        Calculates a set of parameters that represent the transformation within
        the given domain with satisfactory precision. Subclasses SHOULD
        overload this method to provide a class-specific optimised algorithm.

        The base class implements a gradient-free optimisation that uses the
        forward model (params2field) to compute an approximation to the field,
        and refines parameter estimates by gradually minimising the
        sum-of-square differences between the specified field and its estimate.

        :param field: deformation field
        :type field: TField
        :param kwargs:
            Keyword arguments required to calculate the deformation field.
        :type kwargs: Any

        :return: transformation parameters
        :rtype: Union[np.memmap, np.ndarray]

        """
        # Internal cost function (sum of squared differences)
        def field_match_cost(params):
            """ Sum-of-squared cost value for field differences. """
            current_field = self.params2field(params, domain=field.domain)
            return np.sum((current_field - field) ** 2)

        # Optimiser
        from scipy.optimize import fmin
        optparams = dict(xtol=1e-4, ftol=None, maxiter=100, disp=1)
        optparams.update(kwargs)
        if optparams.get("ftol") is None:
            initial_cost = field_match_cost(self.parameters)
            optparams["ftol"] = initial_cost * 0.01

        # Optimisation
        opt = fmin(field_match_cost, self.parameters, **optparams)
        return opt.x

    def field(self, domain=None):
        """
        Synthesises deformation vectors from the transformation parameters
        at each point of the specified domain. If no domain is specified, the
        domain of the transformation object is used.

        This method is analogous to the 'evaluate' method of TField. An
        important difference is that when the params2field method is defined
        analytically, the field can be computed even outside the original
        domain of TxNonLinear.

        :param domain:
            Spatial extent of the new transformation. If None, the domain of
            the current non-linear transformation object will be used.
        :type domain: Domain

        :returns: deformation field over the specified domain
        :rtype: TField

        """
        if domain is None:
            domain = self.domain

        # Note: the copy is a burden on performance, but it is not safe to
        # share the parameters of the transformation with other objects.
        return self.params2field(self.parameters.copy(), domain=domain)

    def regrid(self, domain):
        """
        Returns a new non-linear transformation object defined on the specified
        domain. The base class (TxNonLinear) implements a general algorithm
        that computes the deformation field over the new domain, and calculates
        the respective transformation parameters that best match the new
        displacement field.

        Subclasses MUST override this method if they cannot be uniquely
        constructed from a TField instance.

        Subclasses SHOULD override this method if there is a more efficient way
        of computing new parameters based on the old and new domains.
        If overridden, it must be ensured that the method returns a new object
        instance and does not modify the current object.

        """
        return NotImplementedError()

    def map(self, coords, signature=None):
        """
        Applies the transformation to the input coordinates. Subclasses MUST
        implement this function, and make sure that the parent-class function
        is called first (this will ensure that the input is in the correct
        layout and has the appropriate signature).

        :param coords:
            (n_points, m_coordinates) table of input coordinates
        :type coords: np.ndarray

        :returns: (n_points, k_coordinates) table of transformed coordinates
        :rtype: np.ndarray

        """
        return super(TxNonLinear, self).map(coords, signature)

    def matrix(self, coords=None):
        """
        Returns the local affine matrix at the from the field of the
        non-linear transformation.

        """
        return NotImplementedError()

    def adjustment_matrix(self, coords=None, rule=RULE_SSR):
        """
        Returns the orthogonal rotation component (if RULE_FS) or the
        scale-skew-rotation component (if RULE_SSR) of the local affine
        transformation that is estimated from the absolute displacement field.
        The matrix can be used to rotate vectors and tensors, as a means of
        adjusting their orientation, hence the name of the method.

        Note: TIRL assumes covariant behaviour of the tensor field (as opposed
        to contravariant), hence this matrix represents a covariant adjustment
        of the tensors with the transformation, and it is called by
        transformation chains and domains without further modification. For a
        contravariant transformation this matrix should be adjusted accordingly.

        """
        F = self.matrix(coords=coords)  # local affines (asymmetric)
        r = min(F.shape[-2:])
        F = F[..., :r, :r]

        if rule == RULE_FS:
            u, s, vt = np.linalg.svd(
                F @ F.transpose(0, 2, 1), full_matrices=True)
            mat = np.linalg.inv((u * np.sqrt(s[:, np.newaxis, :])) @ vt) @ F
            # Caveat emptor: ensure that the rotation matrix is positive
            # definite, as we cannot afford checking at this place.

        elif rule == RULE_SSR:
            mat = F

        else:
            raise ValueError(f"Unrecognised vector transformation rule: {rule}")

        return mat

    def map_vector(self, vects, coords=None, rule=RULE_SSR, signature=None):
        """
        Applies transformation to vectors at the locations specified by the
        coordinates. As linear transformations are location-invariant,
        coordinates are optional for linear transformations, but obligatory for
        non-linear transformations, where the transformation may vary from
        point to point.

        """
        vects = super(TxNonLinear, self).map_vector(
            vects, coords=coords, rule=rule, signature=signature)

        if rule in (RULE_FS, RULE_SSR):
            mat = self.adjustment_matrix(coords=coords, rule=rule)
            # assert mat.shape[-1] == vects.shape[-1]
            effective = mat.shape[-1]
            newvects = np.empty_like(vects)
            newvects[:, :effective] = \
                np.take(mat @ vects[:, :effective, np.newaxis], 0, axis=-1)
            newvects[:, effective:] = vects[:, effective:]
        else:
            raise ValueError(f"Unrecognised vector transformation rule: {rule}")

        return newvects

    def map_vector_and_coordinates(
            self, vects, coords=None, rule=None, signature=None):
        """
        Transforms both vectors at certain coordinates and the coordinates
        themselves. This method is called by a chain when performing sequantial
        mappings.

        """
        # Transform vectors at the specified coordinates
        vects = self.map_vector(vects, coords, rule, signature)

        # Transform coordinates
        if coords is None:
            coords = self.domain.get_physical_coordinates()
        coords = self.map(coords)

        return vects, coords

    def map_tensor(self, tensors, coords=None, rule=None, signature=None):
        """
        Applies transformation to tensors at the locations specified by the
        coordinates. As linear transformations are location-invariant,
        coordinates are optional for linear transformations, but obligatory for
        non-linear transformations, where the transformation may vary from
        point to point.

        """
        tensors = super(TxNonLinear, self).map_tensor(
            tensors, coords=coords, rule=rule, signature=signature)

        if rule in (RULE_FS, RULE_SSR):
            mat = self.adjustment_matrix(coords=coords, rule=rule)
            assert mat.shape[-1] == tensors.shape[-1] == tensors.shape[-2]
        elif rule == RULE_PPD:
            F = self.adjustment_matrix(coords=coords, rule=RULE_SSR)
            mat = ppd(F, tensors)
        else:
            raise ValueError(f"Unrecognised tensor transformation rule: {rule}")

        return mat @ tensors @ np.linalg.inv(mat)

    def map_tensor_and_coordinates(
            self, tensors, coords=None, rule=None, signature=None):
        """
        Transforms both tensors at certain coordinates and the coordinates
        themselves. This method is used for sequential mapping by a chain.
        """
        # Transform tensors at the specified coordinates
        tensors = self.map_tensor(tensors, coords, rule, signature)

        # Transform coordinates
        if coords is None:
            coords = self.domain.get_physical_coordinates()
        coords = self.map(coords)

        return tensors, coords


def ppd(F, tensors):
    _, vects = np.linalg.eigh(tensors)
    e1 = vects[..., -1]
    n1 = np.squeeze(F @ e1[..., np.newaxis])
    n1 /= np.linalg.norm(n1, axis=-1, keepdims=True)
    R1 = rotmat(e1, n1)
    if tensors.shape[-1] == 2:
        return R1

    e2 = vects[..., -2]
    n2 = np.squeeze(F @ e2[..., np.newaxis])
    n2 /= np.linalg.norm(n2, axis=-1, keepdims=True)
    Pn2 = n2 - (n2[..., np.newaxis, :] @ n1[..., np.newaxis])[..., 0] * n1
    Pn2 /= np.linalg.norm(Pn2, axis=-1, keepdims=True)
    f2 = np.squeeze(R1 @ e2[..., np.newaxis])
    f2 /= np.linalg.norm(f2, axis=-1, keepdims=True)
    R2 = rotmat(f2, Pn2)
    return R2 @ R1


def rotmat(a, b):
    a = np.asarray(a, dtype=np.float64)
    b = np.asarray(b, dtype=np.float64)
    a = a / np.linalg.norm(a, axis=-1, keepdims=True)
    b = b / np.linalg.norm(b, axis=-1, keepdims=True)
    n = a.shape[0]
    d = a.shape[-1]
    cross = np.cross(a, b)
    sine = np.linalg.norm(cross, axis=-1, keepdims=True)
    cosine = np.einsum("...i,...i", a, b)[..., np.newaxis]
    axis = cross / sine
    K = np.zeros((n, d, d), dtype=np.float64)
    for i in range(d):
        e = np.zeros((d,), dtype=np.float64)
        e[i] = 1
        K[..., i] = np.cross(axis, e)
    R = np.eye(d)[np.newaxis] + sine[..., np.newaxis] * K \
        + (1 - cosine[..., np.newaxis]) * np.matmul(K, K)
    return R


if __name__ == "__main__":  # pragma: no cover
    print("""This module is not intended for execution.""")

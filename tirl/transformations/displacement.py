#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np
import scipy.sparse as sp
from numbers import Number, Integral


# TIRL IMPORTS

import tirl.utils as tu
import tirl.settings as ts
from tirl.domain import Domain
from tirl.tfield import TField
from tirl.interpolation.scipyinterpolator import ScipyInterpolator
from tirl.cmodules.finv import invert
from tirl.cmodules.finv import local_affine
from tirl.cmodules.finv import jacobian2d, jacobian3d
from tirl.transformations.transformation import Transformation
from tirl.transformations.nonlinear import TxNonLinear


# DEFINITIONS

from tirl.constants import *


# IMPLEMENTATION

class TxDisplacementField(TxNonLinear):
    """
    TxDisplacementField class - represents a non-linear transformation as a
    field of displacement vectors.

    """
    RESERVED_KWARGS = ("field", "domain", "interpolator", "bounds", "lock",
                       "name", "invertible", "mode", "vectorder",
                       "metaparameters")

    def __init__(self, field, domain=None, interpolator=None,
                 bounds=None, lock=None, name=None, invertible=True,
                 mode=NL_ABS, vectorder=None, **metaparameters):
        """
        Initialisation of TxDisplacementField.

        :param field:
            Vector field defined as a TField or an NDArray. Note that the order
            of the input TField will be set to TENSOR_MAJOR.
        :type field: TField or np.ndarray
        :param domain:
            Domain that defines the extent of the transformation. This
            argument is ignored if 'field' is a TField. If the field is an
            array, the domain must also be specified.
        :type domain: None or Domain
        :param interpolator:
            Field interpolator instance. This argument takes precedence over
            the TField specification. If None (default), and the field input is
            an array, the DEFAULT_COMPACT_INTERPOLATOR or the
            DEFAULT_NONCOMPACT_INTERPOLATOR will be used based on the domain
            type.
        :type interpolator: Type or Interpolator or None
        :param mode:
            If NL_ABS (default), the vectors of the input TField are
            interpreted as physical vectors, and
        :param metaparameters: metaparameters of the transformation
        :type metaparameters: Any

        """
        # The initialisation sets the parameters directly. Whether these
        # parameters will be interpreted as relative or absolute displacements
        # during the mapping will depend on the mode switch. The mode switch
        # has to be set at initialisation and cannot be changed later to avoid
        # the confusion between the two interpretations of the parameters. The
        # mode is a metaparameter, which is saved with the rest of the
        # parameters and metaparameters, thus there is no ambiguity when
        # loading transformations from file.

        # Input is TField: must have exactly 1 tensor axis
        if hasattr(field, "__tfield__"):
            domain = domain or field.domain
            interpolator = interpolator or field.interpolator.copy()
            dtype = np.result_type(ts.DEFAULT_FLOAT_TYPE, field.dtype)
            data = field.data.astype(dtype)
            f = TField(
                data, taxes=field.taxes, copy=False, domain=domain,
                order=TENSOR_MAJOR, dtype=dtype, interpolator=interpolator,
                name=None, storage=field.storage, header=field.header,
                keepdims=True)

        # Input is NDArray: displacement dimension must be the first axis
        elif hasattr(field, "__array__"):
            if domain is not None:
                field = np.asanyarray(field).reshape(-1, *domain.shape)
            dtype = np.result_type(ts.DEFAULT_FLOAT_TYPE, field.dtype)
            data = field.astype(dtype)
            f = TField(
                data, taxes=(0,), copy=False, domain=domain, order=TENSOR_MAJOR,
                dtype=dtype, interpolator=interpolator, name=None, storage=MEM,
                keepdims=True)

        else:
            raise TypeError(f"Unrecognised input for displacement field: "
                            f"{field.__class__.__name__}")

        # Limited numerical precision may give rise to interpolation problems
        # where the locations are just outside the domain (e.g. by 2e-5). To
        # avoid this, the interpolator is configured for extrapolation.
        extrapolator = f.interpolator.copy()
        if isinstance(extrapolator, ScipyInterpolator):
            extrapolator.kwargs.update(mode="nearest")

        # Calling the superclass initialiser with NL_ABS will ensure that the
        # parameters are taken from the input field values without further
        # transformation.
        super(TxDisplacementField, self).__init__(
            f, domain=f.domain, interpolator=extrapolator, mode=NL_ABS,
            bounds=bounds, lock=lock, name=name, invertible=invertible,
            **metaparameters)

        # Set the mode after the initialisation is complete
        if f.tsize == 1:
            vo = "x"
        elif f.tsize == 2:
            vo = "xy"
        elif f.tsize == 3:
            vo = "xyz"
        else:
            raise ValueError(
                f"Invalid number of vector dimensions: {f.tsize}")
        vectorder = vo if vectorder is None else vectorder
        self.vectorder = vectorder
        assert self.vectdim == f.tsize, \
            f"The specified vector coordinate order does not correspond to " \
            f"the displacement vectors with dimension {f.tsize}."
        self.mode = mode

    def __add__(self, other):
        # self + scalar: increment parameter values globally
        if isinstance(other, Number):
            res = self.copy()
            res.metaparameters.update(domain=self.domain)
            res.parameters.parameters[:] += other  # raises on casting error
            return res

        # Ensure that the rest of this function only deals with Transformations
        if not isinstance(other, Transformation):
            return super(TxDisplacementField, self).__add__(other)

        # Enforce operation handler priority between transformations
        if self._lower_priority(other) and hasattr(other, "__radd__"):
            return other.__radd__(self)

        # self + other
        x = self.domain.get_physical_coordinates()
        xp = self.map(x)
        xp = other.map(xp)
        v = xp - x
        tx = type(self)(v.T.reshape((-1, *self.domain.shape)),
                        self.domain, mode=NL_ABS)
        if self.mode == NL_REL:
            tx = tx.relative()
        return tx

    def __radd__(self, other):
        # scalar + self: increment parameter values globally
        if isinstance(other, Number):
            res = self.copy()
            res.metaparameters.update(domain=self.domain)
            res.parameters.parameters[:] += other  # raises on casting error
            return res

        # Ensure that the rest of this function only deals with Transformations
        if not isinstance(other, Transformation):
            return super(TxDisplacementField, self).__radd__(other)

        # Enforce operation handler priority between transformations
        if self._lower_priority(other) and hasattr(other, "__add__"):
            return other.__add__(self)

        xp = self.domain.get_physical_coordinates()
        oitx = other.inverse()
        du = xp - oitx.map(xp)
        tx = self.copy().absolute()
        tx.parameters.parameters[:] += du.T.ravel()
        if self.mode == NL_REL:
            tx = tx.relative()
        domain = self.domain[:]  # preserves txs in a new domain instance
        tx.metaparameters.update(domain=domain)
        tx.domain.chain.append(oitx)
        return tx

    def __sub__(self, other):
        if isinstance(other, Transformation) and other.invertible:
            return self.__add__(other.inverse())
        else:
            return super(TxDisplacementField, self).__sub__(other)

    def __mul__(self, other):
        # Integer multiples are interpreted as the concatenation of several
        # identical copies of self.
        if isinstance(other, Integral):
            if other >= 1:
                tx = self.copy()
                tx.metaparameters.update(domain=self.domain)
                for _ in range(other - 1):
                    tx = tx + tx
                return tx
        raise NotImplementedError(
            "Multiplication of displacement fields is only defined "
            "for positive integer operands.")

    def __rmul__(self, other):
        # The only defined multiplication is commutative.
        return self.__mul__(other)

    @property
    def vectdim(self):
        return len(self.vectorder)

    @property
    def vectorder(self):
        return self.metaparameters.get("vectorder")

    @vectorder.setter
    def vectorder(self, vo):
        # Set by numbers
        direct_input = [(0,), (1,), (2,), (0, 1), (0, 2), (1, 0), (1, 2),
                        (2, 0), (2, 1), (0, 1, 2), (0, 2, 1), (1, 0, 2),
                        (1, 2, 0), (2, 0, 1), (2, 1, 0)]
        if tuple(vo) in direct_input:
            self.metaparameters.update(vectorder=tuple(vo))
            return

        # Set by string
        valid_inputs = ['x', 'y', 'z', 'xy', 'xz', 'yx', 'yz', 'zx', 'zy',
                        'xyz', 'xzy', 'yxz', 'yzx', 'zxy', 'zyx']
        if not isinstance(vo, str) or (vo.lower() not in valid_inputs):
            raise TypeError(f"Expected str for vector order, "
                            f"got {vo.__class__.__name__} instead.")

        vo = vo.replace("x", "0").replace("y", "1").replace("z", "2")
        vo = tuple(int(ax) for ax in vo)
        self.metaparameters.update(vectorder=vo)

    @property
    def interpolator(self):
        return self.metaparameters.get("interpolator")

    def abs2rel(self, field):
        """
        Takes an input TField, the vectors of which are interpreted as
        'absolute' displacements (relative to the world coordinate system).
        Returns a TField that is defined on the domain of the current
        transformation, and the vectors of which are meant to be interpreted as
        displacements relative to the transformation's own voxel space.

        Shorthand: A[foreign | self] -> R[self]

        """
        # Ensure that the field is in TENSOR_MAJOR orientation
        field.order = TENSOR_MAJOR

        # Case 1: field is defined on the current domain
        # Task: convert abs->rel

        # Case 2: field is defined on a domain that is different form self
        # Task: resample on self domain and convert abs->rel
        if field.domain != self.domain:
            field = field.evaluate(self.domain, rule=None)

        # Convert vectors abs->rel on self domain
        v = field.data.reshape((field.tsize, -1)).T
        c = self.domain.get_physical_coordinates()
        v = self.domain.map_physical_vectors(v, c, rule=RULE_SSR)
        name = f"{field.name}_rel"

        # Create output TField with relative vectors on self domain
        relative_field = TField(
            v.T.ravel().data, domain=self.domain, tshape=(self.vectdim,),
            order=TENSOR_MAJOR, dtype=ts.DEFAULT_FLOAT_TYPE, name=name,
            storage=MEM, interpolator=self.interpolator
        )
        return relative_field

    def rel2rel(self, field):
        """
        Takes an input TField, the vectors of which are interpreted as
        displacements relative to the field's own voxel space.

        Returns a TField that is defined on the domain of the current
        transformation, and the vectors of which are meant to be interpreted as
        displacements relative to the transformation's own voxel space.

        Shorthand: R[foreign | self] -> R[self]

        """
        # Ensure that the field is in TENSOR_MAJOR orientation
        field.order = TENSOR_MAJOR

        # Case 1: field is defined on the current domain
        # Task: nothing to do
        if field.domain == self.domain:
            return field

        # Case 2: field is defined on a domain that is different form self
        # Task: resample on self domain and convert rel->rel
        foreign_chain = field.domain.offset + field.domain.chain
        self_chain = self.domain.offset + self.domain.chain
        c = field.domain.get_voxel_coordinates()  # yes, voxel! (vox2vox)
        field = field.evaluate(self.domain, rule=None)
        v = field.data.reshape((field.tsize, -1)).T
        chain = foreign_chain + self_chain.inverse()
        v = chain.map_vector(v, c, rule=RULE_SSR)
        name = f"{field.name}_rel"

        # Create output TField with relative vectors on self domain
        relative_field = TField(
            v.T.ravel().data, domain=self.domain, tshape=(self.vectdim,),
            order=TENSOR_MAJOR, dtype=ts.DEFAULT_FLOAT_TYPE, name=name,
            storage=MEM, interpolator=self.interpolator
        )
        return relative_field

    def rel2abs(self, field):
        """
        Takes an input TField, the vectors of which are interpreted as
        displacements relative to the field's own voxel space.

        Returns a TField that is defined on the domain of the current
        transformation, and the vectors of which are meant to be interpreted as
        'absolute' displacements (relative to the world coordinate system).

        Shorthand: R[foreign | self] -> A[self]

        """
        # Ensure that the field is in TENSOR_MAJOR orientation
        field.order = TENSOR_MAJOR

        # Case 1: field is defined on the current domain
        # Task: convert rel->abs
        if field.domain == self.domain:
            v = field.data.reshape((field.tsize, -1)).T
            c = field.domain.get_voxel_coordinates()
            v = self.domain.map_voxel_vectors(v, c, rule=RULE_SSR)
            name = f"{field.name}_abs"
            return TField(
                v.T.ravel().data, domain=self.domain, tshape=(self.vectdim,),
                order=TENSOR_MAJOR, dtype=v.dtype, name=name,
                storage=MEM, interpolator=self.interpolator.copy()
            )
        # Case 2: field is defined on a domain that is different form self
        # Task: convert rel->abs and resample on self domain
        foreign_chain = field.domain.offset + field.domain.chain
        v = field.data.reshape((field.tsize, -1)).T
        c = field.domain.get_voxel_coordinates()
        v = foreign_chain.map_vector(v, c, rule=RULE_SSR)
        name = f"{field.name}_abs"
        absolute_field = TField(
            v.T.ravel().data, domain=field.domain, tshape=field.tshape,
            order=TENSOR_MAJOR, dtype=ts.DEFAULT_FLOAT_TYPE, name=name,
            storage=field.storage, interpolator=field.interpolator.copy()
        )
        return absolute_field.evaluate(self.domain, rule=None)

    def abs2abs(self, field):
        """
        Takes an input TField, the vectors of which are interpreted as
        'absolute' displacements (relative to the world coordinate system).

        Returns a TField that is defined on the domain of the current
        transformation, and the vectors of which are meant to be interpreted as
        'absolute' displacements (relative to the world coordinate system).

        Shorthand: A[foreign | self] -> A[self]

        """
        # Ensure that the field is in TENSOR_MAJOR orientation
        field.order = TENSOR_MAJOR

        # Case 1: field is defined on the current domain
        # Task: nothing to do
        if field.domain == self.domain:
            return field

        # Case 2: field is defined on a domain that is different from self
        # Task: resample data
        return field.evaluate(self.domain, rule=None)

    def absolute(self):
        """
        Returns an equivalent DisplacementField transformation (or subclass)
        with absolute displacement vectors.

        """
        if self.mode == NL_ABS:
            return self
        else:
            field = self.rel2abs(self.field())
            return type(self)(field, mode=NL_ABS)

    def relative(self):
        """
        Returns an equivalent DisplacementField transformation (or subclass)
        with relative displacement vectors.

        """
        if self.mode == NL_REL:
            return self
        else:
            field = self.abs2rel(self.field())
            return type(self)(field, mode=NL_REL)

    def std(self, dim=None):
        """
        Returns the equivalent DisplacementField, the vectors of which are
        in standard orientation and have the same number of dimensions as the
        transformation domain, or whatever number is specified as 'dim'.
        Extraneous vector components are set to 0 and locked. The parameter
        bounds are transferred accordingly for all original parameters.

        """
        if dim is None:
            dim = self.domain.ndim

        if self.vectorder == tuple(range(dim)):
            return self

        n = self.domain.numel
        vects = self.parameters.parameters.reshape((self.vectdim, -1)).T
        # Note: the field may deform a planar structure along the z axis,
        # so the total number of dimensions has to account for this.
        n_dims = max(dim, max(self.vectorder) + 1)
        stdvects = np.zeros((n, n_dims), dtype=self.parameters.dtype)
        lb, ub = self.parameters.get_bounds().T
        lb = lb.reshape(-1, n).T
        ub = ub.reshape(-1, n).T
        lb_full = np.full_like(stdvects, fill_value=-np.inf)
        ub_full = np.full_like(stdvects, fill_value=np.inf)
        locked = self.parameters.locked
        locked = np.fromiter(locked, dtype=int, count=len(locked))

        locked_full = set()
        for i in range(n_dims):
            if i not in self.vectorder:
                locked_full.update(np.arange(i * n, (i + 1) * n))
        for i, ax in enumerate(self.vectorder):
            stdvects[:, ax] = vects[:, i]
            lb_full[:, ax] = lb[:, i]
            ub_full[:, ax] = ub[:, i]
            current = (i*n <= locked) & (locked < (i+1)*n)
            locked_full.update(locked[current] + ax * n)

        # Create standard transformation object
        meta = {k: v for k, v in self.metaparameters.items()
                if k not in self.RESERVED_KWARGS}
        shape = (n_dims, *self.domain.shape)
        bounds = (lb_full.T.ravel(), ub_full.T.ravel())
        return type(self)(stdvects.T.reshape(shape), domain=self.domain,
                          interpolator=self.interpolator.copy(), bounds=bounds,
                          lock=locked_full, name=self.name,
                          invertible=self.invertible, mode=self.mode,
                          vectorder=tuple(range(n_dims)), **meta)

    def field2params(self, field, **kwargs):
        """
        Assumes that the input field represents 'absolute' displacements
        (relative to the world coordinate system), and converts this into an
        equivalent parametric representation depending on the 'mode' of the
        transformation.

        Note that 'equivalent' refers to identical mapping within the domain of
        the current transformation. If the input field is defined on a
        different domain, it will be resampled to the transformation's domain.

        """
        field.order = TENSOR_MAJOR

        # Needed for construction, because metaparameters are initialised after
        # parameters, and the mode setting is required to interpret parameters)
        if not hasattr(self, "mode"):
            return field.data.ravel()

        if self.mode == NL_ABS:
            parameters = self.abs2abs(field).data.ravel()
            dtype = np.result_type(ts.DEFAULT_FLOAT_TYPE, parameters.dtype)
            return parameters.astype(dtype)

        else:
            parameters = self.abs2rel(field).data.ravel()
            dtype = np.result_type(ts.DEFAULT_FLOAT_TYPE, parameters.dtype)
            return parameters.astype(dtype)

    def params2field(self, parameters, domain=None, **kwargs):
        """
        Substitutes the parameters of the current transformation and creates an
        equivalent TField representation of the transformation with either
        relative or absolute displacement parameters, depending on the mode of
        the transformation.

        """
        if domain is None:
            domain = self.domain
        elif not isinstance(domain, Domain):
            raise TypeError(f"Invalid domain specification: {domain}")
        parameters = np.asanyarray(parameters)

        # Substitute the input parameters into the transformation to create an
        # alternative field.
        alternative_field = TField(
            parameters.data, domain=self.domain, tshape=(self.vectdim,),
            order=TENSOR_MAJOR, dtype=parameters.dtype, name=None,
            storage=MEM, interpolator=self.interpolator.copy())

        # Only perform evaluation if the domain is different
        if domain != self.domain:
            alternative_field = alternative_field.evaluate(domain, rule=None)

            # If the domain is different and the alternative field was
            # relative, the vectors must also be transformed.
            if self.mode == NL_REL:
                chain = self.domain.all_tx() + domain.all_tx().inverse()
                c = self.domain.get_voxel_coordinates()
                v = alternative_field.data.reshape(
                    (alternative_field.tsize, -1)).T
                v = chain.map_vector(v, c, rule=RULE_SSR)
                alternative_field.data.flat[...] = v.T.ravel()

        return alternative_field

    def regrid(self, domain):
        """
        Evaluate the transformation on a different domain with all the
        metaparameters preserved.

        """
        # Calculate the field over the given domain, and use that to initialise
        # a new TxNonLinear instance of the same type.

        absolute_field = self.std().absolute().field(domain)
        meta = {k: v for k, v in self.metaparameters.items()
                if k not in self.RESERVED_KWARGS}
        absolute_tx = type(self)(
            absolute_field, domain=domain,
            interpolator=self.interpolator.copy(), bounds=None, lock=None,
            name=self.name, invertible=self.invertible, mode=NL_ABS, **meta)
        if self.mode == NL_ABS:
            return absolute_tx
        if self.mode == NL_REL:
            return absolute_tx.relative()

    def resample(self, domain):
        """
        This method recalculates the transformation parameters for a different
        domain. An important assumption is that the new domain spans the same
        physical space as the current domain, such that the change in the
        support of the domain points can be accurately estimated.

        Before the displacement vectors are resampled, in the case of
        downsampling, Gaussian smoothing is applied to compensate for the
        change in the support weight of the new domain points.

        """
        if domain is self.domain:
            return self
        if domain == self.domain:
            return self.copy()

        field = self.field().resize(*domain.shape)
        field.domain = domain
        name = f"{self.name}_resampled"
        meta = {k: v for k, v in self.metaparameters.items()
                if k not in ("name", "mode", "domain", "interpolator")}
        tx = type(self)(field, name=name, mode=self.mode,
                        interpolator=field.interpolator, **meta)
        return tx

    def fit(self, field):
        """
        Evaluates the target field on the transformation domain, and updates
        the transformation parameters in place.

        :param field: target field
        :type field: TField

        """
        params = self.field2params(field).reshape(field.tsize, -1)
        params = params[np.asarray(self.vectorder), :].ravel()
        self.parameters[...] = params

    def copy(self):
        field = self.field().copy()
        metaparameters = tu.rcopy(self.metaparameters)
        meta = {k: v for k, v in metaparameters.items()
                if k not in self.RESERVED_KWARGS}
        name = self.name
        bounds = (self.parameters.lower_bounds, self.parameters.upper_bounds)
        tx = type(self)(field, bounds=bounds, lock=self.parameters.locked,
                        name=name, invertible=self.invertible, mode=self.mode,
                        vectorder=self.vectorder, **meta)
        return tx

    @classmethod
    def _load(cls, dump):

        params = dump.get("parameters")
        meta = dump.get("metaparameters")

        # Create TField
        domain = meta.pop("domain")
        vectorder = meta.pop("vectorder")
        interpolator = meta.pop("interpolator")
        interpolator.data = params.parameters.reshape((-1, *domain.shape))
        name = meta.pop("name")
        invertible = meta.pop("invertible")
        mode = meta.pop("mode")

        field = TField(
            params.parameters.data, domain=domain, tshape=(len(vectorder),),
            order=TENSOR_MAJOR, dtype=params.parameters.dtype, name=None,
            storage=MEM, interpolator=interpolator)

        # Create transformation object
        bounds = params.get_bounds()
        lock = params.locked
        meta = {k: v for k, v in meta.items() if k not in cls.RESERVED_KWARGS}
        obj = cls(field, bounds=tuple(bounds.T), lock=lock, name=name,
                  invertible=invertible, mode=mode, vectorder=vectorder, **meta)

        return obj

    def vectors(self, fullstd=False):
        """
        Returns the relative or absolute displacement vectors depending on the
        mode of TxDisplacementField. If fullstd=True (False by default), the
        vectors are returned in the same number of dimensions as the input
        field and in the standard xyz order of the coordinates.

        """
        # Create vector array
        vects = self.parameters.parameters.reshape((self.vectdim, -1)).T
        # Pad
        if fullstd and (self.vectorder != tuple(range(self.domain.ndim))):
            fullvects = np.zeros((self.domain.numel, self.domain.ndim),
                                 dtype=self.parameters.dtype)
            for i, ax in enumerate(self.vectorder):
                fullvects[:, ax] = vects[:, i]
            else:
                vects = fullvects
        return vects

    def map(self, coords, signature=None):
        """
        Maps input coordinates by applying the local displacement values.

        """
        def apply_vector_order(self, coords, vectors):
            # Take into account the vector order specification
            if vectors.ndim == 1:
                vectors = vectors.reshape((-1, 1))
            if self.vectorder != tuple(range(coords.shape[-1])):
                coords = coords.copy()
                for i, ax in enumerate(self.vectorder):
                    coords[:, ax] += vectors[:, i]
                return coords
            # Map faster if the vector order is standard
            else:
                assert vectors.shape == coords.shape
                return coords + vectors

        # Avoid the overhead of reverse mapping and interpolation if there is a
        # one-to-one correspondence between the displacement vectors and the
        # input coordinates. In most neuroimaging libraries non-linear
        # transformations are defined this way.
        if hasattr(signature, "__iter__") and len(signature) >= 1:
            strsig = signature[0](0) if callable(signature[0]) else signature[0]
        else:
            strsig = None
        if strsig == self.domain.signature()(0):
            coords = super(TxNonLinear, self).map(coords, signature)
            if coords.shape[0] == self.domain.numel:
                # Mapping is a simple addition if the mode is NL_ABS
                if self.mode == NL_ABS:
                    vectors = self.vectors()
                    return apply_vector_order(self, coords, vectors)
                # Displacement vectors must be first mapped to the world frame
                # if the mode is NL_REL
                else:
                    vc = self.domain.get_voxel_coordinates()
                    v = self.domain.map_voxel_vectors(
                        self.vectors(), vc, rule=RULE_SSR)
                    return apply_vector_order(self, coords, v)

        # If one-to-one correspondence is not guaranteed,
        # obtain vectors by interpolation
        coords = super(TxNonLinear, self).map(coords, signature)
        vc = self.domain.map_physical_coordinates(coords)
        field = self.field()
        vectors = field.interpolate(vc)
        # vectors = self.field().interpolate(vc)
        vectors = np.moveaxis(vectors, -1, 0)
        if self.mode == NL_ABS:
            return apply_vector_order(self, coords, vectors)
        # Displacement vectors must be first mapped to the world frame
        # if the mode is NL_REL
        else:
            vectors = self.domain.map_voxel_vectors(vectors, vc, rule=RULE_SSR)
            return apply_vector_order(self, coords, vectors)

    def inverse(self, domain=None):
        """
        Returns a DisplacementField that undoes the mapping of the current
        DisplacementField.

        """
        if domain is None:
            domain = self.domain
        field = self.std().absolute().field(domain)
        vectorder = tuple(range(field.tsize))

        # Create the prototype of the inverse transformation
        invwarp = invert(
            targetlocations=domain.get_physical_coordinates(), field=field.data,
            gridlocations=field.domain.get_physical_coordinates(),
            vectorder=vectorder, output=None, fill_holes=True, maxiter=1000
        )
        invwarp[~np.isfinite(invwarp)] = 0
        # Note: if the transformation domain is an oblique 2D plane embedded in
        # 3D space, the inverse transformation will be 3D, even if it can be
        # reduced to 2D. In order to allow this, we set the vector order to
        # None, and let TxDisplacementField figure out the right number of
        # dimensions based on the result of the inversion.
        invtx = TxDisplacementField(
            invwarp, domain=domain, mode=NL_ABS, vectorder=None)

        # Restore absolute/relative vector handling property in invtx
        if self.mode == NL_ABS:
            return invtx
        else:
            return invtx.relative()

    def matrix(self, coords=None):
        """
        Returns the local affine approximation to the transformation at the
        specified physical locations.

        """
        # Obtain absolute deformation field on an equivalent dense domain
        if self.mode == NL_ABS:
            field = self.field()
        else:
            field = self.rel2abs(self.field())

        # Locations where the local transformation is defined exactly
        gridlocations = field.domain.get_physical_coordinates()
        # spacedim = gridlocations.shape[-1]

        # Obtain the locations within the field
        # where the matrix should be evaluated.
        if coords is None:
            coords = gridlocations
        else:
            coords = np.atleast_2d(coords)

        # Choose local affine routine (C/Cython functions) depending on the
        # dimensions of the physical space, in which the current
        # transformation operates. Note that this may differ from vectdim: the
        # transformation may only operate on x and y coordinates of 3D space.
        affines = local_affine(
            field=field.data, newlocations=coords, gridlocations=gridlocations,
            output=None, vectorder=self.vectorder, maxiter=1000)

        # Fix rank deficiency: if the transformation does not update all
        # coordinates of the input points, the estimated local affine
        # transformation will be rank-deficient, with a zero determinant. This
        # is fixed here by replacing the 0s on the diagonal with 1s.
        # if self.vectdim != spacedim:
        #     for ax in range(spacedim):
        #         if ax not in self.vectorder:
        #             affines[:, ax, ax] = 1
        # Instead of the above, fix rank deficiency empirically, as it may also
        # arise when the displacement along one dimension is infinitesimally
        # small, albeit the dimension per se is defined.
        def fix_rank_deficiency(a, columns, cond=1e-5):
            for col in np.asarray(columns, dtype=int):
                diagonals = a[:, col]
                a[:, col] = np.where(diagonals < cond, 1, diagonals)
            return a

        if affines.shape[-1] == 12:
            affines = fix_rank_deficiency(affines, [0, 4, 8])
            # Note that the C-function uses LAPACK routines and exports the
            # matrix elements in Fortran order
            affines = affines.reshape((-1, 3, 4), order="F")

        elif affines.shape[-1] == 6:
            affines = fix_rank_deficiency(affines, [0, 3])
            affines = affines.reshape((-1, 2, 3), order="F")

        else:
            raise AssertionError(
                f"Unexpected number of affine components: {affines.shape[-1]}")

        # The resultant affines are independent of the transformation's vector
        # order, and they transform xy(z) coordinates. This is in keeping with
        # the fact that DisplacementFields with a non-standard vector order
        # also transform input coordinates in the xy(z) layout. The vector
        # order only refers to the storage order of the vectors within the
        # transformation.

        return affines

    def propagator(self, coords=None):
        return self.matrix(coords=coords)

    def jacobian(self, coords=None):
        """
        Calculates the parameter Jacobian of the transformation at the given
        physical locations. The parameter Jacobian is the total derivative
        of the local displacement vector with respect to the parameters of
        the transformation.

        The displacement vector whose derivative is calculated has the same
        number of dimensions as the input field (irrespective of vectdim), and
        it is represented in the xy(z) coordinate order (irrespective of
        vectorder).

        The parameter Jacobian is returned as a sparse (row) matrix. When
        right-multiplied with the vector of parameter increments ($\Delta p$),
        it yields the incremental displacements at the input points. The order
        of the parameters is u[...] -> v[...] -> w[...], where u, v, and w
        represent the displacement along each axis at all points of the domain
        of the transformation.

        The parameter Jacobian is calculated from the local affine estimation
        of the deformation field, which assumes that the transformation is
        diffeomorphic.

        :params coords:
            Physical coordinates at which the parameter Jacobian should be
            evaluated.
        :rtype: np.ndarray

        :returns:
            (m_coordinates * n_points, p_parameters) stacked sparse matrix
        :rtype: sp.csr_matrix

        """
        if coords is None:
            coords = self.domain.get_physical_coordinates()

        # Obtain absolute deformation field
        if self.mode == NL_ABS:
            field = self.field()
        else:
            field = self.rel2abs(self.field())

        # 2D field
        if self.domain.ndim == 2:
            chain = self.domain.offset + self.domain.chain
            reduced_jacobians, vertex_indices = \
                jacobian2d(field.data, coords, pretx=chain.map,
                           vectorder=self.vectorder, maxiter=1000)
            invalid = np.any(vertex_indices < 0, axis=1)
            vertex_indices[invalid, ...] = 0  # assign invalid to node 0
            u = reduced_jacobians[:, 0, :]
            u[invalid] = 0
            v = reduced_jacobians[:, 1, :]
            v[invalid] = 0
            n_points = coords.shape[0]
            n_parameters = 2 * self.domain.numel
            combined = np.concatenate((u, v), axis=0).ravel()
            col = np.tile(np.concatenate(
                (vertex_indices, self.domain.numel + vertex_indices), axis=1),
                (2, 1)).ravel()
            row = np.repeat(
                np.arange(2 * n_points).reshape(-1, 1), 6, axis=1).ravel()
            pjac = sp.csr_matrix(
                (combined, (row, col)), shape=(n_points * 2, n_parameters))

        # 3D field
        elif self.domain.ndim == 3:
            chain = self.domain.offset + self.domain.chain
            reduced_jacobians, vertex_indices = \
                jacobian3d(field.data, coords, pretx=chain.map,
                           vectorder=self.vectorder, maxiter=1000)
            invalid = np.any(vertex_indices < 0, axis=1)
            vertex_indices[invalid, ...] = 0  # assign all invalid to node 0
            u = reduced_jacobians[: 0, :]
            u[invalid] = 0
            v = reduced_jacobians[: 1, :]
            v[invalid] = 0
            w = reduced_jacobians[: 2, :]
            w[invalid] = 0
            n_points = coords.shape[0]
            n_parameters = 3 * self.domain.numel
            combined = np.concatenate((u, v, w), axis=0).ravel()
            del u; del v; del w
            col = np.tile(np.concatenate(
                (vertex_indices, self.domain.numel + vertex_indices,
                 2 * self.domain.numel + vertex_indices), axis=1),
                (3, 1)).ravel()
            row = np.repeat(
                np.arange(3 * n_points).reshape(-1, 1), 12, axis=1).ravel()
            pjac = sp.csr_matrix(
                (combined, (row, col)), shape=(n_points * 3, n_parameters))

        else:
            raise NotImplementedError(
                "The parameter Jacobian can be calculated only for 2D and "
                "3D displacement fields.")

        return pjac

    def derivative(self, coords=None):
        """
        Returns the spatial derivative of the displacement field at the given
        coordinates.

        """
        # # TODO: Implement this in C to avoid whole field derivation
        # field = self.std().absolute().field()
        # np.stack(np.gradient(field.data, axis=field.vaxes), axis=0)
        return NotImplementedError()


if __name__ == "__main__":  # pragma: no cover
    print("""This module is not intended for execution.""")

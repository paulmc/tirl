#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np
from numbers import Real
from math import sqrt, pi, radians, degrees


# TIRL IMPORTS

from tirl.beta import beta_function
import tirl.utils as tu
from tirl import settings as ts

from tirl.point import Point
from tirl.transformations.linear import TxLinear


# DEFINITIONS

# Absolute tolerance that is used to determine the inverse consistency of a
# rotation matrix candidate. If the matrix fails the inverse consistency test,
# it is not accepted as a valid rotation matrix.
EPSILON = 1e-4


# IMPLEMENTATION

class TxRotation(TxLinear):
    """
    TxRotation (child of TxLinear) - generic base class for rotations.

    This class is very similar to TxLinear, but the transformation matrix must
    be a rotation matrix (orthogonal with det=1), and the shape parameter is
    not needed for initialisation by parameters.

    """

    RESERVED_KWARGS = ("parameters", "shape", "bounds", "lock", "name",
                       "invertible", "metaparameters")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~ INITIALISATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __init__(self, *parameters, shape=None, centre=None, bounds=None,
                 lock=None, name=None, invertible=True, **metaparameters):
        """
        Initialisation of TxRotation.

        :param parameters:
            Input parameters can be specified by one of the following methods:

            1. (n, n) rotation matrix: must be a real-valued orthogonal matrix
               with its determinant equal to one. Matrices that do not fulfill
               these criteria within a given tolerance limit (EPSILON) will
               trigger a ValueError.
            2. (n*n,) iterable: flattened rotation matrix elements (C-order).
               The same criteria apply as for the matrix specification.

        :type parameters: Union[np.ndarray, tuple, list]
        :param shape:
            Shape of the rotation matrix. The shape must be given if the
            object is initialised by parameters, not a matrix. If a matrix is
            given, the shape will be taken from that, overriding any input for
            shape.
        :type shape: Union[tuple, NoneType]
        :param centre:
            The centre of the rotation, which remains stationary under the
            rotation. The centre may be specified either as an array of
            coordinates or as a Point object. With the latter, rotations can be
            defined relative to a moving Domain. Defaults to the origin.
        :type centre: Point or np.ndarray or None
        :param bounds:
            Parameter bounds can be defined in one of the following formats:
            1. ((lb0, ub0),) - sequence of lower and upper bounds for each
               parameter. The order must correspond to the order of the
               transformation parameters.
            2. {param_no: (lb, ub)} - dictionary of lower and upper bound
               pairs, where the keys represent parameter indices (starting
               from 0).
            3. (lb_arr, ub_arr) - flat iterable with lower and upper bounds for
               all parameters.
            4. "auto" - the get_default_bounds() method is called to create
               default lower and upper bounds.
            5. None - parameter bounds are not set.
            6. (lb, ub) - global scalar values for lower and upper bounds
        :type bounds: Union[tuple, list, dict, str]
        :param lock:
            Boolean mask or index sequence that defines which parameters
            are locked, i.e. should not be updated by default. If None, all
            parameters are updated by default when calling the update() method.
        :type lock: Union[None, tuple, int, np.ndarray]
        :param name:
            Name of the Transformation object. If None (default), a default
            name of the form "tx_objectID" will be generated.
        :type name: str
        :param invertible:
            Indicates whether the current transformation is invertible (True) or
            not (False, default). Transformations marked 'invertible' must
            implement the inverse() method.
        :type invertible: bool
        :param metaparameters:
            Metaparameters for TxRotation.
        :type metaparameters: Any

        """
        if parameters == ():
            raise ValueError("No parameters were specified.")

        # Interpret the parameter and shape input.
        parameters, shape = self._read_parameters_and_shape(
            parameters, shape=shape, **metaparameters)
        # If the rotation parameters were inferred from a rotation matrix, a
        # 'centre' attribute was added to the metaparameters, so collect this.
        centre = metaparameters.get("centre", centre)

        # Calculate shape from the number of parameters if it is not yet known
        # This is a convenience feature: if shape was not given, infer it from
        # the number of parameters, assuming that the parameters directly
        # correspond to the elements of the rotation matrix.
        if shape is None:
            n = sqrt(parameters.size)
            if n.is_integer():
                shape = (int(n), int(n))
            else:
                raise IndexError("Invalid number of rotation parameters.")

        # Verify that the parameters correspond to a rotation matrix
        mat = self.params2matrix(parameters, shape=shape, centre=centre,
                                 **metaparameters)
        r = min(mat.shape)
        if not np.allclose(mat[:r, :r].T @ mat[:r, :r], np.eye(r),
                           atol=EPSILON):
            raise ValueError("Input matrix does not satisfy the criteria "
                             "for a rotation matrix.")

        # Call superclass initialisation
        super(TxRotation, self).__init__(
            *parameters, shape=shape, bounds=bounds, lock=lock, name=name,
            invertible=invertible, **metaparameters)

        # Class-specific metaparameters
        self.centre = centre

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATTRIBUTES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @property
    def centre(self):
        return self.metaparameters.get("centre")

    @centre.setter
    def centre(self, c):
        try:
            if c is None:
                self.metaparameters.update(centre=None)
            else:
                self.metaparameters.update(centre=Point(c))
        except:
            raise TypeError(f"Expected array of coordinates or Point for "
                            f"centre, got {c} instead.")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PRIVATE METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PUBLIC METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    # TODO: Deprecate Transformation.jacobian()
    # def jacobian(self, coords):
    #     if self.homogenise:
    #         coords = super(TxLinear, self).map(coords)
    #         return super().jacobian(coords[:, :-1])
    #     else:
    #         return super().jacobian(coords)


class TxRotation2D(TxRotation):
    """
    TxRotation2D (child of TxRotation) - a simplified transformation object
    for rotations about a single axis (2D).

    The rotation angle is always converted to radians and mapped to the
    [-pi, pi) range before it is set as parameter. The default parameters
    bounds are also set to [-pi, pi].

    """

    RESERVED_KWARGS = ("angle", "mode", "bounds", "lock", "name", "invertible",
                       "metaparameters")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~ INITIALISATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __init__(self, angle, mode="deg", centre=None, bounds=(-pi, pi),
                 lock=False, name=None, invertible=True,  **metaparameters):
        """
        Initialisation of TxRotation2D.

        :param angle:
            Rotation angle. Either in degrees (default) or radians, depending
            on the mode setting. The rotation angle is mapped to [-pi, pi)
            before it is stored as a parameter. Alternatively a 2D rotation
            matrix may be given, from which the rotation angle will be
            retrieved.
        :type angle: Union[int, float]
        :param mode:
            Unit of the rotation angle. Either "deg" (default) or "rad". This
            only applies for the input and the value that is returned by the
            'angle' property. Irrespective of this setting the rotation angle
            is always converted to radians before it is stored as a parameter
            of the transformation object.
        :type mode: str
        :param centre:
            The centre of the rotation, which remains stationary under the
            rotation. The centre may be specified either as an array of
            coordinates or as a Point object. With the latter, rotations can be
            defined relative to a moving Domain. Defaults to the origin.
        :type centre: Point
        :param bounds:
            The bound for the rotation angle must be defined in radians.
            Parameter bounds can be defined in one of the following formats:
            1. None (default) - parameter bounds are not set.
            2. "auto" - the get_default_bounds() method is called to create
               default lower and upper bounds.
            3. (lb, ub) - global scalar values for lower and upper bounds
        :type bounds: Union[tuple, list, dict, str]
        :param lock:
            If True, the only parameter of the transformation object is
            locked, indicating that it should not be updated by default.
            Otherwise (False, default) the parameter is free, and can be
            updated by optimisers.
        :type lock: bool
        :param name:
            Name of the Transformation object. If None (default), a default
            name of the form "tx_objectID" will be generated.
        :type name: str
        :param invertible:
            Indicates whether the current transformation is invertible (True) or
            not (False, default). Transformations marked 'invertible' must
            implement the inverse() method.
        :type invertible: bool
        :param metaparameters:
            Metaparameters for Transformation
        :type metaparameters: Any

        """
        # Detect invalid mode setting
        if mode not in ("rad", "deg"):
            raise ValueError("Invalid mode setting.")

        # Convert the angle to radians
        angle = radians(angle) if mode == "deg" else angle
        # Map the angle to [-pi, pi) radians -> this can cause bound violation
        # angle = (angle + np.pi) % (2 * np.pi) - np.pi

        # Change the lock parameter to a "sequence of indices"
        lock = None if not lock else (0,)

        # Set parameters and shape
        shape = (2, 2) if centre is None else (2, 3)
        parameters, shape = self._read_parameters_and_shape(
            (angle,), shape=shape, **metaparameters)

        # Call superclass initialisation
        super(TxRotation2D, self).__init__(
            *parameters, centre=centre, shape=shape, bounds=bounds, lock=lock,
            name=name, invertible=invertible, mode=mode, **metaparameters)

        # Re-set class-specific metaparameters
        self.mode = mode

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATTRIBUTES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @property
    def angle(self):
        phi = self.parameters[0]  # radians in [-pi, pi)
        if self.mode == "rad":
            return phi
        elif self.mode == "deg":
            return degrees(phi)
        else:
            raise ValueError("Unsupported mode. Choose from 'rad' or 'deg'.")

    @angle.setter
    def angle(self, phi):
        if self.mode == "rad":
            self.parameters[0] = phi
        elif self.mode == "deg":
            self.parameters[0] = radians(phi)
        else:
            raise ValueError("Unsupported mode. Choose from 'rad' or 'deg'.")

    @property
    def mode(self):
        return self.metaparameters.get("mode")

    @mode.setter
    def mode(self, m):
        if m not in ("rad", "deg"):
            raise ValueError("Unsupported mode. Choose from 'rad' or 'deg'.")
        else:
            self.metaparameters.update({"mode": m})

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ARITHMETICS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __add__(self, other):
        # Addition with a fellow 2D rotation object
        if isinstance(other, type(self)):
            angle = self.parameters[0] + other.parameters[0]  # radians
            obj = type(self)(angle, mode="rad")
            obj.mode = self.mode
            return obj
        # Addition with a scalar: interpret scalar as a 2D rotation
        elif isinstance(other, Real):
            angle = self.parameters[0] + self._rad(other)
            obj = type(self)(angle, mode="rad")
            obj.mode = self.mode
            return obj
        # Otherwise defer to superclass
        else:
            return super(TxLinear, self).__add__(other)

    # def __radd__(self, other):
    #     # Adding 2D rotations is always commutative.
    #     if isinstance(other, (TxRotation2D, Real)):
    #         return self.__add__(other)
    #     # Defer to superclass
    #     else:
    #         super(TxRotation2D, self).__radd__(other)

    def __sub__(self, other):
        # Subtracting a fellow 2D rotation object
        if isinstance(other, type(self)):
            angle = self.parameters[0] - other.parameters[0]  # radians
            obj = type(self)(angle, mode="rad")
            obj.mode = self.mode
            return obj
        # Addition with a scalar: interpret scalar as a 2D rotation
        elif isinstance(other, Real):
            angle = self.parameters[0] - self._rad(other)
            obj = type(self)(angle, mode="rad")
            obj.mode = self.mode
            return obj
        # Otherwise defer to superclass
        else:
            return super(TxLinear, self).__sub__(other)

    def __rsub__(self, other):
        # If the left operand is a scalar, interpret it as another 2D rotation
        if isinstance(other, Real):
            return self.__neg__().__add__(other)
        # Otherwise defer to superclass. Note: no need to check if left operand
        # is a 2D rotation, as in this case, the __sub__() method of that
        # instance would take precedence over this function.
        else:
            return super(TxRotation2D, self).__rsub__(other)

    def __mul__(self, other):
        if isinstance(other, Real):
            angle = self.parameters[0] * other  # radians
            obj = type(self)(angle, mode="rad")
            obj.mode = self.mode
            return obj
        else:
            raise NotImplementedError("Multiplication of 2D rotations is only "
                                      "defined for real numbers.")

    def __rmul__(self, other):
        # Multiplication with a real number is commutative, otherwise rmul is
        # also undefined just like mul.
        return self.__mul__(other)

    def __truediv__(self, other):
        if isinstance(other, Real):
            # Division by zero would raise a ZeroDivisionError here.
            angle = self.parameters[0] / other  # radians
            obj = type(self)(angle, mode="rad")
            obj.mode = self.mode
            return obj
        else:
            raise NotImplementedError("Floating-point division of 2D rotations "
                                      "is only defined for real numbers.")

    def __rtruediv__(self, other):
        if isinstance(other, Real):
            # Division by zero would raise a ZeroDivisionError here.
            angle = other / self.parameters[0]  # radians
            obj = type(self)(angle, mode="rad")
            obj.mode = self.mode
            return obj
        else:
            raise NotImplementedError("Floating-point division of 2D rotations "
                                      "is only defined for real numbers.")

    def __floordiv__(self, other):
        if isinstance(other, Real):
            # Division by zero would raise a ZeroDivisionError here.
            angle = self.parameters[0] // other  # radians
            obj = type(self)(angle, mode="rad")
            obj.mode = self.mode
            return obj
        else:
            raise NotImplementedError("Floor division of 2D rotations is only "
                                      "defined for real numbers.")

    def __rfloordiv__(self, other):
        if isinstance(other, Real):
            # Division by zero would raise a ZeroDivisionError here.
            angle = other // self.parameters[0]  # radians
            obj = type(self)(angle, mode="rad")
            obj.mode = self.mode
            return obj
        else:
            raise NotImplementedError("Floor division of 2D rotations is only "
                                      "defined for real numbers.")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PRIVATE METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def _rad(self, angle):
        """
        Interprets angle input based on the 'mode' setting of the
        transformation object, and returns the angle in radians.

        The purpose of having this private method is syntactic clarity: being
        able to avoid an explicit checking of self.mode.

        :param angle: angle to be interpreted
        :type angle: Union[int, float]

        :returns: angle (interpreted in deg/rad based on mode) in radians
        :rtype: float

        """
        if self.mode == "rad":
            return angle
        elif self.mode == "deg":
            return radians(angle)
        else:
            raise ValueError("Unsupported mode. Choose from 'rad' or 'deg'.")

    @classmethod
    def _load(cls, dump):
        """
        Creates TxRotation2D (or subclass) instance from object dump.

        :param dump: object dump
        :type dump: dict

        :returns: TxRotation2D instance
        :rtype: TxRotation2D

        """
        meta = dump.get("metaparameters")
        name = meta.pop("name")
        shape = meta.pop("shape")
        invertible = meta.pop("invertible")
        mode = meta.pop("mode")
        centre = meta.pop("centre")
        parameters = dump.get("parameters")
        angle = degrees(parameters[0]) if mode == "deg" else parameters[0]
        lb = parameters.lower_bounds
        ub = parameters.upper_bounds
        lock = bool(parameters.locked)
        obj = cls(angle, mode=mode, centre=centre, bounds=(lb, ub), lock=lock,
                  name=name, invertible=invertible, **meta)
        return obj

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PUBLIC METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @staticmethod
    def matrix2params(mat, **kwargs):
        """
        Calculates rotation angle from (2, 2) rotation matrix. Alternatively,
        if the rotation matrix is (2, 3), the centre of the rotation is set
        accordingly in the metaparameters.

        :param mat: (2, 2) rotation matrix or (2, 3) excentric rotation matrix
        :type mat: np.ndarray
        :param kwargs:
            Keyword arguments required to retrieve parameters from
            transformation matrix.
        :type kwargs: Any

        :returns: rotation angle (single parameter as (1,) array)
        :rtype: np.ndarray

        """
        mat = np.asarray(mat, dtype=ts.DEFAULT_PARAMETER_DTYPE)
        phi = np.arctan2(mat[1, 0], mat[0, 0])

        # Detect conflicting input
        centre = kwargs.get("centre", False)
        if mat.shape[-1] == 2 and centre:
            raise AssertionError("Conflicting input: the rotation matrix is "
                                 "about the origin, but a centre point was "
                                 "also specified.")

        # If the rotation is not about the origin, then update the centre
        # metaparameter.
        if mat.shape[-1] == 3:
            rot = mat[:2, :2]
            trans = mat[:, -1]
            centre = np.linalg.inv(np.eye(2) - rot) @ trans.reshape(-1, 1)
            kwargs.update(centre=Point(centre.ravel()))
            assert np.allclose(mat @ centre, centre)

        return np.asarray([phi], dtype=mat.dtype)

    @staticmethod
    def params2matrix(parameters, **kwargs):
        """
        Creates (2, 2) rotation matrix from rotation angle (single parameter).
        Alternatively, if the centre is not the origin, the resultant matrix
        is (2, 3).

        :param parameters: rotation parameters
        :type parameters: np.ndarray
        :param kwargs:
            Keyword arguments required to retrieve matrix from parameters.
        :type kwargs: Any

        :returns: (2, 2) rotation matrix
        :rtype: np.ndarray

        """
        dtype = ts.DEFAULT_PARAMETER_DTYPE
        parameters = np.asarray(parameters, dtype=dtype)
        shape = kwargs.get("shape")
        mat = np.eye(*shape, dtype=dtype)
        phi = parameters[0]
        mat[:2, :2] = np.asarray([[np.cos(phi), -np.sin(phi)],
                                  [np.sin(phi), np.cos(phi)]], dtype=dtype)

        # Taking the rotation centre into account
        centre = kwargs.get("centre", False)
        if centre:
            centre = Point(centre)
            trans = np.eye(3)
            trans[:-1, -1] = -centre.y
            homo = np.eye(3)
            homo[:-1, :] = mat
            mat = np.linalg.inv(trans) @ homo @ trans
            mat = mat[:2, :]

        return mat

    def inverse(self):
        """
        Creates inverse transformation object by negating the rotation angle.

        :returns: inverse transformation object
        :rtype: TxRotation2D

        """
        meta = tu.rcopy(self.metaparameters)
        name = meta.pop("name")
        name = f"{name}_inv"
        shape = meta.pop("shape")
        invertible = meta.pop("invertible")
        centre = meta.pop("centre")
        mode = meta.pop("mode")
        parameters = self.parameters.copy()
        angle = degrees(parameters[0]) if mode == "deg" else parameters[0]
        # Use same mode and invertibility, default bounds and no lock.
        obj = type(self)(-angle, centre=centre, mode=mode, bounds=(-pi, pi),
                         lock=None, name=name, invertible=invertible, **meta)
        return obj

    # TODO: Deprecate Transformation.jacobian()
    # def jacobian(self, coords):
    #     coords = super(TxLinear, self).map(coords)
    #     phi = self.parameters[0]  # in radians
    #     dR = np.asarray([[-np.sin(phi), -np.cos(phi)],
    #                      [np.cos(phi), -np.sin(phi)]])
    #     d = 2 + int(self.homogenise)
    #     dRh = np.zeros((d, d))
    #     dRh[:2, :2] = dR
    #     if dRh.shape[-1] == coords.shape[-1] + 1:
    #         dRh = dRh[:-1, :-1]
    #     # Formalise the parameter axis in the Jacobian
    #     # jac = np.einsum("ijk,hk", dRh[np.newaxis, ...], coords)
    #     jac = np.matmul(dRh, coords[..., np.newaxis]).transpose(0, 2, 1)
    #     return jac


class TxRotation3D(TxRotation):
    """
    TxRotation3D (child of TxRotation) - simplified base class for 3D rotations.

    There are several different ways to represent 3D rotations, such as by
    axis and angle, Euler angles, Tait-Bryan angles, rotation matrix, and
    quaternions. This class implements methods for the calculation of these
    parametrisations from a common reference, the rotation matrix. As a result,
    subclasses that implement a particular parametrisation will also inherit
    all other representations, which allows them to calculate equivalent
    parameters in any given parameter space.

    This base class uses the rotation matrix to represent 3D rotations. This
    parametrisation is directly inherited from TxRotation, with the additional
    constraint that the rotation matrix must be (3, 3) large.

    """

    AXIS_ORDER = {"x": 0, "y": 1, "z": 2}
    EULER_AXES = {"xyx", "xzx", "yxy", "yzy", "zxz", "zyz"}
    TAIT_BRYAN_AXES = {"xyz", "xzy", "yxz", "yzx", "zxy", "zyx"}

    RESERVED_KWARGS = ("parameters", "bounds", "lock", "name", "invertible",
                       "metaparameters")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~ INITIALISATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __init__(self, *parameters, centre=None, bounds=None, lock=None,
                 name=None, invertible=True, **metaparameters):
        """
        Initialisation of TxRotation3D.

        :param parameters:
            Input parameters can be specified by one of the following methods:

            1. (3, 3) rotation matrix: must be a real-valued orthogonal matrix
               with its determinant equal to one. Matrices that do not fulfill
               these criteria within a given tolerance limit (EPSILON) will
               trigger a ValueError.
            2. (9,) iterable: flattened rotation matrix elements (C-order).
               The same criteria apply as for the matrix specification.

        :type parameters: Union[np.ndarray, tuple, list]
        :param centre:
            The centre of the rotation, which remains stationary under the
            rotation. The centre may be specified either as an array of
            coordinates or as a Point object. With the latter, rotations can be
            defined relative to a moving Domain. Defaults to the origin.
        :type centre: Point
        :param bounds:
            Parameter bounds can be defined in one of the following formats:
            1. ((lb0, ub0),) - sequence of lower and upper bounds for each
               parameter. The order must correspond to the order of the
               transformation parameters.
            2. {param_no: (lb, ub)} - dictionary of lower and upper bound
               pairs, where the keys represent parameter indices (starting
               from 0).
            3. (lb_arr, ub_arr) - flat iterable with lower and upper bounds for
               all parameters.
            4. "auto" - the get_default_bounds() method is called to create
               default lower and upper bounds.
            5. None - parameter bounds are not set.
            6. (lb, ub) - global scalar values for lower and upper bounds
        :type bounds: Union[tuple, list, dict, str]
        :param lock:
            Boolean mask or index sequence that defines which parameters
            are locked, i.e. should not be updated by default. If None, all
            parameters are updated by default when calling the update() method.
        :type lock: Union[None, tuple, int, np.ndarray]
        :param name:
            Name of the Transformation object. If None (default), a default
            name of the form "tx_objectID" will be generated.
        :type name: str
        :param invertible:
            Indicates whether the current transformation is invertible (True) or
            not (False, default). Transformations marked 'invertible' must
            implement the inverse() method.
        :type invertible: bool
        :param metaparameters:
            Metaparameters for TxRotation.
        :type metaparameters: Any

        """
        # Call superclass initialisation
        shape = (3, 3) if centre is None else (3, 4)
        metaparameters.pop("shape", None)
        super(TxRotation3D, self).__init__(
            *parameters, shape=shape, centre=centre, bounds=bounds, lock=lock,
            name=name, invertible=invertible, **metaparameters)

        # Set class-specific metaparameters
        # Nothing to do.

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATTRIBUTES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PRIVATE METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @staticmethod
    def _get_axes(mode):

        mode = str(mode).lower()
        return tuple(TxRotation3D.AXIS_ORDER[ax] for ax in mode)

    @staticmethod
    def _decompose_2_axis(mat, mode="zxz"):
        """
        Decomposes 3x3 rotation matrix into proper intrinsic Euler angles.
        Proper intrinsic Euler angles are identical to extrinsic angles in
        reverse order of the rotation axes.

        :param mat: 3x3 rotation matrix (must be orthogonal)
        :type mat: np.ndarray
        :param mode: axis order; one of the following:
                     ["xyx", "xzx", "yxy", "yzy", "zxz", "zyz"]
                     default: "zxz"
        :type mode: str

        :returns: Proper intrinsic Euler angles in the given axis order
        :rtype: tuple

        Source: Thomas Herter, Klaus Lott, Algorithms for decomposing 3-D
        orthogonal matrices into primitive rotations, Computers & Graphics,
        Volume 17, Issue 5, 1993, Pages 517-527, ISSN 0097-8493,
        doi: 10.1016/0097-8493(93)90003-R

        """
        a0, a1 = TxRotation3D._get_axes(mode[:2])
        pos = (a1 == (a0 + 1) % 3)
        if pos:
            c = (a1 + 1) % 3
        else:
            c = (a1 - 1) % 3

        phi2 = float(np.arccos(mat[a0, a0]))
        eps = np.finfo(np.float32).eps
        assert (phi2 > -eps) and (phi2 - np.pi < eps)

        if np.abs(mat[a0, a0]) < 1 - np.finfo(mat.dtype).eps:
            cos_phi = mat[a0, c]
            cos_phi = -cos_phi if not pos else cos_phi  # negated
            phi3 = float(np.arctan2(mat[a0, a1], cos_phi))

            sin_phi = mat[a1, a0]
            cos_phi = mat[c, a0]
            cos_phi = -cos_phi if pos else cos_phi

        else:
            phi3 = 0
            cos_phi = mat[a1, a1]
            sin_phi = mat[c, a1]
            sin_phi = -sin_phi if not pos else sin_phi  # negated

        phi1 = float(np.arctan2(sin_phi, cos_phi))

        return phi1, phi2, phi3

    # @staticmethod
    # def _decompose_2_axis(mat, mode="zxz"):
    #     """
    #     Decomposes 3x3 rotation matrix into proper intrinsic Euler angles.
    #     Proper intrinsic Euler angles are identical to extrinsic angles in
    #     reverse order of the rotation axes.
    #
    #     :param mat: 3x3 rotation matrix (must be orthogonal)
    #     :type mat: np.ndarray
    #     :param mode: axis order; one of the following:
    #                  ["xyx", "xzx", "yxy", "yzy", "zxz", "zyz"]
    #                  default: "zxz"
    #     :type mode: str
    #
    #     :returns: Proper intrinsic Euler angles in the given axis order
    #     :rtype: tuple
    #
    #     Source: Thomas Herter, Klaus Lott, Algorithms for decomposing 3-D
    #     orthogonal matrices into primitive rotations, Computers & Graphics,
    #     Volume 17, Issue 5, 1993, Pages 517-527, ISSN 0097-8493,
    #     doi: 10.1016/0097-8493(93)90003-R
    #
    #     """
    #     a0, a1 = TxRotation3D._get_axes(mode[:2])
    #     pos = (a1 == (a0 + 1) % 3)
    #     if pos:
    #         c = (a1 + 1) % 3
    #     else:
    #         c = (a1 - 1) % 3
    #
    #     phi2 = float(np.arccos(mat[a0, a0]))
    #     assert (phi2 >= 0) and (phi2 <= np.pi)
    #
    #     if np.abs(mat[a0, a0]) < 1 - np.finfo(mat.dtype).eps:
    #         cos_phi = mat[a0, c]
    #         cos_phi = -cos_phi if not pos else cos_phi  # negated
    #         phi3 = float(np.arctan2(mat[a0, a1], cos_phi))
    #
    #         sin_phi = mat[a1, a0]
    #         cos_phi = mat[c, a0]
    #         cos_phi = -cos_phi if pos else cos_phi  # anti-negated
    #
    #     else:
    #         phi3 = 0
    #         cos_phi = mat[a1, a1]
    #         sin_phi = mat[c, a1]
    #         sin_phi = -sin_phi if not pos else sin_phi  # negated
    #
    #     phi1 = float(np.arctan2(sin_phi, cos_phi))
    #
    #     return phi1, phi2, phi3


    @staticmethod
    def _decompose_3_axis(mat, mode="zyx"):
        """
        Decomposes 3x3 rotation matrix into intrinsic Tait-Bryan angles.
        Intrinsic Tait-Bryan angles are identical to extrinsic angles in
        reverse order of the rotation axes.

        :param mat: 3x3 rotation matrix (must be orthogonal)
        :type mat: np.ndarray
        :param mode: axis order; one of the following:
                     ["xyz", "xzy", "yxz", "yzx", "zxy", "zyx"]
                     default: "zyx" (yaw - pitch - roll)
        :type mode: str

        :returns: Intrinsic Tait-Bryan angles in the given axis order
        :rtype: tuple

        Source: Thomas Herter, Klaus Lott, Algorithms for decomposing 3-D
        orthogonal matrices into primitive rotations, Computers & Graphics,
        Volume 17, Issue 5, 1993, Pages 517-527, ISSN 0097-8493,
        doi: 10.1016/0097-8493(93)90003-R

        """
        a0, a1, a2 = TxRotation3D._get_axes(mode)
        pos = (a1 == (a0 + 1) % 3)

        phi2 = float(np.arcsin(mat[a0, a2]))
        phi2 = -phi2 if not pos else phi2
        eps = np.finfo(np.float32).eps
        assert (phi2 >= -np.pi / 2 - eps) and (phi2 <= np.pi / 2 + eps)

        if np.abs(mat[a0, a2] < 1 - np.finfo(mat.dtype).eps):
            s = mat[a0, a1]
            s = -s if pos else s
            phi3 = float(np.arctan2(s, mat[a0, a0]))
            s = -mat[a1, a2]
            c = mat[a2, a2]
        else:
            phi3 = 0
            s = mat[a2, a1]
            c = mat[a1, a1]

        s = -s if not pos else s
        phi1 = float(np.arctan2(s, c))

        return phi1, phi2, phi3

    # @staticmethod
    # def _decompose_3_axis(mat, mode="zyx"):
    #     """
    #     Decomposes 3x3 rotation matrix into intrinsic Tait-Bryan angles.
    #     Intrinsic Tait-Bryan angles are identical to extrinsic angles in
    #     reverse order of the rotation axes.
    #
    #     :param mat: 3x3 rotation matrix (must be orthogonal)
    #     :type mat: np.ndarray
    #     :param mode: axis order; one of the following:
    #                  ["xyz", "xzy", "yxz", "yzx", "zxy", "zyx"]
    #                  default: "zyx" (yaw - pitch - roll)
    #     :type mode: str
    #
    #     :returns: Intrinsic Tait-Bryan angles in the given axis order
    #     :rtype: tuple
    #
    #     Source: Thomas Herter, Klaus Lott, Algorithms for decomposing 3-D
    #     orthogonal matrices into primitive rotations, Computers & Graphics,
    #     Volume 17, Issue 5, 1993, Pages 517-527, ISSN 0097-8493,
    #     doi: 10.1016/0097-8493(93)90003-R
    #
    #     """
    #     a0, a1, a2 = TxRotation3D._get_axes(mode)
    #     pos = (a1 == (a0 + 1) % 3)
    #
    #     phi2 = float(np.arcsin(mat[a0, a2]))
    #     phi2 = -phi2 if not pos else phi2   # negated
    #     assert (phi2 >= -np.pi / 2) and (phi2 <= np.pi / 2)
    #
    #     if np.abs(mat[a0, a2] < 1 - np.finfo(mat.dtype).eps):
    #         s = mat[a0, a1]
    #         s = -s if pos else s    # anti-negated
    #         phi3 = float(np.arctan2(s, mat[a0, a0]))
    #         s = -mat[a1, a2]
    #         c = mat[a2, a2]
    #     else:
    #         phi3 = 0
    #         s = mat[a2, a1]
    #         c = mat[a1, a1]
    #
    #     s = -s if not pos else s    # negated
    #     phi1 = float(np.arctan2(s, c))
    #
    #     return phi1, phi2, phi3


    @staticmethod
    def _axisangle(mat, mode="deg"):
        """
        Calculates the axis and the angle of the rotation from a rotation
        matrix, given the mode (deg or rad).

        :param mode: Rotation angle units. Either "deg" (default) or "rad".
        :type mode: str

        :returns: rotation angle, rotation axis (unit vector as a tuple)
        :rtype: tuple

        """
        vals, vects = np.linalg.eig(mat)
        n_unit_eigvals = np.count_nonzero(np.isclose(vals, 1))
        if n_unit_eigvals == 1:
            axis_index = int(np.argwhere(np.isclose(vals, 1)))
            axis = vects[:, axis_index].astype(ts.DEFAULT_PARAMETER_DTYPE)
            angle_index = min(filter(lambda x: x != axis_index, [0, 1, 2]))
            angle = float(np.angle(vals[angle_index]))
        elif n_unit_eigvals == 3:
            # For a null-rotation, the axis of rotation is ambiguous.
            angle = 0
            axis = np.zeros(3)
        else:
            # This should never happen
            assert False

        # Return axis and angle in the requested units
        if mode == "deg":
            angle = float(degrees(angle))
            return angle, tuple(axis.tolist())
        elif mode == "rad":
            return angle, tuple(axis.tolist())
        else:
            raise ValueError("Unsupported mode. Choose either 'rad' or 'deg'.")

    @staticmethod
    def _euler(mat, order="zyx", mode="deg"):
        """
        Calculates Euler (or Tait-Bryan) rotation angles from a rotation
        matrix given a specific order of the rotation axes.

        :param mat: rotation matrix
        :type mat: np.ndarray
        :param order:
            Three-letter string specification of the rotation axis order. Any
            permutation of x, y, z is valid. The default setting is "zyx".
        :type order: str
        :param mode:
            Rotation angle units. Either "deg" (default) or "rad".
        :type mode: str

        :returns: Euler or Tait-Bryan angles
        :rtype: tuple

        """
        if order in TxRotation3D.EULER_AXES:
            angles = TxRotation3D._decompose_2_axis(mat, order)
        elif order in TxRotation3D.TAIT_BRYAN_AXES:
            angles = TxRotation3D._decompose_3_axis(mat, order)
        else:
            raise ValueError("Unrecognised axis order.")

        # Adjust angles
        if mode == "deg":
            return tuple(np.rad2deg(angles).tolist())
        elif mode == "rad":
            return tuple(angles)
        else:
            raise ValueError("Unsupported mode. Choose either 'rad' or 'deg'.")

    @staticmethod
    @beta_function
    def _quaternion(mat):
        """
        Calculates the quaternion (4-tuple) that represents the same 3D
        rotation as the specified rotation matrix.

        :returns: quaternion
        :rtype: tuple

        """
        # Source: http://www.euclideanspace.com/maths/geometry/rotations/
        # conversions/matrixToQuaternion/
        m00, m01, m02, m10, m11, m12, m20, m21, m22 = mat.ravel(order="C")

        tr = np.trace(mat)
        if tr > 0:
            s = 2 * sqrt(tr + 1)
            qw = 0.25 * s
            qx = (m21 - m12) / s
            qy = (m02 - m20) / s
            qz = (m10 - m01) / s

        elif (m00 > m11) and (m00 > m22):
            s = 2 * sqrt(1.0 + m00 - m11 - m22)
            qw = (m21 - m12) / s
            qx = 0.25 * s
            qy = (m01 + m10) / s
            qz = (m02 + m20) / s

        elif m11 > m22:
            s = 2 * sqrt(1.0 + m11 - m00 - m22)
            qw = (m02 - m20) / s
            qx = (m01 + m10) / s
            qy = 0.25 * s
            qz = (m12 + m21) / s

        else:
            s = 2 * sqrt(1.0 + m22 - m00 - m11)
            qw = (m10 - m01) / s
            qx = (m02 + m20) / s
            qy = (m12 + m21) / s
            qz = 0.25 * s

        return float(qw), float(qx), float(qy), float(qz)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PUBLIC METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def axisangle(self, mode="deg"):
        """
        Calculates the axis and the angle of the rotation from the rotation
        matrix, given the mode (deg or rad).

        :param mode: Rotation angle units. Either "deg" (default) or "rad".
        :type mode: str

        :returns: rotation angle, rotation axis (unit vector as a tuple)
        :rtype: tuple

        """
        return self._axisangle(self.matrix, mode=mode)

    def euler(self, order="zyx", mode="deg"):
        """
        Calculates Euler (or Tait-Bryan) rotation angles from the rotation
        matrix given a specific order of the rotation axes.

        :param order:
            Three-letter string specification of the rotation axis order. Any
            permutation of x, y, z is valid. The default setting is "zyx".
        :type order: str
        :param mode:
            Rotation angle units. Either "deg" (default) or "rad".
        :type mode: str

        :returns: Euler or Tait-Bryan angles
        :rtype: tuple

        """
        return self._euler(self.matrix, order=order, mode=mode)

    def quaternion(self):
        """
        Calculates the quaternion (4-tuple) representation of the rotation.

        :returns: quaternion
        :rtype: tuple

        """
        return self._quaternion(self.matrix)

    # TODO: Deprecate Transformation.jacobian()
    def jacobian(self, coords):
        return super(TxRotation3D, self).jacobian(coords)


if __name__ == "__main__":  # pragma: no cover
    print("""This module is not intended for execution.""")

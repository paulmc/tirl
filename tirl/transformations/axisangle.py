#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np
from math import degrees, radians, pi


# TIRL IMPORTS

import tirl.utils as tu
from tirl import settings as ts

from tirl.point import Point
from tirl.transformations.rotation import TxRotation3D


# IMPLEMENTATION

class TxAxisAngle(TxRotation3D):
    """
    TxAxisAngle (child of TxRotation3D) class - performs rotation about a given
    axis.

    The axis of the rotation is stored as a 3-D unit vector. The angle of the
    rotation is mapped to the range [-pi, pi) and stored in radians.

    """

    RESERVED_KWARGS = ("angle", "axis", "mode", "bounds", "lock", "name",
                       "invertible", "metaparameters")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~ INITIALISATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __init__(self, angle, axis=(0, 0, 1), mode="deg", centre=None,
                 bounds=((-pi, pi),), lock=None, name=None, invertible=True,
                 **metaparameters):
        """
        Initialisation of TxAxisAngle.

        :param angle:
            Rotation angle in degrees or radians, depending on the 'mode'
            specification. Alternatively, a (3, 3) rotation matrix, from which
            the axis and angle of the rotation will be inferred. In either
            case, the angle of the rotation is always mapped to the range
            [-pi, pi), and stored in radians.
        :type angle: Union[float, int, np.ndarray]
        :param axis:
            Rotation axis. If the angle is specified with a (3, 3) rotation
            matrix, this argument will be overridden by whatever axis can be
            inferred from the rotation matrix. The default rotation axis is the
            z axis, i.e. (0, 0, 1). The rotation axis is always stored as a
            unit vector.
        :type axis: Union[list[float], tuple[float], np.ndarray]
        :param mode:
            Rotation angle units. Either "deg" (default) or "rad".
        :type mode: str
        :param bounds:
            The order of the parameters is (angle, axis_x, axis_y, axis_z).
            Bounds for the rotation angle must be given in radians.
            Parameter bounds can be defined in one of the following formats:
            1. ((lb0, ub0),) - sequence of lower and upper bounds for each
               parameter. The order must correspond to the order of the
               transformation parameters.
            2. {param_no: (lb, ub)} - dictionary of lower and upper bound
               pairs, where the keys represent parameter indices (starting
               from 0).
            3. (lb_arr, ub_arr) - flat iterable with lower and upper bounds for
               all parameters.
            4. "auto" - the get_default_bounds() method is called to create
               default lower and upper bounds.
            5. None - parameter bounds are not set.
            6. (lb, ub) - global scalar values for lower and upper bounds
        :type bounds: Union[tuple, list, dict, str]
        :param lock:
            Boolean mask or index sequence that defines which parameters
            are locked, i.e. should not be updated by default. If None, all
            parameters are updated by default when calling the update() method.
        :type lock: Union[None, tuple, int, np.ndarray]
        :param name:
            Name of the Transformation object. If None (default), a default
            name of the form "tx_objectID" will be generated.
        :type name: str
        :param invertible:
            Indicates whether the current transformation is invertible (True) or
            not (False, default). Transformations marked 'invertible' must
            implement the inverse() method.
        :type invertible: bool
        :param metaparameters:
            Metaparameters for TxAxisAngle
        :type metaparameters: Any

        """
        dtype = ts.DEFAULT_PARAMETER_DTYPE
        parameters = np.asarray([angle, *axis], dtype=dtype)

        # Interpret parameter and shape specification.
        # The input shape is set to None for a practical reason. If the input is
        # specified by a matrix, shape will no longer be None, and the angle
        # will be returned in radians. If the input was a scalar rotation angle,
        # shape will remain None, and the unit of the angle should be what is
        # specified by 'mode'. If instead of None the input shape was (3, 3),
        # there would be no way to decide whether the parameters have come from
        # a matrix or from direct specification, and the unit of the rotation
        # angle would be ambiguous.
        parameters, shape = \
            self._read_parameters_and_shape(parameters, shape=None)
        # Direct parameter specification -> convert angles to radians
        if shape is None:
            shape = (3, 3) if centre is None else (3, 4)
            parameters[0] = self._rad(parameters[0], mode=mode)
        # Matrix specification -> parameters are already in radians
        elif shape in ((3, 3), (3, 4)):
            pass
        # Input matrix was not a (3, 3) or (3, 4) rotation matrix
        else:
            raise ValueError("Invalid rotation angle specification.")

        # Map rotation angle to the [-pi, pi) interval
        if np.isclose(np.abs(parameters[0]), pi):
            parameters[0] = -pi
        else:
            angle = parameters[0]
            parameters[0] = np.sign(angle) * (np.abs(angle) % pi)

        # Normalise axis to unit vector
        parameters[1:] = parameters[1:] / np.linalg.norm(parameters[1:])
        if np.any(~np.isfinite(parameters)):
            raise ValueError("Non-finite value in axis-angle specification.")

        # Call superclass initialisation
        super(TxAxisAngle, self).__init__(
            *parameters, centre=centre, bounds=bounds, lock=lock, name=name,
            invertible=invertible, mode=mode, **metaparameters)

        # Re-set class-specific metaparameters
        self.mode = mode

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATTRIBUTES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @property
    def angle(self):
        """
        Returns the angle of the rotation in degrees or in radians, based on
        the 'mode' setting.

        """
        phi = float(self.parameters[0])
        if self.mode == "rad":
            return phi
        elif self.mode == "deg":
            return degrees(phi)
        else:
            raise ValueError("Unsupported mode. Choose either 'rad' or 'deg'.")

    @angle.setter
    def angle(self, phi):
        """
        Sets the angle of the rotation. The input units are determined by the
        'mode' setting.

        :param phi:
            Rotation angle in degrees or radians, depending on the 'mode'
            setting.
        :type phi: Union[float, int]

        """
        phi = np.asarray(self._rad(phi, self.mode), dtype=self.parameters.dtype)
        self.parameters[0] = phi

    @property
    def axis(self):
        return tuple(self.parameters[1:].tolist())

    @axis.setter
    def axis(self, ax):
        ax = np.asarray(ax, dtype=ts.DEFAULT_PARAMETER_DTYPE)
        ax = ax / np.linalg.norm(ax)
        if np.any(~np.isfinite(ax)):
            raise ValueError("Non-finite value in axis specification.")
        self.parameters[1:] = ax.astype(self.parameters.dtype)

    @property
    def mode(self):
        """
        Returns the rotation angle unit. Either "deg" or "rad".

        """
        return self.metaparameters.get("mode")

    @mode.setter
    def mode(self, m):
        """
        Sets the rotation angle unit. Either "deg" or "rad".

        :param m: rotation angle unit
        :type m: str

        """
        if m in ("rad", "deg"):
            self.metaparameters.update({"mode": m})
        else:
            raise ValueError("Unsupported mode. Choose either 'deg' or 'rad'.")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PRIVATE METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @classmethod
    def _load(cls, dump):
        """
        Creates TxAxisAngle (or subclass) instance from object dump.

        :param dump: object dump
        :type dump: dict

        :returns: TxAxisAngle instance
        :rtype: TxAxisAngle

        """
        parameters = dump.get("parameters")
        angle = parameters[0]  # in radians
        axis = parameters[1:]
        lb = parameters.lower_bounds
        ub = parameters.upper_bounds
        lock = parameters.locked
        meta = dump.get("metaparameters")
        name = meta.pop("name")
        shape = meta.pop("shape")
        invertible = meta.pop("invertible")
        mode = meta.pop("mode")
        centre = meta.pop("centre")
        obj = cls(angle, axis, mode="rad", centre=centre, bounds=(lb, ub),
                  lock=lock, name=name, invertible=invertible, **meta)
        obj.mode = mode
        return obj

    @staticmethod
    def _rad(angle, mode):
        """
        Interprets angle input based on the 'mode' setting of the
        transformation object, and returns the angle in radians.

        The purpose of having this private method is syntactic clarity: being
        able to avoid an explicit checking of self.mode.

        :param angle: angle to be interpreted
        :type angle: Union[int, float]
        :param mode: rotation angle units; either "deg" or "rad"
        :type mode: str

        :returns: angle (interpreted in deg/rad based on mode) in radians
        :rtype: float

        """
        if mode == "rad":
            return angle
        elif mode == "deg":
            return radians(angle)
        else:
            raise ValueError("Unsupported mode. Choose either 'rad' or 'deg'.")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PUBLIC METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @staticmethod
    def matrix2params(mat, **kwargs):
        """
        Calculates the rotation angle and axis from a (3, 3) rotation matrix.

        :param mat: (3, 3) rotation matrix
        :type mat: np.ndarray
        :param kwargs:
            Keyword arguments required to retrieve parameters from rotation
            matrix.
        :type kwargs: Any

        :returns: (angle, axis_x, axis_y, axis_z) sequence of parameters
        :rtype: np.ndarray

        """
        mode = kwargs.get("mode")
        angle, *axis = TxAxisAngle._axisangle(mat[:3, :3], mode=mode)

        # If the rotation matrix is not about the origin, then the
        # centres must be identical.
        centre = kwargs.get("centre", False)
        if mat.shape[-1] == 4:
            rot = mat[:3, :3]
            trans = mat[:, -1]
            centre = np.linalg.inv(np.eye(3) - rot) @ trans.reshape(-1, 1)
            kwargs.update(centre=Point(centre.ravel()))
            assert np.allclose(mat @ centre, centre)
        if mat.shape[-1] == 3 and centre:
            raise AssertionError("Conflicting input: the rotation matrix is "
                                 "about the origin, but a centre point was "
                                 "also specified.")

        return np.asarray([angle, *axis], dtype=ts.DEFAULT_PARAMETER_DTYPE)

    @staticmethod
    def params2matrix(parameters, **kwargs):
        """
        Creates (3, 3) rotation matrix from rotation angle and axis as
        parameters using the Rodrigues formula.

        :param parameters:
            Sequence of angle and axis of the rotation:
            (angle, axis_x, axis_y, axis_z)
        :type parameters: np.ndarray
        :param kwargs:
            Keyword arguments required to retrieve rotation matrix from
            parameters.
        :type kwargs: Any

        :returns: (3, 3) rotation matrix
        :rtype: np.ndarray

        """
        dtype = ts.DEFAULT_PARAMETER_DTYPE
        parameters = np.asarray(parameters, dtype=dtype)
        phi = parameters[0]
        e = parameters[1:]
        cross = np.asarray([[0, -e[2], e[1]],
                            [e[2], 0, -e[0]],
                            [-e[1], e[0], 0]], dtype=dtype)
        I = np.eye(3, dtype=dtype)
        R = np.cos(phi) * I + (1 - np.cos(phi)) * np.outer(e, e) \
            + np.sin(phi) * cross

        # Taking the rotation centre into account
        centre = kwargs.get("centre", False)
        if centre:
            mat = np.eye(4)
            mat[:3, :3] = R
            centre = Point(centre)
            trans = np.eye(4)
            trans[:-1, -1] = -centre.y
            R = np.linalg.inv(trans) @ mat @ trans
            R = R[:3, :]

        return R

    def axisangle(self, mode="deg"):
        """
        Calculates the axis and the angle of the rotation from the rotation
        matrix, given the mode (deg or rad).

        This method was overloaded to avoid unnecessary computations and
        accumulating numerical errors.

        :param mode: Rotation angle units. Either "deg" (default) or "rad".
        :type mode: str

        :returns: rotation angle, rotation axis (unit vector as a tuple)
        :rtype: tuple

        """
        if mode == "deg":
            angle = float(degrees(self.parameters[0]))
        elif mode == "rad":
            angle = float(self.parameters[0])
        else:
            raise ValueError("Unsupported mode. Choose either 'rad' or 'deg'.")
        axis = tuple(self.parameters[1:].tolist())

        return angle, axis

    def inverse(self):
        """
        Creates inverse TxAxisAngle (or subclass) instance that undoes the
        current rotation by inverting the sign of the rotation angle.

        :returns: TxAxisAngle object that undoes the current transformation
        :rtype: TxAxisAngle

        """
        meta = tu.rcopy(self.metaparameters)
        name = meta.pop("name")
        name = f"{name}_inv"
        shape = meta.pop("shape")
        invertible = meta.pop("invertible")
        mode = meta.pop("mode")
        centre = meta.pop("centre")
        # Use same mode and invertibility, default bounds and no lock.
        obj = type(self)(-self.angle, self.axis, mode=mode, centre=centre,
                         bounds=((-pi, pi),), lock=None, name=name,
                         invertible=invertible, **meta)
        return obj

    # TODO: Deprecate Transformation.jacobian()
    # def jacobian(self, coords):
    #     coords = super(TxLinear, self).map(coords)
    #
    #     # This implementation is based on the manually calculated derivatives
    #     # of terms in the 3x3 Rodrigues formula.
    #     phi = self.parameters[0]    # in radians
    #     s = np.sin(phi)
    #     c = np.cos(phi)
    #     e0, e1, e2 = self.axis.copy()
    #     e = np.asarray([[e0], [e1], [e2]])
    #     I = np.eye(3)
    #     eeT = np.dot(e, e.T)
    #     cross = np.asarray([[0, -e2, e1],
    #                         [e2, 0, -e0],
    #                         [-e1, e0, 0]])
    #
    #     # Derivative of rotation matrix with respect to rotation angle
    #     dR_dphi = -s * I + s * eeT + c * cross
    #
    #     # Derivative of rotation matrix with respect to axis coordinate 0
    #     deeT_de0 = np.asarray([[2*e0, e1, e2],
    #                            [e1, 0, 0],
    #                            [e2, 0, 0]])
    #     dcross_de0 = np.asarray([[0, 0, 0],
    #                              [0, 0, -1],
    #                              [0, 1, 0]])
    #     dR_de0 = (1 - c) * deeT_de0 + s * dcross_de0
    #
    #     # Derivative of rotation matrix with respect to axis coordinate 1
    #     deeT_de1 = np.asarray([[0, e0, 0],
    #                            [e0, 2*e1, e2],
    #                            [0, e2, 0]])
    #     dcross_de1 = np.asarray([[0, 0, 1],
    #                              [0, 0, 0],
    #                              [-1, 0, 0]])
    #     dR_de1 = (1 - c) * deeT_de1 + s * dcross_de1
    #
    #     # Derivative of rotation matrix with respect to axis coordinate 2
    #     deeT_de2 = np.asarray([[0, 0, e0],
    #                            [0, 0, e1],
    #                            [e0, e1, 2*e2]])
    #     dcross_de2 = np.asarray([[0, -1, 0],
    #                              [1, 0, 0],
    #                              [0, 0, 0]])
    #     dR_de2 = (1 - c) * deeT_de2 + s * dcross_de2
    #
    #     # (Homogeneous) derivative tensor
    #     d = 3 + int(self.homogenise)
    #     dRh_dphi = np.zeros((d, d))
    #     dRh_dphi[:3, :3] = dR_dphi
    #     dRh_de0 = np.zeros((d, d))
    #     dRh_de0[:3, :3] = dR_de0
    #     dRh_de1 = np.zeros((d, d))
    #     dRh_de1[:3, :3] = dR_de1
    #     dRh_de2 = np.zeros((d, d))
    #     dRh_de2[:3, :3] = dR_de2
    #     dR_dtheta = np.stack((dRh_dphi, dRh_de0, dRh_de1, dRh_de2), axis=0)
    #
    #     # Calculate Jacobian
    #     jac = np.einsum("ijk,hk", dR_dtheta, coords)
    #     return jac


if __name__ == "__main__":  # pragma: no cover
    print("""This module is not intended for execution.""")

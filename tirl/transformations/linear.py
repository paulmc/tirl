#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np
from numbers import Number, Integral


# TIRL IMPORTS

from tirl import utils as tu, settings as ts
from tirl.transformations import Transformation


# DEFINITIONS

from tirl.constants import RULE_FS, RULE_SSR, RULE_PPD


# IMPLEMENTATION

class TxLinear(Transformation):
    """
    TxLinear - generic linear transformation class.

    All linear transformations inherit from this base class. TxLinear
    implements both a forward a reverse mapping between its parameters and the
    transformation matrix. This allows any TxLinear object to be initialised by
    its parameters or by a square transformation matrix. Regardless of the
    initialisation, it is the parameter vector that is stored by the object,
    therefore bounds and locks must be specified with respect to these.

    """
    __tx_priority__ = 1000

    RESERVED_KWARGS = ("parameters", "shape", "bounds", "lock", "name",
                       "invertible", "metaparameters")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~ INITIALISATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __init__(self, *parameters, shape=None, bounds=None, lock=None,
                 name=None, invertible=True, **metaparameters):
        """
        Initialisation of TxLinear.

        :param parameters:
            Input parameters can be specified by one of the following methods:

            1. (n, n) array: linear transformation matrix
            2. (n*n,) iterable: flattened sequence of parameters

        :type parameters: Union[np.ndarray, tuple, list]
        :param shape:
            Shape of the transformation matrix. The shape must be given if the
            object is initialised by parameters, not a matrix. If a matrix is
            given, the shape will be taken from that, overriding any input for
            shape.
        :type shape: Union[tuple, NoneType]
        :param bounds:
            Parameter bounds can be defined in one of the following formats:
            1. ((lb0, ub0),) - sequence of lower and upper bounds for each
               parameter. The order must correspond to the order of the
               transformation parameters.
            2. {param_no: (lb, ub)} - dictionary of lower and upper bound
               pairs, where the keys represent parameter indices (starting
               from 0).
            3. (lb_arr, ub_arr) - flat iterable with lower and upper bounds for
               all parameters.
            4. "auto" - the get_default_bounds() method is called to create
               default lower and upper bounds.
            5. None - parameter bounds are not set.
            6. (lb, ub) - global scalar values for lower and upper bounds
        :type bounds: Union[tuple, list, dict, str]
        :param lock:
            Boolean mask or index sequence that defines which parameters
            are locked, i.e. should not be updated by default. If None, all
            parameters are updated by default when calling the update() method.
        :type lock: Union[None, tuple, int, np.ndarray]
        :param name:
            Name of the Transformation object. If None (default), a default
            name of the form "tx_objectID" will be generated.
        :type name: str
        :param invertible:
            Indicates whether the current transformation is invertible (True) or
            not (False, default). Transformations marked 'invertible' must
            implement the inverse() method.
        :type invertible: bool
        :param metaparameters:
            Metaparameters for Transformation
        :type metaparameters: Any

        """
        if parameters == ():
            raise ValueError("No parameters were specified.")

        # Interprets the parameter input (which might be a matrix or a sequence
        # of parameters), and determines the shape if the input was a matrix.
        # Otherwise the shape is returned from user input.
        parameters, shape = \
            self._read_parameters_and_shape(parameters, shape=shape)

        # Call superclass initialisation
        super(TxLinear, self).__init__(
            *parameters, bounds=bounds, lock=lock, name=name,
            invertible=invertible, shape=shape, **metaparameters)

        # Re-set class-specific metaparameters : matrix shape
        if shape is None:
            raise ValueError(f"Invalid matrix shape: {shape}")
        elif hasattr(shape, "__iter__") and (len(shape) == 2) and \
                (isinstance(d, Integral) and (d > 0) for d in shape):
            self.metaparameters.update({"shape": tuple(shape)})
        else:
            raise ValueError(f"Invalid matrix shape: {shape}")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATTRIBUTES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @property
    def dim(self):
        """
        Returns the number of coordinates expected by the transformation object.

        """
        return self.shape[1]

    @property
    def matrix(self):
        """
        Returns the linear transformation matrix.

        """
        return self.params2matrix(self.parameters[:], **self.metaparameters)

    @matrix.setter
    def matrix(self, mat):
        """
        Resets transformation parameters to match the provided linear
        transformation matrix. Note that resetting the parameters may push them
        out of their bounds.

        :param mat: new linear transformation matrix
        :type mat: np.ndarray

        """
        self._parameters[:] = self.matrix2params(mat, **self.metaparameters)\
            .astype(self.parameters.dtype)

    @property
    def shape(self):
        """
        Returns the shape of the linear transformation matrix.
        Shape is a read-only property.

        """
        return self.metaparameters.get("shape")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PRIVATE METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def _read_parameters_and_shape(self, parameters, **metaparameters):
        """
        Reads the parameter specification of the constructor. This method is
        provided to avoid reimplementing the entire initialisation in
        subclassess if the parameter input must be parsed before the TxLinear
        intialisation can be called.

        This method allows all TxLinear object be constructed from either a set
        of parameters or a transformation matrix.

        """
        # Matrix specification: matrix with numeric values
        mat = parameters[0]
        shape = metaparameters.get("shape")
        if isinstance(mat, np.ndarray) and \
                (mat.ndim == 2) and np.issubdtype(mat.dtype, np.number):
            mat = np.asarray(mat)
            shape = mat.shape
            parameters = self.matrix2params(mat, **metaparameters)

        # Parameter vector: ndarray
        elif isinstance(mat, np.ndarray) and (mat.ndim == 1) and \
                np.issubdtype(mat.dtype, np.number):
            parameters = mat.ravel()

        # Parameter vector: Iterable
        elif hasattr(mat, "__iter__") and \
                all(isinstance(v, Number) for v in mat):
            parameters = np.asarray(mat).ravel()

        # Parameter vector: scalars
        elif all(isinstance(v, Number) for v in parameters):
            parameters = np.asarray(parameters).ravel()

        else:
            raise TypeError(f"Unrecognised parameter specification: "
                            f"{parameters}")

        return parameters, shape

    @classmethod
    def _load(cls, dump):
        """
        Creates a TxLinear (or subclass) instance from object dump. Subclasses
        with a different initialiser signature MUST overload this classmethod.

        :param dump: object dump
        :type dump: dict

        :returns: TxLinear instance
        :rtype: TxLinear
        """
        parameters = dump.get("parameters")
        lb = parameters.lower_bounds
        ub = parameters.upper_bounds
        lock = parameters.locked
        meta = dump.get("metaparameters")
        shape = meta.pop("shape")
        name = meta.pop("name")
        invertible = meta.pop("invertible")
        obj = cls(*parameters[:], shape=shape, bounds=(lb, ub), lock=lock,
                  name=name, invertible=invertible, **meta)
        return obj

    def __repr__(self):
        """
        Returns the string representation of TxLinear.

        """
        nl = self.parameters.count_locked()
        t = self.__class__.__name__
        npar = self.parameters.size
        dim = self.dim
        sh = self.shape
        name = self.name
        ret = f"{t}(parameters={npar}, locked={nl}, dim={dim}, shape={sh}, " \
              f"name={name})"
        return ret

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PUBLIC METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @staticmethod
    def matrix2params(mat, **kwargs):
        """
        Creates parameter vector from transformation matrix. This method
        provides flexibility for subclassing TxLinear by allowing to create
        different parametrisations of the linear transformation matrix. The
        base class implements C-order flattening of the transformation matrix
        to obtain the parameter vector.

        Note that the resultant parameter vector are not linked to the elements
        of the matrix, hence any changes in the resultant parameter vector will
        not be represented in the original matrix.

        The 'matrix' property of the the class can be regarded as the inverse
        operation of this method.

        :param mat: transformation matrix
        :type mat: np.ndarray
        :param kwargs:
            Keyword arguments required to retrieve parameters from
            transformation matrix.
        :type kwargs: Any

        :returns: linear parameter array
        :rtype: np.ndarray

        """
        parameters = np.asarray(mat, dtype=ts.DEFAULT_PARAMETER_DTYPE).ravel()
        return parameters

    @staticmethod
    def params2matrix(parameters, **kwargs):
        """
        Creates linear transformation matrix from transformation parameters.
        Subclasses MUST overload this method to provide their subclass-specific
        forward mapping from parameters to the transformation matrix. The base
        class implements a simple linear indexing of the parameters in the
        transformation matrix according to the C axis order convention (row
        major, i.e. the fastest changing coordinate is the last one).

        Note: the homogeneous option must be taken into account when composing
        the matrix. For example, a rotation object would convert 3 rotation
        parameters into a 3 x 3 matrix. If the homogeneous option is True, the
        output matrix should be 4 x 4 large.

        :param parameters: transformation parameters
        :type parameters: np.ndarray
        :param kwargs:
            Keyword arguments required to retrieve matrix from parameters.
        :type kwargs: Any

        :returns: linear transformation matrix
        :rtype: np.ndarray

        """
        shape = kwargs.get("shape")
        parameters = np.asarray(parameters, dtype=ts.DEFAULT_PARAMETER_DTYPE)
        mat = np.eye(*shape, dtype=parameters.dtype)
        mat.flat[:] = parameters
        return mat

    def jacobian(self, coords=None):
        """
        Returns the derivative of the local displacement vector with respect
        to the change of the transformation parameters. If coordinates are not
        specified, the matrix jacobian is returned, which is a 3rd-order tensor.

        :params coords:
        :type coords:

        :returns:
            If 'coords' is not None: total derivative of the local displacement
            vector with respect to the transformation parameters.
            Array: (points, displacement_dimension, parameters)

            If coords is None: the total matrix derivative with respect to the
            transformation parameters.
            Array: (matrix_rows, matrix_columns, parameters)
        :rtype: np.ndarray

        """
        return NotImplementedError()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ARITHMETICS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __add__(self, other):
        if self._lower_priority(other) and hasattr(other, "__radd__"):
            return other.__radd__(self)

        # Add linear transformations
        elif isinstance(other, TxLinear):
            sms = self.matrix.shape
            oms = other.matrix.shape
            if oms[1] == sms[0]:
                mat = other.matrix @ self.matrix
                return TxLinear(mat)
            elif oms[1] > sms[0]:
                homogenised_self = np.eye(oms[1])
                homogenised_self[:sms[0], :sms[1]] = self.matrix
                mat = other.matrix @ homogenised_self
                return TxLinear(mat)
            else:
                raise TypeError(f"Operands are not compatible: {sms}, {oms}")

        # Defer the call to the superclass otherwise
        else:
            return super(TxLinear, self).__add__(other)

    def __radd__(self, other):
        if self._lower_priority(other) and hasattr(other, "__add__"):
            return other.__add__(self)
        else:
            return super(TxLinear, self).__radd__(other)

    def __sub__(self, other):
        if self._lower_priority(other) and hasattr(other, "__rsub__"):
            return other.__rsub__(self)
        if isinstance(other, TxLinear):
            return self + other.__neg__()
        else:
            return super(TxLinear, self).__sub__(other)

    def __rsub__(self, other):
        if self._lower_priority(other) and hasattr(other, "__sub__"):
            return other.__sub__(self)
        else:
            return super(TxLinear, self).__rsub__(other)

    def __mul__(self, other):
        if isinstance(other, Number):
            if other <= 0:
                raise NotImplementedError(
                    "Non-positive multiples of linear transformations are not "
                    "implemented.")
            if len(set(self.shape)) != 1:
                raise ArithmeticError(
                    "Integer multiples of linear transformations are only "
                    "valid when the transformation matrix is square.")
            mat = np.copy(self.matrix)
            for i in range(other - 1):
                mat = mat @ self.matrix
            return TxLinear(mat)
        else:
            raise NotImplementedError(
                "Only positive integer multiples of linear transformations "
                "are implemented.")

    def __rmul__(self, other):
        # The only valid (positive integer) multiplication is commutative.
        return self.__mul__(other)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PUBLIC METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def inverse(self):
        """
        Creates inverse transformation object by inverting the linear
        transformation matrix.

        :raises:
            ArithmeticError if the matrix is not invertible
            (e.g. because it is not square)
        :returns: inverse linear transformation object
        :rtype: TxLinear

        """
        # Create square matrix
        mat = self.matrix
        m, n = mat.shape
        if m != n:
            newmat = np.eye(max(m, n))
            newmat[:m, :n] = mat
        else:
            newmat = mat
        try:
            inv = np.linalg.inv(newmat)
        except np.linalg.linalg.LinAlgError as exc:
            raise ArithmeticError("Matrix is not invertible.") from exc
        else:
            test_a = np.eye(*inv.shape)
            test_a[:m, :n] = 0
            test_b = inv.copy()
            test_b[:m, :n] = 0
            if not np.allclose(test_a, test_b):
                raise ArithmeticError(
                    "Matrix does not exist on the same manifold.")

        # Otherwise accept the result of the inversion
        inv = inv[:m, :n]
        meta = tu.rcopy(self.metaparameters)
        shape = meta.pop("shape")
        name = meta.pop("name")
        name = f"{name}_inv"
        invertible = meta.pop("invertible")
        return type(self)(inv, shape=None, bounds=None, lock=None,
                          name=name, invertible=invertible, **meta)

    def map(self, coords, signature=None):
        """
        Transforms input coordinates by appending them with additional zeros,
        corresponding to the dimension gap.

        :param coords: (n_points, n_dimensions) table of input coordinates
        :type coords: np.ndarray
        :param signature: signature of the input coordinates
        :type signature: list

        :returns: (n_points, m_dimensions) table of transformed coordinates
        :rtype: np.ndarray

        """
        coords = super(TxLinear, self).map(coords, signature)
        mat = self.matrix
        if mat.shape[-1] == coords.shape[-1]:
            return np.dot(mat, coords.T).T
        elif mat.shape[-1] > coords.shape[-1]:
            dim_gap = mat.shape[-1] - coords.shape[-1]
            ones = np.ones(shape=(dim_gap, coords.shape[0]), dtype=coords.dtype)
            trans = mat[:, -dim_gap:] @ ones
            newcoords = mat[:, :-dim_gap] @ coords.T + trans
            return newcoords.T
        else:
            # Transform the matching subset of dimensions.
            effective = mat.shape[-1]
            coords = coords.copy()  # avoid replacement!
            coords[:, :effective] = (mat @ coords.T[:effective, :]).T
            return coords
            # raise ArithmeticError(
            #     f"The linear transformation matrix {mat.shape} is not "
            #     f"compatible with the number of input coordinates "
            #     f"({coords.shape[-1]}).")

    def adjustment_matrix(self, coords=None, rule=RULE_SSR):
        """
        Returns the rotation matrix (or scale-skew-rotation matrix) that is
        generated from the linear transformation. The matrix can be used to
        rotate vectors and tensors.

        """
        eps = np.finfo(ts.DEFAULT_FLOAT_TYPE).eps
        r = min(self.matrix.shape)

        if rule == RULE_FS:
            F = self.matrix[:r, :r]
            u, s, vt = np.linalg.svd(F @ F.T, full_matrices=True)
            mat = np.linalg.inv(u @ np.diag(np.sqrt(s)) @ vt) @ F
            # Ensure that the rotation matrix is positive definite
            # assert np.abs(np.linalg.det(mat) - 1) < eps

        elif rule == RULE_SSR:
            mat = self.matrix[:r, :r]

        else:
            raise ValueError(f"Unrecognised vector transformation rule: {rule}")

        return mat

    def map_vector(self, vects, coords=None, rule=RULE_SSR, signature=None):
        """
        Applies transformation to vectors at the locations specified by the
        coordinates. As linear transformations are location-invariant,
        coordinates are optional for linear transformations, but obligatory for
        non-linear transformations, where the transformation may vary from
        point to point.

        :param rule:
            RULE_FS: only rotation
            RULE_SSR: rotation + scaling + shears
            Vector reorientation rule. This choice should be made on the
            a-priori knowledge or belief about how the vector quantities in
            question behave under linear transformations. If the vectors are
            invariant to scaling or shearing, but are susceptible to rotation,
            RULE_FS is recommended, as this will only transform the
            vectors by the rigid rotation component of the linear
            transformation. If this is not true, use RULE_SSR (default) that
            will apply not only the rotation but also the scale and shear
            components of the transformation to the vectors.
        :type rule: str

        """
        vects = super(TxLinear, self).map_vector(
            vects, coords=coords, rule=rule, signature=signature)

        if rule in (RULE_FS, RULE_SSR):
            mat = self.adjustment_matrix(coords=coords, rule=rule)
            effective = mat.shape[-1]
            vects = vects.copy()  # avoid replacing values in-place!
            vects[:, :effective] = (mat @ vects.T[:effective]).T
        else:
            raise ValueError(f"Unrecognised vector transformation rule: {rule}")

        return vects

    def map_vector_and_coordinates(
            self, vects, coords=None, rule=RULE_SSR, signature=None):
        """
        Applies transformation to vectors at the locations specified by the
        coordinates. As linear transformations are location-invariant,
        coordinates are optional for linear transformations, but obligatory for
        non-linear transformations, where the transformation may vary from
        point to point.

        :param rule:
            RULE_FS: only rotation
            RULE_SSR: rotation + scaling + shears
            Vector reorientation rule. This choice should be made on the
            a-priori knowledge or belief about how the vector quantities in
            question behave under linear transformations. If the vectors are
            invariant to scaling or shearing, but are susceptible to rotation,
            RULE_FS is recommended, as this will only transform the
            vectors by the rigid rotation component of the linear
            transformation. If this is not true, use RULE_SSR (default) that
            will apply not only the rotation but also the scale and shear
            components of the transformation to the vectors.
        :type rule: str

        """
        vects = self.map_vector(vects, coords, rule, signature)

        # Transform coordinates
        if coords is not None:
            coords = self.map(coords)

        return vects, coords

    def map_tensor(self, tensors, coords=None, rule=RULE_PPD, signature=None):
        """
        Applies transformation to tensors at the locations specified by the
        coordinates. As linear transformations are location-invariant,
        coordinates are optional for linear transformations, but obligatory for
        non-linear transformations, where the transformation may vary from
        point to point.

        :param rule:
            RULE_FS: only rotation
            RULE_SSR: scale + shear + rotation
            RULE_PPD: preserving the relative orientation of eigenvectors

        """
        tensors = super(TxLinear, self).map_tensor(
            tensors, coords=coords, rule=rule, signature=signature)

        if rule in (RULE_FS, RULE_SSR):
            mat = self.adjustment_matrix(coords=coords, rule=rule)
            assert mat.shape[-1] == tensors.shape[-1] == tensors.shape[-2]

        elif rule == RULE_PPD:
            F = self.adjustment_matrix(coords=coords, rule=RULE_SSR)
            mat = ppd(F, tensors)

        else:
            raise ValueError(f"Unrecognised tensor transformation rule: {rule}")

        return mat @ tensors @ np.linalg.inv(mat)

    def map_tensor_and_coordinates(
            self, tensors, coords=None, rule=RULE_PPD, signature=None):
        """
        Applies transformation to tensors at the locations specified by the
        coordinates. As linear transformations are location-invariant,
        coordinates are optional for linear transformations, but obligatory for
        non-linear transformations, where the transformation may vary from
        point to point.

        :param rule:
            RULE_FS: only rotation
            RULE_SSR: scale + shear + rotation
            RULE_PPD: preserving the relative orientation of eigenvectors

        """
        tensors = self.map_tensor(tensors, coords, rule, signature)

        # Transform coordinates
        if coords is not None:
            coords = self.map(coords)

        return tensors, coords


def ppd(F, tensors):
    _, vects = np.linalg.eigh(tensors)
    e1 = vects[..., -1]
    n1 = np.squeeze(F @ e1[..., np.newaxis])
    n1 /= np.linalg.norm(n1, axis=-1, keepdims=True)
    R1 = rotmat(e1, n1)
    if tensors.shape[-1] == 2:
        return R1

    e2 = vects[..., -2]
    n2 = np.squeeze(F @ e2[..., np.newaxis])
    n2 /= np.linalg.norm(n2, axis=-1, keepdims=True)
    Pn2 = n2 - (n2[..., np.newaxis, :] @ n1[..., np.newaxis])[..., 0] * n1
    Pn2 /= np.linalg.norm(Pn2, axis=-1, keepdims=True)
    f2 = np.squeeze(R1 @ e2[..., np.newaxis])
    f2 /= np.linalg.norm(f2, axis=-1, keepdims=True)
    R2 = rotmat(f2, Pn2)
    return R2 @ R1


def rotmat(a, b):
    a = np.asarray(a, dtype=np.float64)
    b = np.asarray(b, dtype=np.float64)
    a = a / np.linalg.norm(a, axis=-1, keepdims=True)
    b = b / np.linalg.norm(b, axis=-1, keepdims=True)
    n = a.shape[0]
    d = a.shape[-1]
    cross = np.cross(a, b)
    sine = np.linalg.norm(cross, axis=-1, keepdims=True)
    cosine = np.einsum("...i,...i", a, b)[..., np.newaxis]
    axis = cross / sine
    K = np.zeros((n, d, d), dtype=np.float64)
    for i in range(d):
        e = np.zeros((d,), dtype=np.float64)
        e[i] = 1
        K[..., i] = np.cross(axis, e)
    R = np.eye(d)[np.newaxis] + sine[..., np.newaxis] * K \
        + (1 - cosine[..., np.newaxis]) * np.matmul(K, K)
    return R


if __name__ == "__main__":  # pragma: no cover
    print("""This module is not intended for execution.""")

/*
   _______   _____   _____    _
  |__   __| |_   _| |  __ \  | |
     | |      | |   | |__) | | |
     | |      | |   |  _  /  | |
     | |     _| |_  | | \ \  | |____
     |_|    |_____| |_|  \_\ |______|

 Copyright (C) 2018-2023 University of Oxford
 Part of the FMRIB Software Library (FSL)
 Author: Istvan N. Huszar
 Copyright (C) 2018-2023 University of Oxford */

/*  CCOPYRIGHT  */


#ifndef FINV_FINV3D_H
#define FINV_FINV3D_H

#include <lapacke.h>

// Basic math

double Dot3D(double v[], double u[]);
int Cross(double v[], double u[], double* Result);
int Difference3D(double v[], double u[], double* Result);
int DifferenceInt3D(int v[], int u[], int* Result);
int Unravel3DIndex(int Index, int* Shape, int* MultiIndex);
int _lin(int* Shape, int i, int j, int k);
int Ravel3DIndex(int* MultiIndex, int* Shape);
lapack_int InvertMat3D(double* A, int N);

// 3D functions

int IsInTetrahedron(double* Point, double** Simplex);

int StepTetrahedron(int** Simplex, int Vertex, int* Shape);

int InitialiseTetrahedron(int ** Simplex, int* Shape);

int CalculateInverse3D(double** Locations, int* NearestNeighbour, int N,
    int* InputShape, double** Coordinates, double** FwdMappedCoordinates,
    double** InvWarp, int Iterations);

int CalculateLocalAffine3D(double** Locations,
    int* NearestNeighbour, int N, int* InputShape,
    double** Coordinates, double** FwdMappedCoordinates,
    double** LocalAffine, int Iterations);

int CalculateTxJacobian3D(double** Locations, int* NearestNeighbour,
    int N, int* InputShape, double** Coordinates,
    double** Jacobian, int** VertexIndex, int Iterations);



int FillHoles3D(double** InvWarp, int* Shape, int MaxIter);


#endif //FINV_FINV3D_H

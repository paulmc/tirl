/*
   _______   _____   _____    _
  |__   __| |_   _| |  __ \  | |
     | |      | |   | |__) | | |
     | |      | |   |  _  /  | |
     | |     _| |_  | | \ \  | |____
     |_|    |_____| |_|  \_\ |______|

 Copyright (C) 2018-2023 University of Oxford
 Part of the FMRIB Software Library (FSL)
 Author: Istvan N. Huszar
 Copyright (C) 2018-2023 University of Oxford */

/*  CCOPYRIGHT  */


#ifndef TIRL_FSLINTERPOLATOR_H
#define TIRL_FSLINTERPOLATOR_H

using namespace std;

namespace FSLINTERPOLATOR {

    template<typename F>
    F q_tri_interpolation(F v000, F v001, F v010, F v011,
                          F v100, F v101, F v110, F v111,
                          double dx, double dy, double dz);

    size_t idx(size_t * shape, size_t x, size_t y, size_t z);

    bool in_neigh_bounds(size_t * shape, long x, long y, long z);

    template<typename F>
    F interpolate(F * data, size_t * shape, double x, double y, double z);

    template<typename F>
    void interpolate_all(F * data, size_t * shape, float * coordinates,
                      size_t n, F * out);

//    // 8 bit
//     template void interpolate_all<char>(char * data, size_t * shape,
//                            float * coordinates, size_t n, char * out);
//     template void interpolate_all<unsigned char>(
//        unsigned char * data, size_t * shape, float * coordinates, size_t n,
//        unsigned char * out);
//
//    // 16 bit
//     template void interpolate_all<short int>(short int * data, size_t * shape,
//                            float * coordinates, size_t n, short int * out);
//     template void interpolate_all<unsigned short int>(
//        unsigned short int * data, size_t * shape, float * coordinates,
//        size_t n, unsigned short int * out);
//
//    // 32 bit
//     template void interpolate_all<int>(int * data, size_t * shape,
//                            float * coordinates, size_t n, int * out);
//     template void interpolate_all<unsigned int>(
//        unsigned int * data, size_t * shape, float * coordinates, size_t n,
//        unsigned int * out);
//     template void interpolate_all<float>(float * data, size_t * shape,
//                             float * coordinates, size_t n, float * out);
//
//    // 64 bit
//     template void interpolate_all<long int>(long int * data, size_t * shape,
//                            float * coordinates, size_t n, long int* out);
//     template void interpolate_all<unsigned long int>(
//        unsigned long int * data, size_t * shape, float * coordinates, size_t n,
//        unsigned long int * out);
//     template void interpolate_all<double>(double * data, size_t * shape,
//                             float * coordinates, size_t n, double * out);
//
//    // 128 bit
//     template void interpolate_all<long double>(
//        long double * data, size_t * shape, float * coordinates, size_t n,
//        long double * out);


}  // namespace: FSLINTERPOLATOR

#endif //TIRL_FSLINTERPOLATOR_H

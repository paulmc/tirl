/*
   _______   _____   _____    _
  |__   __| |_   _| |  __ \  | |
     | |      | |   | |__) | | |
     | |      | |   |  _  /  | |
     | |     _| |_  | | \ \  | |____
     |_|    |_____| |_|  \_\ |______|

 Copyright (C) 2018-2023 University of Oxford
 Part of the FMRIB Software Library (FSL)
 Author: Istvan N. Huszar
 Copyright (C) 2018-2023 University of Oxford */

/*  CCOPYRIGHT  */


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cblas.h>
#include <lapacke.h>
#include <limits.h>

#include "finv2d_src.h"


// Basic math

double Dot2D(double v[], double u[])
{
    double Result = 0.0;
    for (int i = 0; i < 2; i++)
        Result += (v[i] * u[i]);
    return Result;
}

int Difference2D(double v[], double u[], double * Result) {
    // double* Result = calloc(sizeof(double), 2);
    for (int i = 0; i < 2; i++) {
        Result[i] = v[i] - u[i];
    }
    return 0;
}

int DifferenceInt2D(int v[], int u[], int * Result) {
    // int* Result = calloc(sizeof(int), 2);
    for (int i = 0; i < 2; i++) {
        Result[i] = v[i] - u[i];
    }
    return 0;
}

int Unravel2DIndex(int Index, int* Shape, int* MultiIndex) {

    if (Index < (Shape[0] * Shape[1])) {
        MultiIndex[0] = Index / Shape[1];
        MultiIndex[1] = Index % Shape[1];
        return 0;  // Success!
    } else {
        return 1;  // Index is out of bounds!
    }
}

int Ravel2DIndex(int* MultiIndex, int* Shape) {

    int Index = MultiIndex[0] * Shape[1] + MultiIndex[1];
    return Index;

}

lapack_int InvertMat2D(double* A, int N) {
// Performs in-place matrix inversion on A
    int IPIV[N+1];
    lapack_int ret;

    // LU factorisation
    ret = LAPACKE_dgetrf(LAPACK_COL_MAJOR, N, N, A, N, IPIV);
    if (ret != 0)
        return ret;
    // Inverse
    ret = LAPACKE_dgetri(LAPACK_COL_MAJOR, N, A, N, IPIV);
    return ret;
}

// 2D functions

int IsInTriangle(double* Point, double** Simplex) {

    int PointInTriangle;
    int Vertex, Vertex2, Vertex3;
    int FirstVertex = rand() % 3;
    for (int Run = 0; Run < 3; Run++) {
        Vertex = (FirstVertex + Run) % 3;
        Vertex2 = (Vertex + 1) % 3;
        Vertex3 = (Vertex + 2) % 3;
        double Edge[2];
        Difference2D(Simplex[Vertex2], Simplex[Vertex3], Edge);
        double Normal[2] = {-Edge[1], Edge[0]};
        // Vector from index vertex to point of interest
        double PDist[2];
        Difference2D(Point, Simplex[Vertex2], PDist);
        // Vector from index vertex to secondary vertex
        double VDist[2];
        Difference2D(Simplex[Vertex], Simplex[Vertex2], VDist);
        PointInTriangle = ((Dot2D(PDist, Normal) * Dot2D(VDist, Normal)) >= 0) ? -1 : Vertex;
        if (PointInTriangle >= 0)
            break;
    }
    return PointInTriangle;

}

int StepTriangle(int** Simplex, int Vertex, int* Shape) {

    // Find mirror axis: diagonal or gridline?
    int Vertex2 = (Vertex + 1) % 3;
    int Vertex3 = (Vertex + 2) % 3;
    int Edge[2];
    DifferenceInt2D(Simplex[Vertex2], Simplex[Vertex3], Edge);

    // Mirror simplex around diagonal line
    if ((Edge[0] != 0) && (Edge[1] != 0)) {
        for (int Axis = 0; Axis < 2; Axis++) {
            int diff = Simplex[Vertex2][Axis] - Simplex[Vertex][Axis];
            diff += (Simplex[Vertex3][Axis] - Simplex[Vertex][Axis]);
            Simplex[Vertex][Axis] += diff;
        }
    }
    // Mirror Simplex around grid line
    else {
        int Axis = (Edge[0] == 0) ? 0 : 1;
        int NewPos;
        if ((rand() % 2) == 0)
            NewPos = 2 * Simplex[Vertex2][Axis] - Simplex[Vertex][Axis];
        else
            NewPos = 2 * Simplex[Vertex3][Axis] - Simplex[Vertex][Axis];
        if ((NewPos >= 0) && (NewPos < Shape[Axis]))
            Simplex[Vertex][Axis] = NewPos;
        else
            return 1;  // Triangle mirroring unsuccessful: out of FOV!
    }
    return 0;  // Success!

}

int InitialiseTriangle(int ** Simplex, int* Shape) {

    // Coordinate 1
    if (Simplex[0][0] == (Shape[0] - 1)) {
        Simplex[1][0] = Simplex[0][0];
        Simplex[2][0] = Simplex[0][0] - 1;
    }
    else {
        Simplex[1][0] = Simplex[0][0];
        Simplex[2][0] = Simplex[0][0] + 1;
    }

    // Coordinate 2
    if (Simplex[0][1] == (Shape[1] - 1)) {
        Simplex[1][1] = Simplex[0][1] - 1;
        Simplex[2][1] = Simplex[0][1];
    }
    else {
        Simplex[1][1] = Simplex[0][1] + 1;
        Simplex[2][1] = Simplex[0][1];
    }
    return 0;

}


int CalculateInverse2D(double** Locations,
    int* NearestNeighbour, int N, int* InputShape,
    double** Coordinates, double** FwdMappedCoordinates,
    double** InvWarp, int Iterations) {

    // Declare simplex to operate on the input grid coordinates
    int** GridSimplex = calloc(3, sizeof(int *));
        for (int Vertex = 0; Vertex < 3; Vertex++)
            GridSimplex[Vertex] = calloc(2, sizeof(int));
    // Declare simplex to operate on the input coordinates
    double** InputSimplex = calloc(3, sizeof(double *));
    // Declare simplex to operate on the forward-mapped coordinates
    double** MappedSimplex = calloc(3, sizeof(double *));
    double * TargetPoint;

    // Loop over all locations for which the LocalAffine needs to be calculated
    int niter;
    char found_it;
    int err = 0;
    for (int Point = 0; Point < N; Point++) {

        // Initialise GridSimplex with the grid index of the closest of the
        // input points ("NearestNeighbour").
        Unravel2DIndex(NearestNeighbour[Point], InputShape, GridSimplex[0]);
        InitialiseTriangle(GridSimplex, InputShape);

        // Get the coordinates of the current target point
        TargetPoint = Locations[Point];

        niter = 0;
        found_it = 0;
        while (niter < Iterations) {

            // Create InputSimplex and MappedSimplex from GridSimplex
            for (int Vertex = 0; Vertex < 3; Vertex++) {
                int InputIndex = Ravel2DIndex(GridSimplex[Vertex], InputShape);
                InputSimplex[Vertex] = Coordinates[InputIndex];
                MappedSimplex[Vertex] = FwdMappedCoordinates[InputIndex];
            }

            // Check if the target point is within the input simplex
            int VertexToMirror = IsInTriangle(TargetPoint, MappedSimplex);
            if (VertexToMirror >= 0) {
                StepTriangle(GridSimplex, VertexToMirror, InputShape);
                niter++;
            } else {
                found_it = 1;
                break;  // Success
            }
        }

        // If the algorithm found the matching simplex, calculate the local
        // inverse displacement by inverting the local affine transformation.
        if (found_it) {
            double LocalAffine[6] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
            double TargetSimplex[9], SourceSimplex[9];

            // Populate simplex-coordinate matrices in column-major order
            for (int Vertex = 0; Vertex < 3; Vertex++) {
                SourceSimplex[Vertex * 3 + 0] = MappedSimplex[Vertex][0];
                TargetSimplex[Vertex * 3 + 0] = InputSimplex[Vertex][0];
                SourceSimplex[Vertex * 3 + 1] = MappedSimplex[Vertex][1];
                TargetSimplex[Vertex * 3 + 1] = InputSimplex[Vertex][1];
                SourceSimplex[Vertex * 3 + 2] = 1.0;
                TargetSimplex[Vertex * 3 + 2] = 1.0;
            }

            double sst[9];
            double tst[9];
            for (int row = 0; row < 3; row++) {
                for (int col = 0; col < 3; col++) {
                    sst[row * 3 + col] = SourceSimplex[col * 3 + row];
                    tst[row * 3 + col] = TargetSimplex[col * 3 + row];
                }
            }

            double s[3];
            int rank;
            double rcond = 1.e-8;
//            int jpvt[4] = {0, 0, 0};
//            lapack_int invretval = LAPACKE_dgels(LAPACK_COL_MAJOR, 'N', 3, 3, 3, sst, 3, tst, 3);

//            lapack_int invretval = LAPACKE_dgelsy(LAPACK_COL_MAJOR, 3, 3, 3, sst, 3, tst, 3, jpvt, rcond, &rank);
            lapack_int invretval = LAPACKE_dgelsd(
                LAPACK_COL_MAJOR, 3, 3, 3, sst, 3, tst, 3, s, rcond, &rank);

            if (invretval == 0) {

                // Populate local inverse affine
                for (int row = 0; row < 2; row++) {
                    for (int col = 0; col < 3; col++) {
                        LocalAffine[col * 2 + row] = tst[row * 3 + col];
                    }
                }

                // Use the local inverse affine to calculate the inverse displacement from the current target point
                double HomoTargetPoint[3] = {
                    TargetPoint[0], TargetPoint[1], 1.0};
                double BwdMappedTargetPoint[2] = {0.0, 0.0};
                cblas_dgemv(CblasColMajor, CblasNoTrans, 2, 3, 1.0,
                    LocalAffine, 2, HomoTargetPoint, 1, 0.0,
                    BwdMappedTargetPoint, 1);
                InvWarp[0][Point] = BwdMappedTargetPoint[0] - HomoTargetPoint[0];
                InvWarp[1][Point] = BwdMappedTargetPoint[1] - HomoTargetPoint[1];

            } else {
                for (int elem = 0; elem < 2; elem++) {
                    InvWarp[elem][Point] = INFINITY;  // null value
                }
                err++;
            }

        // If the matching simplex could not be found,
        // leave these values undefined and fill them later by local averaging.
        } else {
            for (int elem = 0; elem < 2; elem++) {
                InvWarp[elem][Point] = INFINITY;  // null value
            }
            err++;
        }
    }

    // Free heap pointers
    for (int Vertex = 0; Vertex < 3; Vertex++)
        free(GridSimplex[Vertex]);
    free(GridSimplex);
    free(InputSimplex);
    free(MappedSimplex);

    return err;

}


int CalculateLocalAffine2D(double** Locations,
    int* NearestNeighbour, int N, int* InputShape,
    double** Coordinates, double** FwdMappedCoordinates,
    double** LocalAffine, int Iterations) {

    // Declare simplex to operate on the input grid coordinates
    int** GridSimplex = calloc(sizeof(int *), 3);
        for (int Vertex = 0; Vertex < 3; Vertex++)
            GridSimplex[Vertex] = calloc(sizeof(int), 2);
    // Declare simplex to operate on the input coordinates
    double** InputSimplex = calloc(sizeof(double *), 3);
    // Declare simplex to operate on the forward-mapped coordinates
    double** MappedSimplex = calloc(sizeof(double *), 3);

    // Loop over all locations for which the LocalAffine needs to be calculated
    int niter;
    char found_it;
    int err = 0;
    for (int Point = 0; Point < N; Point++) {

        // Initialise GridSimplex with the grid index of the closest of the
        // input points ("NearestNeighbour").
        Unravel2DIndex(NearestNeighbour[Point], InputShape, GridSimplex[0]);
        InitialiseTriangle(GridSimplex, InputShape);

        // Get the coordinates of the current target point
        double* TargetPoint = Locations[Point];

        niter = 0;
        found_it = 0;
        while (niter < Iterations) {

            // Create InputSimplex and MappedSimplex from GridSimplex
            for (int Vertex = 0; Vertex < 3; Vertex++) {
                int InputIndex = Ravel2DIndex(GridSimplex[Vertex], InputShape);
                InputSimplex[Vertex] = Coordinates[InputIndex];
                MappedSimplex[Vertex] = FwdMappedCoordinates[InputIndex];
            }

            // Check if the target point is within the input simplex
            int VertexToMirror = IsInTriangle(TargetPoint, InputSimplex);
            if (VertexToMirror >= 0) {
                StepTriangle(GridSimplex, VertexToMirror, InputShape);
                niter++;
            } else {
                found_it = 1;
                break;  // Success
            }
        }

        // If the algorithm found the matching simplex, calculate the local
        // inverse displacement by inverting the local affine transformation.
        if (found_it) {
            double TargetSimplex[9], SourceSimplex[9];

            // Populate simplex-coordinate matrices in column-major order
            for (int Vertex = 0; Vertex < 3; Vertex++) {
                SourceSimplex[Vertex * 3 + 0] = InputSimplex[Vertex][0];
                TargetSimplex[Vertex * 3 + 0] = MappedSimplex[Vertex][0];
                SourceSimplex[Vertex * 3 + 1] = InputSimplex[Vertex][1];
                TargetSimplex[Vertex * 3 + 1] = MappedSimplex[Vertex][1];
                SourceSimplex[Vertex * 3 + 2] = 1.0;
                TargetSimplex[Vertex * 3 + 2] = 1.0;
            }

            double sst[9];
            double tst[9];
            for (int row = 0; row < 3; row++) {
                for (int col = 0; col < 3; col++) {
                    sst[row * 3 + col] = SourceSimplex[col * 3 + row];
                    tst[row * 3 + col] = TargetSimplex[col * 3 + row];
                }
            }

            double s[3];
            int rank;
            double rcond = 1.e-8;
            lapack_int invretval = LAPACKE_dgelsd(
                LAPACK_COL_MAJOR, 3, 3, 3, sst, 3, tst, 3, s, rcond, &rank);

            if (invretval == 0) {
                for (int row = 0; row < 2; row++) {
                    for (int col = 0; col < 3; col++) {
                        LocalAffine[Point][col * 2 + row] = tst[row * 3 + col];
                    }
                }

            } else {
                for (int elem = 0; elem < 6; elem++) {
                    LocalAffine[Point][elem] = INFINITY;  // null value
                }
                err++;
            }

        // If the matching simplex could not be found,
        // leave these values undefined and fill them later by local averaging.
        } else {
            for (int elem = 0; elem < 6; elem++) {
                LocalAffine[Point][elem] = INFINITY;  // null value
            }
            err++;
        }
    }

    // Free heap pointers
    for (int Vertex = 0; Vertex < 3; Vertex++)
        free(GridSimplex[Vertex]);
    free(GridSimplex);
    free(InputSimplex);
    free(MappedSimplex);

    return err;

}

int CalculateTxJacobian2D(double** Locations, int* NearestNeighbour,
    int N, int* InputShape, double** Coordinates,
    double** Jacobian, int** VertexIndex, int Iterations) {

    // Declare simplex to operate on the input grid coordinates
    int** GridSimplex = calloc(sizeof(int *), 3);
        for (int Vertex = 0; Vertex < 3; Vertex++)
            GridSimplex[Vertex] = calloc(sizeof(int), 2);
    // Declare simplex to operate on the input coordinates
    double** InputSimplex = calloc(sizeof(int *), 3);

    // Loop over all locations for which the Jacobian needs to be calculated
    int niter;
    char found_it;
    int err = 0;
    for (int Point = 0; Point < N; Point++) {

        // Initialise GridSimplex with the grid index of the closest of the
        // input points ("NearestNeighbour").
        Unravel2DIndex(NearestNeighbour[Point], InputShape, GridSimplex[0]);
        InitialiseTriangle(GridSimplex, InputShape);

        // Get the coordinates of the current target point
        double* TargetPoint = Locations[Point];

        niter = 0;
        found_it = 0;
        while (niter < Iterations) {

            // Create InputSimplex and MappedSimplex from GridSimplex
            for (int Vertex = 0; Vertex < 3; Vertex++) {
                int InputIndex = Ravel2DIndex(GridSimplex[Vertex], InputShape);
                InputSimplex[Vertex] = Coordinates[InputIndex];
            }

            // Check if the target point is within the input simplex
            int VertexToMirror = IsInTriangle(TargetPoint, InputSimplex);
            if (VertexToMirror >= 0) {
                StepTriangle(GridSimplex, VertexToMirror, InputShape);
                niter++;
            } else {
                found_it = 1;
                break;  // Success
            }
        }

        if (found_it) {

            // Invert the homogeneous position matrix
            double PositionMatrix[9] =
                {InputSimplex[0][0], InputSimplex[0][1], 1.0,
                 InputSimplex[1][0], InputSimplex[1][1], 1.0,
                 InputSimplex[2][0], InputSimplex[2][1], 1.0};
            lapack_int InversionRetVal = InvertMat2D(PositionMatrix, 3);

            double* a = &PositionMatrix[0];
            double x[3] = {TargetPoint[0], TargetPoint[1], 1.0};

            // Calculate the 3x2=6 partial derivatives of the local displacement
            // vector with respect to the orthogonal displacements of the
            // vertices of the enclosing triangle.
            // (p: u_0, u_1, u_2, v_0, v_1, v_2)
            // Where u is vertical displacement, v is horizontal displacement,
            // and the indices denote the vertex of the enclosing triangle.
            // Note that the vertices may not always be in the order of their
            // linear indices in the transformation grid.

            // p = u_0
            Jacobian[Point][0]  = a[0] * x[0] + a[3] * x[1] + a[6] * x[2]; // x
            Jacobian[Point][1]  = 0.0;                                     // y
            // p = u_1
            Jacobian[Point][2]  = a[1] * x[0] + a[4] * x[1] + a[7] * x[2]; // x
            Jacobian[Point][3]  = 0.0;                                     // y
            // p = u_2
            Jacobian[Point][4]  = a[2] * x[0] + a[5] * x[1] + a[8] * x[2]; // x
            Jacobian[Point][5]  = 0.0;                                     // y

            // p = v_0
            Jacobian[Point][6]  = 0.0;                                     // x
            Jacobian[Point][7]  = a[0] * x[0] + a[3] * x[1] + a[6] * x[2]; // y
            // p = v_1
            Jacobian[Point][8]  = 0.0;                                     // x
            Jacobian[Point][9]  = a[1] * x[0] + a[4] * x[1] + a[7] * x[2]; // y
            // p = v_2
            Jacobian[Point][10] = 0.0;                                     // x
            Jacobian[Point][11] = a[2] * x[0] + a[5] * x[1] + a[8] * x[2]; // y

            // Return the linear index of the vertices that were used to define
            // the Jacobian. The indices are returned in the order of their
            // appearance in the transformation Jacobian.
            for (int Vertex = 0; Vertex < 3; Vertex++) {
                VertexIndex[Point][Vertex] =
                    Ravel2DIndex(GridSimplex[Vertex], InputShape);
            }
        }

        // If the matching simplex could not be found,
        // leave these values undefined.
        else {
            for (int elem = 0; elem < 12; elem++) {
                Jacobian[Point][elem]  = INFINITY;  // null value
            }

            // Fill the vertex indices with -1 (error value)
            for (int Vertex = 0; Vertex < 3; Vertex++) {
                VertexIndex[Point][Vertex] = -1;
            }

            err++;
        }
    }

    // Free heap pointers
    for (int Vertex = 0; Vertex < 3; Vertex++)
        free(GridSimplex[Vertex]);
    free(GridSimplex);
    free(InputSimplex);

    return err;

}

int FillHoles2D(double** InvWarp, int* Shape, int MaxIter) {

    int cnt = INT_MAX;
    int niter = 0;
    if (MaxIter == 0) MaxIter = INT_MAX;
    while ((cnt > 0) && (niter < MaxIter)) {
        cnt = 0;
        niter++;
        for (int dim = 0; dim < 2; dim++) {
            double* ScalarField = InvWarp[dim];
            for (int i = 0; i < Shape[0]; i++) {
                for (int j = 0; j < Shape[1]; j++) {
                    int Index = i * Shape[0] + j;
                    if (isinf(ScalarField[Index])) {
                        double val = 0.0;
                        double sum = 0.0;
                        int n = 0;
                        // Vertical neighbours
                        if (i > 0) {
                            val = ScalarField[(i - 1) * Shape[0] + j];
                            if (!isinf(val)) { sum += val; n++; }
                        }
                        if (i < (Shape[0] - 1)) {
                            val = ScalarField[(i + 1) * Shape[0] + j];
                            if (!isinf(val)) { sum += val; n++; }
                        }
                        // Horizontal neighbours
                        if (j > 0) {
                            val = ScalarField[i * Shape[1] + j - 1];
                            if (!isinf(val)) { sum += val; n++; }
                        }
                        if (j < (Shape[1] - 1)) {
                            val = ScalarField[i * Shape[1] + j + 1];
                            if (!isinf(val)) { sum += val; n++; }
                        }
                        if (n > 0) {
                            ScalarField[Index] = sum / (double)n;
                            cnt++;
                        }
                    }
                }
            }
        }
    }
    return 0;
}

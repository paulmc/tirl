#!/usr/bin/env python

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np

# TIRL IMPORTS

from tirl.domain import Domain


# DEFINITIONS

from tirl.constants import *


# IMPLEMENTATION


class Point(Domain):
    """
    Point implements a special case of Domain, when the extent is a single
    point. It is in essence a non-compact Domain, but it is a special subclass
    for added convenience features.

    """

    def __new__(cls, coordinates, transformations, name=None):
        """
        Construction of Point.

        """
        # Return existing Point without copying
        if isinstance(coordinates, Point):
            return coordinates

        # Create new Point (as non-compact Domain)
        coordinates = np.atleast_2d(coordinates)
        if len(coordinates) != 1:
            raise AssertionError("Only one set of coordinates should be given "
                                 "to initialise Point!")
        obj = super(Point, cls).__new__(
            cls, coordinates, transformations=transformations,
            offset=None, name=name, storage=MEM)
        return obj

    def __init__(self, coordinates, transformations, name=None):
        """
        Initialisation of Point.

        """
        if isinstance(coordinates, Point):
            pass
        else:
            coordinates = np.atleast_2d(coordinates)
            super(Point, self).__init__(
                coordinates, transformations=transformations,
                offset=None, name=name, storage=MEM)

    @property
    def x(self):
        return self.get_intermediate_coordinates().ravel()

    @property
    def y(self):
        return self.get_physical_coordinates().ravel()

    def __bool__(self):
        """
        The truth value of a Point is True, if it is different from
        the origin. This attribute has a special meaning for rotations.

        """
        return not np.allclose(self.y, 0)

    def copy(self):
        """
        Copy constructor.

        """
        return type(self)(self.x, transformations=self.chain, name=self.name)

    def _dump(self):
        """
        Creates TIRLObject dump.

        """
        # Call TIRLObject's _dump method
        objdump = super(Domain, self)._dump()
        objdump.update({
            "coordinates": self.x.tolist(),
            "chain": self.chain,
            "name": self.name
        })
        return objdump

    @classmethod
    def _load(cls, dump):
        """
        Creates a Point object from a TIRLObject dump.

        """
        coordinates = dump.pop("coordinates")
        chain = dump.pop("chain")
        name = dump.pop("name")
        return cls(coordinates, transformations=chain, name=name)


if __name__ == "__main__":
    print("This module is not intended for direct execution!")

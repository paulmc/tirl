#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEFINITIONS
import os.path

_smm = None      # global SharedMemoryManager


# DESCRIPTION

"""
Tensor Image Registration Library (TIRL) is an open-source general-purpose
image registration platform that integrates into the FMRIB Software Library
(FSL) and allows rapid prototyping of bespoke image registration pipelines.

TIRL was originally developed for registering sparsely sampled small
histological sections to whole-brain post-mortem MRI. However the variety
and customisability of the components should make TIRL suitable for a wider
range of applications.


If you use TIRL in your research, please cite:

IN Huszar, M Pallebage-Gamarallage, S Foxley, BC Tendler, A Leonte, M Hiemstra,
J Mollink, A Smart, S Bangerter-Christensen, H Brooks, MR Turner, O Ansorge,
KL Miller, M Jenkinson; Tensor Image Registration Library: Automated Non-Linear
Registration of Sparsely Sampled Histological Specimens to Post-Mortem MRI of
the Whole Human Brain; bioRxiv 849570; doi: https://doi.org/10.1101/849570

"""


# DEPENDENCIES




# PACKAGE IMPORTS


# DEFINITIONS

__author__ = "Istvan N. Huszar"
__copyright__ = "Copyright (C) 2018-2023 University of Oxford"
__credits__ = ["Mark Jenkinson"]
__license__ = "FSL"
__version__ = "3.6.2"
__maintainer__ = "Istvan N Huszar"
__status__ = "active"


# IMPLEMENTATION

def home(*args) -> str:
    """ 
    Returns the home directory of TIRL (if no arguments are specified).
    Arguments are interpreted as elements of a relative pathology from TIRL's
    home directory. In this case, the full absolute pathology to the specified
    object will be returned.

    """
    import os
    homedir = os.path.dirname(__file__)
    if args:
        return os.path.join(homedir, *args)
    else:
        return homedir


def load(fname):
    """
    Loads a TIRL object from file.

    :param fname: file pathology
    :type fname: str
    :returns: TIRLObject of the respective kind
    :rtype: TIRLObject

    """
    from tirl.tirlobject import TIRLObject
    return TIRLObject.load(fname)


def srchash(fname):
    from hashlib import sha1
    with open(fname, "rb") as fp:
        hash = sha1(fp.read()).hexdigest()
    return hash


def testimg(name="astronaut"):
    from tirl.timage import TImage
    from skimage import data
    return TImage(getattr(data, name)())

#!/usr/bin/env python

# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np
from functools import reduce

# TIRL IMPORTS

from tirl.utils import rcopy
from tirl.tirlobject import TIRLObject
from tirl.domain import Domain
from tirl.tfield import TField
from tirl.transformations.scale import TxScale
from tirl.utils import kw


# DEFINITIONS

from tirl.constants import *
import tirl.settings as ts

# DEVELOPMENT NOTES


# IMPLEMENTATION

class ResolutionManager(TIRLObject):
    """
    Plug-in class for managing different resolution layers of a TImage.

    """

    @classmethod
    def _constr_layers(cls, *layers, **kwargs):
        """
        Creates a ResolutionManager from a list of layers.

        :param layers: TField layers
        :type layers: TField

        :Keyword Arguments:
            * *altbase*=True (``bool``) -- If True, new layers are generated
            from the layer with the lowest resolution. This significantly
            speeds up computations but it may reduce data quality. If False,
            all new layers are generated from the native layer.

            * *presmooth*=True (``bool``) -- If True, the template layer is
            smoothed with a Gaussian kernel (FWHM=sampling factor) when a
            lower-resolution layer is generated. This ensures that information
            from all higher-resolution voxels is represented at the lower
            resolution as well. If False, resampling becomes faster, but newly
            created layers may appear noisy/granular.

            * *update_chain*=True (``bool``) -- If True, all nonlinear
            transformations that are linked dynamically to the active layer
            will be resampled to match the resolution of the new layer. If
            False, nonlinear transformations will not be resampled upon
            generating new layers.

            * *resolution*=1 (``Number or Iterable[Number]``) Resolution of the
            native layer in a.u./px. Anisotropic resolution should be entered
            as an iterable.

        """
        if not all(isinstance(layer, TField) for layer in layers):
            raise TypeError("All input layers must be TField instances.")
        obj = super().__new__(cls)
        obj.layers = [*layers]
        rmkwargs = dict(
            presmooth=True,
            update_chain=True,
            altbase=True
        )
        rmkwargs.update(kw(kwargs, "update_chain", "altbase", "presmooth"))
        obj.kwargs = rmkwargs
        obj.set(0)
        resolution = kwargs.get("resolution", obj.layers[0].resolution)
        if resolution is not None:
            obj.set_resolution(resolution, layer=0)
        return obj

    def __new__(cls, *args, **kwargs):
        """
        Main constructor of ResolutionManager. Construction is supported from
        any of the following specifications (in their order of preference):

        :type args: Union[ResolutionManager, TField, np.ndarray]

        1. ResolutionManager instance (configured)
        2. ResolutionManager(f0, f1, ...): multiple TField inputs (f0 is native)
        3. ResolutionManager(**kwargs): suitable inputs for the native TField

        :Keyword Arguments:
            * *altbase*=True (``bool``) -- If True, new layers are generated
            from the layer with the lowest resolution. This significantly
            speeds up computations but it may reduce data quality. If False,
            all new layers are generated from the native layer.

            * *presmooth*=True (``bool``) -- If True, the template layer is
            smoothed with a Gaussian kernel (FWHM=sampling factor) when a
            lower-resolution layer is generated. This ensures that information
            from all higher-resolution voxels is represented at the lower
            resolution as well. If False, resampling becomes faster, but newly
            created layers may appear noisy/granular.

            * *update_chain*=True (``bool``) -- If True, all nonlinear
            transformations that are linked dynamically to the active layer
            will be resampled to match the resolution of the new layer. If
            False, nonlinear transformations will not be resampled upon
            generating new layers.

        """
        # Construction from an existing ResolutionManager instance
        resmgr = kwargs.pop("resmgr", None)
        if args and isinstance(args[0], cls):
            if resmgr is None:
                resmgr = args[0]
            else:
                raise AssertionError("Multiple inputs for resmgr.")
        if resmgr is not None:
            if not kwargs:
                return resmgr
            else:
                return cls._constr_layers(*resmgr.layers, **kwargs)

        # Construction from TField inputs
        elif args and all(isinstance(arg, TField) for arg in args):
            return cls._constr_layers(*args, **kwargs)

        # Construction from TField-like inputs (for the native layer)
        else:
            try:
                native = TField(*args, **kwargs)
                return cls._constr_layers(native, **kwargs)
            except:
                raise AssertionError("Invalid constructor configuration.")

    def __init__(self, *args, **kwargs):
        """
        Initilisation of ResolutionManager.

        """
        super(ResolutionManager, self).__init__()

    def __repr__(self):
        rs = ", ".join(format(dim, ".4f") for dim in self.get_resolution())
        rs = f"({rs.rstrip(', ')})"
        return f"{type(self).__name__}[" \
               f"layers={len(self.layers)}, active={self.active}, " \
               f"resolution={rs}]"

    # Protected methods

    def _get_domain_and_base(self, newsize):
        """
        Generates new Domain object for the new resolution. Both the internal
        and external transformations of the native domain are preserved
        (by reference).

        """
        # Synchronise chains between all layers
        self.sync()

        # Create new domain
        d = self.base.domain
        invtx = TxScale(*np.divide(d.shape, newsize), lock="all",
                        name="resample")
        newdomain = Domain(
            newsize, transformations=d.chain, offset=[invtx, *d.offset],
            name=None, storage=d.storage, memlimit=d.memlimit)

        # If the "alternative base" option is True, use the smallest
        # possible resolution as the base. For large images this can save a lot
        # of computation time and memory, but may compromise data quality.
        lowest = 0  # default to the native resolution
        if self.kwargs.get("altbase", False):
            for layer_no, layer in enumerate(self.layers):
                if np.all(np.less_equal(newsize, layer.domain.shape)):
                    if layer.domain.numel < self.layers[lowest].domain.numel:
                        lowest = layer_no

        return newdomain, self.layers[lowest]

    def _get_layer(self, sig):
        """
        Retrieves image layer by domain signature. Returns None if the
        requested signature is not found.

        """
        for layer_no, layer in enumerate(self.layers):
            if sig == layer.domain.signature()(0):
                return layer_no
        else:
            return None

    def _resize(self, *newsize, **layerkwargs):
        """
        Retrieves TImage layer as specified by the image (domain) size.
        Generates new TImage layer if the requested layer does not exist.

        :param newsize: new shape of the TImage
        :type newsize: int
        :param layerkwargs: metadata for the new TImage layer
        :type layerkwargs: any

        """
        # Syntactic flexibility: allow shape input as tuple
        if len(newsize) > 0 and hasattr(newsize[0], "__iter__"):
            newsize = newsize[0]

        assert len(newsize) == self.base.domain.ndim, \
            f"Dimension mismatch ({self.base.domain.ndim} " \
            f"vs. {len(newsize)}) " \
            f"between old shape {self.base.domain.shape} " \
            f"and new shape {newsize}."

        # Generate new layer by retrieval or evaluation
        newdomain, base = self._get_domain_and_base(newsize)
        newlayer = self._get_layer(newdomain.signature()(0))
        if newlayer is None:
            if self.kwargs.get("presmooth", ts.TFIELD_PRESMOOTH):
                base_smooth = self._presmooth(base, newdomain)
                base_smooth.domain.reset(internal=False)
                layer = base_smooth.evaluate(newdomain[:0], rule=None)
                layer.domain = newdomain
                del base_smooth
            else:
                base = base.copy()
                base.domain.chain = []
                layer = base.evaluate(newdomain[:0], rule=None)
                layer.domain = newdomain
            self.layers.append(layer)
            newlayer = len(self.layers) - 1

        # Ensure that the chain of the new layer is synchronised with the
        # active layer. If update_chain is true, rescale all nonlinear
        # transformations that were dynamically linked to the active layer.
        self._update_chain(
            new_domain=self.layers[newlayer].domain,
            old_domain=self.get().domain[:],
            force_links=self.kwargs.get("update_chain", True)
        )

        return newlayer

    def _rescale(self, *scales, **layerkwargs):
        """
        Retrieve TImage layer as specified by a global scaling factor or
        dimension-specific scale factors. Generate new TImage layer if the
        requested layer does not exist. The new dimensions of the TImage are
        set to the nearest integer value after scaling.

        :param scales: dimension-specific scaling factors
        :type scales: float
        :param layerkwargs: metadata for the new TImage layer
        :type layerkwargs: any

        """
        # Syntactic flexibility: allow scaling vector input
        if len(scales) > 0 and hasattr(scales[0], "__iter__"):
            scales = scales[0]
        assert all(factor > 0 for factor in scales), \
            "All scale factors must be positive."
        if len(scales) == 1:
            scales = scales * self.base.domain.ndim
        assert len(scales) == self.base.domain.ndim

        newsize = \
            np.round(np.multiply(self.base.domain.shape, scales)).astype(int)
        return self._resize(*newsize, **layerkwargs)

    def _dump(self):
        objdump = super(ResolutionManager, self)._dump()
        objdump.update(
            layers=[*self.layers],  # to avoid in-place replacement with a dict
            active=self.active,
            kwargs=rcopy(self.kwargs)
        )
        return objdump

    @classmethod
    def _load(cls, dump):
        layers = dump["layers"]
        active = int(dump["active"])
        kwargs = dump["kwargs"]
        obj = cls(*layers, **kwargs)
        obj.set(active)
        return obj

    @staticmethod
    def _presmooth(base, new_domain):
        """
        Applies Gaussian smoothing to the TImage layer (TField) before it
        is sampled. The smoothing radius is calculated from the scaling factors.

        """
        # TODO: Make this more efficient by constraining to sampled points only
        from tirl.operations.spatial import SpatialOperator
        from scipy.ndimage.filters import gaussian_filter

        scales = np.divide(new_domain.shape, base.domain.shape)
        coeff = 1. / (2. * np.sqrt(2 * np.log(2)))
        t_sigmas = (0,) * len(base.taxes)
        v_sigmas = tuple(1 / s * coeff if s < 1 else 0 for s in scales)
        sigmas = t_sigmas + v_sigmas  # operator uses TENSOR_MAJOR order
        kernelsize = ts.TFIELD_PRESMOOTH_KERNELSIZE_NSIGMA
        neighbourhood = int(max(np.ceil(kernelsize * max(sigmas)), 1))
        smop = SpatialOperator(
            gaussian_filter, radius=neighbourhood, name="smooth",
            opkwargs=dict(sigma=sigmas, truncate=kernelsize))
        # Force cast to floating-point type if the input is integer
        if np.issubdtype(base.dtype, np.integer):
            base = base.astype(ts.DEFAULT_FLOAT_TYPE)
        return smop(base)

    @staticmethod
    def _update_chain(new_domain, old_domain, force_links=True):
        """
        Copies the transformation chain from an existing domain to the current
        domain. This method is useful when a TField or a TImage is
        resampled to a new grid.

        :param old_domain: template domain
        :type old_domain: Domain
        :param force_links:
            If True, Non-linear transformations that are not dynamically linked
            to the new domain, will be recreated so that they will be also
            linked to the current domain. If False, these transformations will
            be statically linked to the new domain.
        :type force_links: bool

        """
        new_domain.chain = old_domain.chain
        if force_links:
            for i, tx in enumerate(old_domain.chain):
                if tx.kind == TX_NONLINEAR:
                    # Only regrid grid-like domains
                    if not tx.domain.iscompact:
                        continue
                    # If the transformation is not dynamically linked to
                    # the current domain, re-grid that transformation.
                    if (tx.domain != new_domain[:i]) and \
                            (tx.domain == old_domain[:i]):
                        oldtxdom = tx.domain

                        # Erase transformations on the tx domain
                        tx.metaparameters["domain"] = old_domain[:0]
                        # Resample between voxel domains
                        new_tx = tx.regrid(new_domain[:0])
                        # Add back all new antecedent transformations
                        new_tx.metaparameters["domain"] = new_domain[:i]
                        # Add the current transformation to the new domain
                        new_domain.chain[i] = new_tx
                        # Restore domain in the original tx
                        tx.metaparameters["domain"] = oldtxdom

    # Public attributes

    @property
    def base(self):
        """
        Returns the native resolution layer.

        """
        return self.layers[0]

    @property
    def active(self):
        """
        Returns the numerical ID of the active layer.

        """
        return int(self._active)

    # Public methods

    def copy(self):
        layers = [layer.copy() for layer in self.layers]
        obj = type(self)(*layers, **rcopy(self.kwargs))
        obj.set(self.active)
        obj.set_resolution(self.get_resolution(layer=0), layer=0)
        return obj

    def get(self, num=None):
        """
        Returns a specific layer or the active layer if no argument is
        specified.

        :param num: layer number
        :type num: int
        :rtype: TField

        """
        num = num or self.active
        num = num % len(self.layers)
        return self.layers[num]

    @property
    def layers(self):
        return self._layers

    @layers.setter
    def layers(self, ls):
        if not hasattr(ls, "__iter__"):
            ls = [ls]
        assert all(isinstance(layer, TField) for layer in ls), \
            "Input must be TField instances."
        self._layers = list(ls)

    def set(self, num):
        """
        Sets the active resolution layer.

        :param num: layer number (0 is the native layer)
        :type num: int

        """
        # Synchronise chains
        # self.sync()
        # Set active layer
        num = num % len(self.layers)
        if num < len(self.layers):
            self._active = num
        else:
            raise ValueError(
                f"Invalid resolution layer (0-{len(self.layers) - 1}): {num}")

    def delete(self, *res_id):
        """
        Delete one or more TImage layers.

        :param res_id: TImage layer IDs to be deleted
        :type res_id: Union[str,int]

        """
        oldactive = self.active
        self.layers = [layer for i, layer in enumerate(self.layers)
                       if i not in res_id]
        # If the active layer is deleted, revert to the native resolution.
        if oldactive in res_id:
            self.set(0)

    def nbytes(self):
        """
        Size of all data in all layers in bytes.

        """
        return reduce(sum, (layer.nbytes for layer in self.layers))

    def revert(self):
        """
        Delete all TImage layers, keeping only the native layer.

        """
        self.layers = self.layers[0]
        self.set(0)

    def reset(self):
        """
        Delete all TImage layers, except for the active one.

        """
        self.layers = [self.get()]
        self.set(0)

    def resize(self, *newsize, **layerkwargs):
        """
        Resizes TImage data to the specified shape, sets the new layer as the
        active one, and returns the new layer as a TField instance.

        """
        newlayer = self._resize(*newsize, **layerkwargs)
        self.set(newlayer)
        return self.layers[newlayer]

    def rescale(self, scale, *scales, **layerkwargs):
        """
        Rescales TImage data by the specified factor(s), sets the new layer
        as the active one, and returns the new layer as a TField instance.

        """
        newlayer = self._rescale(scale, *scales, **layerkwargs)
        self.set(newlayer)
        return self.layers[newlayer]

    def get_resolution(self, layer=None):
        """
        Retrieves the resolution of a specified image layer, based on the preset
        resolution of the native layer. If no layer is specified, the
        resolution of the active layer is returned.

        :param layer: image layer for which the resolution will be set
        :type layer: int

        :returns: resolution vector
        :rtype: tuple[float]

        """
        layer_no = self.active if layer is None else layer % len(self.layers)
        return self.layers[layer_no].resolution

    def set_resolution(self, res, layer=None):
        """
        Sets image resolution in a.u./px on the specified layer. If no layer is
        specified, the resolution will be set for the active layer. The
        resolution is a global property: changing it on one layer will affect
        all layers. The default resolution is 1 a.u./px at the native layer.

        :param res: resolution in a.u./px (most commonly mm/px)
        :type res: Union[int, float, tuple[int, float]]
        :param layer: image layer for which the resolution will be set
        :type layer: int

        """
        layer_no = self.active if layer is None else layer % len(self.layers)
        for layer in self.layers:
            scales = np.divide(
                self.layers[layer_no].domain.shape, layer.domain.shape)
            layer.resolution = np.multiply(res, scales)

    def sync(self):
        """
        Synchronises the Chain between all layers, such that all layers have
        the same domain transformations as the active layer.

        """
        master_chain = self.get().domain.chain
        for layer in self.layers:
            layer.domain.chain = master_chain

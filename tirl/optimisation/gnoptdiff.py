#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEVELOPMENT NOTES

"""
This optimiser is specifically suited for optimising a dense displacement field
with diffusion regularisation on the displacement vectors. The implementation
follows a Matlab precursor, which was originally written by Mattias P. Henrich.

"""


# DEPENDENCIES

import numpy as np
from time import time
import scipy.sparse as sp
from numbers import Integral, Real
from scipy.sparse.linalg import minres

# TIRL IMPORTS

from tirl.optimisation.optimiser import Optimiser
from tirl.regularisation.diffusion import DiffusionRegulariser
from tirl.transformations.displacement import TxDisplacementField


# DEFINITIONS

from tirl.constants import *

NO_PARAMETERS = 0
MAXITER_REACHED = 1
MAXTIME_REACHED = 2
XTOL_REL_REACHED = 3
XTOL_ABS_REACHED = 4
FTOL_REL_REACHED = 5
FTOL_ABS_REACHED = 6
COSTVAL_REACHED = 7
ASCENT = -1


# IMPLEMENTATION


class GNOptimiserDiffusion(Optimiser):
    """
    GNOptimiserDiffusion -
        optimises a dense displacement field using Gauss-Newton iterations with
        diffusion regularisation.

    """
    RESERVED_KWARGS = ("target", "cost_items", "maxiter", "maxtime", "xtol_rel",
                       "xtol_abs", "ftol_rel", "ftol_abs", "costval", "logger",
                       "metaparameters")

    def __init__(self, target, *cost_items, maxiter=100, maxtime=0, xtol_rel=0,
                 xtol_abs=0, ftol_rel=0, ftol_abs=0, costval=None, logger=None,
                 **metaparameters):
        """
        Initialisation of GNOptimiserDiffusion.

        :param target:
            Optimisation target. A Transformation or an OptimisationGroup that
            specifies the parameters for optimisation. Note that only the free
            (not locked) parameters will be optimised.
        :param cost_items:
        :param maxiter:
        :param maxtime:
        :param xtol_rel:
        :param xtol_abs:
        :paraam ftol_rel:
        :param ftol_abs:
        :param costval:
        :param metaparameters:

        """
        # Call parent-class initialisation
        super(GNOptimiserDiffusion, self).__init__(
            target, *cost_items, logger=logger, **metaparameters)

        # Set class-specific metaparameters
        self.maxiter = maxiter
        self.maxtime = maxtime
        self.xtol_rel = xtol_rel
        self.xtol_abs = xtol_abs
        self.ftol_rel = ftol_rel
        self.ftol_abs = ftol_abs
        self.costval = costval

        # This optimiser class only allows a very specific set of inputs.
        if not isinstance(self.transformation, TxDisplacementField):
            raise AssertionError(f"{self.__class__.__name__} requires a dense "
                                 f"displacement field to optimise.")
        if not self.transformation.domain.iscompact:
            raise AssertionError(f"{self.__class__.__name__} requires a dense "
                                 f"displacement field defined on a compact "
                                 f"domain.")
        if len(self.costs) != 1:
            raise AssertionError(f"{self.__class__.__name__} only allows a "
                                 f"single cost term.")
        if not self.regularisers:
            self.regularisers.append(DiffusionRegulariser(self.transformation))
        elif not isinstance(self.regularisers[0], DiffusionRegulariser):
            raise AssertionError(f"{self.__class__.__name__} requires a "
                                 f"diffusion regulariser.")

        # Identify the source and the target images
        self.moving = self.costs[0]._source  # prefiltered source image
        self.fixed = self.costs[0].target   # prefiltered target image

        # Obtain the combined voxel-to-voxel transfomation chain
        self.chain = self.fixed.domain.offset + \
                     self.fixed.domain.chain + \
                     self.moving.domain.chain.inverse() + \
                     self.moving.domain.offset.inverse()

        # The optimised transformation must be in the chain
        ids = [id(tx) for tx in self.chain]
        if id(self.transformation) not in ids:
            raise AssertionError("The transformation selected for optimisation "
                                 "does not affect the alignment of the images "
                                 "specified by the cost term.")

        # Initialise previous parameter update vector and previous cost
        # (these are initial values for convergence testing)
        self.dp0 = np.zeros(self.transformation.parameters.size)
        self.f0 = np.inf

    @property
    def maxiter(self):
        return self.metaparameters.get("maxiter")

    @maxiter.setter
    def maxiter(self, count):
        if isinstance(count, Integral) and (count >= 0):
            self.metaparameters.update(maxiter=int(count))
        else:
            raise ValueError(f"Expected a nonnegative integer for maximum "
                             f"iteration count, got {count} instead.")

    @property
    def maxtime(self):
        return self.metaparameters.get("maxtime")

    @maxtime.setter
    def maxtime(self, interval):
        if isinstance(interval, Real) and (interval >= 0):
            self.metaparameters.update(maxtime=float(interval))
        else:
            raise ValueError(f"Expected a nonnegative float for maximum "
                             f"optimisation time, got {interval} instead.")

    @property
    def xtol_rel(self):
        return self.metaparameters.get("xtol_rel")

    @xtol_rel.setter
    def xtol_rel(self, val):
        if isinstance(val, Real) and (val >= 0):
            self.metaparameters.update(xtol_rel=float(val))
        else:
            raise ValueError(f"Expected a nonnegative float for relative "
                             f"parameter change, got {val} instead.")

    @property
    def xtol_abs(self):
        return self.metaparameters.get("xtol_abs")

    @xtol_abs.setter
    def xtol_abs(self, val):
        if isinstance(val, Real) and (val >= 0):
            self.metaparameters.update(xtol_abs=float(val))
        else:
            raise ValueError(f"Expected a nonnegative float for absolute "
                             f"parameter change, got {val} instead.")

    @property
    def ftol_rel(self):
        return self.metaparameters.get("ftol_rel")

    @ftol_rel.setter
    def ftol_rel(self, val):
        if isinstance(val, Real) and (val >= 0):
            self.metaparameters.update(ftol_rel=float(val))
        else:
            raise ValueError(f"Expected a nonnegative float for relative "
                             f"cost value change, got {val} instead.")

    @property
    def ftol_abs(self):
        return self.metaparameters.get("ftol_abs")

    @ftol_abs.setter
    def ftol_abs(self, val):
        if isinstance(val, Real) and (val >= 0):
            self.metaparameters.update(ftol_abs=float(val))
        else:
            raise ValueError(f"Expected a nonnegative float for absolute "
                             f"cost value change, got {val} instead.")

    @property
    def costval(self):
        return self.metaparameters.get("costval")

    @costval.setter
    def costval(self, val):
        if isinstance(val, Real):
            self.metaparameters.update(costval=float(val))
        elif val is None:
            self.metaparameters.update(costval=None)
        else:
            raise ValueError(f"Expected a float for target cost function "
                             f"value, got {val} instead.")

    def __call__(self, *args, **kwargs):
        """
        Performs Gauss-Newton optimisation.

        """
        niter = 0
        start_time = time()
        ndim = self.fixed.domain.ndim
        numel = self.fixed.domain.numel
        self.f0 = [self._get_total_cost()]
        format = "csr"

        # Create regularisation
        regulariser = self.regularisers[0]
        L = regulariser.D(2)
        L = sp.block_diag([L] * ndim, format=format)

        vc = self.transformation.domain.get_voxel_coordinates()
        pc = self.transformation.domain.get_physical_coordinates()
        chain = self.transformation.domain.offset + \
            self.transformation.domain.chain
        invchain = None
        if self.transformation.mode == NL_ABS:
            invchain = chain.inverse()
        target = self.costs[0].target

        edgemask = np.zeros(target.vshape)
        for dim in range(target.vdim):
            slicer = (Ellipsis, [0, -1], *(slice(None),) * (target.vdim - 1 - dim))
            edgemask[slicer] = 1
        else:
            edge = np.flatnonzero(edgemask)
        del edgemask

        while True:
            niter += 1
            source = self.costs[0].source  # evaluate only once!
            # Obtain cost differential
            S = (source - target).dataview(VOXEL_MAJOR).reshape(numel, -1, 1)

            # Apply mask on the dissimilarity vector (S)
            combined_mask = self.costs[0].combine_masks(
                target_mask=target.mask,
                source_mask=source.mask
            )
            S = S * combined_mask.reshape(-1, 1, 1)
            # If the transformation parameters are vectors in physical space,
            # then so should be the cost differentials. I.e. if the tx is
            # absolute, map the derivatives.
            dS = self.costs[0].dx(source=source).reshape(numel, -1, ndim)  # rel
            dS[edge, ...] = 0  # Neumann
            # S[edge, ...] = 0  # Dirichlet
            # Note: dx() already applied the mask!
            # dS = dS * combined_mask.ravel()[:, np.newaxis, np.newaxis]
            SdS = np.mean(S * dS, axis=1)
            SdS = SdS.T.ravel()
            dSTdS = dS[..., np.newaxis] @ dS[..., np.newaxis, :]
            dSTdS = np.mean(dSTdS.reshape((numel, -1, ndim, ndim)), axis=1)
            row = []
            diags = []
            for r in range(ndim):
                for c in range(ndim):
                    row.append(sp.spdiags(dSTdS[..., r, c].ravel(), [0],
                                          numel, numel, format=format))
                else:
                    diags.append(row)
                    row = []

            # Set up linear system
            A = sp.bmat(diags, format=format) - L
            vectors = self.transformation.vectors()  # rel or abs
            if self.transformation.mode == NL_ABS:
                vectors = invchain.map_vector(vects=vectors, coords=pc)
            b = L @ vectors.T.ravel() - SdS

            # Solve linear system
            updates = minres(A, b)[0]  # rel or abs
            self.log(str((
                np.percentile(updates, 5), np.percentile(updates, 95))))
            # self.log(str((np.min(updates), np.max(updates))))

            # Update parameters and evaluate solution and convergence
            if self.transformation.mode == NL_ABS:
                updates = updates.reshape(ndim, -1).T
                updates = chain.map_vector(vects=updates, coords=vc)
                updates = updates.T.ravel()
            self.transformation.parameters.parameters[:] += updates
            if self._converged(niter, start_time, updates):
                break

        # As the transformation parameters are update in-place, there is no
        # need to return them from this function.

    def _converged(self, niter, start_time, dp):

        # Convergence by reaching maximum iteration count
        if (self.maxiter > 0) and (niter >= self.maxiter):
            return MAXITER_REACHED
        if (self.maxtime > 0) and (time() - start_time >= self.maxtime):
            return MAXTIME_REACHED

        # Convergence by diminishing relative parameter updates
        dp = np.linalg.norm(dp, 1)
        p0 = np.linalg.norm(self.transformation.parameters, 1)
        if (self.xtol_rel > 0) and (dp < self.xtol_rel * p0):
            return XTOL_REL_REACHED
        # Convergence by diminishing absolute parameter updates
        if (self.xtol_abs > 0) and (dp < self.xtol_abs):
            return XTOL_ABS_REACHED

        if (self.costval is None) and (self.ftol_rel is None) and \
                (self.ftol_abs is None):
            return False

        f = self._get_total_cost()

        # Convergence by reaching a specific cost value
        if self.costval and (f <= self.costval):
            return COSTVAL_REACHED
        # Convergence by diminishing relative cost update
        if (self.ftol_rel > 0) and \
                (np.abs(f - self.f0[0]) < np.abs(self.ftol_rel * self.f0[0])):
            return FTOL_REL_REACHED
        # Convergence by diminishing absolute cost update
        if (self.ftol_abs > 0) and (f - self.f0[0] < self.ftol_abs):
            return FTOL_ABS_REACHED

        # No convergence yet -> update the last cost value
        self.f0[0] = f
        return False

    def _get_total_cost(self):
        """
        Calculates the total scalar cost, which is the sum of all image
        similarity cost terms and all transformation regularisation terms.

        """
        if self.metaparameters.get("visualise", False):
            from tirl import tirlvision
            tirlvision.call("gnoptdiff", "show_image", self)

        all_cost = self.costs + self.regularisers
        return sum(c() for c in all_cost)

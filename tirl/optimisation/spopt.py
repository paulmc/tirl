#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

from attrdict import AttrMap
from scipy import optimize
from scipy.optimize import minimize, Bounds


# TIRL IMPORTS

from tirl.optimisation.optimiser import Optimiser


# IMPLEMENTATION

class SPOpt(Optimiser):
    """
    SPOpt class - TIRL optimiser class that wraps scipy.optimize.minimize().

    """

    def __init__(self, target, *cost_items, method="Powell", jac="2-point",
                 hess="2-point", hessp=None, constraints=(), tol=None,
                 callback=None, options=None, logger=None, **metaparameters):
        """
        Initialisation of SPOpt.

        :param target:
            Transformation or OptimisationGroup object whose parameters will
            be optimised. During the optimisation, the parameters of this
            object will be updated in place.
        :type target:
            Transformation or OptimisationGroup
        :param cost_items:
            One or more Cost or Regulariser objects that together define the
            objective function for the optimisation.
        :type cost_items: Cost or Regulariser
        :param metaparameters:
            Additional keyword argument to the Optimiser object.
        :type metaparameters: Any

        Bounds specifications are disabled for __init__(). These should be set
        and updated directly within the Transformation or OptimisationGroup.

        """
        super(SPOpt, self).__init__(
            target, *cost_items, logger=logger, **metaparameters)

        self.metaparameters.update(
            method=method,
            jac=jac,
            hess=hess,
            hessp=hessp,
            constraints=constraints,
            tol=tol,
            callback=callback,
            options=options,
        )

    def __call__(self, *args, **kwargs):
        """
        Perform optimisation by calling scipy.optimize.minimize.

        """
        p = AttrMap(self.metaparameters)

        # Define parameter bounds using SciPy's Bounds object
        lb, ub = self.transformation.get_bounds().T
        bounds = Bounds(lb, ub)

        # Introducing a level of indirection because scipy.minimize requires
        # parameters in ndarray format, and the objective function must take
        # the parameter vector as its first argument.
        parameters = self.transformation.get()

        def visualise(x):
            self.log(x)
            src = self.costs[0].source
            trg = self.costs[0].target
            (src - trg).preview()

        def printparams(x):
            print(x)

        def fprime(x, *args):
            return optimize.optimize.approx_fprime(
                x, self.measure_cost, 0.00001)

        def fpp(x, p, *args):
            return optimize.optimize.approx_fhess_p\
                (x, p, fprime, 0.00001, *args)

        method = self.metaparameters.get("method")
        jac = self.metaparameters.get("jac")
        hess = self.metaparameters.get("hess")
        hessp = self.metaparameters.get("hessp")
        if method.lower() in ("newton-cg", "trust-ncg", "trust-krylov"):
            if (jac is None) or (jac == "2-point"):
                self.metaparameters.update(jac=fprime)
            if hess in ("2-point", "3-point") and (hessp is None):
                self.metaparameters.update(hess=None)
                self.metaparameters.update(hessp=fpp)

        # Perform the optimisation
        opt = minimize(self.measure_cost, parameters, method=p.method,
                       jac=p.jac, hess=p.hess, hessp=p.hessp, bounds=bounds,
                       constraints=p.constraints, tol=p.tol,
                       callback=printparams, options=p.options)

        # Set the optimum parameters
        self.transformation.set(opt.x)

    def measure_cost(self, x, *args):
        """
        Sets the parameters and returns the current total cost. This method
        constitutes an indirection from the total_cost method, and is needed
        for compatibility with the SciPy optimisers.

        """
        self.transformation.set(x)
        return self.total_cost()

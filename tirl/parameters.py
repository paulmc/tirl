#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np
from numbers import Number
from functools import reduce


# TIRL IMPORTS

from tirl.tirlobject import TIRLObject
from tirl import utils as tu
from tirl import settings as ts


# DEFINITIONS

EPS = 1e-4  # np.finfo(ts.DEFAULT_FLOAT_TYPE).eps * 2


# IMPLEMENTATION

class ParameterVector(TIRLObject, np.lib.mixins.NDArrayOperatorsMixin):
    """
    Stores numeric parameters. Supports parameter bounds and locking.

    """
    # Types that are compatible with ParameterVector under ufuncs.
    _HANDLED_TYPES = (np.ndarray, Number)

    # Set the array priority higher than that of np.matrix with 10.0
    # and np.array with 0.0, so that this class can control ufunc behaviour.
    __array_priority__ = 11.0

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ INITIALISATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __init__(self, *parameters, dtype=None, bounds=None, lock=None,
                 name=None):
        """
        Initialisation of ParameterVector.

        :param parameters:
            Parameter values.
        :type parameters: Union[np.ndarray, list, tuple, NoneType]
        :param dtype:
            Parameter data type. If None (default), data type is inferred from
            the input.
        :type dtype: Union[str, np.generic, type]
        :param bounds:
            Parameter bounds can be defined in one of the following formats:
            1. ((lb0, ub0),) - sequence of lower and upper bounds for each
               parameter. The order must correspond to the order of the
               transformation parameters.
            2. {param_no: (lb, ub)} - dictionary of lower and upper bound
               pairs, where the keys represent parameter indices (starting
               from 0).
            3. (lb_arr, ub_arr) - flat iterable with lower and upper bounds for
               all parameters.
            4. "auto" - the get_default_bounds() method is called to create
               default lower and upper bounds.
            5. None - parameter bounds are not set.
        :type bounds: Union[tuple, list, dict, str]
        :param lock:
            Boolean mask or index sequence that defines which parameters
            are updated by default. If None, all parameters are updated by
            default.
        :type lock: Union[None, tuple, int, np.ndarray, set]
        :param name:
            Name of the ParameterVector instance. If None (default), a name
            of the format 'p_objectID' will be assigned automatically.

        """
        # Call the superclass initialiser
        super(ParameterVector, self).__init__()

        # Initialise index offset to 0
        self._offset = 0

        # Set dtype
        dtype = dtype or ts.DEFAULT_PARAMETER_DTYPE

        # Reduce multi-input to array-like
        if len(parameters) == 0:
            self._parameters = np.array([], dtype=dtype)
        elif (len(parameters) == 1) and hasattr(parameters[0], "__iter__"):
            parameters = np.asanyarray(parameters[0], dtype=dtype).ravel()
        else:
            parameters = np.array(parameters, dtype=dtype).ravel()

        # Handle memmap, ndarray and iterable
        if isinstance(parameters, np.memmap):
            if parameters.dtype != dtype:
                self._parameters = \
                    tu.change_memmap_dtype(parameters, dtype).ravel()
            else:
                self._parameters = parameters.ravel()
        elif isinstance(parameters, np.ndarray):
            self._parameters = \
                np.ascontiguousarray(parameters.ravel(), dtype=dtype)
        elif hasattr(parameters, "__iter__"):
            p = np.asarray(parameters)
            if np.issubdtype(p.dtype, np.number):
                self._parameters = np.ascontiguousarray(p.ravel(), dtype=dtype)
            else:
                raise TypeError("Transformation parameters must be numeric.")
        else:
            raise TypeError("Transformation parameters must be iterable.")

        # Obtain parameter signature (time-based 40-char SHA1 hexdigest)
        self._signature = np.asarray([tu.timehash()], dtype="<U40")

        # Prepare vault and set locked parameters
        self._locked = set()
        self.lock(lock)

        # Set name (before setting bounds to identify the object that might
        # have automatically locked a parameter because of closing bounds.)
        self.name = name or f"p_{id(self)}"

        # Initialise bound variables and set parameter bounds
        self._lower_bounds = None  # None or ndarray
        self._upper_bounds = None  # None or ndarray
        if bounds is not None:
            self.set_bounds(*bounds)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATTRIBUTES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, n):
        if isinstance(n, str):
            self._name = n
        else:
            raise TypeError(f"The name of {self.__class__.__name__} must be "
                            f"specified by a string.")

    @property
    def parameters(self):
        return self._parameters

    @property
    def offset(self):
        """
        Index offset is a read-only property that can be changed by the
        _reassign() method to represent the offset of the first parameter of
        the current instance in the joint parameter vector of the group, when
        the current instance is part of a larger parameter group.

        Note: this attribute must not be changed by hand, otherwise it will
        interfere with parameter locking!

        """
        return self._offset

    @property
    def locked(self):
        """ Returns a set with the indices of all locked parameters. """
        return self._locked

    @property
    def free(self):
        """ Returns a set with the indices of all free parameters. """
        return set(range(self._parameters.size)).difference(self._locked)

    @property
    def lower_bounds(self):
        return self._lower_bounds

    @lower_bounds.setter
    def lower_bounds(self, lb):
        self.set_lower_bounds(lb)

    @property
    def upper_bounds(self):
        return self._upper_bounds

    @upper_bounds.setter
    def upper_bounds(self, ub):
        self.set_upper_bounds(ub)

    @property
    def dtype(self):
        """ Returns the data type of the parameters. """
        return self._parameters.dtype

    @dtype.setter
    def dtype(self, dt):
        """
        Sets the data type of the parameters.

        Note that as a result of changing the data type, the parameters are
        reassigned. If the current instance is part of a larger group, this
        will break the link to the current instance, which makes sense as to
        avoid this all members of the group would have to change their data
        types.

        """
        if isinstance(self._parameters, np.memmap):
            parameters = tu.change_memmap_dtype(self._parameters, dt)
        else:
            parameters = self._parameters.astype(dt)

        # The reason why the reassign() method is used here instead of '=' is
        # that this allows to change the dtype hierarchically in other
        # instances if this dtype change was initiated by a group instance.
        self._reassign({"_parameters": parameters})
        self._signature[:] = tu.timehash()

    @property
    def size(self):
        """ Returns the number of parameters in the ParameterVector, including
        both locked and free parameters. """
        return self._parameters.size

    @property
    def signature(self):
        """
        Returns the time-based hash signature which is used to track parameter
        changes. Signatures are independent of the changes of parameter bounds
        and lock states.

        Since the signatures are time-based, the signatures of two
        ParameterVectors may be different even when all parameters are
        identical.

        """
        return self._signature[0]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MAGIC METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __iter__(self):
        """ Defer iteration to the underlying parameter vector. """
        return self._parameters.__iter__()

    def __len__(self, *args, **kwargs):
        return self._parameters.__len__(*args, **kwargs)

    def __repr__(self):
        """ String representation of ParameterVector. """
        n = self._parameters.size
        dt = np.dtype(self._parameters.dtype).str
        n_locked = self.count_locked()
        return f"ParameterVector({n}, dtype={dt}, locked={n_locked})"

    def __sha1hash__(self):
        import dill
        import hashlib
        dumps = dill.dumps(self)
        hasher = hashlib.sha1()
        hasher.update(dumps)
        return hasher.hexdigest()

    def __array__(self, *args):
        """
        Array interface to the parameter values. This allows access to all
        parameters regardless whether they are locked or free.

        """
        return self._parameters.__array__(*args)

    def __array_ufunc__(self, ufunc, method, *inputs, **kwargs):
        out = kwargs.get('out', ())
        for x in inputs + out:
            # Only support operations with instances of _HANDLED_TYPES.
            # Use ParameterVector instead of type(self) for isinstance to
            # allow subclasses that don't override __array_ufunc__ to
            # handle ParameterVector objects.
            if not isinstance(x, self._HANDLED_TYPES + (ParameterVector,)):
                return NotImplemented

        # Defer to the implementation of the ufunc on unwrapped values.
        inputs = tuple(x.parameters if isinstance(x, ParameterVector) else x
                       for x in inputs)
        if out:
            kwargs['out'] = tuple(
                x.parameters if isinstance(x, ParameterVector) else x
                for x in out)
        result = getattr(ufunc, method)(*inputs, **kwargs)

        # If the ufunc is reflexive, reassign the parameter array
        if out:
            result_ = result if type(result) is tuple else (result,)
            for x, r in zip(out, result_):
                x._reassign({"_parameters": r})
                x._signature[:] = tu.timehash()
            if isinstance(out, tuple) and len(out) == 1:
                return out[0]
            else:
                return out

        if type(result) is tuple:
            # multiple return values
            ret = []
            for r in result:
                bounds = (self.lower_bounds, self.upper_bounds)
                lock = np.fromiter(self.locked, dtype=int)
                ret.append(
                    self.__class__(r, dtype=r.dtype, bounds=bounds, lock=lock,
                                   name=None))
        elif method == 'at':
            # no return value
            return None
        else:
            # one return value
            bounds = (self.lower_bounds, self.upper_bounds)
            lock = np.fromiter(self.locked, dtype=int)
            ret = self.__class__(result, dtype=result.dtype, bounds=bounds,
                                 lock=lock, name=None)
            return ret

    def __getitem__(self, item):
        """
        Defer parameter access to NumPy. This allows access to all parameters
        regardless whether they are locked or free. If access to locked
        parameters is not desired, use the get() method instead.

        """
        self.eventGet()
        try:
            ret = self._parameters.__getitem__(item)
        except Exception as exc:
            raise exc
        else:
            self.eventGet()
            return ret

    def __setitem__(self, key, value):
        """
        Defer parameter assignment to NumPy. This allows access to the
        locked parameters as well. If access to locked parameters is not
        desired, use the set() method instead.

        """
        try:
            self.eventSet()
        except Exception as exc:
            raise exc
        else:
            self._parameters.__setitem__(key, value)
            self._signature[:] = tu.timehash()

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            return False
        elif np.any(self.parameters != other.parameters):
            return False
        elif np.any(self.get_bounds() != other.get_bounds()):
            return False
        elif self.locked != other.locked:
            return False
        else:
            return True

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~ PRIVATE METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def _mi(self, mi, returntype):
        """
        Method that reads the frequently occurring *mask_and_indices argument.

        :param mi: the *mask_or_indices argument
        :param returntype: "mask" or "indices"

        :returns: boolean mask or flat array of indices where mask is True
        :rtype: Union[np.ndarray, NoneType]

        """
        # Reduce multi-input to ndarray
        if len(mi) == 0:
            raise ValueError("No input was specified.")

        if mi[0] is None:
            return None
        elif mi[0] is Ellipsis:
            mi = np.full_like(self.parameters, True, np.bool_)
        elif isinstance(mi[0], set):
            mi = np.fromiter(mi[0], dtype=np.intp, count=len(mi[0]))
        elif isinstance(mi[0], slice):
            slicer = mi[0]
            mi = np.full_like(self.parameters, False, np.bool_)
            mi[slicer] = True
        elif all(isinstance(val, (int, np.integer)) for val in mi):
            mi = np.asarray(mi).ravel()
        elif hasattr(mi[0], "__iter__"):
            mi = np.asarray(mi[0]).ravel()
        else:
            raise TypeError("Invalid mask or indices.")

        if returntype == "mask":
            return tu.indices2mask(mi, maxindex=self.size)
        elif returntype == "indices":
            return tu.mask2indices(mi, indextrue=True)  # where mask is true
        else:
            raise ValueError("Unknown return type.")

    def _read_double_boundspec(self, bound, *other):
        """
        Extract lower and upper bound array from double bounds specification.

        :param bound: parameter bounds specification (array pair, dict or None)
        :type bound: Union[tuple[np.ndarray], dict, NoneType, str]
        :param other:
            Auxiliary bound specification. Based on 'bound', this can be empty,
            a global upper bound specification, or a sequence of (lb_i, ub_i)
            pairs.
        :type other: Union[int, float, NoneType, tuple, list, np.ndarray]

        :returns: (lower bounds, upper bounds) in flat array format
        :rtype: tuple[np.ndarray, NoneType]

        """
        def ispair(t):
            isseq = isinstance(t, (tuple, list))
            istwo = hasattr(t, "__iter__") and len(t) == 2
            return isseq and istwo

        if other == ():
            if bound is None:
                return None, None

            elif not isinstance(bound, np.ndarray) and (bound == "auto"):
                lower = self._read_single_boundspec("auto", "lower")
                upper = self._read_single_boundspec("auto", "upper")
                return lower, upper

            elif isinstance(bound, dict):
                # Load current lower and upper bounds (or default if missing)
                if getattr(self._lower_bounds, "size", 0) == self.size:
                    lower = self._lower_bounds
                else:
                    lower = self.get_default_bounds(lower=True, upper=False)
                if getattr(self._upper_bounds, "size", 0) == self.size:
                    upper = self._upper_bounds
                else:
                    upper = self.get_default_bounds(lower=False, upper=True)
                self._lbub_dict(bound, lower, upper)
                return lower, upper

            elif ispair(bound):
                return self._read_double_boundspec({0: bound})

            else:
                raise ValueError("Invalid double bound specification.")

        if not ispair(bound):
            lower = self._read_single_boundspec(bound, "lower")
            if len(other) == 1:
                upper = self._read_single_boundspec(other[0], "upper")
            else:
                raise ValueError("Invalid double bound specification.")
            return lower, upper

        dict_bounds = dict(enumerate((bound,) + other))
        return self._read_double_boundspec(dict_bounds)

    @staticmethod
    def _lbub_arrseq(bounds, lower, upper):
        if bounds[0] is not None:
            lb = np.asarray(bounds[0], dtype=lower.dtype).ravel()
            if lb.size != lower.size:
                # print(lb, lower)
                raise AssertionError("Invalid number of entries in lower "
                                     "bounds specification.")
            lower[:] = lb

        if bounds[1] is not None:
            ub = np.asarray(bounds[1], dtype=upper.dtype).ravel()
            if ub.size != upper.size:
                raise AssertionError("Invalid number of entries in upper "
                                     "bounds specification.")
            upper[:] = ub

        if (bounds[0] is not None) and (bounds[1] is not None):
            size = min(lb.size, ub.size)
            if np.any(lb[:size] > ub[:size]):
                raise AssertionError("Lower bound cannot be greater than "
                                     "the upper bound.")

    @staticmethod
    def _lbub_dict(bounds, lower, upper):
        for key, (lb, ub) in bounds.items():
            try:
                key = int(key)
            except ValueError:
                raise ValueError(
                    f"Invalid key in parameter bounds specification: {key}")
            if lb > ub:
                raise ValueError(f"Lower bound cannot be greater than the "
                                 f"upper bound: ({lb}, {ub}).")
            try:
                lower.flat[key] = lb
                upper.flat[key] = ub
            except IndexError:
                raise IndexError(f"Invalid parameter index in bounds "
                                 f"specification: {key}")
            except ValueError:
                raise ValueError(f"Invalid pair in parameter bounds "
                                 f"specification: ({lb}, {ub})")

    def _read_single_boundspec(self, bound, boundtype):
        """
        Extract lower or upper bound array from single bounds specification.

        :param bound: parameter bounds specification
        :type bound: Union[tuple[np.ndarray], dict, NoneType, str]
        :param boundtype: "lower" or "upper"
        :type boundtype: str

        :returns: lower bounds in flat array format
        :rtype: np.ndarray

        """
        if bound is None:
            return None

        if boundtype == "lower":
            if getattr(self._lower_bounds, "size", 0) == self.size:
                default = self._lower_bounds
            else:
                default = self.get_default_lower_bounds()
        elif boundtype == "upper":
            if getattr(self._upper_bounds, "size", 0) == self.size:
                default = self._upper_bounds
            else:
                default = self.get_default_upper_bounds()
        else:
            raise NameError("Invalid bound type.")

        if isinstance(bound, Number):
            default[:] = np.asarray(bound, dtype=default.dtype)
        elif not isinstance(bound, np.ndarray) and (bound == "auto"):
            pass
        elif isinstance(bound, dict):
            self._b_dict(bound, default)
        elif hasattr(bound, "__iter__"):
            self._b_arr(np.asarray(bound), default)
        else:
            raise ValueError(f"Invalid single bound specification: {bound}")

        return default

    @staticmethod
    def _b_arr(bound, either):
        b = np.asarray(bound, dtype=either.dtype).ravel()
        if b.size != either.size:
            raise AssertionError("Invalid number of entries in bounds "
                                 "specification.")
        either[:] = b

    @staticmethod
    def _b_dict(bound, either):
        for key, b in bound.items():
            try:
                key = int(key)
            except ValueError:
                raise ValueError(
                    f"Invalid key in parameter bounds specification: {key}")
            try:
                either.flat[key] = b
            except IndexError:
                raise IndexError(f"Invalid parameter index in bounds "
                                 f"specification: {key}")
            except ValueError:
                raise ValueError(f"Invalid value in parameter bounds "
                                 f"specification: {b}")

    @classmethod
    def _load(cls, dump):
        parameters = dump.get("parameters")
        lb = dump.get("lower_bounds")
        ub = dump.get("upper_bounds")
        locked = dump.get("locked")
        name = dump.get("name")
        signature = dump.get("signature")
        obj = cls(parameters, dtype=parameters.dtype, bounds=(lb, ub),
                  lock=locked, name=name)
        obj._signature[:] = np.asarray([signature], dtype="<U40")
        return obj

    def _dump(self):
        dobj = super(ParameterVector, self)._dump()
        dobj.update({
            "parameters": self._parameters,
            "lower_bounds": self._lower_bounds,
            "upper_bounds": self._upper_bounds,
            "locked": np.fromiter(self._locked, dtype=int),
            "name": self.name,
            "signature": self._signature[0]
        })
        return dobj

    def _reassign(self, struct):
        """
        This private method is used for recursive reassignment of attributes,
        which allows synchronisation of parameters and other ParameterVector
        attributes when they are part of a larger ensemble of objects.

        Note that this is a very dangerous function, and it must be used with
        extreme care.

        :param struct:
            Reassignment dictionary. Key and value pairs should represent the
            names of the ParameterVector attributes that should be reassigned
            and the new values should be their new values after the
            reassignment.
        :type struct: dict

        """
        for key, value in struct.items():
            setattr(self, key, value)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ EVENTS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def eventGet(self):
        """
        Defines a course of actions that is supposed to take place in addition
        to returning the requested elements when the ParameterVector's
        __getitem__ is invoked. If the indexing returns an error, the actions
        are not performed either.

        """
        pass

    def eventSet(self):
        """
        Defines a course of actions that is supposed to take place immediately
        before changing the values when the ParameterVector's __setitem__ is
        invoked. If the action fails, new values are not assigned either.

        """
        pass

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PUBLIC METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def lock(self, *mask_or_indices):
        """
        Locks the specified parameters. Locked parameters are invisible outside
        the ParameterVector, hence they cannot be updated. To update all
        parameters, one must ensure that this property is set to None.

        :param mask_or_indices:
            Boolean mask or index sequence that specifies the subset of
            parameters that should be locked. Parameters corresponding to the
            True values of the mask or the indices will be locked.
        :type mask_or_indices: Union[None, int, np.ndarray, set]

        """
        # No arguments: lock all parameters
        if (len(mask_or_indices) == 0) or \
                (str(mask_or_indices[0]).lower() == "all"):
            self._locked = set(range(self._parameters.size))

        # Argument: None: do not lock anything:
        elif mask_or_indices[0] is None:
            return

        else:
            newlocks = set(self._mi(mask_or_indices, "indices"))
            if any(v >= self.size for v in newlocks):
                raise ValueError("Unlock index is out of bounds.")
            # newlocks.difference_update(range(self.size))
            self._locked.update(newlocks)

    def unlock(self, *mask_or_indices):
        """
        Removes the lock from the specified parameters.

        :param mask_or_indices:
            Boolean mask or index sequence that specifies the subset of
            parameters that should be unlocked. Parameters corresponding to the
            True values of the mask or the indices will be unlocked.
        :type mask_or_indices: Union[None, int, np.ndarray]

        """
        # No arguments: unlock all parameters
        if len(mask_or_indices) == 0:
            self._locked = set()

        # Argument: None: do not unlock anything
        elif mask_or_indices[0] is None:
            return

        else:
            newunlock = set(self._mi(mask_or_indices, "indices"))
            if any(v >= self.size for v in newunlock):
                raise ValueError("Unlock index is out of bounds.")
            # newunlock.difference_update(range(self.size))
            self._locked.difference_update(newunlock)

    def get_default_bounds(self, *mask_or_indices, lower=True, upper=True):
        """
        Calculates default parameter bounds. Subclasses SHOULD implement their
        own defaults by overriding this method. The base class returns
        [-Inf, Inf] as default bounds.

        :param mask_or_indices:
            Boolean mask or flat sequence of indices to select the parameters,
            whose respective default bound(s) should be returned.
        :type mask_or_indices: Union[np.ndarray, list, tuple]
        :param lower: If True, lower bounds will be returned.
        :param upper: If True, upper bounds will be returned.

        :returns: default lower and/or upper parameter bounds
        :rtype: Union[np.ndarray, tuple[np.ndarray]]

        """
        # No arguments: get default bounds for all parameters
        if len(mask_or_indices) == 0:
            # indices = Ellipsis
            size = self.size
        else:
            indices = self._mi(mask_or_indices, "indices")
            if indices is None:
                raise ValueError("Invalid mask or indices.")
            size = self.size if indices is None else indices.size

        ret = []
        if lower:
            lb = np.full(size, -np.inf, dtype=self.dtype)
            ret.append(lb)
        if upper:
            ub = np.full(size, np.inf, dtype=self.dtype)
            ret.append(ub)

        ret = ret[0] if len(ret) == 1 else tuple(ret)
        if not isinstance(ret, np.ndarray) and not ret:
            return None
        else:
            return ret

    def get_bounds(self, *mask_or_indices):
        """
        Returns both lower and upper parameter bounds. If either the lower or
        the upper bound is missing, default values are returned instead of
        these.

        :param mask_or_indices:
            Boolean mask or flat sequence of indices to the parameters whose
            bounds should be returned. If None, bounds are returned for all
            parameters.
        :type mask_or_indices: Union[np.ndarray, int, NoneType]
        :returns:
            (n_parameters, 2) array with lower and upper parameter bounds.
            Changing values in the returned array will not affect the bounds.
        :rtype: np.ndarray

        """
        # No arguments: get bounds for all parameters
        if len(mask_or_indices) == 0:
            indices = Ellipsis
        else:
            indices = self._mi(mask_or_indices, "indices")
            if indices is None:
                raise ValueError("Invalid mask or indices.")

        if self._lower_bounds is not None:
            lb = self._lower_bounds.flat[indices]
        else:
            lb = self.get_default_bounds(indices, lower=True, upper=False)
        if self._upper_bounds is not None:
            ub = self._upper_bounds.flat[indices]
        else:
            ub = self.get_default_bounds(indices, lower=False, upper=True)

        bounds = np.stack((lb, ub), axis=-1)
        return bounds

    def set_bounds(self, bound="auto", *other):
        """
        Sets lower and upper bounds for parameters. Wherever these two are
        equal, the respective parameter gets locked. Locked parameters cannot
        get unclocked by expanding the bounds around them.

        :param bound:
            Parameter bounds can be defined in one of the following formats:
            1. {param_no: (lb, ub)} - dictionary of lower and upper bound
               pairs, where the keys represent parameter indices (starting
               from 0).
            2. (lb_arr, ub_arr) - flat iterable with lower and upper bounds
               for all parameters.
            3. "auto" (default) - the get_default_bounds() method is called to
               create default lower and upper bounds.
            4. None - parameter bounds are not set.
            5. lb, ub - global scalar lower and upper bounds
        :type bound: Union[tuple, list, dict, str]
        :param other:
            Auxiliary bound specification. Based on 'bound', this can be empty,
            a global upper bound specification, or a sequence of (lb_i, ub_i)
            pairs.
        :type other: Union[int, float]

        """
        # If the number of parameters is zero or the bounds specification is
        # None, set parameter bounds to None. This is the default option when
        # the class gets initialised.
        if not self.size or bound is None:
            self._reassign({
                "_lower_bounds": None,
                "_upper_bounds": None
            })
            return

        # Otherwise set the specified values or retrieve defaults (if 'auto')
        lower_bounds, upper_bounds = self._read_double_boundspec(bound, *other)

        # Assert that parameter values are not out of bounds with the current
        # bounds specification.
        if lower_bounds is not None:
            outliers = lower_bounds > self._parameters + EPS
            if np.any(outliers) and ts.RAISE_BOUNDS_ERROR:
                # print(lower_bounds[outliers])
                # print(self._parameters[outliers])
                raise ValueError("At least one parameter is out of bounds with "
                                 "the current lower bounds specification.")
            elif ts.RAISE_BOUNDS_ERROR == False:
                lower_bounds[outliers] = self._parameters[outliers] - EPS
        if upper_bounds is not None:
            outliers = upper_bounds < self._parameters - EPS
            if np.any(outliers) and ts.RAISE_BOUNDS_ERROR:
                # print(upper_bounds[outliers])
                # print(self._parameters[outliers])
                raise ValueError("At least one parameter is out of bounds with "
                                 "the current upper bounds specification.")
            elif ts.RAISE_BOUNDS_ERROR == False:
                upper_bounds[outliers] = self._parameters[outliers] + EPS

        # Set bounds
        self._reassign({
            "_lower_bounds": lower_bounds,
            "_upper_bounds": upper_bounds
        })

        # If the parameter bounds close up on certain parameters, lock them.
        if (lower_bounds is not None) and (upper_bounds is not None):
            equality = (lower_bounds - upper_bounds) >= 0
            if np.any(equality):
                import warnings
                newlock = np.flatnonzero(equality)
                warnings.warn(f"Parameters with the following indices were "
                              f"locked in p_{id(self)} because of equal or "
                              f"inverted lower and upper bounds: "
                              f"{newlock.tolist()}")
                self.lock(np.flatnonzero(equality))

    def get_default_lower_bounds(self, *mask_or_indices):
        return self.get_default_bounds(
            *mask_or_indices, lower=True, upper=False)

    def get_lower_bounds(self, *mask_or_indices):
        """
        Returns lower parameter bounds. If the lower bounds are missing, default
        values are returned instead of these.

        :param mask_or_indices:
            Boolean mask or flat sequence of indices to the parameters whose
            bounds should be returned. If None, bounds are returned for all
            parameters.
        :type mask_or_indices: Union[np.ndarray, int, NoneType]
        :returns:
            (n_parameters,) array with lower parameter bounds. Changing values
            in the returned array will not affect the bounds.
        :rtype: np.ndarray

        """
        # No arguments: get lower bounds for all parameters
        if len(mask_or_indices) == 0:
            indices = Ellipsis
        else:
            indices = self._mi(mask_or_indices, "indices")
            if indices is None:
                raise ValueError("Invalid mask or indices.")

        if self._lower_bounds is not None:
            return self._lower_bounds.flat[indices]
        else:
            return self.get_default_lower_bounds(indices)
            # return self.get_default_bounds(indices, lower=True, upper=False)

    def set_lower_bounds(self, bounds="auto"):
        """
        Sets lower bounds for parameters. Setting lower bounds above the
        current values is not allowed. Where the newly assigned lower bound
        matches the current upper bound, the respective parameter will be
        locked. Parameters cannot be unlocked by expanding the bounds on them.

        :param bounds:
            Parameter bounds can be defined in one of the following formats:
            1. (lb0, lb1, ...) - sequence of lower bounds for each
               parameter. The order must correspond to the order of the
               parameters.
            2. {param_no: lb} - dictionary of lower bound values, where the
               keys represent parameter indices (starting from 0).
            3. lb_arr - flat iterable with lower bounds for all parameters
            4. "auto" - the get_default_bounds() method is called to create
               default lower bounds.
            5. None - parameter bounds are not set.
            6. lb - a single scalar value to set lower bounds globally
        :type bounds: Union[tuple, list, dict, str]

        """
        # If the number of parameters is zero or the bounds specification is
        # None, set parameter bounds to None. This is the default option when
        # the class gets initialised.
        if not self.size or bounds is None:
            self._reassign({"_lower_bounds": None})

        # Otherwise set the specified values or retrieve defaults (if 'auto')
        lower_bounds = self._read_single_boundspec(bounds, "lower")

        # Assert that parameter values are not out of bounds with the current
        # bounds specification.
        if lower_bounds is not None:
            outliers = lower_bounds > self._parameters + EPS
            if np.any(outliers) and ts.RAISE_BOUNDS_ERROR:
                # print(lower_bounds[outliers])
                # print(self._parameters[outliers])
                raise ValueError("At least one parameter is out of bounds with "
                                 "the current lower bounds specification.")
            elif ts.RAISE_BOUNDS_ERROR == False:
                lower_bounds[outliers] = self._parameters[outliers] - EPS

            # If the parameter bounds close up on certain parameters, lock them.
            equality = (lower_bounds - self.get_upper_bounds()) >= 0
            if np.any(equality):
                import warnings
                newlock = np.flatnonzero(equality)
                warnings.warn(f"Parameters with the following indices were "
                              f"locked in p_{id(self)} because of equal or "
                              f"inverted lower and upper bounds: "
                              f"{newlock.tolist()}")
                self.lock(np.flatnonzero(equality))

        # Set bounds
        self._reassign({"_lower_bounds": lower_bounds})

    def get_default_upper_bounds(self, *mask_or_indices):
        return self.get_default_bounds(
            *mask_or_indices, lower=False, upper=True)

    def get_upper_bounds(self, *mask_or_indices):
        """
        Returns upper parameter bounds. If the upper bounds are missing, default
        values are returned instead of these.

        :param mask_or_indices:
            Boolean mask or flat sequence of indices to the parameters whose
            bounds should be returned. If None, bounds are returned for all
            parameters.
        :type mask_or_indices: Union[np.ndarray, int, NoneType]
        :returns:
            (n_parameters,) array with upper parameter bounds. Changing values
            in the returned array will not affect the bounds.
        :rtype: np.ndarray

        """
        # No arguments: get upper bounds for all parameters
        if len(mask_or_indices) == 0:
            indices = Ellipsis
        else:
            indices = self._mi(mask_or_indices, "indices")
            if indices is None:
                raise ValueError("Invalid mask or indices.")

        if self._upper_bounds is not None:
            return self._upper_bounds.flat[indices]
        else:
            return self.get_default_bounds(indices, lower=False, upper=True)

    def set_upper_bounds(self, bounds="auto"):
        """
        Sets upper bounds for parameters. Setting upper bounds below the
        current values is not allowed. Where the newly assigned upper bound
        matches the current lower bound, the respective parameter will be
        locked. Parameters cannot be unlocked by expanding the bounds on them.

        :param bounds:
            Parameter bounds can be defined in one of the following formats:
            1. (ub0, ub1, ...) - sequence of upper bounds for each
               parameter. The order must correspond to the order of the
               parameters.
            2. {param_no: ub} - dictionary of upper bound values, where the
               keys represent parameter indices (starting from 0).
            3. ub_arr - flat iterable with upper bounds for all parameters
            4. "auto" - the get_default_bounds() method is called to create
               default upper bounds.
            5. None - parameter bounds are not set.
            6. ub - a single scalar value to set upper bounds globally
        :type bounds: Union[tuple, list, dict, str]

        """
        # If the number of parameters is zero or the bounds specification is
        # None, set parameter bounds to None. This is the default option when
        # the class gets initialised.
        if not self.size or bounds is None:
            self._reassign({"_upper_bounds": None})

        # Otherwise set the specified values or retrieve defaults (if 'auto')
        upper_bounds = self._read_single_boundspec(bounds, "upper")

        # Assert that parameter values are not out of bounds with the current
        # bounds specification.
        if upper_bounds is not None:
            outliers = upper_bounds < self._parameters - EPS
            if np.any(outliers) and ts.RAISE_BOUNDS_ERROR:
                # print(upper_bounds[outliers])
                # print(self._parameters[outliers])
                raise ValueError("At least one parameter is out of bounds with "
                                 "the current upper bounds specification.")
            elif ts.RAISE_BOUNDS_ERROR == False:
                upper_bounds[outliers] = self._parameters[outliers] + EPS

            # If the parameter bounds close up on certain parameters, lock them.
            equality = (self.get_lower_bounds() - upper_bounds) >= 0
            if np.any(equality):
                import warnings
                newlock = np.flatnonzero(equality)
                warnings.warn(f"Parameters with the following indices were "
                              f"locked in p_{id(self)} because of equal or "
                              f"inverted lower and upper bounds: "
                              f"{newlock.tolist()}")
                self.lock(np.flatnonzero(equality))

        # Set bounds
        self._reassign({"_upper_bounds": upper_bounds})

    def copy(self):
        """
        Copy constructor.

        :returns:
            ParameterVector object (or subclass) with identical but independent
            parameters.
        :rtype: ParameterVector

        """
        lb = self.lower_bounds
        if lb is None or lb.size == 0:
            lb = None
        ub = self.upper_bounds
        if ub is None or ub.size == 0:
            ub = None
        lock = np.fromiter(self.locked, dtype=int)
        obj = self.__class__(self.parameters.copy(), dtype=self.dtype,
                             bounds=(lb, ub), lock=lock, name=self.name)
        return obj

    def count_locked(self):
        """ Counts the number of locked parameters. """
        return len(self._locked)

    def count_free(self):
        """ Counts the number of free parameters. """
        return self._parameters.size - len(self._locked)

    def get(self):
        """
        Returns a vector from all free (non-locked) parameters. The returned
        vector is a copy, i.e. modifying its elements will have no effect on
        the parameters.

        :returns: free parameter vector
        :rtype: np.ndarray

        """
        # Only compute indices if some parameters are locked
        if self._locked:
            indices = np.fromiter(self.free, dtype=int)
            return self._parameters.flat[indices]  # this is already a copy
        else:
            return self._parameters[:].copy()

    def set(self, *parameters):
        """
        Assigns new values to the free (non-locked) parameters. The values in
        the input must match the number of free parameters of the
        ParameterVector object. This method cannot change the type of the
        ParameterVector, so the new input values will be converted to the data
        type of the ParameterVector.

        :param parameters: new parameter values
        :type parameters: Union[np.ndarray, int, float]

        """
        if len(parameters) == 0:
            raise ValueError("No parameter values were specified.")
        else:
            if all(isinstance(val, Number) for val in parameters):
                parameters = np.asarray(parameters, dtype=self.dtype).ravel()
            elif hasattr(parameters[0], "__iter__"):
                parameters = np.asarray(parameters[0], dtype=self.dtype).ravel()
            else:
                raise TypeError("Invalid mask or indices.")

        # Only compute indices if some parameters are locked
        if self._locked:
            indices = np.fromiter(self.free, dtype=int)
            self._parameters.flat[indices] = parameters
            self._signature[:] = tu.timehash()
        else:
            self._parameters[:] = parameters
            self._signature[:] = tu.timehash()

    def update(self, *delta):
        """
        Increments all free (non-locked) parameters by the specified amount.

        :param delta: sequence of flat array of parameter increments
        :type delta: Union[np.ndarray, int, float]

        """
        if len(delta) == 0:
            raise ValueError("No parameter increments were specified.")
        else:
            if all(isinstance(val, Number) for val in delta):
                delta = np.asarray(delta, dtype=self.dtype).ravel()
            elif hasattr(delta[0], "__iter__"):
                delta = np.asarray(delta[0], dtype=self.dtype).ravel()
            else:
                raise TypeError("Invalid mask or indices.")
        # Only compute indices if some parameters are locked
        if self._locked:
            indices = np.fromiter(self.free, dtype=int)
            self._parameters.flat[indices] += delta
            self._signature[:] = tu.timehash()
        else:
            self._parameters[:] += delta
            self._signature[:] = tu.timehash()


class ParameterGroup(ParameterVector):

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ INITIALISATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __init__(self, *vectors, name=None):
        """
        Initialisation of ParameterGroup.

        :param vectors: ParameterVector objects that should be grouped.
        :type vectors: ParameterVector
        :param name: name of the ParameterGroup instance
        :type name: str

        """
        # No input
        if len(vectors) == 0:
            raise ValueError("No ParameterVectors were specified for grouping.")

        # Collate various attributes from the input vectors
        sizes = []
        locks = []
        parameters = []
        lower_bounds = []
        upper_bounds = []
        for i, vec in enumerate(vectors):
            # Check object type
            if not isinstance(vec, ParameterVector):
                raise TypeError("All inputs must be ParameterVectors to be "
                                "grouped.")
            # Check data type
            if i > 0:
                if vec.dtype != vectors[i - 1].dtype:
                    raise TypeError(
                        "All ParameterVectors must share the same dtype "
                        "to be grouped.")
            parameters.append(vec.parameters)
            locked = np.fromiter(vec.locked, dtype=int)
            # The conditional is necessary to avoid broadcast error for empty
            # arrays.
            # There was a bug here. Fixed by: cumsum -> sum
            if locked.size > 0:
                locked += np.sum(sizes).astype(locked.dtype)
            locks.append(set(locked))
            sizes.append(vec.size)
            lb = vec.lower_bounds
            if (lb is None) or (lb.size == 0):
                lb = vec.get_default_bounds(lower=True, upper=False)
            lower_bounds.append(lb)
            ub = vec.upper_bounds
            if (ub is None) or (ub.size == 0):
                ub = vec.get_default_bounds(lower=False, upper=True)
            upper_bounds.append(ub)

        # Store vector references
        self._vectors = tuple(vectors)

        # Create unified attributes (parameters, locks, bounds)
        parameters = np.concatenate([v._parameters for v in vectors]).ravel()
        lock = np.fromiter(reduce(set.union, locks), dtype=int)
        lb = np.concatenate(lower_bounds).ravel()
        ub = np.concatenate(upper_bounds).ravel()
        name = name or f"pg_{id(self)}"

        # Call superclass initialisation
        super(ParameterGroup, self).__init__(
            parameters, dtype=None, bounds=(lb, ub), lock=lock, name=name)

        # Reassign parameters and bounds to link member instances
        self._reassign({
            "_parameters": self.parameters,
            "_lower_bounds": self.lower_bounds,
            "_upper_bounds": self.upper_bounds,
            "_offset": self.offset
            # "_signature": np.asarray([tu.timehash()], dtype="<U40")
        })

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATTRIBUTES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @property
    def vectors(self):
        """ Read-only reference to input ParameterVectors. """
        return self._vectors

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MAGIC METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __repr__(self):
        """ String representation of ParameterGroup. """
        n = self._parameters.size
        v = len(self._vectors)
        dt = np.dtype(self._parameters.dtype).str
        n_locked = self.count_locked()
        return f"ParameterGroup({n}, vectors={v}, dtype={dt}, " \
               f"locked={n_locked})"

    def __array_ufunc__(self, ufunc, method, *inputs, **kwargs):
        out = kwargs.get('out', ())
        P = (ParameterVector, ParameterGroup)
        for x in inputs + out:
            # Only support operations with instances of _HANDLED_TYPES.
            # Use ParameterVector instead of type(self) for isinstance to
            # allow subclasses that don't override __array_ufunc__ to
            # handle ParameterVector objects.
            if not isinstance(x, self._HANDLED_TYPES + P):
                return NotImplemented

        # Defer to the implementation of the ufunc on unwrapped values.
        inputs = tuple(x.parameters if isinstance(x, P) else x for x in inputs)
        if out:
            kwargs['out'] = tuple(
                x.parameters if isinstance(x, P) else x for x in out)
        result = getattr(ufunc, method)(*inputs, **kwargs)

        # If the ufunc is reflexive, reassign the parameter array
        if out:
            result_ = result if type(result) is tuple else (result,)
            for x, r in zip(out, result_):
                x._reassign({"_parameters": r})
                x._signature[:] = tu.timehash()
            if isinstance(out, tuple) and len(out) == 1:
                return out[0]
            else:
                return out

        if type(result) is tuple:
            # multiple return values
            ret = []
            for r in result:
                bounds = (self.lower_bounds, self.upper_bounds)
                lock = np.fromiter(self.locked, dtype=int)
                ret.append(ParameterVector(
                    r, dtype=r.dtype, bounds=bounds, lock=lock, name=None))
            else:
                return tuple(ret)

        elif method == 'at':
            # no return value
            return None
        else:
            # one return value
            bounds = (self.lower_bounds, self.upper_bounds)
            lock = np.fromiter(self.locked, dtype=int)
            ret = ParameterVector(result, dtype=result.dtype, bounds=bounds,
                                  lock=lock, name=None)
            return ret

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~ PRIVATE METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def _reassign(self, struct):
        """
        Overloading the reassignment method gives rise to the ParameterGroup
        trick: by branching the reassignment of attributes, every change that
        is made in the current group instance will also be reflected downstream
        in the member ParameterVector instances.

        If the reassignment call was made upstream, the reassignment will first
        change cascade the current object's attributes, then further propagate
        downstream. (This happens when the current ParameterGroup is part of
        another ParameterGroup.)

        Note that the locks are exempt from this reassignment, and it is
        instead handled by the overloaded lock() and unlock() methods.

        """
        # Calling the superclass method will make changes
        # to the current instance
        super(ParameterGroup, self)._reassign(struct)

        # This part of the method enables to propagate the reassignment call
        # further downstream.
        sizes = [vec.size for vec in self.vectors]
        start = 0
        for i, vec in enumerate(self.vectors):
            stop = start + sizes[i]
            possibilities = {
                "_parameters": self.parameters[start:stop],
                "_lower_bounds": self.lower_bounds[start:stop],
                "_upper_bounds": self.upper_bounds[start:stop],
                "_offset": self.offset + start,
                "_signature": self._signature
            }
            # Only propagate what was reassigned by the current instance plus
            # the offset
            actualities = {k: possibilities[k] for k in struct.keys()}
            # actualities.update({"_offset": self.offset + start})
            vec._reassign(actualities)
            start = stop

    @classmethod
    def _load(cls, dump):
        """
        Creates ParameterGroup instance from object dump.

        :param dump: object dump
        :type dump: dict

        :returns: ParameterGroup instance
        :rtype ParameterGroup

        """
        vectors = dump.get("vectors")
        name = dump.get("name")
        return cls(*vectors, name=name)

    def _dump(self):
        """
        Creates ParameterGroup object dump.

        :returns: object dump
        :rtype: dict

        """
        # Call the _dump method of TIRLObject
        dobj = super(ParameterVector, self)._dump()
        # Update basic object dump
        dobj.update({
            "vectors": list(self.vectors),
            "name": self.name
        })
        return dobj

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PUBLIC METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def lock(self, *mask_or_indices):
        """
        Locks the specified parameters. Locked parameters are invisible outside
        the ParameterVector, hence they cannot be updated. To update all
        parameters, one must ensure that this property is set to None.

        Calling this method of a ParameterGroup will lock the respective
        parameters in the group instance, and will recursively lock linked
        parameters in all member ParameterVectors downstream. (Note: parameters
        re linked via their position in the _parameters attribute, taking
        offsets into account.)

        :param mask_or_indices:
            Boolean mask or index sequence that specifies the subset of
            parameters that should be locked. Parameters corresponding to the
            True values of the mask or the indices will be locked.
        :type mask_or_indices: Union[None, int, np.ndarray]

        """
        # Calling the superclass method will lock the new parameters in the
        # current instance.
        super(ParameterGroup, self).lock(*mask_or_indices)

        # Propagate the locking downstream, taking parameter offset into account
        for vec in self.vectors:
            indices = self._mi(mask_or_indices, "indices") - vec.offset
            vec.lock(indices[np.logical_and(indices < vec.size, indices >= 0)])

    def unlock(self, *mask_or_indices):
        """
        Removes the lock from the specified parameters.

        Calling this method of a ParameterGroup will unlock the respective
        parameters in the group instance, and will recursively unlock
        linked parameters in all member ParameterVectors downstream.

        :param mask_or_indices:
            Boolean mask or index sequence that specifies the subset of
            parameters that should be unlocked. Parameters corresponding to the
            True values of the mask or the indices will be unlocked.
        :type mask_or_indices: Union[None, int, np.ndarray]

        """
        # Calling the superclass method will unlock the new parameters in the
        # current instance.
        super(ParameterGroup, self).unlock(*mask_or_indices)

        # Propagate the locking downstream, taking parameter offset into account
        for vec in self.vectors:
            indices = self._mi(mask_or_indices, "indices") - vec.offset
            indices = indices[np.logical_and(indices < vec.size, indices >= 0)]
            vec.unlock(indices)

    def copy(self):
        """
        Copy constructor of ParameterGroup.

        :returns:
            ParameterVector object (or subclass) with identical but independent
            parameters. Note that the result is not ParameterGroup!
        :rtype: ParameterVector

        """
        lb = self.lower_bounds
        if lb is None or lb.size == 0:
            lb = None,
        ub = self.upper_bounds
        if ub is None or ub.size == 0:
            ub = None
        lock = np.fromiter(self.locked, dtype=int)
        obj = ParameterVector(self.parameters.copy(), dtype=self.dtype,
                             bounds=(lb, ub), lock=lock, name=self.name)
        return obj


if __name__ == "__main__":  # pragma: no cover
    print("""This file is not intended for command-line execution. """)

#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _       __      __  _         _
#  |__   __| |_   _| |  __ \  | |      \ \    / / (_)       (_)
#     | |      | |   | |__) | | |       \ \  / /   _   ___   _    ___    _ __
#     | |      | |   |  _  /  | |        \ \/ /   | | / __| | |  / _ \  | '_ \
#     | |     _| |_  | | \ \  | |____     \  /    | | \__ \ | | | (_) | | | | |
#     |_|    |_____| |_|  \_\ |______|     \/     |_| |___/ |_|  \___/  |_| |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DESCRIPTION

"""
This module provides safe initialisation for the visualisation tools and
a uniform call interface for visualisation functions and methods within TIRL.

"""


# DEPENDENCIES

import os
import warnings
from importlib import import_module


# TIRL IMPORTS

from tirl import settings as ts

# Safe import Matplotlib
if ts.ENABLE_VISUALISATION and os.getenv("DISPLAY"):
    # Import Matplotlib with the specified backend if the system has a display
    import matplotlib
    matplotlib.use(ts.MPL_BACKEND)
else:
    # Disable all visualisations if the system does not have a display
    warnings.warn("DISPLAY variable not set. Matplotlib was not loaded. "
                  "Visualisations are disabled.", ImportWarning)
    ts.ENABLE_VISUALISATION = False


# IMPLEMENTATION

def call(module, func, *args, **kwargs):
    """
    Calls a specific visualisation function or method with the provided
    positional and keyword arguments.

    :param module:
        relative pathology to visualisation module (e.g. optimisers.nl_opt)
    :type module: str
    :param func:
        name of the visualisation function or method
    :type func: str
    :param args:
        positional arguments to the visualisation function or method
    :type args: Any
    :param kwargs:
        keyword arguments to the visualisation function or method
    :type kwargs: Any

    """
    if not ts.ENABLE_VISUALISATION:
        warnings.warn("Call to TIRLVision is disabled.", ResourceWarning)
    else:
        if not str(module).startswith("tirl.tirlvision"):
            module = "tirl.tirlvision.{}".format(module)
        imported_module = import_module(module)
        # Note: if the requested object is not callable,
        # this will raise an exception.
        getattr(imported_module, func)(*args, **kwargs)

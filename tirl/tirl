#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar
# Date: 7 June 2020


# SHBASECOPYRIGHT


usage = \
"""
Tensor Image Registration Library (tirl)
Copyright (C) 2018-2023 University of Oxford
Part of the FMRIB Software Library (FSL)
Author: Istvan N. Huszar

Available options:
    config   [text editor]      modify the library configurations
    home                        display the installation directory
    info     <file>             display information about a TIRL file
    register [dir1 ...]         creates shortcuts for existing tirlscripts
    scripts                     list all registered tirlscripts
    scripts  <dir> [dirs...]    recursively list all tirlscripts in dirs 
    twd      [new dir]          display or set the temporary working directory
    version                     display the library version
    <script> [arguments]        run a TIRL script
    
                
"""


# DEPENDENCIES

import os
import sys
import warnings
import tirl.settings as ts


# IMPLEMENTATION

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ BASIC CONFIGURATION ~~~~~~~~~~~~~~~~~~~~~~~~~~ #

if ts.IGNORE_WARNINGS:
    warnings.simplefilter("ignore")

try:
    if not os.path.isdir(ts.TWD):
        os.makedirs(ts.TWD)
except:
    warnings.warn(f"Temporary working directory (TWD) "
                  f"could not be created at {ts.TWD}")

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PUBLIC FUNCTIONS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

def version(*args):
    """
    Displays the installed version of TIRL.

    """
    import tirl
    print(tirl.__version__)


def home(*args):
    """
    Displays the directory where TIRL is installed.

    """
    import tirl
    print(tirl.home())


def config(*args):
    """
    TIRL config utility. Allows the user to edit the settings.py file of
    the active TIRL installation.

    """
    import subprocess

    if args:
        editor = args[0]
    else:
        editor = "vim"
    subprocess.run([editor, ts.CONFIGFILE])


def info(*args):
    """
    Displays the ASCII header of a TIRL file using pretty formatting.

    """
    from tirl.tirlfile import info
    if args:
        info(args[0])
    else:
        print("Usage: tirl info <tirlfile>")
        exit()


def register(*args):

    # No argument -> scan installed tirl-* packages
    if not args:
        import tirl
        packages_dir = os.path.dirname(tirl.home())
        args = [
            os.path.join(packages_dir, i) for i in os.listdir(packages_dir)
            if i.lower().startswith("tirlscripts")
        ]

    # Paths -> find scripts and install them
    scriptlib = get_tirlscripts_from_args(*args)

    # Register scripts
    register_tirlscripts(scriptlib)


def scripts(*args):
    # No arguments -> find registered tirlscripts
    if not args:
        scriptlib = get_tirlscripts_from_register()
    # Paths -> search args recursively and return the tirlscripts among them
    else:
        scriptlib = get_tirlscripts_from_args(*args)
    # Print to STDOUT
    for alias, scrfile in scriptlib.items():
        print(f"{alias}: {scrfile}")


def twd(*args):
    import tirl.settings as ts
    if args:
        import re
        newtwd = os.path.abspath(args[0])
        if not os.path.isdir(newtwd):
            os.makedirs(newtwd)
        with open(ts.CONFIGFILE, "r") as fp:
            cnf = fp.read()
            oldline = re.search(
                    r"temporary-working-directory:.+", cnf).group(0)
            newline = f"temporary-working-directory: {newtwd}"
            newcnf = cnf.replace(oldline, newline)
        with open(ts.CONFIGFILE, "w") as fp:
            fp.write(newcnf)

        import importlib
        ts = importlib.reload(ts)
    print(os.path.abspath(ts.TWD))

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PRIVATE FUNCTIONS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

def register_tirlscripts(scripts):

    import yaml

    scriptlib = get_tirlscripts_from_register()
    old_scripts = scriptlib.copy()
    scriptlib.update(scripts)
    with open(ts.TSREGISTRY, "w") as fp:
        yaml.safe_dump(
            {"tirlscripts": scriptlib}, fp,
            indent=2, sort_keys=True, default_flow_style=False
        )

    # Display report
    new_scripts = set(scripts) - set(old_scripts)
    print("TIRLscript registration complete.")
    if len(new_scripts) > 0:
        print("\nThe following NEW scripts were registered:")
        for sname in new_scripts:
            print(f"{sname}: {scripts[sname]}")
    else:
        print("\nNo NEW scripts were registered.")

    # Overwrite notification
    overwrites = set(scripts.keys()).intersection(set(old_scripts.keys()))
    if overwrites:
        print("\nThe following scripts were OVERWRITTEN in the register:")
    for alias in overwrites:
        print(f"{alias}: {old_scripts[alias]} --> {scripts[alias]}")

    # User notice
    print(f"\nIf you are unhappy with any of the changes above, "
          f"please edit the TSREGISTRY file manually: {ts.TSREGISTRY}")


def get_tirlscript_from_file(sfile):
    import re
    import importlib.util, importlib.machinery
    regex = re.compile(r"__tirlscript__\s*=\s*")
    with open(sfile, "r") as fp:
        code = fp.read()
        if regex.search(code):
            loader = importlib.machinery.SourceFileLoader("scr", sfile)
            spec = importlib.util.spec_from_loader("scr", loader)
            scr = importlib.util.module_from_spec(spec)
            loader.exec_module(scr)
            alias = getattr(scr, "__tirlscript__", None)
            return (alias, sfile)
        else:
            return None


def get_tirlscripts_from_dir(src):
    """
    Searches a directory recursively for tirlscripts, and returns a dictionary
    of availbale scripts. If the input is a tirlscript, it is returned as a
    one-element dictionary. In all other cases, the function returns an empty
    dictionary.

    """
    scriptlib = dict()
    for root, dirs, files in os.walk(src):
        files = [os.path.join(root, f) for f in files
                 if f.lower().endswith(".py")]
        for sfile in files:
            record = get_tirlscript_from_file(sfile)
            if record is not None:
                scriptlib.update([record])
    return scriptlib

def get_tirlscripts_from_args(*args):
    scriptlib = dict()
    for src in args:
        if os.path.isfile(src):
            scriptlib.update([get_tirlscript_from_file(src)])
        elif os.path.isdir(src):
            scriptlib.update(get_tirlscripts_from_dir(src))
        else:
            print(f"Unrecognised argument: {src}")
    return scriptlib


def get_tirlscripts_from_register():
    import yaml
    with open(ts.TSREGISTRY, "r") as fp:
        return yaml.safe_load(fp)["tirlscripts"] or {}


def run_script(scriptfile, *args):
    """
    Runs a TIRL script.

    """
    import os

    # Is it the alias of a registered TIRL script?
    d_scripts = get_tirlscripts_from_register()
    if scriptfile in d_scripts.keys():
        scriptfile = d_scripts[scriptfile]

    # Is it a path to an existing script?
    if not os.path.isfile(scriptfile):
        raise FileNotFoundError(f"TIRL script does not exist: {scriptfile}")

    import subprocess
    env = os.environ.copy()
    # Make relative imports accessible
    env["PATH"] = env["PATH"] + ":" + os.path.dirname(scriptfile)
    cmd = [sys.executable, scriptfile, *args]
    subprocess.run(" ".join(str(arg) for arg in cmd), shell=True, env=env)
    return


def main(*args):
    """ Main program code. """
    args = list(args)
    if args:
        cmd = args.pop(0)
        if cmd in globals().keys():
            globals()[cmd](*args)
        else:
            run_script(str(cmd), *args)
    else:
        print(usage)


# Program execution starts here
if __name__ == "__main__":
    main(*sys.argv[1:])

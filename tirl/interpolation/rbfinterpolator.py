#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np
from scipy.interpolate import Rbf


# TIRL IMPORTS

from tirl.interpolation.interpolator import Interpolator


# DEFINITIONS


# IMPLEMENTATION

class RbfInterpolator(Interpolator):
    """
    The radial basis function interpolator is designed for tensor fields
    defined on a non-compact domain (a set of points instead of a regular grid).

    """

    default_options = dict(
        epsilon=None,
        function="gaussian",
        mode="1-D",
        norm="euclidean",
        smooth=0,
        hold=True
    )

    def __init__(self, *args, **kwargs):
        """
        Initialisation of RbfInterpolator.

        """
        super(RbfInterpolator, self).__init__(*args, **kwargs)
        self.default_options.update(RbfInterpolator.default_options)
        # Set basis
        self.basis = kwargs.pop("basis", None)
        # Set attributes
        self.set_attributes(**kwargs)
        # Create RBF object
        if self.data is not None:
            self._update()

    def copy(self, **newparams):
        from tirl.utils import rcopy
        data = newparams.pop("data", self.data)
        kwargs = rcopy(self.kwargs)
        kwargs.update(newparams)
        return type(self)(data, **kwargs)

    def interpolate(self, coordinates):
        """
        Wraps scipy.interpolate.Rbf.

        """
        if not self._synced:
            self._update()
        ret = self.rbf(*coordinates.T)
        return ret

    def _update(self):
        speckeys = {"epsilon", "function", "mode", "norm", "smooth"}
        rbfkwargs = {k: v for k, v in self.kwargs.items() if k in speckeys}
        self.rbf = Rbf(*self.basis, self.data, **rbfkwargs)
        self._synced = True

    @property
    def basis(self):
        return self.kwargs.get("basis")

    @basis.setter
    def basis(self, b):
        if hasattr(b, "__domain__"):
            basis = np.atleast_2d(b.get_voxel_coordinates().T)
        elif hasattr(b, "domain"):
            basis = np.atleast_2d(b.domain.get_voxel_coordinates().T)
        elif hasattr(b, "__array__"):
            basis = np.atleast_2d(b)
        elif b is None:
            raise AssertionError(
                f"Missing basis specification for {self.__class__.__name__}")
        else:
            raise TypeError(f"Invalid basis type: {b.__class__.__name__}")
        # Set basis
        self.kwargs.update(basis=basis)
        # Request update
        self._synced = False

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, d):
        self._data = d
        # Request update
        self._synced = False

    @property
    def epsilon(self):
        return self.kwargs.get("epsilon")

    @epsilon.setter
    def epsilon(self, eps):
        self.kwargs.update(epsilon=eps)

    @property
    def function(self):
        return self.kwargs.get("function")

    @function.setter
    def function(self, f):
        self.kwargs.update(function=f)

    @property
    def mode(self):
        return self.kwargs.get("mode")

    @mode.setter
    def mode(self, m):
        self.kwargs.update(mode=m)

    @property
    def norm(self):
        return self.kwargs.get("norm")

    @norm.setter
    def norm(self, n):
        self.kwargs.update(norm=n)

    @property
    def smooth(self):
        return self.kwargs.get("smooth")

    @smooth.setter
    def smooth(self, s):
        self.kwargs.update(smooth=s)

#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEVELOPMENT NOTES

# Subclassing Interpolator:

# Every subclass must define the interpolate() method which must implement
# interpolation on regular grid of scalars, as the chunker method will
# distribute the interpolation of tensor components between multiple
# interpolator instances. If this is an undesired behavior, the
# _default_chunker() method should be redirected to the _no_chunker method,
# or - if the large-array compatibility is desirable to keep - the
# _default_chunker() method should be reimplemented with suitable modifications.


# DEPENDENCIES

import os
import inspect
import numpy as np

# TIRL IMPORTS

import tirl
from tirl import utils as tu
from tirl.utils import rcopy
from tirl import settings as ts
from tirl.tirlobject import TIRLObject

# DEFINITIONS


# DEVELOPMENT NOTES

"""
2021-May-04
The Interpolator class was reimplemented to reduce the interpolation overhead. 
The general idea is that the Interpolator class and its specialised subclasses 
should be lightweight, and perform interpolation on a scalar array only. The 
interpolation of tensor data thus can be performed in parallel, by replicating 
the interpolator instance, and feeding the correct subset of data to each of 
them.
 
"""


# IMPLEMENTATION

class Interpolator(TIRLObject):
    """
    The Interpolator base class provides an optimised compatibility framework
    for using custom interpolation routines with tensor field data. The base
    class itself is non-functional. Subclasses MUST implement the 'interpolate'
    method to provide specialised interpolation routines.

    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ INITIALISATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    default_options = dict(
        fill_value=ts.FILL_VALUE,
        hold=False
    )

    @classmethod
    def _constr_class(cls, ipc, **kwargs):
        """
        Class-agnostic Interpolator constructor.

        """
        if issubclass(ipc, cls):
            return ipc(**kwargs)
        else:
            raise TypeError(f"Invalid interpolator type: {ipc.__name__}")

    def __new__(cls, *args, **kwargs):
        """
        Constructor of Interpolator.

        The following are valid construction signatures for Interpolator:

            Interpolator("ipfile.ipol", **kwargs)
            Interpolator(MyInterpolator, **kwargs)
            Interpolator("tirl.interpolation.myip.MyInterpolator", **kwargs)
            Interpolator(myinterpolatorobject, **kwargs)
            Interpolator(**kwargs)

        """
        source = args[0] if args else None

        # Construction from class variable
        if inspect.isclass(source):
            return cls._constr_class(source, **kwargs)

        # Construction from class name or TIRLObject file
        elif isinstance(source, str):
            if os.path.exists(source):
                ipol = tirl.load(source)
                return type(ipol)(ipol, **kwargs)
            else:
                from pydoc import locate
                try:
                    ipc = locate(source)
                except:
                    raise TypeError(f"Class not found: {source}")
                else:
                    return cls._constr_class(ipc, **kwargs)

        elif isinstance(source, cls):
            data = kwargs.pop("data", source.data)
            ipkwargs = rcopy(source.kwargs)
            ipkwargs.update(**kwargs)
            return type(source)(data=data, **ipkwargs)

        else:
            return super().__new__(cls, *args, **kwargs)

    def __init__(self, *args, **kwargs):
        """
        Initialisation of Interpolator.

        """
        # Call parent-class initialisation
        super(Interpolator, self).__init__()
        self.default_options = Interpolator.default_options

        # Link interpolator to a data object, if it is given at construction
        source = args[0] if args else None
        data = kwargs.pop("data", None)
        if data is not None:
            data = np.asanyarray(data)
        self.data = data

        # Set generic interpolator attributes
        self.kwargs = dict()
        self.set_attributes(**kwargs)

    def __interpolator__(self):
        """ TIRL Interpolator interface. """
        return self

    def __call__(self, coordinates, **kwargs):
        """
        Generic Interpolator call signature.

        """
        # Re-link source data if it is given in the call signature
        data = kwargs.get("data", None)
        if data is not None:
            self.data = data
        if self.data is None:
            raise AssertionError("Interpolator is not linked to any data.")
        coordinates = np.atleast_2d(coordinates)
        result = self.interpolate(coordinates)
        return np.atleast_2d(result)

    def __eq__(self, other):
        """
        Two dictionaries are equal if their type and interpolator arguments are
        identical. If they have linked data arrays, they must be linked to the
        same data object to be identical, otherwise the interpolation result
        has an additional dependency that cannot be verified by the comparison
        of the interpolation.

        For the above reason, interpolation are expected to have no data linked
        to them until they are used. Before interpolation, the interpolation
        should be multiplicated and the new instances should be linked to the
        respective data object.

        """
        if type(self) != type(other):
            return False
        if not tu.dictcmp(self.kwargs, other.kwargs):
            return False
        if self.data is not other.data:   # None is None
            return False
        return True

    def _dump(self):
        # Note: do not save data with the interpolator.
        from tirl.utils import rcopy
        objdump = super(Interpolator, self)._dump()
        objdump.update(
            kwargs=rcopy(self.kwargs)
        )
        return objdump

    @classmethod
    def _load(cls, dump):
        # Note: do not load data with the interpolator.
        kwargs = dump.get("kwargs", {})
        return cls(**kwargs)

    def copy(self, **newparams):
        """
        Copy constructor.

        :returns: new Interpolator instance
        :rtype: Interpolator

        """
        ipkwargs = rcopy(self.kwargs)
        ipkwargs.update(newparams)
        return type(self)(self, **ipkwargs)

    def interpolate(self, coordinates):
        """
        Performs the interpolation at the input coordinates and returns
        the result.

        :param coordinates: (n_points, n_dims) array of input coordinates
        :type coordinates: np.ndarray

        :returns: (n_points, [n_dims]) array of interpolated values
        :rtype: np.ndarray

        """
        raise NotImplementedError()

    def set_attributes(self, **kwargs):
        self.default_options.update(getattr(super(), "default_options", {}))
        newkwargs = self.default_options.copy()
        newkwargs.update(kwargs)
        newkwargs = {k: v for k, v in newkwargs.items()
                     if k in self.default_options.keys()}
        self.kwargs.update(newkwargs)

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, d):
        self._data = d

    @property
    def fill_value(self):
        return self.kwargs.get("fill_value")

    @fill_value.setter
    def fill_value(self, value):
        self.kwargs.update(fill_value=value)

    @property
    def hold(self):
        return self.kwargs.get("hold")

    @hold.setter
    def hold(self, h):
        if isinstance(h, bool):
            self.kwargs.update(hold=h)
        else:
            raise TypeError(f"Expected bool, got {type(h)} instead.")


if __name__ == "__main__":  # pragma: no cover
    print("""This module is not intended for execution.""")

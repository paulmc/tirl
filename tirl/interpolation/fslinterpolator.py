#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEVELOPMENT NOTES

"""
This interpolator does not handle the fill_value parameter. A fill_value of
0 is hard-coded in the C++ implementation.

"""


# DEPENDENCIES

import numpy as np


# TIRL IMPORT

from tirl.interpolation.interpolator import Interpolator
from tirl.cmodules import fslinterpolator


# IMPLEMENTATION

class FSLInterpolator(Interpolator):
    """
    This class implements trilinear interpolation with constant (=0)
    extrapolation. The class demonstrates how custom interpolation can be
    used with TIRL.

    """
    default_options = dict(
        hold=False
    )

    def __init__(self, *args, **kwargs):
        """
        Initialisation of FSLInterpolator.

        """
        super(FSLInterpolator, self).__init__(*args, **kwargs)
        self.default_options.update(FSLInterpolator.default_options)

    def interpolate(self, coordinates):
        """
        Implementation of the interpolation algorithm.

        :param coordinates:
            (n_points, n_dimensions) table of coordinates where the input array
            should be evaluated by interpolation
        :type coordinates: np.ndarray

        :returns: (n_points,) interpolated values
        :rtype: np.ndarray

        """
        return fslinterpolator(self.data, coordinates)

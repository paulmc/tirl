#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import os
import re
import tempfile
import numpy as np
import SimpleITK as sitk
from tirl.chain import Chain


# DEFINITIONS

import tirl.settings as ts
from tirl.constants import *


# IMPLEMENTATION

def load_xfm(filename):
    return load_mni_xfm(filename)


def load_mni_xfm(filename):

    regex_xfm = re.compile(
        r"(?s)Transform_Type.+?(?=Transform_Type|\Z)",
        re.MULTILINE | re.DOTALL
    )
    regex_field = re.compile(
        r"(?<=Displacement_Volume)(?:\s*=\s*)(.+)",
        re.MULTILINE
    )
    with open(filename, "r") as fp:
        xfms = regex_xfm.findall(fp.read())

    xfmdir = os.path.dirname(filename)
    transforms = Chain()
    for xfm in xfms:
        # Make any link to a displacement field absolute
        for field_file in regex_field.findall(xfm):
            if not os.path.isabs(field_file):
                xfm = xfm.replace(field_file, os.path.join(xfmdir, field_file))
        # Create an individual transformation file
        fd, fname = tempfile.mkstemp(dir=ts.TWD, suffix=".xfm")
        with open(fname, "w") as fp:
            fp.write("MNI Transform File\n")
            fp.write(f"% Extracted by TIRL from {filename}\n")
            fp.write(xfm)
        sitkTransform = sitk.ReadTransform(fname)
        if sitkTransform.IsLinear():
            transforms += [load_affine(fname)]
        else:
            with open(fname, "r") as fp:
                regex_warpfile = re.compile(
                    r"(?:Displacement_Volume\s*=\s*)(.+)", re.MULTILINE)
                warpfiles = regex_warpfile.findall(fp.read())
            transforms += [load_warp(wf.rstrip(";")) for wf in warpfiles]

        try:
            os.close(fd)
        except:
            pass
        try:
            os.remove(fname)
        except:
            pass

    return transforms


def load_affine(filename):
    """ Loads an affine transformation from an MNI Transform File (.xfm) """

    from tirl.transformations.affine import TxAffine

    sitkAffine = sitk.ReadTransform(filename)
    assert sitkAffine.IsLinear(), \
        "Expected affine transformation is not linear."
    dim = sitkAffine.GetDimension()
    A = np.asarray(sitkAffine.GetMatrix()).reshape(dim, dim)
    c = np.asarray(sitkAffine.GetCenter()).reshape(dim, -1)
    t = np.asarray(sitkAffine.GetTranslation()).reshape(dim, -1)
    v = t + c - A @ c
    affine = np.eye(dim + 1)
    affine[:dim, :dim] = A
    affine[:dim, -1] = v.ravel()
    return TxAffine(affine, name=str(sitkAffine.GetName()))


def load_warp(filename):
    """ Loads a displacement field from a MINC file. """

    from tirl.tfield import TField
    from tirl.loader import MINCLoader
    from tirl.transformations.displacement import TxDisplacementField

    arr, hdr = MINCLoader()(filename)
    fpath, fname = os.path.split(filename)
    fname, ext = os.path.splitext(fname)
    field = TField(arr, header=hdr, external="affine")
    warp = TxDisplacementField(field, mode=NL_ABS, vectorder="xyz", name=fname)
    return warp


def main():
    txs = load_mni_xfm("/Users/inhuszar/minc/example1/out1.xfm")
    print(txs)
    txs = load_mni_xfm("/Users/inhuszar/minc/example1/out2.xfm")
    print(txs)
    txs = load_mni_xfm("/Users/inhuszar/minc/example1/out3.xfm")
    print(txs)


if __name__ == "__main__":
    main()

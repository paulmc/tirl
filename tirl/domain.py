#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import dill
import psutil
import hashlib
import numpy as np
from operator import mul
from functools import reduce
from numbers import Number, Integral


# TIRL IMPORTS

from tirl.cache import Cache
from tirl.chain import Chain
from tirl import utils as tu
from tirl import settings as ts
from tirl.signature import Signature
from tirl.tirlobject import TIRLObject
from tirl.utils import ArgumentMultiplexer
from tirl.transformations.scale import TxScale
from tirl.transformations.translation import TxTranslation


# DEFINITIONS

from tirl.constants import *

INT_DTYPES = (np.int8, np.uint8, np.int16, np.uint16,
              np.int32, np.uint32, np.int64, np.uint64)


# IMPLEMENTATION

DomainArgs = ArgumentMultiplexer(
    internal="offset",
    external=("transformations", "chain")
)


class Domain(TIRLObject):
    """
    The Domain class provides an easy syntactic interface for vectorised
    coordinate transformations, and optimised handling of larger-than-memory
    domains.

    Domain defines a set of points (either on a rectangular grid, or
    scattered), and uses a sequence of transformations to map them into
    physical space. The voxel and physical coordinates are computed once and
    retrieved from the memory or a memory-mapped file on the hard disk for
    higher performance on repeated calls (coordinate array caching).

    Domains can be named for easier referencing.

    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ INITIALISATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __new__(cls, *args, **kwargs):
        """
        Main Domain constructor.

        There are 3 ways to create a new Domain instance:
            * Domain(domain, copy=False) -- from existing Domain instance
            (specify whether the transformations should be copied or passed by
            reference to the new instance)
            * Domain(shape, coordinates=None, storage=MEM) -- create a regular
            grid domain with optional specification of the voxel coordinates
            (if specified, the specified coordinates will be used in place of
            the voxel coordinates by subsequent transformations.) If both the
            shape and coordinates are specified, the number of elements must
            match, and the coordinates will be assigned to the points in 'C'
            order.
            * Domain(*dimensions) -- shape parameters can be specified freely
            * Domain(coordinates) -- create a sparse Domain and specify the
            support locations directly. The coordinates will be used in place
            of the voxel coordinates by further transformations.

        :Leading Argument:
            * *domain* ``Domain`` -- existing Domain instance
            * *shape* ``Iterable[int]`` -- number of voxels per axis
            * *coordinates* ``np.ndarray`` -- (n_points, n_dims) array
        :Keyword Arguments:
            * *copy*=False ``bool`` -- If True, the internal and external
            transformations will be copied over from the existing instance. If
            False, these will be passed by reference, such that the old and the
            new Domain instance will share them.
            * *transformations*=None ``iterable or None`` -- Unless None, the
            external transformations of the existing Domain instance will be
            overridden by the specified chain.
            * *offset*=None ``iterable or None`` -- Unless None, the internal
            transformations of the existing Domain instance will be overridden
            by the specified chain.
            * *name*=Domain_<objectID> ``str`` -- Name of the new Domain
            instance.

        """
        # Interpret the positional arguments
        if args:
            shape = tuple(int(arg) for arg in args if isinstance(arg, Integral))
            if shape:
                if "shape" not in kwargs.keys():
                    kwargs.update(shape=shape)
                else:
                    raise AssertionError("Conflicting shape specifications.")
            else:
                larg = args[0]
                if isinstance(larg, cls):
                    if "domain" not in kwargs.keys():
                        kwargs.update(domain=larg)
                    else:
                        raise AssertionError(
                            "Conflicting template Domain specifications.")
                elif isinstance(larg, np.ndarray):
                    if "coordinates" not in kwargs.keys():
                        kwargs.update(coordinates=larg)
                    else:
                        raise AssertionError(
                            "Conflicting voxel coordinate specifications.")
                elif hasattr(larg, "__iter__") and \
                        all(isinstance(dim, Integral) for dim in larg):
                    if "shape" not in kwargs.keys():
                        kwargs.update(shape=larg)
                    else:
                        raise AssertionError(
                            "Conflicting shape specifications.")
                else:
                    raise AssertionError(
                        f"{cls.__name__}: unrecognised input: {larg}.")

        # Process the keyword arguments and call the relevant constructors
        shape = kwargs.pop("shape", None)
        coordinates = kwargs.pop("coordinates", None)
        domain = kwargs.pop("domain", None)

        # Extract transformations from the positional arguments (if exist)
        if args:
            txs = [arg for arg in args if hasattr(arg, "__tx_priority__")]
            if txs:
                if "external" not in kwargs.keys():
                    kwargs.update(external=txs)
                else:
                    raise AssertionError(
                        "Conflicting external transformation specifications.")

        # Detect transformation specifications (account for synonymous keywords)
        kwargs = DomainArgs(**kwargs)

        # Existing Domain instance
        if domain is not None:
            if coordinates is None:
                coordinates = domain.base_coordinates()
            return cls._constr_domain(domain, coordinates=coordinates, **kwargs)

        # Dense domain from shape
        elif shape is not None:
            assert all(isinstance(dim, Integral) for dim in shape), \
                "Expected a tuple of integers for the Domain shape."
            if coordinates is not None:
                numel = reduce(mul, shape)
                assert isinstance(coordinates, np.ndarray), \
                    "Expected ndarray for domain coordinates."
                assert numel == coordinates.shape[0], \
                    f"Size mismatch: {numel} points vs. {shape} shape."
            obj = cls._constr_dense(shape, coordinates=coordinates, **kwargs)

        # Sparse domain from coordinates
        elif coordinates is not None:
            assert isinstance(coordinates, np.ndarray), \
                "Expected ndarray for domain coordinates."
            obj = cls._constr_sparse(coordinates, **kwargs)

        else:
            raise AssertionError("Invalid constructor signature.")

        # Set transformations
        obj.internal = kwargs.pop("internal", None)
        obj.external = kwargs.pop("external", None)

        # Set attributes
        obj.name = kwargs.pop("name", None)
        obj.cache = kwargs.pop("cache", ts.DOMAIN_USE_CACHE)
        obj.memlimit = kwargs.pop("memlimit", ts.DOMAIN_INSTANCE_MEMORY_LIMIT)
        storage = kwargs.pop("storage", None)
        if storage is not None:
            obj.storage = storage
        else:
            vcsize = 2 * np.dtype(np.intp).itemsize * obj.numel * obj.ndim
            if obj.memlimit < vcsize:
                obj.storage = HDD
            else:
                n_cpu = tu.verify_n_cpu(ts.CPU_CORES)
                parallelisation = (n_cpu.real > 1) or (n_cpu.imag > 1)
                obj.storage = SHMEM if parallelisation else MEM

        # Set minimum data type for voxel coordinates (saving RAM)
        dtype = kwargs.get("dtype", None)
        # Check if the coordinates can be cast to the specified dtype
        # bugfix for large domains - 2022-09-18 INH
        maxval = int(max(obj.shape)) - 1
        if not np.can_cast(maxval, np.dtype(dtype)):
            dtype = None
        # Check if the input is integer type, if not, discard it
        # Bugfix - kwargs passed down by TField constructor - 2022-09-26 INH
        elif not np.issubdtype(np.dtype(dtype), np.integer):
            dtype = None
        if dtype is None:
            for dt in INT_DTYPES:
                if maxval <= np.iinfo(np.dtype(dt)).max:
                    dtype = dt
                    break
            else:
                raise ValueError("Domain is too large.")
        bc = obj.base_coordinates()
        if bc is not None:
            if not np.can_cast(bc.dtype, dtype):
                dtype = bc.dtype
        obj.dtype = dtype

        return obj

    @classmethod
    def _constr_domain(cls, domain, **kwargs):
        """
        Creates a new Domain instance from an existing one.

        :param domain: existing Domain instance
        :type domain: Domain

        :Keyword Arguments:
            * *copy*=False ``bool`` -- If True, the internal and external
            transformations will be copied over from the existing instance. If
            False, these will be passed by reference, such that the old and the
            new Domain instance will share them.
            * *transformations*=None ``iterable or None`` -- Unless None, the
            external transformations of the existing Domain instance will be
            overridden by the specified chain.
            * *offset*=None ``iterable or None`` -- Unless None, the internal
            transformations of the existing Domain instance will be overridden
            by the specified chain.
            * *name*=Domain_<objectID> ``str`` -- Name of the new Domain
            instance.

        """
        coordinates = kwargs.pop("coordinates", domain.base_coordinates())
        if kwargs.pop("copy", False):
            txs = domain.chain.copy()
            internal = domain.internal.copy()
            if coordinates is not None:
                coordinates = coordinates.copy()
        else:
            txs = domain.chain
            internal = domain.internal
        newkwargs = dict(
            shape=domain.shape,
            coordinates=coordinates,
            internal=internal,
            external=txs,
            name=domain.name,
            dtype=domain.dtype,
            storage=domain.storage,
            cache=domain.cache,
            memlimit=domain.memlimit,
        )
        newkwargs.update(**kwargs)
        return cls(**newkwargs)

    @classmethod
    def _constr_dense(cls, shape, coordinates=None, **kwargs):
        """
        Creates a regular grid Domain from a shape specification. If
        coordinates are additionally provided, these will be stored in the
        Domain instance, and used in place of voxel coordinates by the domain
        transformations.

        :param shape: number of voxels per axis
        :type shape: Iterable[int]
        :param coordinates: (n_points, n_dims) array of coordinates
        :type coordinates: np.ndarray

        """
        obj = super().__new__(cls)
        obj._shape = tuple(int(dim) for dim in shape)
        if coordinates is not None:
            coordinates = np.atleast_2d(coordinates)
        obj._coordinates = coordinates
        return obj

    @classmethod
    def _constr_sparse(cls, coordinates, **kwargs):
        """
        Creates a sparse domain from the specified points.

        """
        coordinates = np.atleast_2d(coordinates)
        obj = super().__new__(cls)
        obj._shape = (coordinates.shape[0],)
        obj._coordinates = coordinates
        return obj

    def __init__(self, *args, **kwargs):
        """
        Initialisation of Domain.

        :Keyword Arguments:
            * *copy*=False ``bool`` -- If True, the internal and external
            transformations will be copied over from the existing instance. If
            False, these will be passed by reference, such that the old and the
            new Domain instance will share them.
            * *transformations*=None ``iterable or None`` -- Unless None, the
            external transformations of the existing Domain instance will be
            overridden by the specified chain.
            * *offset*=None ``iterable or None`` -- Unless None, the internal
            transformations of the existing Domain instance will be overridden
            by the specified chain.
            * *name*=Domain_<objectID> ``str`` -- Name of the new Domain
            instance.
            * *dtype*=None ``dtype`` -- Data type of the base coordinates
            (or voxel coordinates).

        """
        super(Domain, self).__init__()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATTRIBUTES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @property
    def cache(self):
        return self._cache is not None

    @cache.setter
    def cache(self, c):
        if c is True:
            self._cache = Cache(maxsize=1)
        elif c is False:
            self._cache = None
        else:
            raise TypeError("Expected bool for cache.")

    @property
    def chain(self) -> Chain:
        """ Transformation chain getter. """
        return self._external

    @chain.setter
    def chain(self, c):
        """ Transformation chain setter. """
        self._external = self._get_chain(c)

    @property
    def dtype(self):
        return self._dtype

    @dtype.setter
    def dtype(self, dt):
        self._dtype = np.dtype(dt).str

    @property
    def external(self):
        """ This is a proxy for self.chain. """
        return self._external

    @external.setter
    def external(self, txs):
        """ This is a proxy for setting self.chain. """
        self.chain = txs

    @property
    def internal(self):
        """ This is a proxy for self.offset. """
        return self._internal

    @internal.setter
    def internal(self, otx):
        """ This is a proxy for setting self.offset. """
        self.offset = otx

    @property
    def iscompact(self) -> bool:
        """
        A Domain is compact if and only if it is does not consist of a
        single point, and its voxel coordinates have not been defined
        explicitly.

        """
        return (not self.ispoint) and (self.base_coordinates() is None)

    @property
    def ispoint(self):
        return self.numel == 1

    @property
    def memlimit(self):
        """ Instance memory limit in bytes. """
        return self._memlimit

    @memlimit.setter
    def memlimit(self, m):
        """
        Instance memory limit setter. Number of bytes or str with k,M,G suffix.
        """
        self._memlimit = tu.nbytes(m)

    @property
    def name(self) -> str:
        return self._name

    @name.setter
    def name(self, n):
        if isinstance(n, str):
            self._name = n
        elif n is None:
            self._name = super().name
        else:
            raise TypeError(f"Expected str for Domain name, "
                            f"got {n.__class__.__name__} instead.")

    @property
    def ndim(self) -> int:
        """ Read-only property. """
        coordinates = self.base_coordinates()
        if coordinates is not None:
            return int(coordinates.shape[1])
        elif self.ispoint:
            return 0
        else:
            return len(self.shape)

    @property
    def numel(self) -> int:
        return reduce(mul, self.shape)

    @property
    def offset(self) -> Chain:
        """
        Returns the Chain of internal (offset) transformations that precede the
        external (user-set) domain transformations.

        :returns: offset transformations
        :rtype: list

        """
        return self._internal

    @offset.setter
    def offset(self, otx):
        """
        The Chain of offset-transformations is a list of translation and
        scale transformations that ensure that sliced TFields/TImages can
        easily preserve their location in physical space.

        """
        from tirl.transformations.embedding import TxEmbed
        from tirl.transformations.scale import TxScale, TxIsoScale
        from tirl.transformations.translation import TxTranslation
        allowed_types = (TxScale, TxIsoScale, TxTranslation, TxEmbed)
        offset = self._get_chain(otx)
        for tx in offset:
            if not any(issubclass(type(tx), cls) for cls in allowed_types):
                raise TypeError(f"Invalid transformation type in the internal "
                                f"chain: {type(tx)}")
        else:
            self._internal = offset

    @property
    def shape(self) -> tuple:
        """ Read-only property. """
        return tuple(int(dim) for dim in self._shape)

    @property
    def storage(self):
        return self._storage

    @storage.setter
    def storage(self, s):
        if s in STORAGE_MODES:
            if (s == HDD) and not self.iscompact:
                raise NotImplementedError(
                    "HDD mode is not supported for non-compact domains.")
            else:
                self._storage = s
        else:
            raise ValueError("Invalid storage specification.")

    @property
    def transformations(self):
        """ This is a proxy for self.chain. """
        return self._external

    @transformations.setter
    def transformations(self, txs):
        """ This is a proxy for setting self.chain. """
        self.chain = txs

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MAGIC METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __domain__(self):
        """ Domain interface. """
        return self

    def __eq__(self, other):
        # Is the other operand also a Domain?
        if not isinstance(other, type(self)):
            return False
        # Are the Domain shapes equal?
        if self.shape != other.shape:
            return False
        # For non-compact domains, are the voxel coordinates equal?
        c1 = self.base_coordinates()
        c2 = other.base_coordinates()
        if (c1 is None) != (c2 is None):
            return False
        elif (c1 is not None) and (c2 is not None) and \
                ((c1.shape != c2.shape) or np.any(c1 != c2)):
            return False
        # Are all offsets and transformations equal?
        all_tx = self.internal + self.external
        other_all_tx = other.internal + other.external
        if all_tx != other_all_tx:
            return False
        # Declare Domains equal
        return True

    def __repr__(self):
        """
        String representation of Domain.

        """
        if self.iscompact:
            extent = " x ".join(str(dim) for dim in self.shape)
        elif self.ispoint:
            extent = f"Point, dim={self.ndim}"
        else:
            extent = f"{self.shape[0]} points, dim={self.ndim}"

        return f"{type(self).__name__}({extent}, " \
               f"internal={len(self.internal)}, " \
               f"external={len(self.external)})"

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~ PRIVATE METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def _dump(self):
        objdump = super(Domain, self)._dump()
        objdump.update(
            name=self.name,
            shape=self.shape,
            coordinates=self.base_coordinates(),
            dtype=self.dtype,
            internal=self.internal,
            external=self.external,
            memlimit=self.memlimit,
            storage=self.storage
        )
        return objdump

    @classmethod
    def _load(cls, dump):
        name = dump.get("name")
        shape = dump.get("shape")
        coordinates = dump.get("coordinates")
        dtype = dump.get("dtype")
        internal = dump.get("internal")
        external = dump.get("external")
        memlimit = dump.get("memlimit")
        storage = dump.get("storage")

        return cls(shape=shape, coordinates=coordinates, dtype=dtype,
                   external=external, internal=internal, name=name,
                   memlimit=memlimit, storage=storage)

    @staticmethod
    def _get_chain(c):
        # Pass chain by reference
        if isinstance(c, Chain):
            return c

        # Single input
        elif hasattr(c, "__tx_priority__"):
            return Chain(c)
        elif c is None:
            return Chain()
        elif not c:
            return Chain()

        # Multi-input
        elif hasattr(c, "__iter__") and \
                all(hasattr(tx, "__tx_priority__") for tx in c):
            return Chain(c)
        else:
            raise TypeError(f"Expected Transformation or Chain, "
                            f"got {c.__class__.__name__} instead.")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PUBLIC METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def base_coordinates(self):
        return self._coordinates

    def copy(self, **kwargs):
        """
        Returns a copy of the current Domain instance. All transformations,
        including the direct coordinate specifications will be copied, hence
        the new instance will not share any of these with the old instance.

        :param kwargs:
            Any argument that is specified here overrides the parameters of the
            object being copied.

        :Keyword Arguments:
            * *dtype*=None ``dtype`` -- Data type of the base coordinates
            (or voxel coordinates).
            * *name*=Domain_<objectID> ``str or None`` --
            Name of the new Domain instance.
            * *offset*=None ``Chain or None`` -- New internal transformations.
            * *transformations*=None ``Chain or None`` --
            New external transformations.

        :returns:
            A new Domain instance with identical points and transformations.
            The Transformations of the new instance are independent from the
            original transformations.
        :rtype: Domain

        """
        kwargs.pop("copy", None)
        return type(self)(self, copy=True, **kwargs)

    def get_voxel_coordinates(self, dtype=None, chunk=None):
        """
        Retrieve voxel coordinates from cache if available.

        """
        dtype = dtype if dtype is not None else self.dtype
        signature = (np.dtype(dtype).str, chunk)  # for hashing
        caller = "get_voxel_coordinates"
        if self._cache is not None:
            key, exists = self._cache.query(signature, caller=caller)
            if exists:
                vcoords = self._cache.retrieve(key, caller=caller)
            else:
                vcoords = self._calculate_voxel_coordinates(dtype, chunk)
                self._cache.store(key, vcoords, caller=caller)
        else:
            vcoords = self._calculate_voxel_coordinates(dtype, chunk)

        return vcoords

    def _calculate_voxel_coordinates(self, dtype=None, chunk=None):
        """

        """
        dtype = dtype or self.dtype
        if isinstance(chunk, slice):
            chunk = (chunk,)

        # If the Domain is a point...
        if self.ispoint:
            if self.base_coordinates() is not None:
                return self.base_coordinates().astype(dtype)
            else:
                return 0

        # If the Domain is sparse...
        elif not self.iscompact:
            if chunk is None:
                vc = self.base_coordinates()
            elif isinstance(chunk, slice):
                vc = self.base_coordinates()[chunk, :]
            elif isinstance(chunk, tuple) and (len(chunk) == 1) and \
                    isinstance(chunk[0], slice):
                chunk = chunk[0]
                vc = self.base_coordinates()[chunk, :]
            elif isinstance(chunk, tuple) and \
                    all(isinstance(ix, Integral) for ix in chunk) and \
                    len(chunk) == 2:
                start, stop = chunk
                vc = self.base_coordinates()[start:stop, :]
            elif hasattr(chunk, "__iter__") and \
                    all(isinstance(ix, Integral) for ix in chunk):
                indices = np.asarray(chunk, dtype=int)
                vc = self.base_coordinates()[indices, :]
            else:
                raise TypeError(f"Unrecognised chunk specification: {chunk}")
            return vc.astype(dtype)

        if self.storage in (MEM, SHMEM):
            return self._calculate_voxel_coordinates_mem(dtype, chunk)
        elif self.storage == HDD:
            available = psutil.virtual_memory().available
            memlimit = min(available, self.memlimit)
            return self._calculate_voxel_coordinates_hdd(dtype, chunk, memlimit)
        else:
            raise ValueError(f"Invalid storage mode: {self.storage}")

    def _calculate_voxel_coordinates_mem(self, dtype, chunk):
        """
        Returns the voxel (point) coordinates of the domain.

        :param dtype:
            Data type used to represent the voxel coordinates.
        :type dtype: np.generic or str
        :param chunk:
            None (default):
                coordinates of all points in the Domain will be returned
            tuple (l_start, l_end):
                defines a subset of points by linear indices
                (l_start inclusive, l_end exclusive)
            tuple[slice(start, end, step)]:
                defines a subset of points by multi-dimensional indexing
                (start inclusive, end exclusive)
        :type chunk: slice or tuple or None

        :returns: (n_points, m_dimensions) coordinate table
        :rtype: np.ndarray

        """
        coordinates = self.base_coordinates()

        # All coordinates
        if chunk is None:
            if coordinates is None:
                coordinates = np.indices(self.shape, dtype=dtype)
                coordinates = coordinates.reshape(-1, self.numel).T

        # Linear chunk (C-order)
        elif isinstance(chunk, tuple) and \
                all(isinstance(ix, Integral) for ix in chunk) and \
                len(chunk) == 2:
            start, end = chunk
            indices = np.arange(start, end)
            # Note: implicit conversion to int64 is RAM-intensive
            if coordinates is None:
                coordinates = np.unravel_index(indices, self.shape, order="C")
                coordinates = np.stack(coordinates, axis=0).T
            else:
                coordinates = coordinates[start:end]

        # Multidimensional indexing
        elif isinstance(chunk, tuple) and \
                all(isinstance(ix, slice) for ix in chunk):
            slicer = [slice(dim) for dim in self.shape]
            slicer[:len(chunk)] = list(chunk)
            if coordinates is None:
                coordinates = np.mgrid[tuple(slicer)].reshape(self.ndim, -1).T
            else:
                coordinates = coordinates.reshape(self.shape, -1)[tuple(slicer)]
                coordinates = coordinates.reshape(-1, self.ndim)

        else:
            raise ValueError(f"Unrecognised chunk specification: {chunk}")

        return coordinates.astype(dtype)

    def _count_voxels(self, chunk=None):
        """
        Returns the number of voxels selected by the chunk definition.

        """
        # All points
        if chunk is None:
            return self.numel

        # Linear chunk (C-order)
        elif isinstance(chunk, tuple) and \
                all(isinstance(ix, Integral) for ix in chunk) and \
                len(chunk) == 2:
            start, end = chunk
            return end - start

        # Multidimensional indexing
        elif isinstance(chunk, tuple) and \
                all(isinstance(ix, slice) for ix in chunk):
            slicer = [slice(dim) for dim in self.shape]
            slicer[:len(chunk)] = list(chunk)
            n_points = 1
            for sl, dim in zip(slicer, self.shape):
                start, end, step = sl.indices(dim)
                n_points = n_points * (end - start) // step
            else:
                return n_points
        else:
            raise ValueError(f"Unrecognised chunk specification: {chunk}")

    def _calculate_voxel_coordinates_subchunk(self, dtype, chunk, batchsize):
        """
        A private helper method for the 'get_voxel_coordinates' public method.
        Given a selection of voxels defined by 'chunk', this method implements
        a generator that returns the voxel coordinate table for these
        coordinates in several batches, as defined by the batchsize argument.

        This method is called when the size of the voxel coordinate table for
        the selected voxels exceeds the amount of RAM that has been allocated
        to the current Domain instance.

        """
        coordinates = self.base_coordinates()

        # All coordinates
        if chunk is None:
            for start in range(0, self.numel, batchsize):
                end = min(start + batchsize, self.numel)
                if coordinates is None:
                    vc = np.unravel_index(np.arange(start, end), self.shape)
                    yield np.stack(vc, axis=0).T.astype(dtype)
                else:
                    yield coordinates[start:end]

        # Linear chunk (C-order)
        elif isinstance(chunk, tuple) and \
                all(isinstance(ix, Integral) for ix in chunk) and \
                len(chunk) == 2:
            start_outer, end_outer = chunk
            for start in range(start_outer, end_outer, batchsize):
                end = min(start + batchsize, end_outer)
                if coordinates is None:
                    vc = np.unravel_index(np.arange(start, end), self.shape)
                    yield np.stack(vc, axis=0).T.astype(dtype)
                else:
                    yield coordinates[start:end]

        # Multidimensional (gap)slicing
        # TODO: Revise this implementation when there is more time
        elif isinstance(chunk, tuple) and \
                all(isinstance(ix, slice) for ix in chunk):
            slicer = [slice(dim) for dim in self.shape]
            slicer[:len(chunk)] = list(chunk)
            marginals = [np.arange(*sl.indices(dim))
                         for sl, dim in zip(slicer, self.shape)]
            n_points = reduce(mul, [axis.size for axis in marginals])
            for start in range(0, n_points, batchsize):
                end = min(start + batchsize, n_points)
                vcoords = np.empty((end - start, self.ndim), dtype)
                roll = np.cumprod(
                    [1] + [axis.size for axis in marginals[::-1]])
                unravel_subchunk(vcoords, start, end, roll, *marginals)
                if coordinates is None:
                    yield vcoords
                else:
                    indices = np.ravel_multi_index(vcoords.T, self.shape)
                    yield coordinates[indices]

        else:
            raise ValueError("Unrecognised chunk specification: {chunk}")

    def _calculate_voxel_coordinates_hdd(self, dtype, chunk, memlimit):
        """
        Returns the voxel coordinates of the domain.

        :param dtype:
            Data type used to represent the voxel coordinates.
        :type dtype: np.generic or str
        :param chunk:
            None (default):
                coordinates of all points in the Domain will be returned
            tuple (l_start, l_end):
                defines a subset of points by linear indices
                (l_start inclusive, l_end exclusive)
            tuple[slice(start, end, step)]:
                defines a subset of points by multi-dimensional indexing
                (start inclusive, end exclusive)
        :type chunk: slice or tuple or None

        :returns: (n_points, m_dimensions) coordinate table
        :rtype: np.ndarray

        """
        n_points = self._count_voxels(chunk)
        batchsize = memlimit // (np.dtype(np.intp).itemsize * 2 * self.ndim)
        if batchsize > n_points:
            return self._calculate_voxel_coordinates_mem(dtype, chunk)

        # Create HDD array
        # This will live as long as voxel coordinates need to be recalculated
        # or the Domain instance is garbage collected.
        shape = (n_points, self.ndim)
        self._vcdataobj = self._newdataobj("memmap", shape=shape, dtype=dtype)
        coordinates = self._vcdataobj.value

        # Retrieve batches
        generator = \
            self._calculate_voxel_coordinates_subchunk(dtype, chunk, batchsize)
        start = 0
        for i, batch in enumerate(generator):
            n_batch = batch.shape[0]
            slicer = slice(start, start + n_batch)
            coordinates[slicer] = batch
            start += n_batch
        else:
            coordinates.flush()
            return coordinates

    def get_intermediate_coordinates(self, dtype=None, chunk=None):
        """
        Returns the voxel coordinates transformed by the offset chain
        (internal transformations).

        This offset-free "intermediate" pseudo-voxel coordinate space often
        stands as the "original" or "real" voxel coordinate space, where the
        user-defined transformations map the voxels into "physical" space.

        E.g. 1.: the voxel array of a non-compact domain is 1-dimensional, but
        the support points of such a domain usually represent locations in 2D
        or 3D space. Consecutive elements of the 1D array are therefore mapped
        into this "intermediate" voxel-coordinate space, where the
        interpolation is defined.

        E.g. 2.: when a TImage is sliced along the voxel dimensions, the
        resultant part of the image preserves its location in physical space.
        Instead of altering the transformation chain, an offset transformation
        is introduced that maps the new voxel coordinates into this
        intermediate space, which is in 1:1 correspondence with the "original"
        (unsliced) voxel space.

        """
        dtype = ts.DEFAULT_FLOAT_TYPE if dtype is None else dtype
        vc = self.get_voxel_coordinates(dtype=None, chunk=chunk)
        return self.internal.map(vc).astype(dtype)

    def get_physical_coordinates(self, dtype=None, chunk=None):
        """
        Returns the physical (real-world) coordinates for select voxels of the
        Domain, as specified by 'chunk'.

        """
        # This function is called too often to perform a memory check here, so
        # the consensus is that when the storage mode of the Domain is MEM,
        # a calculation is attempted irrespective of the available RAM. If
        # however the storage mode is HDD, the calculation is optimised for
        # the available memory (taking into account the instance memory limit).

        # Setting default options
        if dtype is None:
            dtype = ts.DEFAULT_FLOAT_TYPE
        if isinstance(chunk, slice):
            chunk = (chunk,)

        if self.storage in (MEM, SHMEM):
            return self.get_physical_coordinates_mem(dtype, chunk)
        elif self.storage == HDD:
            available = psutil.virtual_memory().available
            memlimit = min(available, self.memlimit)
            return self.get_physical_coordinates_hdd(dtype, chunk, memlimit)
        else:
            raise ValueError(f"Invalid storage mode: {self.storage}")

    def get_physical_coordinates_mem(self, dtype=None, chunk=None):
        """
        Computes the physical coordinates of the requested domain points
        without checking the available RAM.

        """
        coordinates = self.get_voxel_coordinates(chunk=chunk)
        signature = [self.signature()]  # signature chain starter
        return self.map_voxel_coordinates(coordinates, signature, dtype=dtype)

    def get_physical_coordinates_hdd(self, dtype=None, chunk=None,
                                     memlimit=None):
        """
        Optimises the calculation of physical coordinates for the requested
        domain points for a certain amount of RAM.
        """
        signature = [self.signature()]
        n_points = self._count_voxels(chunk)
        vcoords = self.get_voxel_coordinates(chunk=chunk)
        vbytes = 0 if isinstance(vcoords, np.memmap) else vcoords.nbytes
        pdim = self.map_voxel_coordinates([[1] * self.ndim]).shape[-1]
        batchsize = (memlimit - vbytes) // (pdim * np.dtype(dtype).itemsize * 2)
        if batchsize > n_points:
            return self.map_voxel_coordinates(vcoords, signature, dtype=dtype)

        # Create HDD array
        shape = (n_points, pdim)
        self._pcdataobj = self._newdataobj("memmap", shape=shape, dtype=dtype)
        coordinates = self._pcdataobj.value

        # Retrieve batches
        for start in range(0, n_points, batchsize):
            end = min(start + batchsize, n_points)
            coordinates[start:end] = \
                self.map_voxel_coordinates(
                    vcoords[start:end], signature, dtype=dtype)
        else:
            coordinates.flush()
            return coordinates

    def map_voxel_coordinates(self, coords, signature=None, dtype=None):
        """
        Calls the transformation chain to map voxel coordinates to physical
        coordinates.

        """
        if dtype is None:
            dtype = ts.DEFAULT_FLOAT_TYPE

        signature = [] if signature is None else signature  # chain starter
        coords = self.all_tx().reduce().map(
            coords, merge="linear", signature=signature)
        # signature.append(self.chain.signature())  # exclude offset!

        return coords.astype(dtype)

    def map_physical_coordinates(self, coords, signature=None):
        """
        Inverts the transformation chain to map physical coordinates to voxel
        coordinates. The resultant voxel coordinates are floating-point type,
        subject to interpolation.

        """
        inv_tx = self.all_tx().reduce().inverse()
        signature = [] if signature is None else signature  # chain starter
        coords = inv_tx.map(coords, merge="linear", signature=signature)
        # signature.append(inv_chain.signature())  # exclude offset!

        return coords

    # TODO: The following methods should be aware of memory and/or storage mode.

    def all_tx(self):
        return self.internal + self.external

    def map_voxel_vectors(
            self, vects, coords=None, rule=RULE_SSR, signature=None):
        return self.all_tx().map_vector(vects=vects, coords=coords, rule=rule)

    def map_physical_vectors(
            self, vects, coords=None, rule=RULE_SSR, signature=None):
        all_tx = self.all_tx().inverse()
        return all_tx.map_vector(vects=vects, coords=coords, rule=rule)

    def map_voxel_tensors(
            self, tensors, coords=None, rule=RULE_PPD, signature=None):
        return self.all_tx().map_tensor(
            tensors=tensors, coords=coords, rule=rule)

    def map_physical_tensors(
            self, tensors, coords=None, rule=RULE_PPD, signature=None):
        all_tx = self.all_tx().inverse()
        return all_tx.map_vector(tensors=tensors, coords=coords, rule=rule)

    # And the same set of methods for combined vector/tensor + coordinate xfm

    def map_voxel_vectors_and_coordinates(
            self, vects, coords=None, rule=RULE_SSR, signature=None):
        return self.all_tx().map_vector_and_coordinates(
            vects=vects, coords=coords, rule=rule)

    def map_physical_vectors_and_coordinates(
            self, vects, coords=None, rule=RULE_SSR, signature=None):
        all_tx = self.all_tx().inverse()
        return all_tx.map_vector_and_coordinates(
            vects=vects, coords=coords, rule=rule)

    def map_voxel_tensors_and_coordinates(
            self, tensors, coords=None, rule=RULE_PPD, signature=None):
        return self.all_tx().map_tensor_and_coordinates(
            tensors=tensors, coords=coords, rule=rule)

    def map_physical_tensors_and_coordinates(
            self, tensors, coords=None, rule=RULE_PPD, signature=None):
        all_tx = self.all_tx().inverse()
        return all_tx.map_vector_and_coordinates(
            tensors=tensors, coords=coords, rule=rule)

    def __getitem__(self, item):
        """
        Domain slicing allows the creation of domains with identical extent and
        settings, but with a selected subset of transformations. The new domain
        will share the transformations with the old domain, so any changes of
        the transformation parameters in either of the objects will be
        represented in the other.

        Note 1: offset-transformations will be shared regardless of the slice
        specification.

        Note 2: while discontinuous slices are allowed, these may not result in
        valid Domain objects if the resultant chain of transformations is not
        internally congruent.

        :param item:
            Slice term. This must be 1-dimensional, and indices must be chosen
            relative to the list of "volatile" transformations (those
            accessible via Domain.chain), excluding all "offset"
            transformations (those accessible via Domain.offset) that logically
            precede the former.
        :type item:
            Union[int, np.integer, slice, Ellipsis, list, np.ndarray]

        :returns:
            Child Domain object that shares the selected transformations with
            the input Domain instance.
        :rtype: Domain

        """
        if isinstance(item, tuple) and len(item) == 2:
            isec, esec = item
        else:
            isec, esec = slice(None), item

        newoffset = self.internal.__getitem__(isec)
        newchain = self.external.__getitem__(esec)
        return type(self)(self, internal=newoffset, external=newchain)

    def __setitem__(self, key, value):
        """
        Allows to set elements in the transformation chain by directly
        indexing the Domain object.
        """
        self.chain.__setitem__(key, value)

    def signature(self):
        """
        Returns the signature of the Domain instance. The domain signature
        depends on the shape of the domain, and the individual signatures of
        the internal and external transformations. A Domain signature uniquely
        identifies the mapping from a set of voxel coordinates to a set of
        physical coordinates. If any of the transformations or the domain
        itself changes, it is reflected in the change of the signature.
        Note that identical transformations may have different signatures.

        :returns: domain signature
        :rtype: str

        """
        # Level 0: voxel shape (compact) / point set (non-compact) signature
        def level0():
            hasher = hashlib.sha1()
            coordinates = self.base_coordinates()
            if coordinates is not None:
                hasher.update(dill.dumps(np.asarray(coordinates)))
            else:
                hasher.update(dill.dumps(self.shape))
            return hasher.hexdigest()

        # Level 1: External transformation chain ("chain") signature
        def level1():
            return self.external.signature()

        # Level 2: Internal transformation chain ("offset") signature
        def level2():
            return self.internal.signature()

        return Signature(level0, level1, level2)

    def resize(self, *args, copy=False, **kwargs):
        """
        Changes the number of voxels in a compact domain and prepends the
        intrinsic transformations (offset chain) with a scaling, such that the
        new voxel array is mapped to the same physical location and has the
        same physical extent along all dimensions.

        :notes:

            1. For non-compact domains, this option is not available, and
               raises an AssertionError.
            2. Unless explicitly specified, the shape of the new Domain
               instance will be rounded to the nearest integer that is higher
               (ceil).
            3. Transformations are shared between the old and the new Domain
               instances (unless copy=True), but kwargs are copied either way.

        :param shape_or_scale:
            Target shape as a tuple, or non-negative numerical scaling factor
            for the 0-th voxel dimension.
        :type shape_or_scale:
            Union[tuple[int], float, np.floating, int, np.integer]
        :param scales:
            Scaling factors for the 1st, 2nd, etc. voxel dimensions.
        :type scales: Union[int, float, np.integer, np.floating]

        :returns:
            New resized Domain instance with identical transformations and
            metaparameters.
        :rtype: Domain

        :raises AssertionError:
            If the input Domain is non-compact.

        """
        assert self.base_coordinates() is None, \
            "Cannot resize Domain with voxelwise coordinate assignment."

        # Infer target shape from the keyword arguments
        shape = kwargs.pop("shape", None)
        if shape is not None:
            try:
                assert all(isinstance(dim, Integral) for dim in shape)
                shape = tuple(int(dim) for dim in shape)
            except:
                raise ValueError(f"Invalid target shape specification: {shape}")
        scale = kwargs.pop("scale", None)
        if scale is not None:
            if isinstance(scale, Number):
                scale = (float(scale),) * self.ndim
            elif hasattr(scale, "__iter__"):
                try:
                    scale = tuple(float(dim) for dim in scale)
                except:
                    raise ValueError(f"Invalid scale specification: {scale}")

        # Infer target shape form the positional arguments
        if args:
            if hasattr(args[0], "__iter__"):
                if shape is not None:
                    raise AssertionError(
                        "Conflicting target shape specifications.")
                else:
                    try:
                        shape = tuple(int(dim) for dim in args[0])
                    except:
                        pass
            else:
                if scale is not None:
                    raise AssertionError(
                        "Conflicting scale factor specifications.")
                else:
                    try:
                        scale = tuple(float(dim) for dim in args)
                    except:
                        pass

        if (shape is None) == (scale is None):
            raise AssertionError(
                "Missing or ambiguous target shape specification.")

        # Update scales based on target shape definition
        if scale is not None:
            shape = tuple(np.ceil(np.multiply(self.shape, scale)).astype(int))

        if any(dim < 1 for dim in shape):
            raise ValueError(f"Domain must have positive dimensions: {shape}")
        if len(shape) != self.ndim:
            raise ValueError(f"Resizing a Domain must preserve the number of "
                             f"dimensions in the Domain. Old dimensions: "
                             f"{len(shape)}, new dimensions: {self.ndim}")

        # Create resized Domain
        chain = self.external.copy() if copy else self.external[:]
        factors = np.divide(self.shape, shape).astype(float)
        tx = TxScale(*factors, name="ResizeCompensation")
        offset = tx + self.internal.copy() if copy else tx + self.internal[:]
        return type(self)(shape=shape, internal=offset, external=chain,
                          storage=self.storage, memlimit=self.memlimit)

    def reset(self, internal=False):
        """
        Resets all external transformations (chain). Internal transformations
        (offset) are preserved if 'offset' is False (default).

        """
        self._external = Chain()
        if internal:
            self._internal = Chain()

    def vcentre(self):
        coordinates = self.base_coordinates()
        if coordinates is not None:
            vc = np.mean(coordinates, axis=0)
            return tuple(float(dim) for dim in vc.ravel())
        else:
            return tuple(float(dim / 2) for dim in self.shape)

    def pcentre(self):
        pc = np.mean(self.get_physical_coordinates(), axis=0)
        return tuple(float(dim) for dim in pc.ravel())

    def centralise(self):
        """
        Appends the chain of transformations with a TxTranslation object,
        which moves the geometrical centre of the domain to the origin. All
        points/voxels bear equal weights in averaging.

        """
        offset = -np.asarray(self.pcentre())
        tx = TxTranslation(*offset, name="Centralise")
        self.chain.append(tx)
        return self

    def copy_transformations(self, old_domain, links=True):
        """
        Copies the transformation chain from an existing domain to the current
        domain. This method is useful when a TField or a TImage is
        resampled to a new grid.

        :param old_domain: template domain
        :type old_domain: Domain
        :param links:
            If True, Non-linear transformations that were dynamically linked to
            the other domain, will be recreated so that they will be also
            linked to the current domain. If False, these transformations will
            be statically linked to the new domain.
        :type links: bool

        """
        self.chain = old_domain.chain
        if links:
            for i, tx in enumerate(old_domain.chain):
                if tx.kind == TX_NONLINEAR:
                    # If the transformation was dynamically linked to
                    # the current domain, re-grid that transformation.
                    if tx.domain == old_domain[:i]:
                        # Erase transformations on the tx domain
                        tx.metaparameters["domain"] = old_domain[:0]
                        # Resample among voxel domains
                        new_tx = tx.regrid(self[:0])
                        # Add back all new antecedent transformations
                        new_tx.metaparameters["domain"] = self[:i]
                        # Add the current transformation to the new domain
                        self.chain[i] = new_tx

    def get_transformation(self, name, case_sensitive=False, index=False):
        return self.get_external_transformation(name, case_sensitive, index)

    def get_external_transformation(
            self, name, case_sensitive=False, index=False):
        """
        Parses the domain transformations to find a Transformation object with
        the specified name. All transformations with the same name are returned
        as a tuple. Note that offset transformations are excluded from the
        search.

        If the transformation with a given name is not found, IndexError is
        raised.

        :param name:
            Name of the desired Transformation object.
        :type name: str
        :param case_sensitive:
            Turn this option on to make the search case-sensitive.
        :type case_sensitive: bool
        :param index:
            If True, transformations' indices are also returned
        :type index: bool

        :returns:
            Transformation object(s) with the specified name. Indices.
        :rtype: Union[Transformation, tuple[Tranformation, int],
                tuple[Transformation], tuple[tuple]]
        :raises: IndexError if the transformation is not found in the chain

        """
        group = self.external
        return self._get_tx_from_chaingroup(group, name, case_sensitive, index)

    def get_internal_transformation(self, name, case_sensitive=False,
                                    index=False):
        """
        Parses the offset transformations of the domain to find a Transformation
        object with the specified name. All transformations with the same name
        are returned as a tuple. Note that offset transformations are excluded
        from the search.

        If the transformation with a given name is not found, IndexError is
        raised.

        :param name:
            Name of the desired Transformation object.
        :type name: str
        :param case_sensitive:
            Turn this option on to make the search case-sensitive.
        :type case_sensitive: bool
        :param index:
            If True, transformations' indices are also returned
        :type index: bool

        :returns:
            Transformation object(s) with the specified name. Indices.
        :rtype: Union[Transformation, tuple[Tranformation, int],
                tuple[Transformation], tuple[tuple]]
        :raises: IndexError if the transformation is not found in the chain

        """
        group = self.internal
        return self._get_tx_from_chaingroup(group, name, case_sensitive, index)

    def _get_tx_from_chaingroup(self, group, name, case_sensitive=False,
                                index=False):
        """
        Private method that implements the common parts of the public methods
        Domain.get_transformation() and Domain.get_internal_transformation().

        :param group:
            Either Domain.offset or Domain.chain. Defines the scope where the
            algorithm searches for matching transformations.
        :type group: list

        """
        txs = []
        indices = []
        name = str(name).lower() if case_sensitive else str(name)
        if case_sensitive:
            txnames = [getattr(tx, "name", "") for tx in group]
        else:
            txnames = [str(getattr(tx, "name", "")).lower() for tx in group]
        for i, n in enumerate(txnames):
            if name == n:
                txs.append(group[i])
                if index:
                    indices.append(i)

        # Return transformation(s)
        if len(txs) == 0:
            if case_sensitive:
                msg = "No transformation with name '{}' (case sensitive)."
            else:
                msg = "No transformation with name '{}'."
            raise IndexError(msg.format(name))
        if len(txs) == 1:
            if index:
                return txs[0], indices[0]
            else:
                return txs[0]
        else:
            if index:
                return tuple(txs), tuple(indices)
            else:
                return tuple(txs)


def unravel_subchunk(vcoords, start, end, roll_ax, *marginals):
    """
    This is a numba-compiled helper function for get_voxel_coordinates_hdd,
    that calculates the voxel coordinates for voxels that were selected using
    multi-dimensional (gap)slicing.

    """
    for point_no in range(start, end):
        vcoords[point_no - start] = \
            [axis[(point_no // roll) % len(axis)]
             for axis, roll in zip(marginals, roll_ax[::-1][1:])]
    else:
        return vcoords

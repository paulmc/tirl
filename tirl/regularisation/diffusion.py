#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np
import scipy.sparse as sp
from functools import reduce


# TIRL IMPORTS

from tirl.cache import Cache
from tirl.regularisation.txregulariser import TransformationRegulariser


# IMPLEMENTATION

class DiffusionRegulariser(TransformationRegulariser):
    """
    DiffusionRegulariser - penalises the gradient magnitude of a dense
    displacement field.

    """
    RESERVED_KWARGS = ("transformation", "weight", "logger", "metaparameters")

    def __init__(self, transformation, weight=1.0, logger=None,
                 **metaparameters):
        """
        Initialisation of DiffusionRegularisation.

        :param transformation:
            Transformation object whose parameters are used to compute to the
            regularisation term.
        :type transformation: Transformation or OptimisationGroup
        :param weight: Regularisation weight.
        :type weight: Real
        :param logger:
            If None (default), logs will be redirected to the RootLogger
            instance. You may alternatively specify a different Logger instance
            or its name.
        :type logger: None or str or Logger
        :param kwargs:
            Additional keyword arguments required to compute the
            regularisation term.
        :type kwargs: Any

        """
        if not hasattr(transformation, "domain") or \
            not transformation.domain.iscompact:
            raise TypeError(f"Expected non-linear transformation on a compact "
                            f"domain, got {transformation} instead.")

        # Call parent-class initialisation
        super(DiffusionRegulariser, self).__init__(
            transformation, weight=weight, logger=logger, **metaparameters)

        self.cache = Cache(maxsize=1)

    def function(self):
        """
        Calculates the total scalar regularisation cost.

        """
        field = self.transformation.std().absolute().field()
        grad = np.stack(np.gradient(field.data, axis=field.vaxes), axis=-1)
        del field
        return np.sum(grad ** 2)

    def grad1(self):
        """
        Calculates the total derivative of the regularisation cost with respect
        to the parameters. Note that this is a Gateaux-derivative, that is
        co-dependent on the field update v: since R = U'D'DU,
        dR[u;v] = <$\Delta$u, v>.

        """
        vectors = self.transformation.vectors(fullstd=True)
        L = sp.block_diag([self.grad2()] * vectors.shape[-1])
        grad = L @ vectors.T.reshape(-1, 1)
        return grad.ravel()

    def grad2(self):
        """
        Calculates the total second derivative of the regularisation cost with
        respect to the parameters.

        Note that this quantity is independent from the current field or its
        update: since R = U'D'DU, the second derivative is the Laplace
        operator, which is returned in discrete (grid) form, as a sparse array
        of shape (l*m*n, l*m*n) or (l*m, l*m). The letters l, m, and n denote
        the shape of the transformation domain.

        """
        shape = self.transformation.domain.shape
        signature = (shape,)  # for hashing
        key, exists = self.cache.query(signature, caller="grad2")
        if exists:
            laplace = self.cache.retrieve(key, caller="grad2")
        else:
            laplace = discrete_laplace_operator(*shape, order="C")
            # Apply correction for Neumann boundary condition
            diag = np.asarray(np.sum(laplace, axis=0)).ravel() + 2 * len(shape)
            laplace = sp.dia_matrix(laplace)
            laplace.setdiag(-diag)
            laplace = self.weight * laplace
            self.cache.store(key, laplace, caller="grad2")

        return laplace


def discrete_laplace_operator(*dims, order="C"):
    """
    Creates a sparse discrete Laplace operator for a (dim0, dim1, ...) grid.

    :param dims: grid dimensions
    :type dims: int

    :returns: sparse discrete Laplace operator

    """

    # Check input
    dims = (dims,) if not isinstance(dims, (tuple, list)) else tuple(dims)
    if not all(isinstance(dim, (int, np.integer)) for dim in dims):
        raise ValueError("Dimensions of the N-dimensional domain must be "
                         "integers.")
    ndim = len(dims)
    size = np.product(dims)
    if not ndim >= 1:
        raise ValueError("Laplace operator must be at least 1-dimensional.")

    # Revert dimensions for Fortran-order
    if order.upper() == "F":
        dims = dims[::-1]

    terms = []
    for current_index, dim in enumerate(dims):
        dim = int(dim)
        ones = np.ones(dim)
        D = sp.spdiags([ones, -2*ones, ones], [-1, 0, 1], dim, dim)
        if current_index > 0:
            pre = reduce(lambda a, b: sp.kron(a, b),
                         map(lambda c: sp.identity(dims[c]),
                             range(0, current_index)))
            L = sp.kron(pre, D)
        else:
            L = D
        if current_index < ndim - 1:
            for i in range(current_index + 1, ndim):
                L = sp.kron(L, sp.identity(int(dims[i])))
        assert L.shape == (size, size)
        terms.append(L)
    else:
        result = reduce((lambda a, b: a + b), terms)

    return result

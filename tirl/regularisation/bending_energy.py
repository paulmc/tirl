#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEVELOPMENT NOTES

"""
28-June-2020 Istvan N Huszar:
This class now Works with v2.1.
23-June-2020 Istvan N Huszar:
This is a legacy class from v1.0, which currently does not work with v2.0,
although it is a high-priority task to fix this class.

"""


# DEPENDENCIES

import numpy as np


# TIRL IMPORTS

from tirl.regularisation.txregulariser import TransformationRegulariser


# IMPLEMENTATION

class RegBendingEnergy(TransformationRegulariser):
    """
    RegBendingEnergy - membrane energy regularisation. Calculates the
    regularisation cost as the sum of the second derivative of all
    displacements on a dense domain.

    """
    RESERVED_KWARGS = ("transformation", "weight", "logger", "metaparameters")

    def __init__(self, transformation, weight=1.0, logger=None,
                 **metaparameters):
        """
        Initialisation of RegBendingEnergy.

        :param transformation:
            Displacement field type transformation.
        :type transformation: TxDisplacementField
        :param weight: regularisation weight (default=1.0)
        :type weight: Union[int, float, np.integer, np.floating]
        :param logger:
            If None (default), logs will be redirected to the RootLogger
            instance. You may alternatively specify a different Logger instance
            or its name.
        :type logger: None or str or Logger

        """
        super(RegBendingEnergy, self).__init__(
            transformation=transformation, weight=weight,
            logger=logger, **metaparameters)

    @property
    def transformation(self):
        return self.metaparameters.get("transformation")

    @transformation.setter
    def transformation(self, tx):
        if not hasattr(tx, "domain") or not hasattr(tx, "field"):
            raise TypeError(
                "Diffusion regularisation can only work with a displacement "
                "field type transformation.")
        else:
            self.metaparameters.update(transformation=tx)

    def function(self):
        if self.transformation.domain.iscompact:
            field = self.transformation.std().absolute().field()
        else:
            from tirl.domain import Domain
            d0 = self.transformation.domain
            dense_domain = Domain(
                self.transformation.dense_shape, transformations=d0.chain,
                offset=d0.offset[1:])
            field = self.transformation.std().absolute().field(dense_domain)

        grad1 = np.stack(np.gradient(field.data, axis=field.vaxes), axis=0)
        grad2 = np.stack(np.gradient(grad1, axis=field.vaxes), axis=0)
        res = np.sum(np.abs(grad2)) / field.domain.numel
        self.log("Bending energy: {}".format(res * self.weight))
        return res

    def jacobian(self, parameters):
        return NotImplementedError()

    def hessian(self, parameters):
        return NotImplementedError()

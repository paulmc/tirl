#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DESCRIPTION

"""
This module maps the user configurations onto the respective source code
variables, as the latter are not necessarily easy to comprehend by users.

User-specific library configurations are defined in the locally installed
tirl.yml file using human-readable keys. These configurations are intended
for resource optimisation and changing reporting behavior only.

By definition, user configurations must not affect registrations performed
by the library.

"""

# DEPENDENCIES

import yaml


# DEFINITIONS

USER_CONFIGURATIONS = {
    "cpu-cores":                         "CPU_CORES",
    "default-shell":                     "DEFAULT_SHELL",
    "domain-instance-memory-limit":      "DOMAIN_INSTANCE_MEMORY_LIMIT",
    "domain-use-cache":                  "DOMAIN_USE_CACHE",
    "float-type":                        "DEFAULT_FLOAT_TYPE",
    "ignore-warnings":                   "IGNORE_WARNINGS",
    "interpolator-fill-value":           "FILL_VALUE",
    "loglevel-cost":                     "COST_VALUE_LOG_LEVEL",
    "loglevel-parameter-update":         "PARAMETER_UPDATE_LOG_LEVEL",
    "matplotlib-backend":                "MPL_BACKEND",
    "multiprocessing-mode":              "MP_CONTEXT",
    "operation-chunk-threshold":         "OPERATION_CHUNK_THRESHOLD",
    "parameter-type":                    "DEFAULT_PARAMETER_DTYPE",
    "temporary-working-directory":       "TWD",
    "tfield-chunk-threshold":            "CHUNK_THRESHOLD",
    "tfield-chunksize":                  "TFIELD_CHUNKSIZE",
    "tfield-instance-memory-limit":      "TFIELD_INSTANCE_MEMORY_LIMIT",
    "timage-default-snapshot-extension": "TIMAGE_DEFAULT_SNAPSHOT_EXT",
    "timage-instance-memory-limit":      "TIMAGE_INSTANCE_MEMORY_LIMIT",
    "visualisation":                     "ENABLE_VISUALISATION"
}


# IMPLEMENTATION

def safe_load_user_configurations(configfile):
    """ Returns a dict of user configurations. """
    try:
        with open(configfile, "r") as fp:
            return yaml.safe_load(fp) or dict()
    except FileNotFoundError:
        return dict()


def replace_configuration_handles(usercnf):
    """ Returns a dict of user-set values of source-code variables. """
    cnf = dict()
    for key, value in usercnf.items():
        if key not in USER_CONFIGURATIONS:
            continue
        cnf.update({USER_CONFIGURATIONS[key]: value})
    return cnf


def fix_dtypes(usercnf):
    for key, value in usercnf.items():
        if key == "CPU_CORES":
            usercnf[key] = complex(value)
            break
    return usercnf


def load_user_variables(configfile):
    usercnf = safe_load_user_configurations(configfile)
    usercnf = replace_configuration_handles(usercnf)
    return fix_dtypes(usercnf)

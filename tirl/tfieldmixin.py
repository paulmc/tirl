#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np


# TIRL IMPORTS

import tirl.settings as ts
from tirl.tfieldindexer import TFieldIndexer
from tirl.transformations.scale import TxScale
from tirl.transformations.translation import TxTranslation


# DEFINITIONS

from tirl.constants import *


# DEVELOPMENT NOTES


# IMPLEMENTATION

class TFieldMixin(object):
    """
    TFieldMixin - Public attributes of TField-like objects.
    To use this class, child classes must exhibit a __tfield__() method.

    """

    def __eq__(self, other):
        """
        Two TFields are equal if their Domains are equal, their tensor shapes
        are equal, their data type is identical, and their field data is equal.

        """
        tf = self.__tfield__()
        if not isinstance(other, type(self)):
            return False
        if other.domain != tf.domain:
            return False
        if other.tshape != tf.tshape:
            return False
        if other.dtype != tf.dtype:
            return False
        if other.order == tf.order:
            common = tf.order
        else:
            common = tf.order
        if not np.allclose(other.dataview(common), tf.dataview(common)):
            return False
        return True

    def abs(self, *args, **kwargs):
        # Result is an object of identical type
        return np.abs(self, *args, **kwargs)

    def amin(self, *args, **kwargs):
        # Numpy congruency alias
        return self.min(*args, **kwargs)

    def amax(self, *args, **kwargs):
        # Numpy congruency alias
        return self.max(*args, **kwargs)

    def astype(self, dtype):
        # Result is an object of identical type, with different dtype
        return type(self)(self, dtype=dtype)

    def max(self, *args, **kwargs):
        # Reducing operation, result may be a scalar, an ndarray, or type(self)
        axis = args.pop(0) if args else kwargs.get("axis", None)
        kwargs.update({"axis": axis})
        return np.maximum.reduce(self, *args, **kwargs)

    def median(self, *args, **kwargs):
        # Reducing operation, result may be a scalar, an ndarray, or type(self)
        ret = np.median(self.data, *args, **kwargs)
        return self.__array_wrap__(ret, context=("median", None, self.order))

    def mean(self, *args, **kwargs):
        # Reducing operation, result may be a scalar, an ndarray, or type(self)
        ret = np.core.fromnumeric._methods._mean(self, *args, **kwargs)
        return self.__array_wrap__(ret, context=("mean", None, self.order))
        # return np._methods._mean(self.data, *args, **kwargs)

    def min(self, *args, **kwargs):
        # Reducing operation, result may be a scalar, an ndarray, or type(self)
        axis = args.pop(0) if args else kwargs.get("axis", None)
        kwargs.update({"axis": axis})
        return np.minimum.reduce(self, *args, **kwargs)

    @property
    def data(self):
        """
        This is a read-only proxy attribute to prevent accidental dereferencing 
        and garbage collection of the instance buffer.

        """
        return self.__tfield__()._data

    def dataview(self, order):
        """
        Returns a view of the TField data in the specified tensor layout:
        TENSOR_MAJOR or VOXEL_MAJOR. This method is to avoid the reassignment
        of the main data object, especially where such reassignments are hidden
        (such as ufuncs).

        """
        tf = self.__tfield__()
        if order == TENSOR_MAJOR:
            axes = tf.taxes + tf.vaxes
        elif order == VOXEL_MAJOR:
            axes = tf.vaxes + tf.taxes
        else:
            raise ValueError(f"Invalid tensor layout: {order}")

        return tf.data.transpose(*axes)

    @property
    def dtype(self):
        return self.__tfield__().data.dtype

    @dtype.setter
    def dtype(self, dt):
        if self.__tfield__().storage == MEM:
            newdata = self.__tfield__().data.astype(dt)
        elif self.__tfield__().storage == SHMEM:
            from tirl.dataobj import DataObject
            data = self.__tfield__().data
            newdata = DataObject("shared", shape=data.shape, dtype=dt)
            newdata.value[...] = data.astype(dt)
            newdata.flush()
        else:
            newdata = self.__tfield__()._change_memmap_dtype(
                self.__tfield__().data, dt)
        # Update data object
        self.__tfield__()._update_tfield(
            data=newdata, tensor_axes=self.__tfield__().taxes)

    @property
    def domain(self):
        """ Domain of the TField instance."""
        return self.__tfield__()._domain

    @domain.setter
    def domain(self, d):
        """
        The Domain attribute must be set with a Domain instance that matches
        the voxel shape of the TField instance. This way of changing the
        domain is for advanced users only. Please consider using the evaluate()
        method instead.

        """
        if hasattr(d, "__domain__"):
            if self.__tfield__().vshape == d.shape:
                self.__tfield__()._domain = d
            else:
                raise AssertionError(
                    f"Cannot assign domain with shape {d.shape} to a "
                    f"{self.__tfield__().__class__.__name__} with voxel shape "
                    f"{self.__tfield__().vshape}. Consider using the "
                    f"evaluate() method instead.")
        else:
            raise TypeError("The domain attribute must be set with a Domain "
                            "instance.")

    @property
    def evmgr(self):
        return self.__tfield__()._evmgr

    @evmgr.setter
    def evmgr(self, ev):
        from tirl.evmgr import EvaluationManager
        if isinstance(ev, EvaluationManager):
            self.__tfield__()._evmgr = ev
        else:
            raise TypeError(f"Expected EvaluationManager instance, "
                            f"got {type(ev)} instead.")

    @property
    def header(self):
        return self.__tfield__()._header

    @header.setter
    def header(self, hdr):
        if isinstance(hdr, dict):
            self.__tfield__()._header = dict(**hdr)  # pure dict, not a subclass
        else:
            raise TypeError(f"Expected dict for header, "
                            f"got {hdr.__class__.__name__} instead.")

    @property
    def interpolator(self):
        """ Delegated interpolator object. """
        return self.__tfield__()._interpolator

    @interpolator.setter
    def interpolator(self, ip):
        """
        Setter method for delegated interpolator object.

        :param ip:
            Interpolator class or instance. If None, the interpolator is chosen
            automatically to match the domain type (compact vs non-compact).
        :type ip: Union[Interpolator, NoneType]

        """
        import inspect
        from tirl.interpolation.interpolator import Interpolator
        if inspect.isclass(ip) and issubclass(ip, Interpolator):
            self.__tfield__()._interpolator = ip()
        elif hasattr(ip, "__interpolator__"):
            self.__tfield__()._interpolator = ip
        else:
            self.__tfield__()._interpolator = \
                self.__tfield__()._get_interpolator_specification(
                    array=self.__tfield__().data, interpolator=ip,
                    layout=self.__tfield__().layout,
                    domain=self.__tfield__().domain)
        # Link the new interpolator to the field data
        self.__tfield__().interpolator.data = self.__tfield__().data
        # self.__tfield__().interpolator.tensor_axes = self.__tfield__().taxes

    @property
    def layout(self):
        return self.__tfield__()._layout

    @layout.setter
    def layout(self, lo):
        if hasattr(lo, "__layout__"):
            if (lo.shape == self.__tfield__().data.shape):
                self.__tfield__()._layout = lo
            else:
                raise AssertionError(f"Shape mismatch: {lo.shape} vs. "
                                     f"{self.__tfield__().data.shape}")
        else:
            raise TypeError(
                f"Expected Layout instance, got {lo.__class__.__name__}")

    @property
    def ndim(self):
        return self.__tfield__().layout.ndim

    @property
    def nbytes(self):
        """ Total number of array elements. Read-only. """
        return self.__tfield__().data.nbytes

    @property
    def numel(self):
        return self.__tfield__().vsize

    @property
    def order(self):
        return self.__tfield__().layout.order

    @order.setter
    def order(self, o):
        o = str(o).upper()
        if o not in (TENSOR_MAJOR, VOXEL_MAJOR):
            raise ValueError(f"Expected {TENSOR_MAJOR} or {VOXEL_MAJOR} order, "
                             f"got {o} instead.")

        # Create rearranged views
        if (self.__tfield__().order == TENSOR_MAJOR) and (o == VOXEL_MAJOR):
            data = np.transpose(
                self.__tfield__().data,
                self.__tfield__().vaxes + self.__tfield__().taxes)
            new_taxes = tuple(
                range(self.__tfield__().vdim, self.__tfield__().ndim))
        elif (self.__tfield__().order == VOXEL_MAJOR) and (o == TENSOR_MAJOR):
            data = np.transpose(
                self.__tfield__().data,
                self.__tfield__().taxes + self.__tfield__().vaxes)
            new_taxes = tuple(range(self.__tfield__().tdim))
        else:
            # Nothing to do
            return

        # Move the data to create contiguous arrays upon request
        if self.__tfield__().storage == MEM:
            data = np.ascontiguousarray(data)
        elif self.__tfield__().storage == SHMEM:
            from tirl.dataobj import DataObject
            contdata = DataObject("shared", shape=data.shape, dtype=data.dtype)
            contdata.value[...] = data[...]
            contdata.flush()
            data = contdata
        elif self.__tfield__().storage == HDD:
            # Create new contiguous memmap
            fp, fn = self.__tfield__().tmpfile(prefix="TField_", dir=ts.TWD)
            contdata = np.memmap(filename=fn, dtype=data.dtype, mode="r+",
                                 offset=0, shape=data.shape, order="C")
            contdata[...] = data[...]
            contdata.flush()
            data = contdata

        # Update data object
        self.__tfield__()._update_tfield(data, tensor_axes=new_taxes)

    @property
    def resolution(self):
        offset = self.domain.offset.map(np.zeros((self.domain.ndim,))).ravel()
        scale = self.domain.offset.map(np.ones((self.domain.ndim,))).ravel()
        res = np.abs(scale.ravel() - offset.ravel())
        return tuple(float(dim) for dim in res)

    @resolution.setter
    def resolution(self, res):
        """
        Sets the resolution of the TField in arbitrary units per voxel. This is
        formally equivalent to setting the first element of the internal
        transformation chain (the offset chain) with a TxScale transformation
        object.

        If one or more TxScale transformations with the name "resolution"
        already exist, the parameters of the first of these will be overwritten
        by the input values. Otherwise the chain will be prepended with a new
        transformation object.

        Note that changing the resolution will have no effect on the field
        data, only the physical extent of the field. To change the _sampling_
        of the TField, use the rescale() or resize() method. Changing the
        sampling of the image data will have no effect on the physical extent
        of the image, because the voxel coordinates of the down- or upsampled
        field will be mapped to the original voxel coordinates by an additional
        scaling transformation in the internal part of the transformation chain.

        :param res:
            Separate a.u. / voxel scaling factors along the voxel axes
            or a single value to define isotropic resolution.
        :type res: Union[float, tuple[float]]

        .. note::
             1. Setting the resolution at construction time is recommended to
                make transformation parameters physically interpretable, such
                that unreasonable values can be more easily identified.
             2. The number of scaling factors may not match the number of
                voxel dimensions. This occurs when a 2D domain is embedded in a
                3D field and the coordinates are transformed perpendicularly to
                the domain.
             3. Multiple resolution setter transformations may exist in the
                transformation chain if further axes are defined by embedding
                transformations. It is recommended that resolutions are set
                manually in these cases, not by this method.

        """
        # Validate input
        if not hasattr(res, "__iter__"):
            res = (res,)
        if len(res) == 1:
            res = res * self.domain.ndim
        assert all(dim > 0 for dim in res), \
            "Resolution must be positive for all axes."

        # Get the current offset and change the scaling according to the new res
        # Also assign the internal transformations to nascent TField objects
        if len(self.domain.internal) == 0 or \
                (not np.allclose(res, self.resolution)):
            offset = self.domain.offset.map(np.zeros((self.domain.ndim,)))
            offset = TxTranslation(*offset.ravel(), name="offset")
            scale = TxScale(*res, name="resolution")
            self.domain.offset = [scale, offset]

    @property
    def rule(self):
        return self.__tfield__()._rule

    @rule.setter
    def rule(self, r):
        if r in (RULE_FS, RULE_SSR, RULE_PPD, None):
            self.__tfield__()._rule = r
        else:
            raise ValueError(f"Invalid tensor transformation rule: {r}")

    @property
    def shape(self):
        return self.__tfield__().layout.shape

    @property
    def storage(self):
        if isinstance(self.__tfield__().data, np.memmap):
            return HDD
        elif isinstance(self.__tfield__().data, np.ndarray):
            dataobj = self.__tfield__()._dataobj
            if dataobj.kind == "SharedMemory":
                return SHMEM
            else:
                return MEM
        else:  # pragma: no cover
            raise AssertionError(
                "TIRL Internal error: unrecognised data object type.")

    @storage.setter
    def storage(self, mode):
        if mode not in STORAGE_MODES:
            raise ValueError(f"Expected one of {STORAGE_MODES} "
                             f"for storage mode, got {mode} instead.")

        # Nothing to do
        if self.__tfield__().storage == mode:
            return

        # Dump onto disk
        if (self.__tfield__().storage in (MEM, SHMEM)) and (mode == HDD):
            from tirl.dataobj import DataObject
            data = self.__tfield__().data
            dataobj = DataObject("memmap", shape=data.shape, dtype=data.dtype)
            dataobj.value[...] = data[...]
            dataobj.flush()
            self.__tfield__().header.update(filename=dataobj.address)
            self.__tfield__()._update_tfield(
                data=dataobj, tensor_axes=self.__tfield__().taxes)

        # Load into memory
        elif (self.__tfield__().storage in (SHMEM, HDD)) and (mode == MEM):
            olddata = self.__tfield__().data
            data = np.empty(shape=olddata.shape, dtype=olddata.dtype)
            data[...] = olddata[...]
            self.__tfield__()._update_tfield(
                data=data, tensor_axes=self.__tfield__().taxes)

        elif (self.__tfield__().storage in (MEM, HDD)) and (mode == SHMEM):
            from tirl.dataobj import DataObject
            data = self.__tfield__().data
            dataobj = DataObject("shared", shape=data.shape, dtype=data.dtype)
            dataobj.value[...] = data[...]
            dataobj.flush()
            self.__tfield__()._update_tfield(
                data=dataobj, tensor_axes=self.__tfield__().taxes)

        else:
            raise AssertionError(
                "TIRL Internal error: unexpected storage change request.")

    @property
    def tdim(self):
        """ Number of tensor dimensions. Read-only. """
        return self.__tfield__().layout.tdim

    @property
    def taxes(self):
        """ Tensor axes. Read-only. """
        return self.__tfield__().layout.taxes

    @property
    def tsize(self):
        return self.__tfield__().layout.tsize

    def tm(self):
        """
        Returns an equivalent TField instance with TENSOR_MAJOR layout.

        """
        tf = self.__tfield__()
        data = tf.dataview(TENSOR_MAJOR)
        obj = type(tf)(
            data, tshape=tf.tshape, vshape=tf.vshape,
            order=TENSOR_MAJOR, header=tf.header.copy(),
            domain=tf.domain[:, :], dtype=data.dtype,
            interpolator=tf.interpolator.copy(), name=tf.name,
            evmgr=tf.evmgr.copy(), rule=tf.rule)
        return self.__operation_finalise__(obj)

    def vm(self):
        """
        Returns an equivalent TField instance with VOXEL_MAJOR layout.

        """
        tf = self.__tfield__()
        data = tf.dataview(VOXEL_MAJOR)
        obj = type(tf)(
            data, tshape=tf.tshape, vshape=tf.vshape,
            order=VOXEL_MAJOR, header=tf.header.copy(),
            domain=tf.domain[:, :], dtype=data.dtype,
            interpolator=tf.interpolator.copy(), name=tf.name,
            evmgr=tf.evmgr.copy(), rule=tf.rule)
        return self.__operation_finalise__(obj)

    @property
    def vdim(self):
        """ Number of voxel dimensions. Read-only. """
        return self.__tfield__().layout.vdim

    @property
    def vaxes(self):
        """ Voxel axes. Read-only. """
        return self.__tfield__().layout.vaxes

    @property
    def vsize(self):
        return self.__tfield__().layout.vsize

    @property
    def vshape(self):
        """ Voxel shape. Read-only. """
        return self.__tfield__().layout.vshape

    @property
    def tensors(self):
        return TFieldIndexer(
            self, axes=self.__tfield__().vaxes, context="tensors")

    @property
    def voxels(self):
        return TFieldIndexer(
            self, axes=self.__tfield__().taxes, context="voxels")

    @property
    def tshape(self):
        """ Tensor shape. Read-only. """
        return self.__tfield__().layout.tshape

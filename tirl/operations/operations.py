#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import multiprocessing as mp
from functools import partial

# TIRL IMPORTS

import tirl.utils as tu
from tirl.constants import *
from tirl import settings as ts
from tirl.tirlobject import TIRLObject


# DEFINITIONS

process_wide_job_specific_globals = dict()


# IMPLEMENTATION

class Operator(TIRLObject):
    """
    Operator base class to perform computations on TField-like objects. The
    Operator unifies the signature of operations regardless of the tensor
    layout, and distributes the computation among multiple threads or processes
    in a memory-efficient way. Operations on small, in-memory TField instances
    are performed serially by a single process to avoid the overhead.

    Subclasses of this base class SHOULD implement default chunkers, that are
    optimised for a certain kind of operation, such as operations on the
    tensors alone, or spatial filters.

    """

    def __init__(self, operation, chunker=None, n_cpu=ts.CPU_CORES,
                 name=None, threshold=ts.OPERATION_CHUNK_THRESHOLD,
                 opargs=None, opkwargs=None, **kwargs):
        """
        Initialisation of Operator.

        :param operation:
            Operation function that will be carried out. The operation function
            must define its first argument as the input TField.
        :type operation: function
        :param opargs:
            Additional positional arguments for the operation function.
        :type opargs: Any
        :param opkwargs:
            Additional keyword arguments for the operation function.
        :type opkwargs: Any
        :param chunker:
            Chunker function. If None, the Operator instance's default chunker
            will be used to slice the tensor field if necessary.
        :type chunker: Union[function, None]
        :param n_cpu:
            Number of CPU cores used to process chunks. Note that each CPU core
            must have enough memory to accommodate a chunk with the maximum
            allowable chunksize given by ts.TFIELD_CHUNKSIZE in MiB. If -1, all
            available logical cores will be used.
        :type n_cpu: Union[int, np.integer]
        :param name:
            Name of the Operator for easier identification.
        :type name: Union[str, None]

        """
        super(Operator, self).__init__()

        # Set operation function
        if callable(operation):
            self._operation = operation
        else:
            raise TypeError("Operation must be callable.")
        # Save opargs and opkwargs
        self.default_opargs = opargs
        self.default_opkwargs = opkwargs

        # Set chunker function
        assert callable(chunker) or chunker is None, "Chunker must be callable."
        self._chunker = chunker

        # Set the number of CPU cores
        self.cpu = n_cpu
        # Set the name of the Operator
        self.name = name
        # Set chunk threshold
        self.threshold = threshold
        self.initargs = ()
        self.chunksize = kwargs.get("chunksize", None)
        self.memlimit = tu.nbytes(kwargs.get(
            "memlimit", ts.TFIELD_INSTANCE_MEMORY_LIMIT))

    def __reduce__(self):
        args = (self._operation, self._chunker, self.cpu,
                self.name, self.threshold, self.opargs, self.opkwargs)
        kwargs = dict(
            chunksize=self.chunksize,
            memlimit=self.memlimit,
            initargs=self.initargs,
            _default_opargs=self.default_opargs,
            _default_opkwargs=self.default_opkwargs,
            _opargs=self.opargs,
            _opkwargs=self.opkwargs
        )
        return self.__class__, args, kwargs

    def _auto_chunk(self, tf, out=None):
        """
        Starts _default_chunker if the input tensor field is not in memory;
        otherwise calls the _no_chunker method for faster performance.

        """
        if self.chunking():
            yield from self._default_chunker(tf, out)
        else:
            yield (Ellipsis,)

    def __call__(self, tf, *opargs, out=None, **opkwargs):
        """
        Operator call.

        :param tfield:
            Tensor field to operate on.
        :type tfield: TField
        :param out:
            Tensor field output. If specified, the output of the operation will
            be written to the given TField instance. If None, the operation
            returns a new TField instance.
        :type out: Union[TField, None]
        :param opkwargs:
            Additional keyword arguments to operation.
        :type opkwargs: Any

        :returns:
            Result of operation (only if out=None).
        :rtype: Union[TField, None]

        """
        self.tf = getattr(tf, "__tfield__", lambda: None)()
        assert self.tf is not None, "TField-like input required for operation."
        self.opargs = self.default_opargs if not opargs else opargs
        self.opkwargs = self.default_opkwargs.copy()
        self.opkwargs.update(**opkwargs)

        # Chunking
        self.chunks = self.chunker(self.tf, out=out)

        # Prepare operation
        self.prepare()
        token = self.initargs[0]

        # Apply the operation on the chunks of the input TField
        chunking = self.chunking()
        if chunking and (self.cpu.real > 1):
            import multiprocessing as mp
            workerfunc = partial(self.worker, token)
            ctx = mp.get_context(ts.MP_CONTEXT)
            with ctx.Pool(processes=int(self.cpu.real),
                          initializer=self.init_worker,
                          initargs=self.initargs) as p:
                p.map(workerfunc, self.chunks)
                p.close()
                p.join()

        elif chunking and (self.cpu.imag > 1):
            import multiprocessing.dummy as mt
            workerfunc = partial(self.worker, token)
            with mt.Pool(processes=int(self.cpu.imag),
                         initializer=self.init_worker,
                         initargs=self.initargs) as p:
                p.map(workerfunc, self.chunks)
                p.close()
                p.join()
        else:
            self.init_worker(*self.initargs)
            for chunk in self.chunks:
                self.worker(token, chunk)

        # Finalise operation
        ret = self.finalise()

        # Allow type casting and other type-specific afterworks
        ret = getattr(tf, "__operation_finalise__", lambda *x: ret)(
            ret, op=self, template=tf)

        # Free up memory
        self.free()

        if out is not None:
            out[...] = ret.data[...]
        else:
            return ret

    def chunker(self, *args, **kwargs):
        if self._chunker is not None:
            return self._chunker(*args, **kwargs)
        else:
            return self._auto_chunk(*args, **kwargs)

    def chunking(self):
        return self.tf.numel > self.threshold

    def _default_chunker(self, tf, out=None):
        """
        Default chunker function for the operator instance. This method SHOULD
        be overloaded in subclasses to create highly specialised Operators that
        perform advanced chunking that is optimised for a certain type of
        operation.

        If the method is not implemented, the Operator must be initialised with
        a suitable chunker function.

        :param tf:
            Placeholder argument for input TField instance.
        :type tf: TField
        :param op:
            Placeholder argument for operation.
        :type op: callable
        :param out:
            Placeholder argument for the output TField. If None, the chunker
            function must redirect the output of the operation to a new TField
            instance.
        :type out: Union[TField, None]
        :param opargs:
            Additional positional arguments to the operation function. The
            chunker function must be implemented in a way that it passes this
            on to the operation function.
        :type args: Any
        :params opkwargs:
            Additional keyword arguments to the operation function. The chunker
            function must be implemented in a way that it passes these on to
            the operation function.
        :type opkwargs: Any

        """
        pass

    @property
    def default_opargs(self):
        return self._default_opargs

    @default_opargs.setter
    def default_opargs(self, args):
        if args is None:
            self._default_opargs = ()
        elif isinstance(args, (tuple, list)):
            self._default_opargs = tuple(args)
        else:
            raise TypeError(f"Invalid positional arguments "
                            f"for operation: {args}")

    @property
    def default_opkwargs(self):
        return self._default_opkwargs

    @default_opkwargs.setter
    def default_opkwargs(self, kwargs):
        if kwargs is None:
            self._default_opkwargs = dict()
        elif isinstance(kwargs, dict):
            self._default_opkwargs = dict(**kwargs)
        else:
            raise TypeError(f"Invalid keyword arguments "
                            f"for operation: {kwargs}")

    def finalise(self):
        pass

    def free(self):
        del self.chunks
        del self.tf
        self._opargs = ()
        self._opkwargs = dict()

    def init_worker(self, *args, **kwargs):
        pass

    @property
    def cpu(self):
        return self._cpu

    @cpu.setter
    def cpu(self, n_cpu):
        import multiprocessing as mp
        # Note: the EvaluationManager might operate in a daemon process, where
        # it is unable to fork more processes.
        if mp.parent_process() is None:
            self._cpu = tu.verify_n_cpu(n_cpu)
        else:
            self._cpu = 1

    @property
    def opargs(self):
        return self._opargs

    @opargs.setter
    def opargs(self, args):
        if args is None:
            self._opargs = ()
        elif isinstance(args, (tuple, list)):
            self._opargs = tuple(args)
        else:
            raise TypeError(f"Invalid positional arguments "
                            f"for operation: {args}")

    @property
    def opkwargs(self):
        return self._opkwargs

    @opkwargs.setter
    def opkwargs(self, kwargs):
        if kwargs is None:
            self._opkwargs = dict()
        elif isinstance(kwargs, dict):
            self._opkwargs = dict(**kwargs)
        else:
            raise TypeError(f"Invalid keyword arguments "
                            f"for operation: {kwargs}")

    def operation(self, x):
        return self._operation(x, *self.opargs, **self.opkwargs)

    def prepare(self):
        pass

    def worker(self, token, job):
        pass


if __name__ == "__main__":
    print("This module is not intended for execution.")

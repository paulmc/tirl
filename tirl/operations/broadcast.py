#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np


# TIRL IMPORTS

from tirl import settings as ts
from tirl.operations.tensor import TensorOperator


# IMPLEMENTATION

class TensorBroadcast(TensorOperator):
    """
    TensorBroadcast (child of TensorOperator) -
        broadcasting a tensor field to match a specified tensor shape.

    """
    def __init__(self, target_shape, n_cpu=ts.CPU_CORES, name=None):
        """
        Initialisation of TensorBroadcast.

        :param target_shape: Target tensor shape.
        :type target_shape: tuple[int]

        """
        super(TensorOperator, self).__init__(
            self.__operation, chunker=None, n_cpu=n_cpu, name=name,
            target_shape=target_shape)

    @staticmethod
    def __operation(x, target_shape):
        """
        Broadcast operation for voxel-major oriented chunks.

        :param x: (n_voxels, *tensor_shape) voxel-major oriented chunk
        :type x: np.ndarray
        :param target_shape: target tensor shape
        :type target_shape: tuple[int]

        :return: broadcasted chunk
        :rtype: np.ndarray

        """
        shape = x.shape[1:]
        extra_dims = len(target_shape) - len(shape)
        slicer = (slice(None), *((np.newaxis,) * extra_dims), Ellipsis)
        x = x[slicer]
        if extra_dims:
            for axis, reps in enumerate(target_shape[:extra_dims]):
                x = np.repeat(x, reps, axis=axis + 1)
        else:
            if shape[0] == 1:
                x = np.repeat(x, target_shape[0], axis=1)
            else:
                raise AssertionError("Tensor shape {} is not broadcastable "
                                     "to {}.".format(shape, target_shape))
        return x


if __name__ == "__main__":
    print("This module is not intended for execution.")

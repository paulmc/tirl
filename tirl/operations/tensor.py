#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import psutil
import numpy as np
import multiprocessing as mp


# TIRL IMPORTS

from tirl.constants import *
from tirl import utils as tu
from tirl import settings as ts
from tirl.dataobj import DataObject
from tirl.operations.operations import Operator


# DEFINITIONS

process_wide_job_specific_globals = dict()


# IMPLEMENTATION

class TensorOperator(Operator):
    """
    The TensorOperator class is a container suitable for operations that are
    carried out on the tensor domain of TField independently for each voxel.

    An example operation of this kind would be calculating the determinant of
    square tensors, or reducing vectors by calculating their L2 norm at each
    voxel.

    """
    def __init__(self, operation, n_cpu=ts.CPU_CORES, name=None,
                 threshold=ts.CHUNK_THRESHOLD, opargs=None, opkwargs=None,
                 **kwargs):
        """
        Intialisation of TensorOperator.

        :param operation:
            Operation that will be carried out on the tensor domain of the
            input TField, defined as a function with the input TField as its
            first argument.
        :type operation: function
        :param n_cpu:
            Number of CPU cores used to process chunks. Note that each CPU core
            must have enough memory to accommodate a chunk with the maximum
            allowable chunksize given by ts.TFIELD_CHUNKSIZE in MiB. If -1, all
            available logical cores will be used.
        :type n_cpu: Union[int, np.integer]

        """
        super(TensorOperator, self).__init__(
            operation, chunker=None, n_cpu=n_cpu, name=name,
            threshold=threshold, opargs=opargs, opkwargs=opkwargs, **kwargs)

    def _auto_chunk(self, tf, out=None):
        """
        Starts _default_chunker if the input tensor field is not in memory;
        otherwise calls the _no_chunker method for faster performance.

        """
        if self.chunking():
            yield from self._default_chunker(tf, out)
        else:
            yield (slice(None),)

    def finalise(self):
        """
        Creates TField instance from output buffer.

        """
        self.out.owner = self.outbuff_ownership

        # Return TField with in-memory DataObject if the source was also in-mem
        inmem = self.out.nbytes <= tu.nbytes(ts.TFIELD_INSTANCE_MEMORY_LIMIT)
        if (self.tf._dataobj.kind == "Memory") and inmem:
            newdataobj = DataObject(shape=self.out.shape, dtype=self.out.dtype)
            newdataobj.value[...] = self.out.value[...]
            self.out = newdataobj

        return type(self.tf)(
            self.out, vshape=self.tf.vshape, order=self.tf.order,
            dtype=self.out.dtype, name=None, storage=self.tf.storage,
            copy=False, domain=self.tf.domain[:, :],
            header=self.tf.header.copy(), evmgr=self.tf.evmgr,
            rule=self.tf.rule)

    def free(self):
        super(TensorOperator, self).free()

        # Remove global variables pertinent to the last completed operation
        # Note: this is crucial to be here to avoid memory leaks. Memory leak
        # was observed when this was part of the call instructions of the base
        # class.
        global process_wide_job_specific_globals
        if process_wide_job_specific_globals:
            token = self.initargs[0]
            del process_wide_job_specific_globals[token]

        del self.out_dtype
        del self.out_tshape
        self.out = None
        input_buffer = self.initargs[1]
        input_buffer.owner = self.inbuff_ownership
        self.initargs = ()
        del input_buffer

    def prepare(self):
        """
        Prepares input and output buffers as DataObject instances.

        """
        # Infer output tshape and dtype by testing the operation
        self.test_operation()

        # Input buffer
        dataobj = self.tf._dataobj
        parallelisation = (self.cpu.real > 1) or (self.cpu.imag > 1)
        if self.tf._dataobj.kind in ("Memory", "Buffer"):
            if parallelisation:
                inbuff = DataObject(
                    "shared", shape=dataobj.shape, dtype=dataobj.dtype)
                inbuff.value[...] = dataobj.value[...]
                inbuff.flush()
            else:
                inbuff = dataobj
        elif dataobj.kind == "MemoryMap":
            inbuff = dataobj
        elif dataobj.kind == "SharedMemory":
            inbuff = dataobj
        else:
            raise ValueError(f"Invalid kind of DataObject: {dataobj.kind}")

        # Output buffer
        # Assumption: vshape and tensor layout are unchanged
        if self.tf.order == TENSOR_MAJOR:
            shape = (*self.out_tshape, *self.tf.vshape)
        elif self.tf.order == VOXEL_MAJOR:
            shape = (*self.tf.vshape, *self.out_tshape)
        else:
            assert False, "Internal error. Please report bug."
        if parallelisation:
            kind = "shared" if self.tf.storage in (SHMEM, MEM) else "memmap"
            outsize = np.dtype(self.out_dtype).itemsize * np.prod(shape)
            if outsize > tu.nbytes(ts.TFIELD_INSTANCE_MEMORY_LIMIT):
                kind = "memmap"
            outbuff = DataObject(kind, shape=shape, dtype=self.out_dtype)
        else:
            if dataobj.kind == "MemoryMap":
                args = ("memmap",)
            elif dataobj.kind == "SharedMemory":
                args = ("shared",)
            else:
                args = ()
            outbuff = DataObject(*args, shape=shape, dtype=self.out_dtype)

        self.inbuff_ownership = inbuff.owner
        self.outbuff_ownership = outbuff.owner
        inbuff.owner = False
        outbuff.owner = False

        # Add to initialisation arguments
        self.out = outbuff
        token = tu.timehash() + "_" + str(mp.current_process().pid)
        self.initargs = (token, inbuff, self.tf.taxes, self.tf.vaxes,
                         self.tf.order, outbuff)

    def _default_chunker(self, tf, out=None):
        """
        The default chunker method of TensorOperator imposes reduced (1-D)
        VOXEL_MAJOR ordering on the input TField, and vectorises the specified
        operation to be carried out on all tensors. If necessary, the voxels
        are processed in chunks, optionally by parellel processes. The input
        operation must be able to work with (n_voxels, *tensor_shape) inputs,
        and return

        :param tf: Input TField.
        :type tf: TField
        :param out:
            Output TField. If None, the chunker function will redirect the
            output of the operation to a new TField instance.
        :type out: Union[TField, None]

        """
        self.test_operation()
        # Set the shape of the first tile
        chunksize = self.chunksize or \
            self._chunksize_from_memlimit(self.out_tshape, self.out_dtype)
        for start in range(0, self.tf.vsize, chunksize):
            end = int(min(start + chunksize, self.tf.vsize))
            yield (slice(start, end),)

    def _get_minimal_vmsample(self):
        return self.tf.dataview(VOXEL_MAJOR).reshape(-1, *self.tf.tshape)[[0]]

    def __reduce__(self):
        args = (self._operation, self.cpu, self.name, self.threshold,
                self.opargs, self.opkwargs)
        kwargs = dict(
            chunksize=self.chunksize,
            memlimit=self.memlimit,
            initargs=self.initargs,
            _default_opargs=self.default_opargs,
            _default_opkwargs=self.default_opkwargs,
            _opargs=self.opargs,
            _opkwargs=self.opkwargs
        )
        return self.__class__, args, kwargs

    def init_worker(self, token, datain, taxes, vaxes, order, dataout):
        # Convert input buffer to reduced (1-D) VOXEL_MAJOR layout
        tshape = [dim for ax, dim in enumerate(datain.shape) if ax in taxes]
        wdata = datain.value.transpose(vaxes + taxes).reshape(-1, *tshape)
        # Set global variables for worker process
        global process_wide_job_specific_global
        process_wide_job_specific_globals[token] = dict(
            wdata=wdata,
            order=order,
            wout=dataout
            # lock=l
        )

    def test_operation(self):
        # Test the effect of the operation on tensors
        minsample = self._get_minimal_vmsample()
        res = self.operation(minsample)
        assert res.shape[0] == minsample.shape[0], \
            "The tensor operation must preserve the number of voxels."
        self.out_tshape = res.shape[1:]
        self.out_dtype = res.dtype
        return res

    def worker(self, token, job):
        """
        Parallel worker that applies the operation on a single chunk of the
        source data and writes the approved portion of the result to the output
        buffer.

        """
        # Load job-specific globals
        varargs = process_wide_job_specific_globals[token]
        wdata   = varargs["wdata"]
        order   = varargs["order"]
        wout    = varargs["wout"]
        # lock    = varargs["lock"]

        # Operate on a specific segment of the input buffer (wdata)
        # wdata is in reduced (1-D) VOXEL_MAJOR layout
        ss = tuple(job) + (Ellipsis,)
        sample = wdata[ss]
        res = self.operation(sample)
        start, end, step = ss[0].indices(wdata.shape[0])
        assert res.shape[0] == int(max(end - start, 1)), \
            "Tensor operation must not change the number of voxels."
        tshape = res.shape[1:]

        if order == VOXEL_MAJOR:
            #lock.acquire()
            wout.value.reshape(-1, *tshape)[start:end] = res
            if hasattr(wout, "flush"):
                wout.flush()
            #lock.release()
        else:
            #lock.acquire()
            wout.value.reshape(*tshape, -1)[..., start:end] = res
            if hasattr(wout, "flush"):
                wout.flush()
            #lock.release()

        del res

    def _chunksize_from_memlimit(self, tshape=None, dtype=None):
        """
        Calculates the maximum number of image points that are allowed to be
        operated on given the currently available memory and the instance
        memory limit.

        """
        # To avoid the overhead of optimising chunk sizes based on too many
        # factors, we are taking the assumption that an operation requires 3+
        # times as much memory as the size of chunk, and the chunk data is cast
        # to the data type of the output first. We further assume that the
        # internal container is 64-bit. This still ignores a potentially
        # inefficiently implemented operator, as well as the fact that the
        # output may have different tensor and/or voxel shapes.
        dtype = self.tf.dtype if dtype is None else dtype
        tshape = self.tf.tshape if tshape is None else tshape
        available = min(psutil.virtual_memory().available, self.memlimit)
        n_cpu = self.cpu.real if self.cpu.real > 1 else max(self.cpu.imag, 1)
        # itemsize = max(np.dtype(dtype).itemsize,
        #                np.dtype(self.tf.dtype).itemsize)
        tsize = max(self.tf.tsize, np.prod(tshape))
        itemsize = tsize * np.dtype(float).itemsize * 3
        return int(available / n_cpu / itemsize)

#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np
from operator import mul
from functools import reduce
from numbers import Integral


# TIRL IMPORTS

from tirl import settings as ts
from tirl.operations.spatial import SpatialOperator


# IMPLEMENTATION

class Robust(SpatialOperator):
    """
    Clips tensor values below and above the element-specific robust range. The
    robust range is defined by percentiles (default: 1 and 99).

    """
    def __init__(self, low=1, high=99, n_cpu=ts.CPU_CORES, name=None,
                 threshold=ts.OPERATION_CHUNK_THRESHOLD, opargs=None,
                 opkwargs=None, **kwargs):
        """
        Initialisation of Robust.

        :param low:
        :param high:
        :param n_cpu:
        :param name:
        :param opkwargs:
        """
        super(Robust, self).__init__(
            self._clip, radius=1, n_cpu=n_cpu, name=name,
            threshold=threshold, opargs=opargs, opkwargs=opkwargs)
        self.low = low
        self.high = high

    def __reduce__(self):
        args = (self.low, self.high, self.cpu, self.name,
                self.threshold, self.opargs, self.opkwargs)
        kwargs = dict(
            chunksize=self.chunksize,
            memlimit=self.memlimit,
            initargs=self.initargs,
            _default_opargs=self.default_opargs,
            _default_opkwargs=self.default_opkwargs,
            _opargs=self.opargs,
            _opkwargs=self.opkwargs
        )
        return self.__class__, args, kwargs

    def _clip(self, chunk, **opkwargs):
        tshape = tuple(chunk.shape[1:])
        taxes = tuple(range(1, chunk.ndim))
        n_channels = reduce(mul, tshape or (1,))
        a_min = opkwargs.get("a_min")
        if not hasattr(a_min, "__iter__") or len(a_min) == 1:
            a_min = (a_min,) * n_channels
        a_max = opkwargs.get("a_max")
        if not hasattr(a_max, "__iter__") or len(a_max) == 1:
            a_max = (a_max,) * n_channels
        result = np.zeros_like(chunk)
        if taxes:
            for i in range(n_channels):
                indices = np.unravel_index(i, tshape)
                slicer = tuple(slice(int(ix), int(ix) + 1) for ix in indices)
                result[slicer] = \
                    np.clip(chunk[slicer], a_min=a_min[i], a_max=a_max[i])
        else:
            result = np.clip(chunk, a_min=a_min[0], a_max=a_max[0])
        return result

    def prepare(self):
        # Calculate clipping values from percentiles
        low = np.percentile(self.tf.data, q=self.low, axis=self.tf.vaxes)
        high = np.percentile(self.tf.data, q=self.high, axis=self.tf.vaxes)
        # Set clipping values
        a_min = self.opkwargs.pop("a_min", low.ravel())
        a_max = self.opkwargs.pop("a_max", high.ravel())
        self.opkwargs.update(a_min=a_min, a_max=a_max)
        return super(Robust, self).prepare()

    @property
    def low(self):
        return self._low

    @low.setter
    def low(self, lo):
        self._low = self._set_value(lo)

    @property
    def high(self):
        return self._high

    @high.setter
    def high(self, hi):
        self._high = self._set_value(hi)

    def _set_value(self, val):
        if not isinstance(val, Integral):
            raise TypeError(f"Expected an integer, got {val} instead.")
        if (val >= 0) and (val <= 100):
            return int(val)
        else:
            raise ValueError(f"Invalid percentile [0,100]: {val}")

#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import psutil
import numpy as np
import multiprocessing as mp
from itertools import product


# TIRL IMPORTS

from tirl.constants import *
from tirl import utils as tu
from tirl import settings as ts
from tirl.dataobj import DataObject
from tirl.operations.operations import Operator


# DEFINITIONS

process_wide_job_specific_globals = dict()


# IMPLEMENTATION

class SpatialOperator(Operator):
    """
    The SpatialOperator class is a container suitable for operations that are
    carried out on the spatial domain of TField independently for each tensor
    element.

    An example operation of this kind would be the component-wise Gaussian
    smoothing or gradient filtering of a 3D displacement field.

    """
    def __init__(self, operation, radius=1, n_cpu=ts.CPU_CORES, name=None,
                 threshold=ts.CHUNK_THRESHOLD, opargs=None, opkwargs=None,
                 **kwargs):
        """
        Intialisation of SpatialOperator.

        :param operation:
            Operation that will be carried out on the spatial domain of the
            input TField, defined as a function with the input TField as its
            first argument.
        :type operation: function
        :param radius:
            Extent of the spatial neighbourhood of each voxel. This is taken
            into account when creating chunks to satisfy the neighbourhood
            dependencies of the calculation at all voxels. Default value is 1
            (Markov condition). If the operation depends on more distant
            neighbours than "radius", results at the chunk interfaces will be
            inaccurate.
        :type radius: Union[int, np.integer]
        :param n_cpu:
            Number of CPU cores used to process chunks. Note that each CPU core
            must have enough memory to accommodate a chunk with the maximum
            allowable chunksize given by ts.TFIELD_CHUNKSIZE in MiB. If -1, all
            available logical cores will be used.
        :type n_cpu: Union[int, np.integer]

        """
        # Initialise by the superclass method. Set chunker to None to use the
        # default chunker, which implements spatially convex chunking of the
        # tensor-major array, which is most suitable to support spatial
        # operations.
        super(SpatialOperator, self).__init__(
            operation, chunker=None, n_cpu=n_cpu, name=name,
            threshold=threshold, opargs=opargs, opkwargs=opkwargs, **kwargs)

        # Set radius
        self.radius = radius

    def __reduce__(self):
        args = (self._operation, self.radius, self.cpu, self.name,
                self.threshold, self.opargs, self.opkwargs)
        kwargs = dict(
            chunksize=self.chunksize,
            memlimit=self.memlimit,
            initargs=self.initargs,
            _default_opargs=self.default_opargs,
            _default_opkwargs=self.default_opkwargs,
            _opargs=self.opargs,
            _opkwargs=self.opkwargs
        )
        return self.__class__, args, kwargs

    def _auto_chunk(self, tf, out=None):
        """
        Starts _default_chunker if the input tensor field is not in memory;
        otherwise calls the _no_chunker method for faster performance.

        """
        if self.chunking():
            return tuple(sl for sl in self._default_chunker(tf, out))
        else:
            return tuple([(slice(None),) * tf.vdim])

    def finalise(self):
        """
        Creates TField from output buffer.

        """
        self.out.owner = self.outbuff_ownership

        # Return TField with in-memory DataObject if the source was also in-mem
        inmem = self.out.nbytes <= tu.nbytes(ts.TFIELD_INSTANCE_MEMORY_LIMIT)
        if (self.tf._dataobj.kind == "Memory") and inmem:
            newdataobj = DataObject(shape=self.out.shape, dtype=self.out.dtype,
                                    name="remem_output_buffer")
            newdataobj.value[...] = self.out.value[...]
            self.out = newdataobj

        return type(self.tf)(
            self.out, vshape=self.tf.vshape, order=self.tf.order,
            dtype=self.out.dtype, name=None, storage=self.tf.storage,
            copy=False, domain=self.tf.domain[:, :],
            header=self.tf.header.copy(), evmgr=self.tf.evmgr,
            rule=self.tf.rule)

    def free(self):
        super(SpatialOperator, self).free()

        # Remove global variables pertinent to the last completed operation
        # Note: this is crucial to be here to avoid memory leaks. Memory leak
        # was observed when this was part of the call instructions of the base
        # class.
        global process_wide_job_specific_globals
        if process_wide_job_specific_globals:
            token = self.initargs[0]
            del process_wide_job_specific_globals[token]

        del self.out_dtype
        del self.out_tshape
        self.out = None
        input_buffer = self.initargs[1]
        input_buffer.owner = self.inbuff_ownership
        self.initargs = ()
        del input_buffer

    def prepare(self):
        """
        Prepares input and output buffers as DataObject instances.

        """
        # Infer output tshape and dtype by testing the operation
        self.test_operation()

        # Input buffer
        dataobj = self.tf._dataobj
        parallelisation = (self.cpu.real > 1) or (self.cpu.imag > 1)
        if self.tf._dataobj.kind in ("Memory", "Buffer"):
            if parallelisation:
                inbuff = DataObject(
                    "shared", shape=dataobj.shape, dtype=dataobj.dtype,
                    name="shared_input_buffer")
                inbuff.value[...] = dataobj.value[...]
                inbuff.flush()
            else:
                inbuff = dataobj
        elif dataobj.kind == "MemoryMap":
            inbuff = dataobj
        elif dataobj.kind == "SharedMemory":
            inbuff = dataobj
        else:
            raise ValueError(f"Invalid kind of DataObject: {dataobj.kind}")

        # Output buffer
        # Assumption: vshape and tensor layout are unchanged
        if self.tf.order == TENSOR_MAJOR:
            shape = (*self.out_tshape, *self.tf.vshape)
        elif self.tf.order == VOXEL_MAJOR:
            shape = (*self.tf.vshape, *self.out_tshape)
        else:
            assert False, "Internal error. Please report bug."
        if parallelisation:
            kind = "shared" if self.tf.storage in (SHMEM, MEM) else "memmap"
            outsize = np.dtype(self.out_dtype).itemsize * np.prod(shape)
            if outsize > tu.nbytes(ts.TFIELD_INSTANCE_MEMORY_LIMIT):
                kind = "memmap"
            outbuff = DataObject(kind, shape=shape, dtype=self.out_dtype,
                                 name="memmap_output_buffer")
        else:
            if dataobj.kind == "MemoryMap":
                args = ("memmap",)
            elif dataobj.kind == "SharedMemory":
                args = ("shared",)
            else:
                args = ()
            outbuff = DataObject(*args, shape=shape, dtype=self.out_dtype)

        self.inbuff_ownership = inbuff.owner
        self.outbuff_ownership = outbuff.owner
        inbuff.owner = False
        outbuff.owner = False

        # Add to initialisation arguments
        self.out = outbuff
        token = tu.timehash() + "_" + str(mp.current_process().pid)
        self.initargs = (token, inbuff, self.tf.taxes, self.tf.vaxes,
                         self.tf.order, outbuff)

    def _default_chunker(self, tf, out=None):
        """
        The default chunker method of SpatialOperator imposes tensor-major
        ordering on the input TField, and creates spatially convex chunks with
        the largest possible extent over the contiguous array dimension
        (last dimension for C-contiguous arrays) and decreasing sizes towards
        the axis with the largest strides. The spatial extent is chosen
        along each dimension such that the neighbourhood dependency is
        satisfied for all spatial dimensions.

        The following assumptions are made:

            1. The operation does not change the voxel shape.
            2. The result of the operation cannot be trusted on the margin of
               the tile, within the extent that is defined by the radius.
            3. The (added) memory footprint of the operation is at most the
               size of the output data at 64-bit data type.
            4. The best chunk shape is what minimises read overhead and uses
               the maximum available memory to perform the operation, also
               taking the number of processes into account.

        :param tf: Input TField.
        :type tf: TField
        :param out:
            Output TField. If None, the chunker function will redirect the
            output of the operation to a new TField instance.
        :type out: Union[TField, None]


        """
        self.test_operation()
        # Set the shape of the first tile
        chunksize = self.chunksize or \
            self._chunksize_from_memlimit(self.out_tshape, self.out_dtype)
        chunkshape = np.asarray(tf.tshape + (1 + 2 * self.radius,) * tf.vdim)
        if np.product(chunkshape) > chunksize:
            raise MemoryError("Not enough memory for minimal chunk.")
        tmshape = tf.tshape[:tf.tdim] + tf.vshape
        for ax in range(tf.tdim + tf.vdim - 1, -1, -1):
            axfactor = np.product(chunkshape) / chunkshape[ax]
            newsize = chunksize / axfactor
            if newsize - chunkshape[ax] >= 1:
                chunkshape[ax] = min(int(newsize), tmshape[ax])
            else:
                break

        # Generate overlapping chunks
        axissteps = [range(0, ts - 2 * self.radius, cs - 2 * self.radius)
                     for ts, cs in zip(tf.vshape, chunkshape[tf.tdim:])]
        for offset in product(*axissteps):
            shape = np.minimum(np.subtract(tf.vshape, offset),
                               chunkshape[tf.tdim:])
            yield tuple(slice(start, stop) for start, stop
                        in zip(offset, np.add(offset, shape)))

    def _get_minimal_tmsample(self):
        vs = tuple(slice(int(min(2 * self.radius + 1, ax)))
                   for ax in self.tf.vshape)
        slicer = (Ellipsis, *vs)
        return self.tf.dataview(TENSOR_MAJOR)[slicer]

    # def __getstate__(self):
    #     return dict(
    #         _operation=self._operation,
    #         _chunker=self._chunker,
    #         n_cpu=self.cpu,
    #         name=self.name,
    #         threshold=self.threshold,
    #         _default_opargs=self.default_opargs,
    #         _default_opkwargs=self.default_opkwargs,
    #         _opargs=self.opargs,
    #         _opkwargs=self.opkwargs,
    #         chunksize=self.chunksize,
    #         memlimit=self.memlimit,
    #         _radius=self.radius
    #     )
    #
    # def __setstate__(self, state):
    #     for key, value in state.items():
    #         setattr(self, key, value)

    def test_operation(self):
        # Test the effect of the operation on tensors
        minsample = self._get_minimal_tmsample()
        res = self.operation(minsample)
        assert res.shape[-self.tf.vdim:] == minsample.shape[-self.tf.vdim:], \
            "The spatial operation must preserve the number of voxels."
        self.out_tshape = res.shape[:-self.tf.vdim]
        self.out_dtype = res.dtype
        return res

    def _chunksize_from_memlimit(self, tshape=None, dtype=None):
        """
        Calculates the maximum number of image points that are allowed to be
        operated on given the currently available memory and the instance
        memory limit.

        """
        # To avoid the overhead of optimising chunk sizes based on too many
        # factors, we are taking the assumption that an operation requires 3+
        # times as much memory as the size of chunk, and the chunk data is cast
        # to the data type of the output first. We further assume that the
        # internal container is 64-bit. This still ignores a potentially
        # inefficiently implemented operator, as well as the fact that the
        # output may have different tensor and/or voxel shapes.
        dtype = self.tf.dtype if dtype is None else dtype
        available = min(psutil.virtual_memory().available, self.memlimit)
        n_cpu = self.cpu.real if self.cpu.real > 1 else max(self.cpu.imag, 1)
        itemsize = max(np.dtype(dtype).itemsize,
                       np.dtype(self.tf.dtype).itemsize)
        tsize = max(self.tf.tsize, np.prod(tshape))
        itemsize = itemsize * tsize * 3
        return int(available / n_cpu / itemsize)

    @property
    def radius(self):
        return self._radius

    @radius.setter
    def radius(self, r):
        if isinstance(r, (int, np.integer)):
            if r >= 0:
                self._radius = int(r)
            else:
                raise ValueError("Neighbourhood radius cannot be negative.")
        else:
            raise ValueError("Neighbourhood radius must be integer.")

    def init_worker(self, token, datain, taxes, vaxes, order, dataout):
        # pid = mp.current_process().pid
        # Convert input buffer to TENSOR_MAJOR layout
        vshape = [dim for ax, dim in enumerate(datain.shape) if ax in vaxes]
        wdata = datain.value.transpose(taxes + vaxes)
        # Set global variables for worker process
        global process_wide_job_specific_global
        process_wide_job_specific_globals[token] = dict(
            wdata=wdata,
            vshape=vshape,
            order=order,
            wout=dataout,
            # lock=l
        )

    def worker(self, token, job):
        """
        Parallel worker that applies the operation on a single chunk of the
        source data and writes the approved portion of the result to the output
        buffer.

        """
        # Load job-specific globals
        varargs = process_wide_job_specific_globals[token]
        wdata     = varargs["wdata"]
        vshape    = varargs["vshape"]
        order     = varargs["order"]
        wout      = varargs["wout"]
        # lock      = varargs["lock"]

        # pid = mp.current_process().pid
        # parent = mp.parent_process()
        # if parent is not None:
        #     print(f"Process {pid} has parent: {parent.pid}")

        # Operate on a specific segment of the input buffer
        ss = (Ellipsis,) + tuple(job)
        res = self.operation(wdata[ss])

        # Establish which voxel results are approved
        # E.g. voxels close to the boundary are not approved, because their
        # exact values would depend on neighbouring voxels that are not part of
        # the current segment. self.radius defines the extent of the untrusted
        # marginal zone.
        vdim = len(job)
        outslicer = []
        resslicer = []
        for ax, sl in enumerate(job):
            start, stop, step = sl.indices(vshape[ax])
            oarg1 = start + self.radius if start > 0 else start
            rarg1 = self.radius if start > 0 else 0
            oarg2 = stop - self.radius if stop < vshape[ax] else stop
            dim = stop - start
            rarg2 = dim - self.radius if stop < vshape[ax] else None
            outslicer.append(slice(oarg1, oarg2))
            resslicer.append(slice(rarg1, rarg2))
        else:
            resslicer = (Ellipsis,) + tuple(resslicer)
            if order == TENSOR_MAJOR:
                outslicer = (Ellipsis,) + tuple(outslicer)
            else:
                outslicer = tuple(outslicer) + (Ellipsis,)

        # Write result to the target buffer
        if order == TENSOR_MAJOR:
            # lock.acquire()
            wout[outslicer] = res[resslicer]
            if hasattr(wout, "flush"):
                wout.flush()
            # lock.release()
        else:
            tdim = res.ndim - vdim
            axes = (*tuple(range(tdim, res.ndim)), *tuple(range(tdim)))
            # lock.acquire()
            wout[outslicer] = res[resslicer].transpose(axes)
            if hasattr(wout, "flush"):
                wout.flush()
            # lock.release()
        del res

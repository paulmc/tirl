#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import os
import dill
import numpy as np
from numbers import Number


# TIRL IMPORTS

import tirl
from tirl.tirlobject import TIRLObject
from tirl.tfieldmixin import TFieldMixin
from tirl.resmgr import ResolutionManager
from tirl.domain import Domain
from tirl.tfield import TField
from tirl.operations.operations import Operator
from tirl.operations.spatial import SpatialOperator
from tirl.transformations.translation import TxTranslation
from tirl import tirlvision, utils as tu, settings as ts


# DEFINITIONS

from tirl.constants import *


# DEVELOPMENT NOTES


# IMPLEMENTATION

class TImage(TIRLObject, TFieldMixin, np.lib.mixins.NDArrayOperatorsMixin):
    """
    The TImage class implements a versatile image format that automatically
    loads data from the most common image formats, readily supports memory
    mapping, and provides full compatibility with all transformation objects in
    TIRL.

    TImage is a subclass of TField. The TImage class additionally supports
    dynamic up- and downscaling of the image without degradation of image
    quality on repeated samplings, masking, and additional image manipulation
    methods.

    """
    # Set the array priority higher than that of TField (15.0),
    # np.matrix (10.0), and np.array (0.0), so that this class can control
    # ufunc behaviour.
    __array_priority__ = 25.0

    ############################## CONSTRUCTORS ################################

    @classmethod
    def _constr_timage(cls, src, **kwargs):
        """
        Returns the input TImage instance if no other arguments are specified.

        If keyword arguments are specified, a new TImage instance is returned,
        whose attributes are updated according to the keywords. If the keywords
        permit, the new TImage instance will share data (and transformations)
        with the original instance. Where keywords prohibit, the data and/or
        the transformations will be copied instead. These changes will not
        affect the input instance, but image or parameter data may still be
        shared between the objects.

        :param src: source TImage instance
        :type src: TImage
        :param kwargs: keywords arguments for the new TImage instance
        :type kwargs: any

        :returns: new TImage instance with updated parameters
        :rtype: TImage

        """
        assert isinstance(src, cls), \
            f"Expected {cls.__name__} input, " \
            f"got {src.__class__.__name__} instead."

        if not kwargs:
            return src
        else:
            imgkwargs = dict(
                resmgr=src.resmgr,
                maskmgr=src.maskmgr,
                header=src.header
            )
            imgkwargs.update(kwargs)
            return cls._constr_resmgr(**imgkwargs)

    @classmethod
    def _constr_resmgr(cls, resmgr, **kwargs):
        """
        Creates TImage from a configured ResolutionManager instance.

        :param resmgr: configured ResolutionManager instance
        :type resmgr: ResolutionManager

        :returns: new TImage
        :rtype: TImage

        """
        obj = super().__new__(cls)
        obj.resmgr = resmgr
        layout = resmgr.layers[0].layout
        kwargs.pop("layout", None)
        obj.maskmgr = cls._get_mask_specification(layout=layout, **kwargs)
        obj.header = kwargs.pop("header", {})
        return obj

    @classmethod
    def _constr_tfield(cls, field, **kwargs):
        """
        Creates a TImage from a TField instance.

        :param field: input TField instance
        :type field: TField

        """
        assert isinstance(field, TField), \
            f"Expected TImage input, " \
            f"got {field.__class__.__name__} instead."

        field = TField(field, **kwargs)
        resmgr = ResolutionManager(field, **kwargs)
        kwargs.pop("layout", None)
        maskmgr = cls._get_mask_specification(layout=field.layout, **kwargs)
        header = kwargs.pop("header", {})  # no use of this as of now
        return cls._constr_resmgr(
            resmgr, maskmgr=maskmgr, header=header, **kwargs)

    @classmethod
    def _constr_ndarray(cls, data, **kwargs):
        """
        Creates a TImage from an ndarray (or memmap) instance. Shape

        :param arr: input array
        :type arr: np.ndarray

        """
        layer0 = TField(data, **kwargs)
        resmgr = ResolutionManager(layer0, **kwargs)
        kwargs.pop("layout", None)
        maskmgr = cls._get_mask_specification(layout=layer0.layout, **kwargs)
        header = kwargs.pop("header", {})  # no use of this as of now
        return cls._constr_resmgr(
            resmgr, maskmgr=maskmgr, header=header, **kwargs)

    @classmethod
    def _constr_file(cls, src, **kwargs):
        """
        Constructs a new TImage instance from file input.

        This constructor uses filetype-specific Loader instances to return an
        ndarray and header information. Specific loaders can be specified under
        the "loader" keyword in either of the following ways to override the
        default loader:

        1. Configured Loader instance
        2. Loader type (new instance with default arguments)
        3. Name of Loader type (new instance with default arguments)
        4. dict: {"type": "loadername", **loader_kwargs} (to add arguments)

        :returns: a new TImage instance
        :rtype: TImage

        """
        # TImage file
        if src.lower().endswith(ts.EXTENSIONS["TImage"]):
            timg = tirl.load(src)
            assert isinstance(timg.data, np.memmap)  # active layer only
            storage = kwargs.pop("storage", None)
            # Choose the most suitable storage mode if not specified
            if storage is None:
                maxsize = tu.nbytes(ts.TIMAGE_INSTANCE_MEMORY_LIMIT)
                if timg.data.nbytes > maxsize:
                    storage = HDD
                else:
                    n_cpu = tu.verify_n_cpu(ts.CPU_CORES)
                    parallelisation = (n_cpu.real > 1) or (n_cpu.imag > 1)
                    storage = SHMEM if parallelisation else MEM
            # Assign data from the appropriate container
            if storage in (MEM, SHMEM):
                for layer in timg.layers:
                    layer._update_data(
                        np.array(layer.data), tensor_axes=timg.taxes)
            elif storage == HDD:
                timg.header.update(filename=timg.data.filename)
            return cls._constr_timage(timg, storage=storage, **kwargs)

        # TField file -> create a TField first
        elif src.lower().endswith(ts.EXTENSIONS["TField"]):
            imfield = TField(src, **kwargs)
            # Transfer the contents of the TField header to the TImage header
            imfield.header.update(kwargs.pop("header", {}))
            return cls(imfield, **kwargs)

        # Any other image file --> use a loader
        else:
            loader = kwargs.pop("loader", None)
            loader = TField._get_loader_specification(
                file=src, loader=loader, **kwargs)
            # Apply Loader and configure output
            data, hdr = loader(src)
            header = kwargs.pop("header", {})
            if hdr is not None:
                header.update(hdr)
            if isinstance(data, np.ndarray):
                return cls._constr_ndarray(data, header=header, **kwargs)
            elif hasattr(data, "__iter__"):
                resmgr = ResolutionManager(*tuple(TField(arr) for arr in data))
                return cls._constr_resmgr(resmgr, header=header, **kwargs)
            else:
                raise AssertionError("Internal error. Please report bug.")

    @staticmethod
    def _get_mask_specification(**kwargs):
        """
        Returns a ResolutionManager from multiple different mask specifications.
        Returns None if no mask is specified in the inputs.
        Raises AssertionError if the specifications are contradicting.

        """
        maskmgr = kwargs.pop("maskmgr", None)
        mask = kwargs.pop("mask", None)
        dtype = kwargs.pop("dtype", None)
        layout = TField._get_shape_specification(**kwargs)

        if (maskmgr is None) and (mask is None):
            return None
        elif isinstance(maskmgr, ResolutionManager):
            mvsh = maskmgr.get().vshape
            if mvsh != layout.vshape:
                aspect_ratio = np.divide(mvsh, layout.vshape)
                if not np.allclose(aspect_ratio, aspect_ratio[0], rtol=1e-1):
                    raise AssertionError(
                        f"The aspect ratio of the mask is different from "
                        f"that of the image domain.")
            return maskmgr
        elif hasattr(mask, "__array__"):
            resolution = getattr(mask, "resolution", None)
            resolution = kwargs.get("resolution", resolution)
            resolution = resolution or np.ones(layout.vdim)
            mask = getattr(mask, "__array__")()
            input_dtype = mask.dtype
            if mask.max() > 1:
                mask = mask / mask.max()  # avoid dtype problem
            if mask.min() < 0:
                mask[mask < 0] = 0
            # Save disk space if the mask is binary (INH 7 Aug 2022)
            if np.issubdtype(input_dtype, np.integer):
                if np.unique(mask).size <= 2:
                    mask = np.asarray(mask, dtype=np.uint8)
                    dtype = np.uint8
            else:
                dtype = mask.dtype

            # Note: the specified mask must have conformant shape with the
            # active image layer. Otherwise an error will be triggered.
            name = "mask0"
            dtype = dtype or ts.DEFAULT_FLOAT_TYPE
            n_cpu = tu.verify_n_cpu(ts.CPU_CORES)
            parallelisation = (n_cpu.real > 1) or (n_cpu.imag > 1)
            memselect = SHMEM if parallelisation else MEM
            storage = HDD if isinstance(mask, np.memmap) else memselect
            storage = kwargs.pop("storage", storage)
            ipol = kwargs.pop("interpolator", ts.TIMAGE_MASK_INTERPOLATOR)
            kwargs.pop("taxes", None)
            mask = TField(
                mask, taxes=(), copy=False, dtype=dtype, name=name,
                storage=storage, interpolator=ipol)
            if mask.vshape != layout.vshape:
                aspect_ratio = np.divide(mask.vshape, layout.vshape)
                if not np.allclose(aspect_ratio, aspect_ratio[0], rtol=1e-1):
                    raise AssertionError(
                        f"The aspect ratio of the mask is different from "
                        f"that of the image domain.")
                resolution = np.divide(resolution, aspect_ratio)
            return ResolutionManager(mask, resolution=resolution)
        else:
            raise TypeError(
                f"Invalid mask format: {mask.__class__.__name__}")

    def __new__(cls, src, *args, **kwargs):
        """
        Main constructor of TImage. Options for construction:

        # Input data can be specified in the following formats:
            1. TImage
            2. ResolutionManager
            3. TField
            4. ndarray + shape specifications
            5. continuous buffer + data type specification + shape specification
            6. TImage/TField file
            7. Image file

        """
        if issubclass(src.__class__, cls):
            return cls._constr_timage(src, **kwargs)

        elif isinstance(src, ResolutionManager):
            return cls._constr_resmgr(src, **kwargs)

        elif isinstance(src, TField):
            return cls._constr_tfield(src, **kwargs)

        elif isinstance(src, np.ndarray) or isinstance(src, bytearray) or \
                hasattr(src, "__array__"):
            return cls._constr_ndarray(src, **kwargs)

        elif isinstance(src, str):
            if os.path.exists(src):
                return cls._constr_file(src, **kwargs)
            else:
                raise FileNotFoundError(f"File not found: {src}")

        else:
            raise AssertionError("Invalid constructor configuration.")

    def __init__(self, src, *args, **kwargs):
        """
        Initialisation of TImage.

        """
        super(TImage, self).__init__()
        self.name = kwargs.get("name", None)
        # Do not set global tensor transformation rule
        # (assume it has been already set for each layer separately)

    ############################# MAGIC METHODS ################################

    def __array__(self, *args):
        """
        Array interface of TImage.

        """
        return self.data.__array__(*args)

    def __array_ufunc__(self, ufunc, method, *inputs, **kwargs):

        default_order = VOXEL_MAJOR  # enables matrix-aware ufuncs!
        # Create a list of inputs converted to default-oriented ndarray
        args = []
        for input_ in inputs:
            if hasattr(input_, "__tfield__"):
                args.append(input_.dataview(default_order))
            else:
                args.append(input_)

        # Create a list of outputs converted to default-oriented ndarray
        outputs = kwargs.pop("out", None)
        if outputs:
            out_args = []
            for output in outputs:
                if hasattr(inputs[-1], "__tfield__"):
                    out_args.append(output.dataview(default_order))
                else:
                    out_args.append(output)
            kwargs["out"] = tuple(out_args)
        else:
            outputs = (None,) * ufunc.nout

        # If any of the inputs is a memory mapped array, redirect ufunc
        # operation to the dask implementation.
        # input_arrays = tuple(arr for arr in inputs if hasattr(arr, "__array__"))
        # if any(isinstance(getattr(arr, "__array__")(), np.memmap)
        #        for arr in input_arrays):
        #     raise NotImplementedError(
        #         "ufuncs for HDD objects are not supported in the current "
        #         "version of TIRL. Consider using objects from the TIRL "
        #         "Operator class.")

        # If all inputs are in-memory, redirect ufunc operation to the
        # NumPy implementation (coded in C).
        # else:
        results = self.data.__array_ufunc__(ufunc, method, *args, **kwargs)

        # Create a collection of the final results
        if ufunc.nout == 1:
            results = (results,)
        final_results = []
        for i, (result, output) in enumerate(zip(results, outputs)):
            if output is not None:
                if hasattr(output, "__tfield__"):
                    output.dataview(default_order)[...] = result
                else:
                    output[...] = result
                final_results.append(output)
            else:
                context = (ufunc.__name__, method, default_order)
                tfout = self.__array_wrap__(result, context=context)
                # The order of the output should match that of the ufunc handler
                if hasattr(tfout, "__tfield__"):
                    tfout.order = self.order
                final_results.append(tfout)

        if results is NotImplemented:
            return NotImplemented

        return final_results[0] if len(final_results) == 1 else final_results

    def __array_wrap__(self, arr, context=None):
        """
        Finishing step after one of NumPy's ufuncs was applied to the TImage.
        Subclasses SHOULD implement this method to provide appropriate type
        casting.

        :param arr: Result of the ufunc.
        :type arr: np.ndarray
        :param context:
            Ufunc context information. Specifies the order of the input TImage.
        :type context: Any

        :returns: type cast ufunc output object
        :rtype: TImage

        """
        # Call TField method (creates either a TField or an ndarray)
        result = TField.__array_wrap__(self.__tfield__(), arr, context=context)

        if isinstance(result, TField):
            if (self.mask is not None) and (result.vshape == self.vshape):
                mask = self.mask.copy()
            else:
                mask = None
            name = self.name + "_ufunc"
            result = type(self)(result, copy=False, mask=mask, name=name)

        return result

    def __operation_finalise__(self, tfout, op=None, template=None):
        """
        This method is called after an operation is performed on the TImage
        instance. Since members of the Operator class use the TField interface
        of the TImage, casting the output of the operation to TImage is
        necessary to ensure that the operation is 'type consistent', i.e. the
        result of the operation is also a TImage. Subclasses SHOULD overload
        this method to become fully compatible with Operators.

        :param tfout: TField output of the operation
        :type: tfout: TField
        :param op: operator
        :type op: Operator
        :param template: input object of the operation
        :type template: Any

        :returns: result of the operation
        :rtype: Any

        """
        # Call the TField method
        tfout = TField.__operation_finalise__(tfout)

        if self.maskmgr:
            mask = self.maskmgr.resize(*tfout.vshape)
        else:
            mask = None

        return type(self)(tfout, mask=mask)

    def __repr__(self):
        if self.order == VOXEL_MAJOR:
            s = "{0}(layer={1}/{2}, domain={3}, tensor={4}, dtype={5}, {6})"
            arg1 = self.vshape
            arg2 = self.tshape
        else:
            s = "{0}(layer={1}/{2}, tensor={3}, domain={4}, dtype={5}, {6})"
            arg1 = self.tshape
            arg2 = self.vshape
        return s.format(
            self.__class__.__name__, self.resmgr.active + 1,
            len(self.resmgr.layers), arg1, arg2, self.dtype, self.storage)

    def __slice_wrap__(self, ret, context=None):
        """
        Post-hoc type casting the result of TImage slicing. This prevents
        creating TField outputs when slicing a TImage.

        :param ret: TField slice, returned by TFieldIndexer
        :type ret: TField
        :param context: ("voxels"/"tensors", item) slicing context information
        :type: Any

        :returns: TImage slice
        :rtype: TImage

        """
        # Call TField method
        ret = TField.__slice_wrap__(self.__tfield__(), ret, context=context)

        # Slice mask if necessary
        mask = self.tmask()
        if mask is not None:
            if context[0] == "voxels":
                item = context[1]
                mask = mask.voxels[item]
            ret.mask = mask
        else:
            mask = None

        # Create TImage result with the same resolution manager
        return TImage(ret, copy=False, dtype=self.dtype, mask=mask,
                      storage=self.storage, rule=self.rule)

    def __tfield__(self):
        """
        Converts image data into a TField to provide compatibility with
        TField-specific operations.

        :returns: TImage data as TField
        :rtype: TField

        """
        return self.resmgr.get()

    def __timage__(self):
        """
        TImage interface. Subclasses of TImage should overload this method to
        provide backwards compatibility with TImage-specific functions and
        methods.

        """
        return self

    ############################ PROTECTED METHODS #############################

    def _dump(self):
        objdump = super(TImage, self)._dump()
        # The header might contain non-serialisable metadata
        try:
            hdr = dill.dumps(self.header)
        except dill.PicklingError:
            hdr = dill.dumps({})
        objdump.update(
            resmgr=self.resmgr,
            maskmgr=self.maskmgr,
            header=hdr,
            name=self.name
        )
        return objdump

    @classmethod
    def _load(cls, dump):
        resmgr = dump["resmgr"]
        maskmgr = dump["maskmgr"]
        header = dill.loads(dump["header"])
        name = dump["name"]
        return cls(resmgr, maskmgr=maskmgr, header=header, name=name)

    ############################ PUBLIC ATTRIBUTES #############################

    @property
    def header(self):
        return self._header

    @header.setter
    def header(self, hdr):
        if isinstance(hdr, dict):
            self._header = dict(**hdr)  # pure dict, not a subclass
        else:
            raise TypeError(f"Expected dict for header, "
                            f"got {hdr.__class__.__name__} instead.")

    @property
    def layers(self):
        """
        This property is provided to intuitively access the various layers of
        a TImage. A tuple is returned to prevent replacing layers or changing
        their order.

        """
        return tuple(self.resmgr.layers)

    @property
    def mask(self):
        """
        TImage mask.

        :returns: image mask
        :rtype: np.ndarray

        """
        tmask = self.tmask()
        return tmask.data if tmask is not None else None

    @property
    def maskmgr(self):
        return self._maskmgr

    @maskmgr.setter
    def maskmgr(self, mgr):
        if mgr is None:
            self._maskmgr = None
        elif isinstance(mgr, ResolutionManager):
            self._maskmgr = self._get_mask_specification(
                layout=self.layout, maskmgr=mgr)
        else:
            raise TypeError(f"Expected ResolutionManager instance, "
                            f"got {mgr.__class__.__name__} instead.")

    @property
    def resmgr(self):
        return self._resmgr

    @resmgr.setter
    def resmgr(self, mgr):
        if isinstance(mgr, ResolutionManager):
            self._resmgr = mgr
        else:
            raise TypeError(f"Expected ResolutionManager instance, "
                            f"got {mgr.__class__.__name__} instead.")

    @property
    def resolution(self):
        return self.resmgr.get_resolution()  # active layer

    @resolution.setter
    def resolution(self, res):
        """ Setting the resolution of TImage influences the resolution setting
        of all layers. """
        self.resmgr.set_resolution(res)  # active layer and others
        if self.maskmgr is not None:
            self.maskmgr.set_resolution(res)

    @mask.setter
    def mask(self, newmask):
        """
        TImage mask setter method. Regardless of the input format, only voxel
        shapes are matched, no evaluation takes place. This is to avoid
        erroneous mask definitions.

        """
        if newmask is None:
            self._maskmgr = None
        elif hasattr(newmask, "__array__"):
            self._maskmgr = self._get_mask_specification(
                layout=self.layout, mask=newmask, resolution=self.resolution)
        else:
            raise TypeError(f"Expected array-like instance, got "
                            f"{newmask.__class__.__name__} instead.")

    @property
    def rule(self):
        return self.__tfield__().rule

    @rule.setter
    def rule(self, r):
        """ Sets the tensor transformation rule globally for all layers. """
        if r in (RULE_SSR, RULE_FS, RULE_PPD, None):
            for layer in self.layers:
                layer.rule = r

    ############################ PUBLIC METHODS ################################

    def apply_mask(self, mask, normalise=True):
        """
        Multiplies TImage by the mask. Safe for shape errors and unnormalised
        values, which are common, due to images having 8-bit (0..255)
        intensities.

        """
        if normalise and np.count_nonzero(mask):
            mask = mask / np.max(mask)
        self.dataview(TENSOR_MAJOR)[...] *= mask

    def centralise(self, weighted=False):
        """
        Appends TImage transformations with a TxTranslation object to
        centralise the image in physical space.

        :param weighted:
            If True, tensors are reduced, and this scalar field is further
            multiplied by mask values at each voxel to give voxel-wise
            weighting factors for the coordinates.
        :type weighted: bool

        """
        offset = tuple(-x for x in self.centre(weighted))
        tx = TxTranslation(*offset, name="centralise")
        self.domain.chain.append(tx)

    def centre(self, weighted=False):
        """
        Returns the centre of the image in physical space.

        :param weighted:
            If weighted is True, the centre is the centre of gravity. If False,
            it is the geometrical centre of the image.

        :return: physical coordinates of image centre
        :rtype: tuple

        """
        pc = self.domain.get_physical_coordinates()
        if weighted:
            weights = self.tensors.reduce().data.ravel()
            if self.mask is not None:
                weights = weights * self.mask.ravel()
            factor = np.sum(weights)
        else:
            weights = 1
            factor = pc.shape[0]
        c = np.sum(pc.T * weights, axis=-1) / factor
        return tuple(float(x) for x in c.T)

    def copy(self, **kwargs):
        """
        TImage copy constructor.

        :param kwargs:
            Constructor keyword arguments provided here will override their
            values in the original object.
        :type kwargs: Any

        :returns:
            A new instance of TImage that will be the copy of the original
            object.
        :rtype: TImage

        """
        from tirl.utils import rcopy
        resmgr = self.resmgr.copy()
        if self.maskmgr:
            maskmgr = self.maskmgr.copy()
        else:
            maskmgr = None
        header = rcopy(self.header)
        rule = kwargs.pop("rule", self.rule)
        return type(self)(resmgr, maskmgr=maskmgr, header=header,
                          rule=rule, **kwargs)

    def interpolate(self, coords):
        """
        Returns the values of the active TImage layer at the specified voxel
        coordinates. The TImage mask is ignored.

        :param coords: (n_points, n_dims) array of voxel coordinates
        :type coords: np.ndarray

        :returns:
            (n_points, *t_shape) or (*t_shape, n_points) array of interpolants,
            depending on the tensor layout. The tensors are not transformed.
        :rtype: np.ndarray

        """
        return self.__tfield__().interpolate(coords)

    def evaluate(self, domain, rule="auto"):
        """
        Evaluates the active layer (and any accompanying mask) of the TImage
        on a specified domain. The original TImage instance is returned if the
        target domain is identical to the source domain. If the domain
        instances are different but equal, a copy of the original TImage
        instance is returned. Note that in every case, evaluation discards
        non-active layers of the TImage, hence the new instance will

        :param domain:
            Target domain. Tensor values will be evaluated at the points of
            this domain, specified by their physical coordinates.
        :type domain: Domain
        :param rule:
            Defines what happens to the tensors after evaluation at the target
            domain.
            1. None (default): tensor components are interpolated, and no
               further operations take place. This is recommended for
               transformation-invariant tensor fields, such as colour space
               vectors.
            2. "ppd": Preservation of principal directions.
            3. "FS"
        :type rule: str or None

        :returns:
            New TField instance with field estimates evaluated on the specified
            domain.
        :rtype: TField

        """
        rule = self.rule if str(rule).lower() == "auto" else rule

        # Domains are identical -> pass active layers by reference
        if domain is self.domain:
            newimg = self.resmgr.get()
            newmask = self.tmask()
            return TImage(newimg, mask=newmask, header=self.header)

        elif domain == self.domain:
            newimg = self.__tfield__().copy()
            newmask = self.tmask()
            if newmask is not None:
                newmask = newmask.copy()
            return TImage(newimg, mask=newmask, header=tu.rcopy(self.header))

        # Pass on the call to the active image layers (img and mask)
        if ts.TIMAGE_MASK_MISSING_DATA:
            self.interpolator.kwargs.update(fill_value=np.inf)
        else:
            self.interpolator.kwargs.update(fill_value=ts.FILL_VALUE)
        newimg = self.__tfield__().evaluate(domain, rule=rule)
        newmask = self.tmask()
        if newmask is not None:
            newmask.interpolator.kwargs.update(order=1, prefilter=False)
            newmask = newmask.evaluate(domain, rule=rule).data

        # Feature: auto-mask non-overlapping image area during registration
        if ts.TIMAGE_MASK_MISSING_DATA:
            if newmask is None:
                newmask = np.ones(domain.shape, dtype=ts.DEFAULT_FLOAT_TYPE)
            if newimg.taxes:
                indices = np.flatnonzero(np.all(
                    newimg.data == np.inf, axis=newimg.taxes))
            else:
                indices = np.flatnonzero(newimg.data == np.inf)
            newimg.data[~np.isfinite(newimg.data)] = ts.FILL_VALUE
            newmask.flat[indices] = 0
        header = tu.rcopy(self.header)
        return TImage(newimg, mask=newmask, header=header)

    def normalise(self):
        """
        Global normalisation of the image values to (0, 1). The changes are
        made in place, and may include changing the data type.

        """
        # TODO: This is not compatible with HDD arrays.
        data = (self.data - self.data.min())
        data = data / data.max()
        self.resmgr.get()._update_tfield(data, tensor_axes=self.taxes)

    def preview(self, centre=None, normal=None, resolution=1, **kwargs):
        """
        TImage preview by Tirlvision.

        :param centre:
            Centre of the viewing plane in physical coordinates
            (for 3D TImages only).
        :type centre: Union[tuple, list, np.ndarray]
        :param normal:
            Normal vector to the viewing plane (for 3D TImages only).
        :type normal: Union[tuple, list, np.ndarray]
        :param resolution:
            Canvas resolution (for 3D TImages only). Scale is arbitrary
            (depends on the scale of the TImage), but mm/px is the recommended
            convention.
        :type resolution: Union[int, float, np.integer, np.floating]

        """
        if self.vdim == 1:
            tirlvision.call("timage", "preview1d", self, **kwargs)
        elif self.vdim == 2:
            tirlvision.call("timage", "preview2d", self, **kwargs)
        elif self.vdim == 3:
            normal = normal or (0, 0, 1)
            tirlvision.call("timage", "preview3d", self, centre, normal,
                            resolution, **kwargs)
        else:
            raise NotImplementedError(
                "Preview is not implemented for image dimensions above 3.")

    def resample(self, *resolution, **kwargs):
        """
        Resamples the TImage data to match a specific resolution. The target
        resolution may be defined globally (isotropic sampling) or separately
        for each axis (anisotropic sampling).

        :param resolution: target resolution
        :type resolution: float
        :param kwargs: metadata for the new TImage level
        :type kwargs: any

        """
        target_scale = np.divide(self.layers[0].resolution, resolution)
        return self.rescale(*target_scale, **kwargs)

    def rescale(self, *scales, **kwargs):
        """
        Changes the resolution of the TImage instance by scaling its voxels.
        The rescaling only affects the resolution of the TImage, but not its
        physical extent. The dimensions of the new TImage are rounded to the
        nearest integer value after scaling.

        :param scales: dimension-specific scaling factors (or a global one)
        :type scales: float
        :param kwargs: metadata for the new TImage level
        :type kwargs: any

        :param copy:
            If True, a resampled version of the current TImage will be
            returned, which will share transformations with the original
            TImage instance. The new instance will also inherit the resolution
            manager of the current instance.
            If False  (default), the image data and the mask buffers of the
            current TImage instance will be overwritten, and the method will
            return nothing.
        :type copy: bool
        :param update_chain:
            If True (default), non-linear transformations that are linked with
            the TImage domain will remain linked, which implies that these
            will be resampled via their regrid() method. If False, all
            non-linear transformations will be detached and left unchanged.
            In either case the identity of all transformation objects is
            preserved (no copying will take place).
        :type update_chain: bool
        :param presmooth:
            Applies Gaussian presmoothing before downsampling (no smoothing is
            performed for upsampling). This ensures that all domain points at
            the lower resolution have proper support in the higher-resolution
            image, therefore all original information is (at least indirectly)
            represented.
        :type presmooth: bool

        """
        scales = scales * self.vdim if len(scales) == 1 else scales
        assert scales and all(dim > 0 for dim in scales), \
            "All scale factors must be positive."
        assert len(scales) == self.vdim, \
            "Scaling factors do not match the TImage dimensions."

        current_scale = \
            np.divide(self.resmgr.get_resolution(0), self.resolution)
        if np.allclose(scales, current_scale):
            imglevel = self.resmgr.active
        else:
            imglevel = self.resmgr._rescale(*scales)
        # A mask does not necessarily exist at the current resolution,
        # so create it if necessary.
        if self.maskmgr is not None:
            masklevel = self.maskmgr._rescale(*scales)
        else:
            masklevel = None

        # Create output if copy=True
        if kwargs.pop("copy", True):
            if masklevel is not None:
                mask = self.maskmgr.get(masklevel)
            else:
                mask = None
            img = type(self)(self.resmgr.get(imglevel), copy=True, mask=mask,
                             header=self.header)
            return img

        # Set new active layers if copy=False
        # Make sure that the Chain is synchronised with the previously
        # active layer.
        else:
            self.resmgr.set(imglevel)
            if masklevel is not None:
                self.maskmgr.set(masklevel)

    def resize(self, *newsize, **kwargs):
        """
        Changes the resolution of the TImage data by resampling it to a
        specific domain size. The rescaling only affects the resolution of the
        TImage, but not its physical extent.

        :param newsize: new shape of the TImage
        :type newsize: int
        :param kwargs: keyword arguments for resampling
        :type kwargs: any

        :param copy:
            If True (default), a resampled version of the current TImage will be
            returned, which will share transformations with the original
            TImage instance. The new instance will also inherit the resolution
            manager of the current instance.
            If False, the image data and the mask buffers of the current TImage
            instance will be overwritten, and the method will return nothing.
        :type copy: bool
        :param update_chain:
            If True (default), non-linear transformations that are linked with
            the TImage domain will remain linked, which implies that these
            will be resampled via their regrid() method. If False, all
            non-linear transformations will be detached and left unchanged.
            In either case the identity of all transformation objects is
            preserved (no copying will take place).
        :type update_chain: bool
        :param presmooth:
            Applies Gaussian presmoothing before downsampling (no smoothing is
            performed for upsampling). This ensures that all domain points at
            the lower resolution have proper support in the higher-resolution
            image, therefore all original information is (at least indirectly)
            represented.
        :type presmooth: bool
        """
        assert newsize and all(dim > 0 for dim in newsize), \
            "Invalid target image shape."

        if newsize == self.vshape:
            imglevel = self.resmgr.active
        else:
            imglevel = self.resmgr._resize(*newsize)
        # A mask does not necessarily exist at the current resolution,
        # so create it if necessary.
        if self.maskmgr is not None:
            masklevel = self.maskmgr._resize(*newsize)
        else:
            masklevel = None

        # Create output if copy=True
        if kwargs.pop("copy", False):
            if masklevel is not None:
                mask = self.maskmgr.get(masklevel)
            else:
                mask = None
            img = type(self)(self.resmgr.get(imglevel), copy=True, mask=mask)
            return img

        # Set new active layers if copy=False
        # Make sure that the Chain is synchronised with the previously
        # active layer.
        else:
            self.resmgr.set(imglevel)
            if masklevel is not None:
                self.maskmgr.set(masklevel)

    def smooth(self, *sigmas, spatial_op=None, copy=True):
        """
        Smooths the image uniformly for every tensor component. Anisotropic
        smoothing can be applied by specifying smoothing radii for each spatial
        dimension. If a spatial operator is specified, radii are passed to the
        operator as its first n arguments.

        Note: repeated smoothing of a TImage leads to the degradation of image
        quality, as the smoothing is performed on the active buffer of TImage,
        not the high-resolution buffer. This also implies that smoothing
        operations at different resolution levels can be undone by resampling
        the TImage (as long as the TImage is not smoothed at native resolution).

        :param sigmas:
            If scalar, it is a global smoothing radius. For the default
            Gaussian smoothing kernel, it is the sigma value in voxels.
            Smoothing radii can be defined for each axis for anisotropic
            smoothing.
        :type sigmas: Number
        :param spatial_op:
            Smoothing operation. If None, Gaussian smoothing is applied with
            4 * sigma anisotropic kernel size. The operator must not change the
            voxel shape of the TImage. The operator must accept smoothing radii
            as leading arguments.
        :type spatial_op: SpatialOperator
        :param copy:
            If True (default), the smoothing result is returned as a copy of
            the original TImage (the Domain is preserved). If False, the values
            will be changed in place.

        :returns: smoothed image
        :rtype: TImage

        """
        if copy:
            simg = self.__tfield__().smooth(
                *sigmas, spatial_op=spatial_op, copy=True)
            mask = self.tmask()
            if mask is not None:
                mask = mask.smooth(*sigmas, spatial_op=spatial_op, copy=True)
            return TImage(simg, mask=mask, header=tu.rcopy(self.header))

        else:
            self.__tfield__().smooth(*sigmas, spatial_op=spatial_op, copy=False)
            self.tmask().smooth(*sigmas, spatial_op=spatial_op, copy=False)

    def snapshot(self, fname, overwrite=False):
        """
        Saves a 2D TImage as a regular image file. Only the image data is
        saved, excluding all transformations, high-resolution buffers and
        the mask. If the image is 3D, it is saved into NIfTI format.

        :param fname:
            Path to and name of the target image file.
        :type fname: str
        :param overwrite:
            If True, the target image will be overwritten automatically
        :return:
        """
        # TODO: This was a very quick implementation - revise!
        from PIL import Image

        # Prepare file name
        fname = tu.verify_fname(fname, overwrite=overwrite)
        if not fname:
            return
        fn, ext = os.path.splitext(fname)
        fn = fn.rstrip(".")
        ext = ext.lstrip(".")

        # Save 3D image as NIfTI
        # TODO: Support tensor axes, header information mismatch case
        if self.vdim == 3:
            import nibabel as nib
            tx = (self.domain.offset + self.domain.chain).reduce(merge="all")
            if len(tx) == 0:
                affine = np.eye(4)
            elif len(tx) == 1 and tx[0].kind == "linear":
                affine = np.eye(4)
                affine[:3, :] = tx[0].matrix
            else:
                # TODO: Decompose, apply warp and save with affine
                raise NotImplementedError()

            try:
                hdr = self.header["nifti_header"].copy()
            except Exception:
                hdr = nib.Nifti1Header()
            hdr.set_qform(np.eye(4), code=0)
            hdr.set_sform(affine, code=2)
            hdr["descrip"] = f"TIRL v{tirl.__version__}"
            nifti = nib.Nifti1Image(self.dataview(VOXEL_MAJOR), affine, hdr)
            fn = fn.replace(".nii", "")
            nib.save(nifti, f"{fn}.nii.gz")

        # Save 2D image
        elif (self.vdim == 2) and (self.tdim in (0, 1)):
            ext = ts.TIMAGE_DEFAULT_SNAPSHOT_EXT if not ext else ext
            newimg = tu.imgop(self.vm())
            # imgdata = tu.to_img(self.dataview(VOXEL_MAJOR))
            pilimg = Image.fromarray(newimg.data)
            pilimg.save(".".join([fn, ext]))
            del newimg
            del pilimg
        else:
            raise AssertionError(
                "Snapshots can only be taken from 3D TImages and "
                "2D TImages with a single tensor dimension.")

    def tmask(self):
        """
        Returns mask as a TField instance.

        :returns: mask as TField on the current TImage domain
        :rtype: TField

        """
        if self.maskmgr is None:
            return None
        else:
            ip = self.maskmgr.get().interpolator
            ip.kwargs.update(order=1, prefilter=False)
            # Get the mask for the current domain
            tm = self.maskmgr.resize(*self.vshape)
            tm.domain = self.domain
            return tm

#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import os
import random
import string
import numpy as np
from copy import deepcopy
import scipy.sparse as sp
from functools import reduce
from skimage.color import hsv2rgb
from skimage.exposure import rescale_intensity


# TIRL IMPORTS

from tirl import settings as ts


# DEFINITIONS


# IMPLEMENTATION

class ArgumentMultiplexer(dict):
    """
    Takes keyword arguments, identifies synonymous keywords, detects conflicts
    between them, and returns a reduced set of inputs, which only contains the
    principal argument from each set of synonymous keywords.

    """

    def __init__(self, **rules):
        super(ArgumentMultiplexer, self).__init__()
        self._rules = dict()
        for key, value in rules.items():
            if isinstance(value, str) or (not hasattr(value, "__iter__")):
                value = (value,)
            self.add_rule(key, *value)

    def __call__(self, **kwargs):
        self.clear()
        for key, value in kwargs.items():
            newkey = self._rules.get(key, key)
            if newkey in self.keys():
                raise AssertionError(f"Conflicting arguments: {key}, {newkey}.")
            else:
                self[newkey] = value
        return self

    def add_rule(self, *args):
        key, *altkeys = args
        for alt in altkeys:
            self._rules.update({alt: key})

    def delete_rule(self, arg):
        for key, value in self._rules:
            if (key == arg) or (value == arg):
                self._rules.pop(key)

    def reset(self):
        self.clear()
        self._rules = dict()


def assertEqualRecursive(d1, d2, report=False):
    """
    Raises AssertionError if the two arguments are not equal. Being
    recursive, this test checks the pairwise equality of all elements in
    collections.

    """
    try:
        if type(d1) is not type(d2):
            assert False
        if isinstance(d1, (tuple, list)):
            for a, b in zip(d1, d2):
                assertEqualRecursive(a, b)
        elif isinstance(d1, dict):
            for key, item in d1.items():
                assertEqualRecursive(item, d2[key])
        elif isinstance(d1, np.ndarray):
            if np.any(d1 != d2):
                assert False
        else:
            if d1 != d2:
                assert False
    except AssertionError:
        if report:
            print(d1, d2)


def cart2pol(z):
    """Convert Cartesian coordinates to polar coordinates using complex
    exponential notation."""
    return np.abs(z), np.angle(z)


def change_memmap_dtype(m, dt, chunksize):
    """
    Changes the data type of stored data by rechunking into a new
    binary storage file.

    """
    from tirl import utils as tu
    # Create new binary storage on hard disk
    fd, fname = tu.tmpfile(prefix="TField_", dir=ts.TWD)
    # Calculate chunk sizes for data transfer
    numel = np.product(m.shape)
    chunklen = nbytes(chunksize) // (m.itemsize + np.dtype(dt).itemsize)
    for start in range(0, numel, chunklen):
        stop = min(start + chunklen, numel)
        offset = start * np.dtype(dt).itemsize
        new = np.memmap(fname, dtype=dt, mode="r+", offset=offset,
                        shape=(stop - start,))
        new[:] = m.flat[start:stop].astype(dt)
        del new
    return np.memmap(fname, mode="r+", shape=m.shape, dtype=dt, offset=0)


def ci(seq):
    """
    Copy iterator. Returns a copy of the iterable in the argument, but every
    element is a copy of the original element at that position. This function
    is used to prevent the creation of hidden links across transformations.

    """

    def copy(arg):
        """
        Friend function. Invokes the elements' copy() method. If an argument
        does not have a copy() method, the function calls deepcopy() on the
        argument.

        """
        try:
            return arg.copy()
        except AttributeError:
            return deepcopy(arg)

    if not hasattr(seq, "__iter__"):
        yield copy(seq)
    else:
        for element in seq:
            yield copy(element)


def dictcmp(a, b):
    """
    Compares dictionaries that have ndarrays among their values. Hint:
    ordinary dict comparison would throw an exception, because of the
    ambiguity of multiple comparisons.

    """
    assert isinstance(a, dict) and isinstance(b, dict), \
        "Dict inputs are required for comparison."
    a_lst = sorted(a.items(), key=lambda elem: elem[0])
    b_lst = sorted(b.items(), key=lambda elem: elem[0])
    bstack = []
    for tpair in zip(a_lst, b_lst):
        value1 = tpair[0][1]
        value2 = tpair[1][1]
        if type(value1) != type(value2):
            # print(value1, value2)
            return False
        if isinstance(value1, dict):
            bstack.append(dictcmp(value1, value2))
        elif isinstance(value1, (tuple, list)):
            bstack.append(lstcmp(value1, value2))
        elif isinstance(value1, np.ndarray):
            # Note: The truth value of an empty array is ambiguous. Direct
            # equality comparison is deprecated in NumPy v1.15.
            equal = (value1.size == value2.size)
            if equal and (value1.size > 0):
                equal = np.all(value1 == value2)
            bstack.append(equal)
        else:
            bstack.append(value1 == value2)
    return all(bstack)


def discrete_laplace_operator(*dims):
    """
    Creates a sparse discrete Laplace operator for a (dim0, dim1, ...) grid.
        :param dims: grid dimensions
        :type dims: int

        :returns: sparse discrete Laplace operator

    """

    # Check input
    dims = (dims,) if not isinstance(dims, (tuple, list)) else tuple(dims)
    if not all(isinstance(dim, (int, np.integer)) for dim in dims):
        raise ValueError("Dimensions of the N-dimensional grid must be "
                         "integers.")
    ndim = len(dims)
    size = np.product(dims)
    if not ndim >= 1:
        raise ValueError("Laplace operator must be at least 1-dimensional.")

    terms = []
    for current_index, dim in enumerate(dims):
        dim = int(dim)
        ones = np.ones(dim)
        D = sp.spdiags([ones, -2*ones, ones], [-1, 0, 1], dim, dim)
        if current_index > 0:
            pre = reduce(lambda a, b: sp.kron(a, b),
                         map(lambda c: sp.identity(dims[c]),
                             range(0, current_index)))
            L = sp.kron(pre, D)
        else:
            L = D
        if current_index < ndim - 1:
            for i in range(current_index + 1, ndim):
                L = sp.kron(L, sp.identity(int(dims[i])))
        assert L.shape == (size, size)
        terms.append(L)
    else:
        result = reduce((lambda a, b: a + b), terms)

    return result


def field_to_rgb(field, axis=-1):
    """Use this function to visualise the deformation field using HSV colour
    space. Works for 1, 2, and 3 dimensions."""

    # Verify input
    field = np.array(field, dtype=np.complex)
    assert type(axis) is int, "field_to_hsv: The axis variable must be integer."
    assert (axis < field.ndim) and (axis >= -field.ndim), \
        "field_to_hsv: The axis variable must correspond to a valid " \
        "dimension of the deformation field."
    if axis < 0:
        axis %= field.ndim
    dim = field.shape[axis]

    # Initialise output
    hsv = np.ones(tuple(
            d for i, d in enumerate(field.shape) if i != axis) + (3,))

    # 1D field
    if dim == 1:
        slicer = [slice(0, 1, 1)] + [slice(0, None, 1)] * (field.ndim - 1)
        slicer[0], slicer[axis] = slicer[axis], slicer[0]
        slicer = tuple(slicer)
        vals = field[slicer]
        del field
        hsv[..., 0] = rescale_intensity(robust(vals, axis=None))
    # 2D field
    elif dim == 2:
        slicer = [slice(1, 2, 1)] + [slice(0, None, 1)] * (field.ndim - 1)
        slicer[0], slicer[axis] = slicer[axis], slicer[0]
        slicer = tuple(slicer)
        field[slicer] *= 1j
        vals = np.sum(field, axis)
        del field
        radii, angles = cart2pol(vals)
        hsv[..., 0] = rescale_intensity(-angles / (2 * np.pi) + 0.5)
        hsv[..., 1] = rescale_intensity(robust(radii, axis=None))
    # 3D field
    elif dim == 3:
        magnitudes = np.linalg.norm(field, axis=axis)
        # Set element 1 along "axis" to imaginary
        slicer = [slice(1, 2, 1)] + [slice(0, None, 1)] * (field.ndim - 1)
        slicer[0], slicer[axis] = slicer[axis], slicer[0]
        slicer[axis] = slice(0, 2, 1)
        slicer = tuple(slicer)
        vals = np.sum(field[slicer], axis=axis)
        del field
        radii, angles = cart2pol(vals)
        hsv[..., 0] = rescale_intensity(-angles / (2 * np.pi) + 0.5)
        hsv[..., 1] = rescale_intensity(robust(radii, axis=None))
        hsv[..., 2] = rescale_intensity(robust(magnitudes, axis=None))
    else:
        raise ValueError("field_to_rgb: Only fields of 1, 2, and 3 dimensions "
                         "are supported.")

    rgb = 255 - (255 * hsv2rgb(hsv)).astype(np.uint8)

    return rgb


def indices2mask(mask_or_indices, maxindex=None):
    # If the input is a boolean mask
    if np.issubdtype(mask_or_indices.dtype, np.bool_):
        mask = mask_or_indices

    # if the input is a sequence of indices
    elif np.issubdtype(mask_or_indices.dtype, np.integer):
        maxindex = maxindex or mask_or_indices.max() + 1
        if np.any(mask_or_indices >= maxindex) or \
                np.any(mask_or_indices < 0):
            raise IndexError("Index is out of bounds.")
        indices = mask_or_indices
        mask = np.zeros(maxindex, dtype=np.bool)
        mask.flat[indices] = True
    else:
        raise TypeError("Invalid input type.")

    return mask


def ismatrix(x):
    """ Checks if the input is a matrix, ie. 2-D numerical array with finite
    values. """
    try:
        x = np.asarray(x)
        return x.ndim == 2 and \
               np.issubdtype(x.dtype, np.number)
    except Exception:  # pragma: no cover
        return False


def kw(src, *keys):
    return {key: value for key, value in src.items() if key in keys}


def laplace_neighbour_corrected(*dims):
    """
    Creates discrete Laplace operator for finite grid. Correction is applied
    for edges of the grid to account for the smaller number of neighbours.

        :param dims: grid dimensions
        :type dims: tuple

        :returns:

    """
    L = discrete_laplace_operator(*dims)
    ndim = len(dims)
    diag = np.asarray(np.sum(L, axis=0)).ravel() + 2 * ndim
    L = sp.dia_matrix(L)
    L.setdiag(-diag)
    return L


def lstcmp(a, b):
    assert isinstance(a, (tuple, list)) and isinstance(b, (tuple, list)), \
        "List/tuple inputs are required for comparison."
    bstack = []
    for pair in zip(a, b):
        value1, value2 = pair
        if type(value1) != type(value2):
            return False
        if isinstance(value1, dict):
            bstack.append(dictcmp(value1, value2))
        elif isinstance(value1, (tuple, list)):
            bstack.append(lstcmp(value1, value2))
        elif isinstance(value1, np.ndarray):
            # Note: The truth value of an empty array is ambiguous. Direct
            # equality comparison is deprecated in NumPy v1.15.
            equal = (value1.size == value2.size)
            if equal and (value1.size > 0):
                equal = np.all(value1 == value2)
            bstack.append(equal)
        else:
            bstack.append(value1 == value2)
    return all(bstack)


def mask2indices(mask_or_indices, indextrue=True):
    # If the input is a boolean mask
    if np.issubdtype(mask_or_indices.dtype, np.bool_):
        if indextrue:
            indices = np.flatnonzero(mask_or_indices)
        else:
            indices = np.flatnonzero(np.logical_not(mask_or_indices))

    # if the input is a sequence of indices
    elif np.issubdtype(mask_or_indices.dtype, np.integer):
        indices = mask_or_indices.ravel()
    else:
        raise TypeError("Invalid input type.")

    return indices


def nbytes(mem):
    """
    Converts an amount of memory specified with binary units into bytes.

    """
    from math import ceil
    from numbers import Integral
    if isinstance(mem, Integral):
        return int(mem)
    elif isinstance(mem, str):
        unit = mem[-1]
        value = float(mem[:-1])
        factors = ["k", "M", "G", "T", "P", "E", "Z", "Y"]
        try:
            exp = factors.index(unit) + 1
            return int(ceil(value * 1024 ** exp))
        except IndexError:
            raise ValueError(f"Invalid binary prefix: {unit}")
    else:
        raise ValueError("Invalid memory limit specification.")


def polytriangle(mat):
    """ Replaces supernumerary elements in a polynomial coefficient matrix with
    zeros. The order of the polynomial is equal to the largest shape of the
    input matrix. Coefficients pertaining to higher-order terms in the
    lower-right triangle of the matrix are removed. """

    if not ismatrix(mat):
        raise ValueError("Input must be a matrix.")
    mat = np.asarray(mat)
    xo, yo = np.asarray(mat.shape) - 1
    polyorder = max(xo, yo)
    x_order, y_order = np.ogrid[0:xo + 1, 0:yo + 1]
    termorder = x_order + y_order
    mat[termorder > polyorder] = 0

    return mat


def rcopy(obj):
    """
    Recursively copies all elements in a dict.

    :param obj: dict object
    :type obj: dict
    :return: new dict instance
    :rtype: dict

    """
    from copy import deepcopy
    if isinstance(obj, dict):
        new = {}
        for key, value in obj.items():
            new[key] = rcopy(value)
        else:
            return new
    elif isinstance(obj, (list, tuple)):
        new = []
        for val in obj:
            new.append(rcopy(val))
        else:
            if isinstance(obj, tuple):
                new = tuple(new)
            return new
    elif isinstance(obj, np.ndarray):
       return obj.copy()
    else:
        return getattr(obj, "copy", lambda: deepcopy(obj))()


def rndstr(length=32, seed=None):
    """Generate a random string of fixed length """
    letters = string.ascii_letters
    if seed is not None:
        random.seed(seed)
    return "".join(random.choice(letters) for i in range(length))


def robust(x, axis=None, lower_p=1, upper_p=99):
    """Create robust range of an array by clipping at the specified lower and
     upper percentiles to exclude outliers."""

    # Verify input
    _x = np.array(x)
    assert (type(lower_p) is int) and (lower_p >= 0) and (lower_p <= 100), \
        "robust: Lower percentile must be a valid integer percentile."
    assert (type(upper_p) is int) and (upper_p >= 0) and (upper_p <= 100), \
        "robust: Lower percentile must be a valid integer percentile."
    assert lower_p < upper_p, "robust: Lower percentile must be smaller " \
                              "than upper percentile."
    assert True if axis is None else type(axis) is int, \
        "robust: The axis variable must be integer or NoneType."
    # The following assertion is unnecessary given the line following it, but
    # demonstrates good practice by giving feedback to the user.
    assert True if axis is None else ((axis < _x.ndim) and (axis >= -_x.ndim)),\
        "robust: The axis variable must correspond to a valid " \
        "dimension of the deformation field."
    _axis = None if axis is None else axis % _x.ndim     # handle overflow

    # Move axis of interest into the last position to allow array broadcasting
    _x = _x.ravel() if _axis is None else np.moveaxis(_x, _axis, x.ndim - 1)

    # Do the clipping
    complementary_axes = tuple(range(_x.ndim - 1)) or None
    res = np.clip(_x, np.percentile(_x, lower_p, complementary_axes),
                  np.percentile(_x, upper_p, complementary_axes))

    # Return result in orignal shape (axis order)
    return res.reshape(x.shape) if axis is None else np.moveaxis(res, -1, _axis)


def timehash():
    """ Creates unique time-based hash. """
    from time import time
    import hashlib
    hash = hashlib.sha1()
    hash.update(str(time()).encode("utf-8"))
    return hash.hexdigest()


def tmpfile(prefix=None, dir=ts.TWD, suffix=None):
    import tempfile
    import multiprocessing as mp
    suffix = "" if suffix is None else suffix
    suffix = f".proc_{mp.current_process().pid}" + suffix
    return tempfile.mkstemp(prefix=prefix, dir=dir, suffix=suffix)


def to_img(imgdata):
    return rescale_intensity(
        np.squeeze(imgdata), out_range=np.uint8).astype(np.uint8)


def arr2uint8(arr, vmin, vmax):
    arr = np.squeeze(arr)
    in_range = (vmin, vmax)
    out_range = np.uint8
    return rescale_intensity(arr, in_range, out_range).astype(np.uint8)


def imgop(img):
    from tirl.operations.spatial import SpatialOperator
    vmin = img.data.min()
    vmax = img.data.max()
    # Bugfix: if n_cpu > 1, TIRL enters an infinite loop in interactive mode
    # because the caller is not guarded, i.e. there is no __name__ == "__main__"
    # to prevent the child process from also executing this function.
    spop = SpatialOperator(arr2uint8, opargs=(vmin, vmax), n_cpu=1)
    ret = spop(img)
    del spop
    return ret


def verify_fname(fname, overwrite):
    """
    Verifies whether the specified file exists, and prompts confirmation to
    overwrite if needed.

    :param fname: Target file.
    :type fname: str
    :param overwrite:
        If True, the file will be allowed to be overwritten without further
        notification. If False, on overwrite confirmation is requested.
    :type overwrite: bool

    :returns: Name and pathology of the target file.
    :rtype: str

    """
    fname = os.path.abspath(fname)
    path, fn = os.path.split(fname)
    if not os.path.isdir(path):
        raise IOError("Directory {} does not exist.".format(path))
    if os.path.isfile(fname):
        while not overwrite:
            confirmation = input(
                "The file {} already exists. Would you like to overwrite "
                "it? [yes/no] ".format(fname))
            if confirmation.lower() in ["n", "no"]:
                print("File was not saved.")
                return
            if confirmation.lower() in ["y", "ye", "yes", "\n"]:
                break
    return fname


def verify_n_cpu(n):
    import multiprocessing as mp
    from numbers import Integral
    n = ts.CPU_CORES if n is None else n
    # Number of physical cores
    if isinstance(n, complex):
        r = n.real
        i = n.imag
        if r < 0:
            r = r % mp.cpu_count() + 1
        else:
            r = min(r, mp.cpu_count())
        if i < 0:
            raise ValueError(f"Invalid number of CPU threads: {i}")
        return r + 1j * i
    # Number of logical cores
    elif isinstance(n, Integral):
        if n < 0:
            n = n % mp.cpu_count() + 1
        else:
            n = min(n, mp.cpu_count())
        return int(n)
    # Invalid input
    else:
        raise ValueError(f"Invalid number of CPU cores: {n}")


if __name__ == "__main__":  # pragma: no cover
    print("""This module is not intended for execution.""")

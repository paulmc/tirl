#!/bin/bash

# Install the python package to the build directory
${PYTHON} -m pip install -vv --no-deps .

# Replace placeholders with the appropriate licensing information
${PYTHON} ${RECIPE_DIR}/license.py ${PREFIX}
